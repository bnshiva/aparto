UserApartment = new Mongo.Collection('user_apartment');
Schema = {};

    Schema.UserProfile = new SimpleSchema({
        firstName: {
            type: String,
        },
        lastName: {
            type: String,
        },
        dataOfBirth: {
            type: Date
        },
        sex: {
            type: String,
			autoform:{
				options:{
					male:"Male",
					female:"Female"
				}
			 }	
        },
        photoUrl: {
            type: String,
        },
        userStatus: {
            type: Number
        },
        primaryPhone: {
            type: Number
        },
		secondaryPhone: {
            type: Number,
        },
		permanentAddress:{
			type:String
		}
    });

    Schema.User = new SimpleSchema({
         emails: {
			type: Array,
			// For accounts-password, either emails or username is required, but not both. It is OK to make this
			// optional here because the accounts-password package does its own validation.
			// Third-party login packages may not require either. Adjust this schema as necessary for your usage.
			optional: true
			},
			"emails.$": {
			type: Object
			},
			"emails.$.address": {
			type: String,
			regEx: SimpleSchema.RegEx.Email
			},
			"emails.$.verified": {
			type: Boolean
			},
			services: {
				type: Object,
				optional: true,
				blackbox: true,
				autoform: {
			  type: "hidden",
			  label: false
			}
			},

        profile: {
           type: Schema.UserProfile,
			optional:true
        }    
    });

    Meteor.users.attachSchema(Schema.User);
	
	
	TabularTables.Users = new Tabular.Table({
  name: "Users",
  collection: Meteor.users,
  columns: [
    {data: "profile.firstName", title: "Name"},
    {data: "emails[1].address", title: "Email"},
    {data: "profile.primaryPhone", title: "Phone"},
	{data: "profile.permanentAddress", title: "Address"},
	{
      tmpl: Meteor.isClient && Template.editUserSection
    }
  ],
  extraFields:["_id"]
});

	
	TabularTables.UserList = new Tabular.Table({
  name: "UserList",
  collection: Meteor.users,
  columns: [
    {data: "profile.firstName", title: "Name"},
    {data: "emails[1].address", title: "Email"},
    {data: "profile.primaryPhone", title: "Phone"},
	{data: "profile.permanentAddress", title: "Address"},
	{
      tmpl: Meteor.isClient && Template.assignUser
    }
  ],
  extraFields:["_id"]
});

	TabularTables.AssignedUserList = new Tabular.Table({
  name: "AssignedUserList",
  collection: Meteor.users,
  columns: [
    {data: "profile.firstName", title: "Name"},
    {data: "emails[1].address", title: "Email"},
    {data: "profile.primaryPhone", title: "Phone"},
	{data: "profile.permanentAddress", title: "Address"},
	{
      tmpl: Meteor.isClient && Template.removeUserAssignSection
    }
  ],
  extraFields:["_id"]
});



Meteor.methods({
	'addUser':function(user){
		 var currentUserId = Meteor.userId();
		 user.password="shiva";
		 console.log(user);
		 user.email=user.emails[0].address;
        if(currentUserId){
			Accounts.createUser(user);
		}
		
	},
	'editUser': function(user,val){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
		user.email=user.emails[0].address;
        if(currentUserId){
            Meteor.users.update({_id:user._id},user.modifier,function(err,data){
			});
        }
    },
	'getAllUsers':function(refreshFlag){	
		return Meteor.users.find().fetch();
	},
	'getUserByUserId':function(userId, refreshFlag){
		return Meteor.users.findOne({_id:userId});
		
	},
	'removeUser':function(userId){
		return Meteor.users.remove(userId);
	},
	'getUsersByApartmentId':function(apartmentId,refreshFlag){
		u = UserApartment.find({"apartmentId": apartmentId });
		idlist= [];
		u.forEach(function(myDoc) { 
		idlist.push(myDoc.userId ); 
		});
		return idlist;
		//console.log(Meteor.users.find({"_id": {$in : idlist} }).fetch());
		//return Meteor.users.find({"_id": {$in : idlist} });
	},
	'addUserApartment':function(userId,apartmentId){
		if(userId!=null && apartmentId!=null){
			assignmentRecord=UserApartment.findOne( { userId : userId , apartmentId: apartmentId } );
			if(assignmentRecord==null || assignmentRecord.count()==0){
				UserApartment.insert({
					userId:userId,
					apartmentId:apartmentId,
					assignmentStartDate:new Date()
				})
			}
			
		}		
	},
	'removeUserAssignment':function(userId, apartmentId){
		if(userId!=null && apartmentId!=null){
			assignmentRecord=UserApartment.findOne( { userId : userId , apartmentId: apartmentId } );
			UserApartment.remove(assignmentRecord);
		}
	}
});
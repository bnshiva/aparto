ApartmentDocuments = new Mongo.Collection('apartment_documents');
ApartmentUpdates = new Mongo.Collection('apartment_updates');
ApartmentDocumentFiles = new Mongo.Collection('apartment_document_file');
ApartmentUpdateFiles =  new Mongo.Collection('apartment_update_file');
ApartmentPayment = new Mongo.Collection('apartment_payment');
ApartmentPaymentFiles = new Mongo.Collection('apartment_payment_file');
ApartmentServiceRequests = new Mongo.Collection('apartment_service_request');

Schema = {};

Schema.ApartmentUpdates = new SimpleSchema({
		apartmentId:{
			type:String,
			autoform: {
			  type: "hidden",
			  label: false
			},
		},
        updateType: {
            type: String,
			autoform:{
				options:{
					living:"Living Area",
					utility:"Utilities",
					bathroom:"Bathroom",					
				}
			 }	
        },
        updateDescription: {
            type: String,
        }
    });
ApartmentUpdates.attachSchema(Schema.ApartmentUpdates);
 Schema.ApartmentDocuments = new SimpleSchema({
		apartmentId:{
			type:String,
			autoform: {
			  type: "hidden",
			  label: false
			},
		},
		documentNumber:{
			type:String,
			label:'Document Number'			
		},
		documentDescription:{
			type:String,
			label:'Description'			
		},
        documentType: {
            type: String,
			autoform:{
				options:{
					loan:"Loan",
					property:"Property",
				},
				label:'Document Type'
			 }	
        },
        issueAuthority: {
            type: String,
			label: 'Issuing Authority'
        },
		issueDate: {
            type: String,
			label: 'Issue Date'
        },
		expiryDate: {
            type: String,
			label: 'Expiry Date'
        },
    });
ApartmentDocuments.attachSchema(Schema.ApartmentDocuments);

Schema.ApartmentPayment = new SimpleSchema({
		apartmentId:{
			type:String,
			autoform: {
			  type: "hidden",
			  label: false
			},
		},
		paymentNumber:{
			type:String,
			label:'Ref. Number'			
		},
		paymentAmount:{
			type:Number,
			decimal:true,
			label:'Amount'			
		},
        paymentType: {
            type: String,
			autoform:{
				options:{
					Loan:"Loan",
					Cheque:"Cheque",
					Cash:"Cash",
					BankersCheque:"Banker's Cheque",
					ElectronicTransfer:"Electronic Transfer"
				},
				label:'Payment Type'
			 }	
        },		
		paymentDescription:{
			type:String,
			label:'Description'			
		},
		paymentStatus: {
            type: String,
			autoform:{
				options:{
					Pending:"Pending",
					Rejected:"Rejected",
					Completed:"Completed",
					Processing:"Processing"
				},
				label:'Payment Status'
			 }	
        },
		paymentDate: {
            type: String,
			label: 'Payment Date'
        },
    });
ApartmentPayment.attachSchema(Schema.ApartmentPayment);
Schema.ApartmentServiceRequests = new SimpleSchema({
		apartmentId:{
			type:String,
			autoform: {
			  type: "hidden",
			  label: false
			},
		},	
		requestType: {
            type: String,
			autoform:{
				options:{
					utilities:"Utilities",
					lockedOut:"Locked Out",
					others:"Others"
				},
				label:'Type'
			 }	
        },		
		requestDescription:{
			type:String,
			label:'Description',
			autoform: {
			  rows:'3'
			},			
		},
		requestContactNumber:{
			type:String,
			label:'Phone No.'			
		},
		requestStatus: {
            type: String,
			autoform:{
				options:{
					Pending:"Pending",
					Rejected:"Rejected",
					Completed:"Completed",
					Processing:"In Progress"
				},
				label:'Status',
			 }	
        },
		requestResolvedDate:{
			type:String,
			label:'Resolved On',
			optional:true
		},
		
    });
ApartmentServiceRequests.attachSchema(Schema.ApartmentServiceRequests);

TabularTables.ApartmentDocuments = new Tabular.Table({
  name: "ApartmentDocuments",
  collection: ApartmentDocuments,
  columns: [
    {data: "documentNumber", title: "Number"},
    {data: "documentDescription", title: "Description"},
    {data: "documentType", title: "Type"},
	{data: "issueAuthority", title: "Issued By"},
	{data: "issueDate", title: "Issued On"},
	{data: "expiryDate", title: "Expiry On"},
	{
      tmpl: Meteor.isClient && Template.TeditApartmentDocumentsSection
    }
  ],
  extraFields:['_id','apartmentId']
});

TabularTables.Client_ApartmentDocuments = new Tabular.Table({
  name: "ApartmentDocuments",
  collection: ApartmentDocuments,
  columns: [
    {data: "documentNumber", title: "Number"},
    {data: "documentDescription", title: "Description"},
    {data: "documentType", title: "Type"},
	{data: "issueAuthority", title: "Issued By"},
	{data: "issueDate", title: "Issued On"},
	{data: "expiryDate", title: "Expiry On"},
	{
      tmpl: Meteor.isClient && Template.TClient_viewApartmentDocumentsSection
    }
  ],
  extraFields:['_id','apartmentId']
});

TabularTables.ApartmentPayment = new Tabular.Table({
  name: "ApartmentPayment",
  collection: ApartmentPayment,
  columns: [
    {data: "paymentNumber", title: "Number"},
    {data: "paymentAmount", title: "Amount"},
    {data: "paymentType", title: "Type"},
	{data: "paymentDescription", title: "Description"},
	{data: "paymentStatus", title: "Status"},
	{data: "paymentDate", title: "Paid On"},
	{
      tmpl: Meteor.isClient && Template.TeditApartmentPaymentSection
    }
  ],
  extraFields:['_id','apartmentId']
});

TabularTables.Client_ApartmentPayment = new Tabular.Table({
  name: "Client_ApartmentPayment",
  collection: ApartmentPayment,
  columns: [
    {data: "paymentNumber", title: "Number"},
    {data: "paymentAmount", title: "Amount"},
    {data: "paymentType", title: "Type"},
	{data: "paymentDescription", title: "Description"},
	{data: "paymentStatus", title: "Status"},
	{data: "paymentDate", title: "Paid On"},
	{
      tmpl: Meteor.isClient && Template.TClient_viewApartmentPaymentSection
    }
  ],
  extraFields:['_id','apartmentId']
});
	
TabularTables.ApartmentUpdates = new Tabular.Table({
  name: "ApartmentUpdates",
  collection: ApartmentUpdates,
  columns: [
    {data: "updateType", title: "Type"},
    {data: "updateDescription", title: "Description"},
    {data: "createdOn", title: "Updated On"},
	{
      tmpl: Meteor.isClient && Template.TeditApartmentUpdatesSection
    }
  ],
  extraFields:['_id','apartmentId']
});

TabularTables.Client_ApartmentUpdates = new Tabular.Table({
  name: "Client_ApartmentUpdates",
  collection: ApartmentUpdates,
  columns: [
    {data: "updateType", title: "Type"},
    {data: "updateDescription", title: "Description"},
    {data: "createdOn", title: "Updated On"},
	{
      tmpl: Meteor.isClient && Template.TClient_viewApartmentUpdatesSection
    }
  ],
  extraFields:['_id','apartmentId']
});

TabularTables.ApartmentServiceRequests = new Tabular.Table({
  name: "ApartmentServiceRequests",
  collection: ApartmentServiceRequests,
  columns: [
    {data: "requestType", title: "Type"},
    {data: "requestDescription", title: "Description"},
    {data: "contactNumber", title: "Updated On"},
    {data: "requestStatus", title: "Status"},
	{data: "requestResolvedDate", title: "Resolved On"},
	{
      tmpl: Meteor.isClient && Template.TeditApartmentServiceRequestSection
    }
  ],
  extraFields:['_id','apartmentId']
});
	Meteor.methods({
		'createApartmentDocument': function(apartmentDocument){
        var currentUserId = Meteor.userId();
        if(currentUserId && apartmentDocument.apartmentId!=null){
			apartmentDocument.createdOn=new Date();
			ApartmentDocuments.insert(apartmentDocument, function(err,data){
			});
        }
    },
	'editApartmentDocument': function(apartmentDocument){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            ApartmentDocuments.update({_id:apartmentDocument._id},apartmentDocument.modifier,function(err,data){
			});
        }
    },
	'getApartmentDocumentByApartmentDocumentId': function(apartmentDocumentId,refreshFlag){
		return ApartmentDocuments.findOne({_id:apartmentDocumentId});
    },
	'createApartmentDocumentFile':function(apartmentDocumentFile){
		var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
			if(apartmentDocumentFile.apartmentId!=null ||apartmentDocumentFile.apartmentDocumentId!=null){
					apartmentDocumentFile.createdOn=new Date();
					ApartmentDocumentFiles.insert(apartmentDocumentFile, function(err,data){
				});
			}
			else{
				throw new Meteor.Error( 500, 'Apartment/Document Not selected' );
			}			
            
        }
	},
	'getApartmentDocumentsFilesByDocumentId':function(apartmentDocumentId,refreshFlag){
		if(apartmentDocumentId!=null){
				return ApartmentDocumentFiles.find({apartmentDocumentId:apartmentDocumentId}).fetch();
		}		
	},
	'removeApartmentDocumentFile':function(id){
		ApartmentDocumentFiles.remove({_id:id});
	},
	
	// Apartment Update Methods
		'createApartmentUpdate': function(apartmentUpdate){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			apartmentUpdate.createdOn=new Date();
			ApartmentUpdates.insert(apartmentUpdate, function(err,data){
			});
        }
    },
	'editApartmentUpdate': function(apartmentUpdate){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            ApartmentUpdates.update({_id:apartmentUpdate._id},apartmentUpdate.modifier,function(err,data){
			});
        }
    },
	'removeApartmentUpdate':function(id){
		if(id!=null){
			ApartmentUpdates.remove({_id:id});
		}
		
	},
	'getApartmentUpdateByApartmentUpdateId': function(apartmentUpdateId,refreshFlag){
		console.log("Finding"+apartmentUpdateId);
		console.log( ApartmentUpdates.findOne({_id:apartmentUpdateId}));
		return ApartmentUpdates.findOne({_id:apartmentUpdateId});
    },
	'createApartmentUpdateImage':function(apartmentUpdateImage){
		var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
			if(apartmentUpdateImage.apartmentId!=null ||apartmentUpdateImage.apartmentUpdateId!=null){
					apartmentUpdateImage.createdOn=new Date();
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function(err,data){
				});
			}
			else{
				throw new Meteor.Error( 500, 'Project/Update Not selected' );
			}			
            
        }
	},
	'getApartmentUpdateImageByUpdateId':function(apartmentUpdateId,refreshFlag){
		if(apartmentUpdateId!=null){
				console.log("Fetching Update Files");
				return ApartmentUpdateFiles.find({apartmentUpdateId:apartmentUpdateId}).fetch();
		}		
	},
	'removeApartmentUpdateImage':function(id){
		ApartmentUpdateFiles.remove({_id:id});
	},

	//End Apartment Update
	
	//Apartment Payment Section
		'createApartmentPayment': function(apartmentPayment){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			apartmentPayment.createdOn=new Date();
			ApartmentPayment.insert(apartmentPayment, function(err,data){
			});
        }
    },
	'editApartmentPayment': function(apartmentPayment){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            ApartmentPayment.update({_id:apartmentPayment._id},apartmentPayment.modifier,function(err,data){
			});
        }
    },
	'removeApartmentPayment':function(id){
		if(id!=null){
			ApartmentPayment.remove({_id:id});
		}
		
	},
	'getApartmentPaymentByApartmentPaymentId': function(apartmentPaymentId,refreshFlag){
		return ApartmentPayment.findOne({_id:apartmentPaymentId});
    },
	'createApartmentPaymentFile':function(apartmentPaymentFile){
		var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
			if(apartmentPaymentFile.apartmentId!=null ||apartmentPaymentFile.apartmentPaymentId!=null){
					apartmentPaymentFile.createdOn=new Date();
					ApartmentPaymentFiles.insert(apartmentPaymentFile, function(err,data){
				});
			}
			else{
				throw new Meteor.Error( 500, 'Project/Payment Not selected' );
			}			
            
        }
	},
	'getApartmentPaymentFileByPaymentId':function(apartmentPaymentId,refreshFlag){
		if(apartmentPaymentId!=null){
				return ApartmentPaymentFiles.find({apartmentPaymentId:apartmentPaymentId}).fetch();
		}		
	},
	'removeApartmentPaymentFile':function(id){
		ApartmentPaymentFiles.remove({_id:id});
	},
	
// Apartment Service Request

		'createApartmentServiceRequest': function(apartmentServiceRequest){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			apartmentServiceRequest.createdOn=new Date();
			ApartmentServiceRequests.insert(apartmentServiceRequest, function(err,data){
			});
        }
    },
	'editApartmentServiceRequest': function(apartmentServiceRequest){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            ApartmentServiceRequests.update({_id:apartmentServiceRequest._id},apartmentServiceRequest.modifier,function(err,data){
			});
        }
    },
	'removeApartmentServiceRequest':function(id){
		if(id!=null){
			ApartmentServiceRequests.remove({_id:id});
		}		
	},
	'getApartmentServiceRequestByApartmentServiceRequestId': function(apartmentServiceRequestId,refreshFlag){
		return ApartmentServiceRequests.findOne({_id:apartmentServiceRequestId});
    },

    //Client Apartmemt Update section


    'getApartmentUpdateByApartmentUpdateId': function(apartmentUpdateId){
		return ApartmentUpdates.findOne({_id:apartmentUpdateId});
    },
	'createApartmentUpdateFile':function(apartmentUpdateImage){
		var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
			if(apartmentUpdateImage.apartmentId!=null ||apartmentUpdateImage.apartmentUpdateId!=null){
					console.log("Creating a new Apartment Image");
					apartmentUpdateImage.createdOn=new Date();
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function(err,data){
				});
			}
			else{
				throw new Meteor.Error( 500, 'Apartment/Update Not selected' );
			}			
            
        }
	},
	'getApartmentUpdateImageByUpdateId':function(apartmentUpdateId,refreshFlag){
		if(apartmentUpdateId!=null){
				return ApartmentUpdateFiles.find({apartmentUpdateId:apartmentUpdateId}).fetch();
		}		
	},
	'removeApartmentUpdateFile':function(id){
		ApartmentUpdateFiles.remove({_id:id});
	},
    
	});


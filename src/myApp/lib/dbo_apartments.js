Apartments = new Mongo.Collection('apartment');
ApartmentSchemas ={};
ApartmentSchemas.Apartments= new SimpleSchema({
			projectId:{
			type:String,
			autoform:{
				options:function () {
				return Projects.find().map(function (c) {
				  return {label: c.projectName, value: c._id};
				});
			  }
			 }				
			},
			flatNumber:{
				type:String,
				label:'Flat Number'
			},
			flatBlock:{
				type:String,
				label:'Block'
			},
			flatArea:{
				type:String,
				label:'Area (Sqft.)'
			},
			flatFloor:{
				type:Number,
				label:'Floor'
			},
			flatStatusId:{
				type:Number,
				label:'Status'
			},
			flatDescription:{
				type:String,
				label:'Info'
			},
			twoWheelerParking:{
				type:Number,
				label:'2 Wheeler Spots'
			},
			fourWheelerSpots:{
				type:Number,
				label:'4 Wheeler Spots'
			},
		});
Apartments.attachSchema(ApartmentSchemas.Apartments);
TabularTables = {};

TabularTables.Apartments = new Tabular.Table({
  name: "Apartments",
  collection: Apartments,
  columns: [
    {data: "flatNumber", title: "Number"},
    {data: "flatBlock", title: "Block"},
    {data: "flatFloor", title: "Floor"},
	{data: "flatArea", title: "Area"},
	{data: "flatDescription", title: "Info"},
	{
      tmpl: Meteor.isClient && Template.editApartmentSection
    }
  ],
  extraFields:["_id"]
});
Meteor.methods({
    'createApartment': function(apartment){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			apartment.createdOn=new Date();
			Apartments.insert(apartment, function(err,data){
				if(data){
					console.log("Success Apartment insertion");
					
				}
			});
        }
    },
	'editApartment': function(apartment,val){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
		console.log(apartment.modifier);
        if(currentUserId){
            Apartments.update({_id:apartment._id},apartment.modifier,function(err,data){
			});
        }
    },
	'getAllApartments':function(refreshFlag){	
		return Apartments.find().fetch();
	},
	'getApartmentByProjectId':function(projectId, refreshFlag){
		return Apartments.find({projectId:projectId}).fetch();
	},
	'getApartmentByApartmentId':function(apartmentId, refreshFlag){
		return Apartments.findOne({_id:apartmentId});
		
	},
	'removeApartment':function(apartmentId){
		return Apartments.remove(apartmentId);
	},
	'addAssignment':function(apartmentId,userId){		
		UserApartment.insert({
			userId:userId,
			apartmentId:apartmentId,
			assignmentStartDate:new Date()
		})
	},
	'removeAssignment':function(apartmentAssignementId){
		UserApartment.remove(apartmentAssignementId);
	}
	
});
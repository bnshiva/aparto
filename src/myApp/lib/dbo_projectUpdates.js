ProjectUpdates = new Mongo.Collection('project_update');
ProjectUpdateImages = new Mongo.Collection('product_update_image');
Schema = {};

    Schema.ProjectUpdates = new SimpleSchema({
		projectId:{
			type:String,
			autoform: {
			  type: "hidden",
			  label: false
			},
		},
        updateType: {
            type: String,
			autoform:{
				options:{
					recreation:"Recreation",
					building:"Building",
					amenities:"Amenities",
				}
			 }	
        },
        updateDescription: {
            type: String,
        }
    });
ProjectUpdates.attachSchema(Schema.ProjectUpdates);

TabularTables.ProjectUpdates = new Tabular.Table({
  name: "ProjectUpdates",
  collection: ProjectUpdates,
  columns: [
    {data: "updateType", title: "Type"},
    {data: "updateDescription", title: "Description"},
    {data: "createdOn", title: "Updated On"},
	{
      tmpl: Meteor.isClient && Template.TeditProjectUpdatesSection
    }
  ],
  extraFields:['_id','projectId']
});
	
TabularTables.Client_ProjectUpdates = new Tabular.Table({
  name: "Client_ProjectUpdates",
  collection: ProjectUpdates,
  columns: [
    {data: "updateType", title: "Type"},
    {data: "updateDescription", title: "Description"},
    {data: "createdOn", title: "Updated On"},
	{
      tmpl: Meteor.isClient && Template.TClient_viewProjectUpdatesSection
    }
  ],
  extraFields:['_id','projectId']
});

	Meteor.methods({
		'createProjectUpdate': function(projectUpdate){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			projectUpdate.createdOn=new Date();
			ProjectUpdates.insert(projectUpdate, function(err,data){
			});
        }
    },
	'editProjectUpdate': function(projectUpdate){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            ProjectUpdates.update({_id:projectUpdate._id},projectUpdate.modifier,function(err,data){
			});
        }
    },
	'getProjectUpdateByProjectUpdateId': function(projectUpdateId){
		return ProjectUpdates.findOne({_id:projectUpdateId});
    },
	'createProjectUpdateImage':function(projectUpdateImage){
		var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
			if(projectUpdateImage.projectId!=null ||projectUpdateImage.projectUpdateId!=null){
					projectUpdateImage.createdOn=new Date();
					ProjectUpdateImages.insert(projectUpdateImage, function(err,data){
				});
			}
			else{
				throw new Meteor.Error( 500, 'Project/Update Not selected' );
			}			
            
        }
	},
	'getProjectUpdateImageByUpdateId':function(projectUpdateId,refreshFlag){
		//return [{'url':'google.com'},{'url':'faceobooj.com'}];
		if(projectUpdateId!=null){
				return ProjectUpdateImages.find({projectUpdateId:projectUpdateId}).fetch();
		}		
	},
	'removeProjectUpdateImage':function(id){
		ProjectUpdateImages.remove({_id:id});
	}
	});

Projects = new Mongo.Collection('project');
ProjectSchemas ={};
ProjectSchemas.Projects= new SimpleSchema({
			projectName:{
				type:String,
				label:'Project Name'
			},
			projectDescription:{
				type:String,
				label:'Project Description'
			},
			projectAddress:{
				type:String,
				label:'Project Address'
			},
			projectAdditionalInfo:{
				type:String,
				label:'Project Info'
			}
		});
Projects.attachSchema(ProjectSchemas.Projects);
TabularTables = {};

TabularTables.Projects = new Tabular.Table({
  name: "Projects",
  collection: Projects,
  columns: [
    {data: "projectName", title: "Name"},
    {data: "projectDescription", title: "Description"},
    {data: "projectAddress", title: "Address"},
	{
      tmpl: Meteor.isClient && Template.editProjectSection
    }
  ],
  extraFields:['_id']
});
Meteor.methods({
    'createProject': function(project){
        var currentUserId = Meteor.userId();
        if(currentUserId){
			project.createdOn=new Date();
			Projects.insert(project, function(err,data){
				if(data){
					console.log("Success project insertion");
					
				}
			});
        }
    },
	'editProject': function(project,val){
        var currentUserId = Meteor.userId();	
		currentUser = Meteor.users.findOne({_id:currentUserId});
        if(currentUserId){
            Projects.update({_id:project._id},project.modifier,function(err,data){
			});
        }
    },
	'getAllProjects':function(refreshFlag){
		return Projects.find().fetch();
	},
	'getProjectByProjectId':function(projectId, refreshFlag){
		return Projects.findOne({_id:projectId});
		
	},
	'removeProject':function(projectId){
		console.log("Removing Project"+projectId);
		return Projects.remove(projectId);
	},
	'isAuthorized':function(userId,document){
		if(userId) return true;
		return false;
	}
	
});
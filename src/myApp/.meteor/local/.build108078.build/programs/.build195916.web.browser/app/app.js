var require = meteorInstall({"client":{"admin":{"adminTemplate.html":["./template.adminTemplate.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/adminTemplate.html                                                                                 //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
module.exports = require("./template.adminTemplate.js");                                                           // 1
                                                                                                                   // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.adminTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/template.adminTemplate.js                                                                          //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
                                                                                                                   // 1
Template.__checkName("admin_section");                                                                             // 2
Template["admin_section"] = new Template("Template.admin_section", (function() {                                   // 3
  var view = this;                                                                                                 // 4
  return HTML.DIV("\n		", HTML.DIV({                                                                               // 5
    "class": "row"                                                                                                 // 6
  }, "\n			", HTML.DIV({                                                                                           // 7
    "class": "jumbotron container"                                                                                 // 8
  }, "			\n			", HTML.DIV({                                                                                        // 9
    "class": "text-center"                                                                                         // 10
  }, "\n				", HTML.DIV({                                                                                          // 11
    role: "tabpanel"                                                                                               // 12
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="project-tab" data-target="#project" data-toggle="tab">Project Management</a></li>\n						<li role="presentation"><a id="apartment-tab" data-target="#apartment" data-toggle="tab">Apartment Management</a></li>\n						<li role="presentation"><a id="user-tab" data-target="#user" data-toggle="tab">User Management</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                         // 14
  }, "					\n						", HTML.DIV({                                                                                   // 15
    role: "tabpanel",                                                                                              // 16
    "class": "tab-pane active text-center",                                                                        // 17
    id: "project"                                                                                                  // 18
  }, "\n							", HTML.DIV({                                                                                       // 19
    "class": "row"                                                                                                 // 20
  }, "\n								", HTML.DIV({                                                                                      // 21
    "class": "col-md-12"                                                                                           // 22
  }, "\n									", Spacebars.include(view.lookupTemplate("projectForm")), "\n									", Spacebars.include(view.lookupTemplate("editProjectForm")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row "                                                                                                // 24
  }, "\n								", HTML.DIV({                                                                                      // 25
    "class": "col-md-12"                                                                                           // 26
  }, "\n									", HTML.DIV({                                                                                     // 27
    "class": "table-responsive"                                                                                    // 28
  }, "\n										", Spacebars.include(view.lookupTemplate("projectList")), "\n									"), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row "                                                                                                // 30
  }, "\n								", HTML.DIV({                                                                                      // 31
    "class": "col-md-12"                                                                                           // 32
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectManagement")), " \n								"), "\n							"), "\n														\n						"), "\n						", HTML.DIV({
    role: "tabpanel",                                                                                              // 34
    "class": "tab-pane text-center",                                                                               // 35
    id: "apartment"                                                                                                // 36
  }, "\n							", HTML.DIV({                                                                                       // 37
    "class": "row"                                                                                                 // 38
  }, "\n								", HTML.DIV({                                                                                      // 39
    "class": "col-md-12"                                                                                           // 40
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentManagementMain")), "						\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                 // 42
  }, "\n								", HTML.DIV({                                                                                      // 43
    "class": "col-md-12"                                                                                           // 44
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentForm")), "	\n								", Spacebars.include(view.lookupTemplate("editApartmentForm")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                 // 46
  }, "\n								", HTML.DIV({                                                                                      // 47
    "class": "col-md-12"                                                                                           // 48
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentList")), "						\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                 // 50
  }, "\n								", HTML.DIV({                                                                                      // 51
    "class": "col-md-12"                                                                                           // 52
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentManagement")), "						\n								"), "\n							"), "	\n						"), "\n						", HTML.DIV({
    role: "tabpanel",                                                                                              // 54
    "class": "tab-pane text-center",                                                                               // 55
    id: "user"                                                                                                     // 56
  }, "\n							", HTML.DIV({                                                                                       // 57
    "class": "row"                                                                                                 // 58
  }, "\n								", HTML.DIV({                                                                                      // 59
    "class": "col-md-12"                                                                                           // 60
  }, "\n								", Spacebars.include(view.lookupTemplate("userForm")), "	\n								", Spacebars.include(view.lookupTemplate("editUserForm")), "\n								"), "\n								", HTML.DIV({
    "class": "row"                                                                                                 // 62
  }, "\n								", HTML.DIV({                                                                                      // 63
    "class": "col-md-12"                                                                                           // 64
  }, "\n								", Spacebars.include(view.lookupTemplate("userList")), "						\n								"), "\n							"), "\n							"), "	\n							"), "\n\n							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		"), "\n	");
}));                                                                                                               // 66
                                                                                                                   // 67
Template.__checkName("apartmentManagementMain");                                                                   // 68
Template["apartmentManagementMain"] = new Template("Template.apartmentManagementMain", (function() {               // 69
  var view = this;                                                                                                 // 70
  return Spacebars.include(view.lookupTemplate("projectListOptions"));                                             // 71
}));                                                                                                               // 72
                                                                                                                   // 73
Template.__checkName("projectListOptions");                                                                        // 74
Template["projectListOptions"] = new Template("Template.projectListOptions", (function() {                         // 75
  var view = this;                                                                                                 // 76
  return HTML.SELECT({                                                                                             // 77
    id: "projectNameDropDown",                                                                                     // 78
    "class": "projectNameDropDown"                                                                                 // 79
  }, HTML.Raw('\n		<option disabled="disabled" selected="selected">Please Select</option> \n		'), Blaze.Each(function() {
    return Spacebars.call(view.lookup("projects"));                                                                // 81
  }, function() {                                                                                                  // 82
    return [ "\n			", HTML.OPTION({                                                                                // 83
      value: function() {                                                                                          // 84
        return Spacebars.mustache(view.lookup("_id"));                                                             // 85
      }                                                                                                            // 86
    }, Blaze.View("lookup:projectName", function() {                                                               // 87
      return Spacebars.mustache(view.lookup("projectName"));                                                       // 88
    })), "\n		" ];                                                                                                 // 89
  }), "\n	");                                                                                                      // 90
}));                                                                                                               // 91
                                                                                                                   // 92
Template.__checkName("projectForm");                                                                               // 93
Template["projectForm"] = new Template("Template.projectForm", (function() {                                       // 94
  var view = this;                                                                                                 // 95
  return [ HTML.Raw('<button class="btn btn-primary js-createProject">Create Project</button>\n	'), HTML.DIV({     // 96
    "class": "modal fade",                                                                                         // 97
    tabindex: "-1",                                                                                                // 98
    id: "createProjectModal",                                                                                      // 99
    role: "dialog"                                                                                                 // 100
  }, "\n	  ", HTML.DIV({                                                                                           // 101
    "class": "modal-dialog"                                                                                        // 102
  }, "\n		", HTML.DIV({                                                                                            // 103
    "class": "modal-content"                                                                                       // 104
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add/Edit Projects</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 106
  }, "\n			 ", HTML.Raw('<span id="project_fail" class="response_error" style="display: none;">Project Save Failed!</span>'), "\n			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                       // 108
      collection: Spacebars.call("Projects"),                                                                      // 109
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 110
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 111
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 112
      id: Spacebars.call("addNewProject"),                                                                         // 113
      type: Spacebars.call("method"),                                                                              // 114
      meteormethod: Spacebars.call("createProject"),                                                               // 115
      resetOnSuccess: Spacebars.call(true)                                                                         // 116
    };                                                                                                             // 117
  }, function() {                                                                                                  // 118
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 119
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 121
                                                                                                                   // 122
Template.__checkName("editProjectForm");                                                                           // 123
Template["editProjectForm"] = new Template("Template.editProjectForm", (function() {                               // 124
  var view = this;                                                                                                 // 125
  return [ HTML.DIV({                                                                                              // 126
    "class": "modal fade",                                                                                         // 127
    tabindex: "-1",                                                                                                // 128
    id: "editProjectModal",                                                                                        // 129
    role: "dialog"                                                                                                 // 130
  }, "\n	  ", HTML.DIV({                                                                                           // 131
    "class": "modal-dialog"                                                                                        // 132
  }, "\n		", HTML.DIV({                                                                                            // 133
    "class": "modal-content"                                                                                       // 134
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Project</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 136
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                               // 137
    return {                                                                                                       // 138
      collection: Spacebars.call("Projects"),                                                                      // 139
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 140
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 141
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 142
      id: Spacebars.call("editProjectForm"),                                                                       // 143
      type: Spacebars.call("method-update"),                                                                       // 144
      meteormethod: Spacebars.call("editProject"),                                                                 // 145
      resetOnSuccess: Spacebars.call(true),                                                                        // 146
      doc: Spacebars.call(view.lookup("editProjectDocument")),                                                     // 147
      singleMethodArgument: Spacebars.call(true)                                                                   // 148
    };                                                                                                             // 149
  }, function() {                                                                                                  // 150
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 151
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 153
                                                                                                                   // 154
Template.__checkName("editProjectSection");                                                                        // 155
Template["editProjectSection"] = new Template("Template.editProjectSection", (function() {                         // 156
  var view = this;                                                                                                 // 157
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editProject">Edit</button>\n  <button type="button" class="btn btn-xs js_removeProject btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteProjectDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Project? The changes cannot be undone!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveProject">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveProject btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                               // 159
                                                                                                                   // 160
Template.__checkName("projectList");                                                                               // 161
Template["projectList"] = new Template("Template.projectList", (function() {                                       // 162
  var view = this;                                                                                                 // 163
  return Blaze._TemplateWith(function() {                                                                          // 164
    return {                                                                                                       // 165
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Projects")),                              // 166
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 167
      id: Spacebars.call("projectList")                                                                            // 168
    };                                                                                                             // 169
  }, function() {                                                                                                  // 170
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 171
  });                                                                                                              // 172
}));                                                                                                               // 173
                                                                                                                   // 174
Template.__checkName("apartmentList");                                                                             // 175
Template["apartmentList"] = new Template("Template.apartmentList", (function() {                                   // 176
  var view = this;                                                                                                 // 177
  return Blaze._TemplateWith(function() {                                                                          // 178
    return {                                                                                                       // 179
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Apartments")),                            // 180
      selector: Spacebars.call(view.lookup("projectIdSelector")),                                                  // 181
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 182
      id: Spacebars.call("apartmentList")                                                                          // 183
    };                                                                                                             // 184
  }, function() {                                                                                                  // 185
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 186
  });                                                                                                              // 187
}));                                                                                                               // 188
                                                                                                                   // 189
Template.__checkName("userList");                                                                                  // 190
Template["userList"] = new Template("Template.userList", (function() {                                             // 191
  var view = this;                                                                                                 // 192
  return Blaze._TemplateWith(function() {                                                                          // 193
    return {                                                                                                       // 194
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Users")),                                 // 195
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 196
      id: Spacebars.call("userList")                                                                               // 197
    };                                                                                                             // 198
  }, function() {                                                                                                  // 199
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 200
  });                                                                                                              // 201
}));                                                                                                               // 202
                                                                                                                   // 203
Template.__checkName("editApartmentSection");                                                                      // 204
Template["editApartmentSection"] = new Template("Template.editApartmentSection", (function() {                     // 205
  var view = this;                                                                                                 // 206
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editApartment">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartment btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteApartmentDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete!</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Apartment? The changes cannot be undone!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveApartment">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveApartment btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                               // 208
                                                                                                                   // 209
Template.__checkName("apartmentForm");                                                                             // 210
Template["apartmentForm"] = new Template("Template.apartmentForm", (function() {                                   // 211
  var view = this;                                                                                                 // 212
  return [ HTML.Raw('<button class="btn btn-primary js-createApartment">Create Apartment</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                         // 214
    tabindex: "-1",                                                                                                // 215
    id: "createApartmentModal",                                                                                    // 216
    role: "dialog"                                                                                                 // 217
  }, "\n	  ", HTML.DIV({                                                                                           // 218
    "class": "modal-dialog"                                                                                        // 219
  }, "\n		", HTML.DIV({                                                                                            // 220
    "class": "modal-content"                                                                                       // 221
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Apartment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 223
  }, "\n			 ", HTML.Raw('<span id="apartment_fail" class="response_error" style="display: none;">Apartment Save Failed!</span>'), "\n			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                       // 225
      collection: Spacebars.call("Apartments"),                                                                    // 226
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 227
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 228
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 229
      id: Spacebars.call("addNewApartment"),                                                                       // 230
      type: Spacebars.call("method"),                                                                              // 231
      meteormethod: Spacebars.call("createApartment"),                                                             // 232
      resetOnSuccess: Spacebars.call(true)                                                                         // 233
    };                                                                                                             // 234
  }, function() {                                                                                                  // 235
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 236
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 238
                                                                                                                   // 239
Template.__checkName("editApartmentForm");                                                                         // 240
Template["editApartmentForm"] = new Template("Template.editApartmentForm", (function() {                           // 241
  var view = this;                                                                                                 // 242
  return [ HTML.DIV({                                                                                              // 243
    "class": "modal fade",                                                                                         // 244
    tabindex: "-1",                                                                                                // 245
    id: "editApartmentModal",                                                                                      // 246
    role: "dialog"                                                                                                 // 247
  }, "\n	  ", HTML.DIV({                                                                                           // 248
    "class": "modal-dialog"                                                                                        // 249
  }, "\n		", HTML.DIV({                                                                                            // 250
    "class": "modal-content"                                                                                       // 251
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Apartment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 253
  }, "\n			 ", HTML.Raw('<span id="apartment_edit_fail" class="response_error" style="display: none;">Apartment Edit Failed!</span>'), "			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                       // 255
      collection: Spacebars.call("Apartments"),                                                                    // 256
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 257
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 258
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 259
      id: Spacebars.call("editApartmentForm"),                                                                     // 260
      type: Spacebars.call("method-update"),                                                                       // 261
      meteormethod: Spacebars.call("editApartment"),                                                               // 262
      resetOnSuccess: Spacebars.call(true),                                                                        // 263
      doc: Spacebars.call(view.lookup("editApartmentDocument")),                                                   // 264
      singleMethodArgument: Spacebars.call(true)                                                                   // 265
    };                                                                                                             // 266
  }, function() {                                                                                                  // 267
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 268
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 270
                                                                                                                   // 271
Template.__checkName("userForm");                                                                                  // 272
Template["userForm"] = new Template("Template.userForm", (function() {                                             // 273
  var view = this;                                                                                                 // 274
  return [ HTML.Raw('<button class="btn btn-primary js-createUser">Create User</button>\n	'), HTML.DIV({           // 275
    "class": "modal fade",                                                                                         // 276
    tabindex: "-1",                                                                                                // 277
    id: "createUserModal",                                                                                         // 278
    role: "dialog"                                                                                                 // 279
  }, "\n	  ", HTML.DIV({                                                                                           // 280
    "class": "modal-dialog"                                                                                        // 281
  }, "\n		", HTML.DIV({                                                                                            // 282
    "class": "modal-content"                                                                                       // 283
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 285
  }, "\n			 \n			   ", Blaze._TemplateWith(function() {                                                            // 286
    return {                                                                                                       // 287
      collection: Spacebars.call("Meteor.users"),                                                                  // 288
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 289
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 290
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 291
      id: Spacebars.call("addNewUser"),                                                                            // 292
      type: Spacebars.call("method"),                                                                              // 293
      meteormethod: Spacebars.call("addUser"),                                                                     // 294
      resetOnSuccess: Spacebars.call(true)                                                                         // 295
    };                                                                                                             // 296
  }, function() {                                                                                                  // 297
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 298
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 300
                                                                                                                   // 301
Template.__checkName("editUserForm");                                                                              // 302
Template["editUserForm"] = new Template("Template.editUserForm", (function() {                                     // 303
  var view = this;                                                                                                 // 304
  return [ HTML.DIV({                                                                                              // 305
    "class": "modal fade",                                                                                         // 306
    tabindex: "-1",                                                                                                // 307
    id: "editUserModal",                                                                                           // 308
    role: "dialog"                                                                                                 // 309
  }, "\n	  ", HTML.DIV({                                                                                           // 310
    "class": "modal-dialog"                                                                                        // 311
  }, "\n		", HTML.DIV({                                                                                            // 312
    "class": "modal-content"                                                                                       // 313
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 315
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                               // 316
    return {                                                                                                       // 317
      collection: Spacebars.call("Meteor.users"),                                                                  // 318
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 319
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 320
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 321
      id: Spacebars.call("editUserForm"),                                                                          // 322
      type: Spacebars.call("method-update"),                                                                       // 323
      meteormethod: Spacebars.call("editUser"),                                                                    // 324
      resetOnSuccess: Spacebars.call(true),                                                                        // 325
      doc: Spacebars.call(view.lookup("editUserDocument")),                                                        // 326
      singleMethodArgument: Spacebars.call(true)                                                                   // 327
    };                                                                                                             // 328
  }, function() {                                                                                                  // 329
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 330
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 332
                                                                                                                   // 333
Template.__checkName("editUserSection");                                                                           // 334
Template["editUserSection"] = new Template("Template.editUserSection", (function() {                               // 335
  var view = this;                                                                                                 // 336
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editUser">Edit</button>\n  <button type="button" class="btn btn-xs js_removeUser btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteUserDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the User? He may be assigned to an apartment!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveUser">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveUser btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                               // 338
                                                                                                                   // 339
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"apartmentManagement.html":["./template.apartmentManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/apartmentManagement.html                                                                           //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
module.exports = require("./template.apartmentManagement.js");                                                     // 1
                                                                                                                   // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.apartmentManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/template.apartmentManagement.js                                                                    //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
                                                                                                                   // 1
Template.__checkName("apartmentManagement");                                                                       // 2
Template["apartmentManagement"] = new Template("Template.apartmentManagement", (function() {                       // 3
  var view = this;                                                                                                 // 4
  return HTML.DIV({                                                                                                // 5
    "class": "row"                                                                                                 // 6
  }, "\n	", HTML.DIV({                                                                                             // 7
    "class": "jumbotron container"                                                                                 // 8
  }, "	\n		", HTML.DIV({                                                                                           // 9
    role: "tabpanel"                                                                                               // 10
  }, "\n							", HTML.Raw("<!-- Nav tabs -->"), "\n							", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n								<li role="presentation" class="active"><a id="update-tab" data-target="#updateManagement" data-toggle="tab">Updates</a></li>\n								<li role="presentation"><a id="userManagement-tab" data-target="#userManagement" data-toggle="tab">User Management</a></li>\n								<li role="presentation"><a id="documents-tab" data-target="#documentManagement" data-toggle="tab">Documents</a></li>\n							</ul>'), "\n						\n							", HTML.Raw("<!-- Tab panes -->"), "\n							", HTML.DIV({
    "class": "tab-content"                                                                                         // 12
  }, "						\n								", HTML.Raw('<div role="tabpanel" class="tab-pane active text-center" id="updateManagement">\n									<div class="row">\n									</div>																\n								</div>'), "\n								", HTML.DIV({
    role: "tabpanel",                                                                                              // 14
    "class": "tab-pane text-center",                                                                               // 15
    id: "userManagement"                                                                                           // 16
  }, "\n									", HTML.DIV({                                                                                     // 17
    "class": "row"                                                                                                 // 18
  }, "\n										", HTML.DIV({                                                                                    // 19
    "class": "col-md-12"                                                                                           // 20
  }, "												\n											", Spacebars.include(view.lookupTemplate("userAssignment")), "\n										"), "\n									"), "	\n									", HTML.DIV({
    "class": "row"                                                                                                 // 22
  }, "\n										", HTML.DIV({                                                                                    // 23
    "class": "col-md-12"                                                                                           // 24
  }, "\n											", Spacebars.include(view.lookupTemplate("assignedUsers")), "\n										"), "\n									"), "									\n								"), "\n								", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="documentManagement">\n									<div class="row">\n									</div>\n									</div>'), "						\n								"), "\n							"), "\n						"), "\n	");
}));                                                                                                               // 26
                                                                                                                   // 27
Template.__checkName("userAssignment");                                                                            // 28
Template["userAssignment"] = new Template("Template.userAssignment", (function() {                                 // 29
  var view = this;                                                                                                 // 30
  return [ HTML.Raw('<button class="btn btn-primary js-assignOwners">Assign Owners</button>\n  '), HTML.DIV({      // 31
    "class": "modal fade",                                                                                         // 32
    tabindex: "-1",                                                                                                // 33
    id: "assignUserModal",                                                                                         // 34
    role: "dialog"                                                                                                 // 35
  }, "\n	  ", HTML.DIV({                                                                                           // 36
    "class": "modal-dialog"                                                                                        // 37
  }, "\n		", HTML.DIV({                                                                                            // 38
    "class": "modal-content"                                                                                       // 39
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Assign User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 41
  }, "\n			 ", Spacebars.include(view.lookupTemplate("assignUsers")), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n			<button type="button" class="btn btn-xs btn-warning js_closeAssignUserModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 43
                                                                                                                   // 44
Template.__checkName("assignUsers");                                                                               // 45
Template["assignUsers"] = new Template("Template.assignUsers", (function() {                                       // 46
  var view = this;                                                                                                 // 47
  return Blaze._TemplateWith(function() {                                                                          // 48
    return {                                                                                                       // 49
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "UserList")),                              // 50
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 51
      id: Spacebars.call("assignUsersList")                                                                        // 52
    };                                                                                                             // 53
  }, function() {                                                                                                  // 54
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 55
  });                                                                                                              // 56
}));                                                                                                               // 57
                                                                                                                   // 58
Template.__checkName("assignedUsers");                                                                             // 59
Template["assignedUsers"] = new Template("Template.assignedUsers", (function() {                                   // 60
  var view = this;                                                                                                 // 61
  return [ Blaze._TemplateWith(function() {                                                                        // 62
    return {                                                                                                       // 63
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "AssignedUserList")),                      // 64
      selector: Spacebars.call(view.lookup("userIdSelector")),                                                     // 65
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 66
      id: Spacebars.call("assignedUserList")                                                                       // 67
    };                                                                                                             // 68
  }, function() {                                                                                                  // 69
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 70
  }), "\n				", Spacebars.include(view.lookupTemplate("removeAssignmentModal")) ];                                 // 71
}));                                                                                                               // 72
                                                                                                                   // 73
Template.__checkName("removeUserAssignSection");                                                                   // 74
Template["removeUserAssignSection"] = new Template("Template.removeUserAssignSection", (function() {               // 75
  var view = this;                                                                                                 // 76
  return HTML.Raw('<button type="button" class="btn btn-xs btn-danger js_removeUserAssignment">Remove</button>');  // 77
}));                                                                                                               // 78
                                                                                                                   // 79
Template.__checkName("assignUser");                                                                                // 80
Template["assignUser"] = new Template("Template.assignUser", (function() {                                         // 81
  var view = this;                                                                                                 // 82
  return HTML.Raw('<button type="button" class="btn btn-xs btn-danger js_assignUserToApartment">Assign</button>');
}));                                                                                                               // 84
                                                                                                                   // 85
Template.__checkName("removeAssignmentModal");                                                                     // 86
Template["removeAssignmentModal"] = new Template("Template.removeAssignmentModal", (function() {                   // 87
  var view = this;                                                                                                 // 88
  return HTML.Raw('<div class="modal fade" tabindex="-1" id="confirmRemoveUserAssignmentModal" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete?</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to remove the Assignment? He will lose all the updates!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveUserAssignment">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveUserAssignment btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                               // 90
                                                                                                                   // 91
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"projectManagement.html":["./template.projectManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/projectManagement.html                                                                             //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
module.exports = require("./template.projectManagement.js");                                                       // 1
                                                                                                                   // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.projectManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/template.projectManagement.js                                                                      //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
                                                                                                                   // 1
Template.__checkName("TprojectUpdate");                                                                            // 2
Template["TprojectUpdate"] = new Template("Template.TprojectUpdate", (function() {                                 // 3
  var view = this;                                                                                                 // 4
  return HTML.Raw('<input type="file" id="uploadedFile">\n	<input type="button" class="btn btn-primary js_uploadImage">');
}));                                                                                                               // 6
                                                                                                                   // 7
Template.__checkName("TprojectUpdates");                                                                           // 8
Template["TprojectUpdates"] = new Template("Template.TprojectUpdates", (function() {                               // 9
  var view = this;                                                                                                 // 10
  return Blaze._TemplateWith(function() {                                                                          // 11
    return {                                                                                                       // 12
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ProjectUpdates")),                        // 13
      selector: Spacebars.call(view.lookup("projectIdSelector")),                                                  // 14
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                    // 15
      id: Spacebars.call("projectUpdatesTable")                                                                    // 16
    };                                                                                                             // 17
  }, function() {                                                                                                  // 18
    return Spacebars.include(view.lookupTemplate("tabular"));                                                      // 19
  });                                                                                                              // 20
}));                                                                                                               // 21
                                                                                                                   // 22
Template.__checkName("TeditProjectUpdatesSection");                                                                // 23
Template["TeditProjectUpdatesSection"] = new Template("Template.TeditProjectUpdatesSection", (function() {         // 24
  var view = this;                                                                                                 // 25
  return HTML.Raw('<button type="button" class="btn btn-xs btn-success js_manageProjectUpdateFiles">Files</button>\n  <button type="button" class="btn btn-xs btn-warning js_editProjectUpdate">Edit</button>\n  <button type="button" class="btn btn-xs js_removeProjectUpdate btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteProjectDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Project Update? You will loose all the files!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveProjectUpdate">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveProjectUpdate btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div>');
}));                                                                                                               // 27
                                                                                                                   // 28
Template.__checkName("TprojectUpdateForm");                                                                        // 29
Template["TprojectUpdateForm"] = new Template("Template.TprojectUpdateForm", (function() {                         // 30
  var view = this;                                                                                                 // 31
  return [ HTML.Raw('<button class="btn btn-primary js-createProjectUpdate">Add Project Update</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                         // 33
    tabindex: "-1",                                                                                                // 34
    id: "createProjectUpdateModal",                                                                                // 35
    role: "dialog"                                                                                                 // 36
  }, "\n	  ", HTML.DIV({                                                                                           // 37
    "class": "modal-dialog"                                                                                        // 38
  }, "\n		", HTML.DIV({                                                                                            // 39
    "class": "modal-content"                                                                                       // 40
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 42
  }, "\n			 ", HTML.DIV({                                                                                          // 43
    "class": "row"                                                                                                 // 44
  }, "\n				", HTML.DIV({                                                                                          // 45
    "class": "col-md-12"                                                                                           // 46
  }, "\n				", HTML.H4(" Project Name: ", Blaze.View("lookup:projectName", function() {                            // 47
    return Spacebars.mustache(view.lookup("projectName"));                                                         // 48
  })), "\n				"), "\n			 "), "\n			 ", HTML.DIV({                                                                  // 49
    "class": "row"                                                                                                 // 50
  }, "\n				", HTML.DIV({                                                                                          // 51
    "class": "col-md-12"                                                                                           // 52
  }, "\n					", Blaze._TemplateWith(function() {                                                                   // 53
    return {                                                                                                       // 54
      collection: Spacebars.call("ProjectUpdates"),                                                                // 55
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 56
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 57
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 58
      id: Spacebars.call("addNewProjectUpdate"),                                                                   // 59
      type: Spacebars.call("method"),                                                                              // 60
      meteormethod: Spacebars.call("createProjectUpdate"),                                                         // 61
      doc: Spacebars.call(view.lookup("defaultValues")),                                                           // 62
      resetOnSuccess: Spacebars.call(true)                                                                         // 63
    };                                                                                                             // 64
  }, function() {                                                                                                  // 65
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 66
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 68
                                                                                                                   // 69
Template.__checkName("TeditProjectUpdateForm");                                                                    // 70
Template["TeditProjectUpdateForm"] = new Template("Template.TeditProjectUpdateForm", (function() {                 // 71
  var view = this;                                                                                                 // 72
  return [ HTML.DIV({                                                                                              // 73
    "class": "modal fade",                                                                                         // 74
    tabindex: "-1",                                                                                                // 75
    id: "editProjectUpdateModal",                                                                                  // 76
    role: "dialog"                                                                                                 // 77
  }, "\n	  ", HTML.DIV({                                                                                           // 78
    "class": "modal-dialog"                                                                                        // 79
  }, "\n		", HTML.DIV({                                                                                            // 80
    "class": "modal-content"                                                                                       // 81
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                          // 83
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                               // 84
    return {                                                                                                       // 85
      collection: Spacebars.call("ProjectUpdates"),                                                                // 86
      template: Spacebars.call("bootstrap3-horizontal"),                                                           // 87
      "label-class": Spacebars.call("col-sm-2"),                                                                   // 88
      "input-col-class": Spacebars.call("col-sm-10"),                                                              // 89
      id: Spacebars.call("editProjectUpdateForm"),                                                                 // 90
      type: Spacebars.call("method-update"),                                                                       // 91
      meteormethod: Spacebars.call("editProjectUpdate"),                                                           // 92
      resetOnSuccess: Spacebars.call(true),                                                                        // 93
      doc: Spacebars.call(view.lookup("editProjectUpdateDocument")),                                               // 94
      singleMethodArgument: Spacebars.call(true)                                                                   // 95
    };                                                                                                             // 96
  }, function() {                                                                                                  // 97
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                    // 98
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                               // 100
                                                                                                                   // 101
Template.__checkName("TprojectManagement");                                                                        // 102
Template["TprojectManagement"] = new Template("Template.TprojectManagement", (function() {                         // 103
  var view = this;                                                                                                 // 104
  return HTML.DIV({                                                                                                // 105
    "class": "row"                                                                                                 // 106
  }, "\n			", HTML.DIV({                                                                                           // 107
    "class": "jumbotron container"                                                                                 // 108
  }, "			\n			", HTML.DIV({                                                                                        // 109
    "class": "text-center"                                                                                         // 110
  }, "\n				", HTML.DIV({                                                                                          // 111
    role: "tabpanel"                                                                                               // 112
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="project-tab" data-target="#pmProject" data-toggle="tab">Project Update</a></li>\n						<li role="presentation"><a id="pmTBD1-tab" data-target="#pmTBD1" data-toggle="tab">TBD</a></li>\n						<li role="presentation"><a id="pmTBD2-tab" data-target="#pmTBD2" data-toggle="tab">TBD</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                         // 114
  }, "					\n						", HTML.DIV({                                                                                   // 115
    role: "tabpanel",                                                                                              // 116
    "class": "tab-pane active text-center",                                                                        // 117
    id: "pmProject"                                                                                                // 118
  }, "\n							", HTML.DIV({                                                                                       // 119
    "class": "row"                                                                                                 // 120
  }, "\n								", HTML.DIV({                                                                                      // 121
    "class": "col-md-12"                                                                                           // 122
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectUpdateForm")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                 // 124
  }, "\n								", HTML.DIV({                                                                                      // 125
    "class": "col-md-12"                                                                                           // 126
  }, "\n								", HTML.DIV({                                                                                      // 127
    "class": "table-responsive"                                                                                    // 128
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectUpdates")), "\n								"), "\n									", Spacebars.include(view.lookupTemplate("TeditProjectUpdateForm")), "\n								"), "\n							"), "							\n						"), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="pmTBD1">\n							<div class="row">\n								<div class="col-md-12">						\n								</div>\n							</div>\n							\n						</div>'), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="pmTBD2">							\n							<div class="row">\n								<div class="col-md-12">\n								</div>\n							</div>\n							</div>'), "\n\n							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		");
}));                                                                                                               // 130
                                                                                                                   // 131
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"adminFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./adminTemplate.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/adminFunction.js                                                                                   //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var _meteor = require('meteor/meteor');                                                                            // 1
                                                                                                                   //
var _templating = require('meteor/templating');                                                                    // 2
                                                                                                                   //
var _reactiveVar = require('meteor/reactive-var');                                                                 // 3
                                                                                                                   //
require('./adminTemplate.html');                                                                                   // 5
                                                                                                                   //
//Template.registerHelper("Collections", Collections);                                                             //
_templating.Template.admin_section.events({                                                                        // 8
                                                                                                                   //
	'click .js-createProject': function () {                                                                          // 10
		function clickJsCreateProject(event) {                                                                           // 10
			$("#createProjectModal").modal();                                                                               // 11
		}                                                                                                                //
                                                                                                                   //
		return clickJsCreateProject;                                                                                     //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.admin_section.helpers({                                                                       // 17
	'isProjectSelected': function () {                                                                                // 18
		function isProjectSelected() {                                                                                   // 18
			return Session.get('pmProjectId');                                                                              // 19
		}                                                                                                                //
                                                                                                                   //
		return isProjectSelected;                                                                                        //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.projectList.events({                                                                          // 24
	'click tbody > tr': function () {                                                                                 // 25
		function clickTbodyTr(event) {                                                                                   // 25
			var dataTable = $(event.target).closest('table').DataTable();                                                   // 26
			var rowData = dataTable.row(event.currentTarget).data();                                                        // 27
			Session.set("pmProjectId", rowData._id);                                                                        // 28
			Session.set("pmProjectName", rowData.projectName);                                                              // 29
			console.log("PM ProjectId" + Session.get("pmProjectId"));                                                       // 30
		}                                                                                                                //
                                                                                                                   //
		return clickTbodyTr;                                                                                             //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.projectListOptions.helpers({                                                                  // 36
	'projects': function () {                                                                                         // 37
		function projects() {                                                                                            // 37
			return ReactiveMethod.call('getAllProjects', Session.get("getProjectsRefreshFlag"));                            // 38
		}                                                                                                                //
                                                                                                                   //
		return projects;                                                                                                 //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.projectListOptions.events({                                                                   // 42
	'change .projectNameDropDown': function () {                                                                      // 43
		function changeProjectNameDropDown(event) {                                                                      // 43
			var projectId = $(event.currentTarget).val();                                                                   // 44
			Session.set("selectedProjectId", projectId);                                                                    // 45
			console.log("ProjectId : " + projectId);                                                                        // 46
		}                                                                                                                //
                                                                                                                   //
		return changeProjectNameDropDown;                                                                                //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.apartmentForm.events({                                                                        // 50
	'click .js-createApartment': function () {                                                                        // 51
		function clickJsCreateApartment(event) {                                                                         // 51
			event.preventDefault();                                                                                         // 52
			$("#createApartmentModal").modal('show');                                                                       // 53
		}                                                                                                                //
                                                                                                                   //
		return clickJsCreateApartment;                                                                                   //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.apartmentList.helpers({                                                                       // 56
	projectIdSelector: function () {                                                                                  // 57
		function projectIdSelector() {                                                                                   // 57
			return { projectId: Session.get("selectedProjectId") }; // this could be pulled from a Session var or something that is reactive
		}                                                                                                                // 57
                                                                                                                   //
		return projectIdSelector;                                                                                        //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.apartmentList.events({                                                                        // 61
	'click tbody > tr': function () {                                                                                 // 62
		function clickTbodyTr(event) {                                                                                   // 62
			var dataTable = $(event.target).closest('table').DataTable();                                                   // 63
			var rowData = dataTable.row(event.currentTarget).data();                                                        // 64
			Session.set("selectedApartmentId", rowData._id);                                                                // 65
			Session.set("apartmentSelectFlag", new Date());                                                                 // 66
		}                                                                                                                //
                                                                                                                   //
		return clickTbodyTr;                                                                                             //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.userForm.events({                                                                             // 73
	'click .js-createUser': function () {                                                                             // 74
		function clickJsCreateUser(event) {                                                                              // 74
			event.preventDefault();                                                                                         // 75
			$("#createUserModal").modal('show');                                                                            // 76
		}                                                                                                                //
                                                                                                                   //
		return clickJsCreateUser;                                                                                        //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.editApartmentSection.events({                                                                 // 80
	'click .js_editApartment': function () {                                                                          // 81
		function clickJs_editApartment(event) {                                                                          // 81
			event.preventDefault();                                                                                         // 82
			Session.set("getProjectsRefreshFlag", new Date());                                                              // 83
			Session.set("editApartmentDocument", this);                                                                     // 84
			$("#editApartmentModal").modal('show');                                                                         // 85
		}                                                                                                                //
                                                                                                                   //
		return clickJs_editApartment;                                                                                    //
	}(),                                                                                                              //
	'click .js_removeApartment': function () {                                                                        // 87
		function clickJs_removeApartment(event) {                                                                        // 87
			event.preventDefault();                                                                                         // 88
			Session.set("getProjectsRefreshFlag", new Date());                                                              // 89
			$("#confirmDeleteApartmentDialog").modal('show');                                                               // 90
		}                                                                                                                //
                                                                                                                   //
		return clickJs_removeApartment;                                                                                  //
	}(),                                                                                                              //
	'click .js_cancelRemoveApartment': function () {                                                                  // 92
		function clickJs_cancelRemoveApartment(event) {                                                                  // 92
			event.preventDefault();                                                                                         // 93
			$("#confirmDeleteApartmentDialog").modal('hide');                                                               // 94
		}                                                                                                                //
                                                                                                                   //
		return clickJs_cancelRemoveApartment;                                                                            //
	}(),                                                                                                              //
	'click .js_confirmRemoveApartment': function () {                                                                 // 96
		function clickJs_confirmRemoveApartment(event) {                                                                 // 96
			event.preventDefault();                                                                                         // 97
			$("#confirmDeleteApartmentDialog").modal('hide');                                                               // 98
			if (Session.get("editApartmentDocument") != null) {                                                             // 99
				_meteor.Meteor.call('removeApartment', Session.get("editApartmentDocument")._id);                              // 100
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return clickJs_confirmRemoveApartment;                                                                           //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
_templating.Template.editProjectSection.events({                                                                   // 106
	'click .js_editProject': function () {                                                                            // 107
		function clickJs_editProject(event) {                                                                            // 107
			event.preventDefault();                                                                                         // 108
			Session.set("getProjectRefreshFlag", new Date());                                                               // 109
			Session.set("editProjectDocument", this);                                                                       // 110
			$("#editProjectModal").modal('show');                                                                           // 111
		}                                                                                                                //
                                                                                                                   //
		return clickJs_editProject;                                                                                      //
	}(),                                                                                                              //
	'click .js_removeProject': function () {                                                                          // 113
		function clickJs_removeProject(event) {                                                                          // 113
			event.preventDefault();                                                                                         // 114
			Session.set("getProjectRefreshFlag", new Date());                                                               // 115
			Session.set("editProjectDocument", this);                                                                       // 116
			$("#confirmDeleteProjectDialog").modal('show');                                                                 // 117
		}                                                                                                                //
                                                                                                                   //
		return clickJs_removeProject;                                                                                    //
	}(),                                                                                                              //
	'click .js_cancelRemoveProject': function () {                                                                    // 119
		function clickJs_cancelRemoveProject(event) {                                                                    // 119
			event.preventDefault();                                                                                         // 120
			$("#confirmDeleteProjectDialog").modal('hide');                                                                 // 121
		}                                                                                                                //
                                                                                                                   //
		return clickJs_cancelRemoveProject;                                                                              //
	}(),                                                                                                              //
	'click .js_confirmRemoveProject': function () {                                                                   // 123
		function clickJs_confirmRemoveProject(event) {                                                                   // 123
			event.preventDefault();                                                                                         // 124
			$("#confirmDeleteProjectDialog").modal('hide');                                                                 // 125
			if (Session.get("editProjectDocument") != null) {                                                               // 126
				_meteor.Meteor.call('removeProject', Session.get("editProjectDocument")._id);                                  // 127
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return clickJs_confirmRemoveProject;                                                                             //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
_templating.Template.editApartmentForm.helpers({                                                                   // 133
	'editApartmentDocument': function () {                                                                            // 134
		function editApartmentDocument() {                                                                               // 134
			if (Session.get("editApartmentDocument") != null) {                                                             // 135
				return ReactiveMethod.call('getApartmentByApartmentId', Session.get("editApartmentDocument")._id, Session.get("getProjectsRefreshFlag"));
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editApartmentDocument;                                                                                    //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.editProjectForm.helpers({                                                                     // 140
	'editProjectDocument': function () {                                                                              // 141
		function editProjectDocument() {                                                                                 // 141
			if (Session.get("editProjectDocument") != null) {                                                               // 142
				return ReactiveMethod.call('getProjectByProjectId', Session.get("editProjectDocument")._id, Session.get("getProjectsRefreshFlag"));
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editProjectDocument;                                                                                      //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.editUserForm.helpers({                                                                        // 147
	'editUserDocument': function () {                                                                                 // 148
		function editUserDocument() {                                                                                    // 148
			if (Session.get("editUserDocument") != null) {                                                                  // 149
				return ReactiveMethod.call('getUserByUserId', Session.get("editUserDocument")._id, Session.get("userRefreshFlag"));
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editUserDocument;                                                                                         //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.editUserSection.events({                                                                      // 154
	'click .js_editUser': function () {                                                                               // 155
		function clickJs_editUser(event) {                                                                               // 155
			event.preventDefault();                                                                                         // 156
			Session.set("editUserDocument", this);                                                                          // 157
			$("#editUserModal").modal('show');                                                                              // 158
		}                                                                                                                //
                                                                                                                   //
		return clickJs_editUser;                                                                                         //
	}(),                                                                                                              //
	'click .js_removeUser': function () {                                                                             // 160
		function clickJs_removeUser(event) {                                                                             // 160
			event.preventDefault();                                                                                         // 161
			Session.set("editUserDocument", this);                                                                          // 162
			$("#confirmDeleteUserDialog").modal('show');                                                                    // 163
		}                                                                                                                //
                                                                                                                   //
		return clickJs_removeUser;                                                                                       //
	}(),                                                                                                              //
	'click .js_cancelRemoveUser': function () {                                                                       // 165
		function clickJs_cancelRemoveUser(event) {                                                                       // 165
			event.preventDefault();                                                                                         // 166
			$("#confirmDeleteUserDialog").modal('hide');                                                                    // 167
		}                                                                                                                //
                                                                                                                   //
		return clickJs_cancelRemoveUser;                                                                                 //
	}(),                                                                                                              //
	'click .js_confirmRemoveUser': function () {                                                                      // 169
		function clickJs_confirmRemoveUser(event) {                                                                      // 169
			event.preventDefault();                                                                                         // 170
			$("#confirmDeleteUserDialog").modal('hide');                                                                    // 171
			if (Session.get("editUserDocument") != null) {                                                                  // 172
				_meteor.Meteor.call('removeProject', Session.get("editUserDocument")._id);                                     // 173
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return clickJs_confirmRemoveUser;                                                                                //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
AutoForm.addHooks(['editApartmentForm'], {                                                                         // 180
	onSuccess: function () {                                                                                          // 181
		function onSuccess(operation, result, template) {                                                                // 181
			$("#editApartmentModal").modal('hide');                                                                         // 182
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['editProjectForm'], {                                                                           // 185
	onSuccess: function () {                                                                                          // 186
		function onSuccess(operation, result, template) {                                                                // 186
			$("#editProjectModal").modal('hide');                                                                           // 187
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['addNewProject'], {                                                                             // 190
	onSuccess: function () {                                                                                          // 191
		function onSuccess(operation, result, template) {                                                                // 191
			Session.set("getProjectsRefreshFlag", new Date());                                                              // 192
			$("#createProjectModal").modal('hide');                                                                         // 193
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['addNewApartment'], {                                                                           // 196
	onSuccess: function () {                                                                                          // 197
		function onSuccess(operation, result, template) {                                                                // 197
			$("#createApartmentModal").modal('hide');                                                                       // 198
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['addNewUser'], {                                                                                // 201
	onSuccess: function () {                                                                                          // 202
		function onSuccess(operation, result, template) {                                                                // 202
			$("#createUserModal").modal('hide');                                                                            // 203
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['editUserForm'], {                                                                              // 206
	onSuccess: function () {                                                                                          // 207
		function onSuccess(operation, result, template) {                                                                // 207
			$("#editUserModal").modal('hide');                                                                              // 208
			Session.set("userRefreshFlag", new Date());                                                                     // 209
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"apartmentManagementFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./apartmentManagement.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/apartmentManagementFunction.js                                                                     //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var _meteor = require('meteor/meteor');                                                                            // 1
                                                                                                                   //
var _templating = require('meteor/templating');                                                                    // 2
                                                                                                                   //
var _reactiveVar = require('meteor/reactive-var');                                                                 // 3
                                                                                                                   //
require('./apartmentManagement.html');                                                                             // 5
                                                                                                                   //
_templating.Template.assignedUsers.helpers({                                                                       // 7
	userIdSelector: function () {                                                                                     // 8
		function userIdSelector() {                                                                                      // 8
			userIdList = ReactiveMethod.call('getUsersByApartmentId', Session.get("selectedApartmentId"), Session.get("apartmentSelectFlag"));
			return { '_id': { $in: userIdList } };                                                                          // 10
		}                                                                                                                //
                                                                                                                   //
		return userIdSelector;                                                                                           //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.userAssignment.events({                                                                       // 14
	'click .js-assignOwners': function () {                                                                           // 15
		function clickJsAssignOwners(event) {                                                                            // 15
			$("#assignUserModal").modal('show');                                                                            // 16
		}                                                                                                                //
                                                                                                                   //
		return clickJsAssignOwners;                                                                                      //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.assignUser.events({                                                                           // 19
	'click .js_assignUserToApartment': function () {                                                                  // 20
		function clickJs_assignUserToApartment(event) {                                                                  // 20
			apartmentId = Session.get("selectedApartmentId");                                                               // 21
			userId = this._id;                                                                                              // 22
			console.log(apartmentId + " User:" + userId);                                                                   // 23
			if (apartmentId != null && userId != null) {                                                                    // 24
				_meteor.Meteor.call('addUserApartment', userId, apartmentId);                                                  // 25
			}                                                                                                               //
			Session.set("apartmentSelectFlag", new Date());                                                                 // 27
		}                                                                                                                //
                                                                                                                   //
		return clickJs_assignUserToApartment;                                                                            //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.removeUserAssignSection.events({                                                              // 31
	'click .js_removeUserAssignment': function () {                                                                   // 32
		function clickJs_removeUserAssignment(event) {                                                                   // 32
			Session.set("selectedUserId", this._id);                                                                        // 33
			$("#confirmRemoveUserAssignmentModal").modal('show');                                                           // 34
		}                                                                                                                //
                                                                                                                   //
		return clickJs_removeUserAssignment;                                                                             //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.removeAssignmentModal.events({                                                                // 38
	'click .js_cancelRemoveUserAssignment': function () {                                                             // 39
		function clickJs_cancelRemoveUserAssignment(event) {                                                             // 39
			$("#confirmRemoveUserAssignmentModal").modal('hide');                                                           // 40
		}                                                                                                                //
                                                                                                                   //
		return clickJs_cancelRemoveUserAssignment;                                                                       //
	}(),                                                                                                              //
	'click .js_confirmRemoveUserAssignment': function () {                                                            // 42
		function clickJs_confirmRemoveUserAssignment(event) {                                                            // 42
			userId = Session.get("selectedUserId");                                                                         // 43
			apartmentId = Session.get("selectedApartmentId");                                                               // 44
			if (userId != null && apartmentId != null) {                                                                    // 45
				_meteor.Meteor.call('removeUserAssignment', userId, apartmentId);                                              // 46
				Session.set("apartmentSelectFlag", new Date());                                                                // 47
			}                                                                                                               //
			$("#confirmRemoveUserAssignmentModal").modal('hide');                                                           // 49
		}                                                                                                                //
                                                                                                                   //
		return clickJs_confirmRemoveUserAssignment;                                                                      //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"projectManagementFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./projectManagement.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/admin/projectManagementFunction.js                                                                       //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var _meteor = require('meteor/meteor');                                                                            // 1
                                                                                                                   //
var _templating = require('meteor/templating');                                                                    // 2
                                                                                                                   //
var _reactiveVar = require('meteor/reactive-var');                                                                 // 3
                                                                                                                   //
require('./projectManagement.html');                                                                               // 5
                                                                                                                   //
$.cloudinary.config({                                                                                              // 7
	cloud_name: "aparto"                                                                                              // 8
});                                                                                                                //
                                                                                                                   //
_templating.Template.TprojectUpdate.events({                                                                       // 13
	'click .js_uploadImage': function () {                                                                            // 14
		function clickJs_uploadImage(event) {                                                                            // 14
			var file = $('#uploadedFile').get(0).files[0];                                                                  // 15
			var result = {};                                                                                                // 16
			console.log(file);                                                                                              // 17
			Cloudinary._upload_file(file, { "folder": "updates" }, function (err, res) {                                    // 18
				if (err) {                                                                                                     // 19
					console.log(err);                                                                                             // 20
				} else {                                                                                                       //
					console.log(res);                                                                                             // 23
					result.url = res.url;                                                                                         // 24
					result.publicId = res.public_id;                                                                              // 25
					console.log(result);                                                                                          // 26
				}                                                                                                              //
			});                                                                                                             //
		}                                                                                                                //
                                                                                                                   //
		return clickJs_uploadImage;                                                                                      //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.TprojectUpdateForm.events({                                                                   // 34
	'click .js-createProjectUpdate': function () {                                                                    // 35
		function clickJsCreateProjectUpdate(event) {                                                                     // 35
			$("#createProjectUpdateModal").modal('show');                                                                   // 36
		}                                                                                                                //
                                                                                                                   //
		return clickJsCreateProjectUpdate;                                                                               //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.TeditProjectUpdatesSection.events({                                                           // 41
                                                                                                                   //
	'click .js_editProjectUpdate': function () {                                                                      // 43
		function clickJs_editProjectUpdate(event) {                                                                      // 43
			event.preventDefault();                                                                                         // 44
			Session.set("editProjectUpdateRecord", this);                                                                   // 45
			Session.set("getProjectUpdatesRefreshFlag", new Date());                                                        // 46
			$("#editProjectUpdateModal").modal('show');                                                                     // 47
		}                                                                                                                //
                                                                                                                   //
		return clickJs_editProjectUpdate;                                                                                //
	}(),                                                                                                              //
                                                                                                                   //
	'click .js_manageProjectUpdateFiles': function () {                                                               // 50
		function clickJs_manageProjectUpdateFiles(event) {                                                               // 50
			event.preventDefault();                                                                                         // 51
		}                                                                                                                //
                                                                                                                   //
		return clickJs_manageProjectUpdateFiles;                                                                         //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.TeditProjectUpdateForm.helpers({                                                              // 58
	'editProjectUpdateDocument': function () {                                                                        // 59
		function editProjectUpdateDocument() {                                                                           // 59
			if (Session.get("editProjectUpdateRecord") != null) {                                                           // 60
				return ReactiveMethod.call('getProjectUpdateByProjectUpdateId', Session.get("editProjectUpdateRecord")._id, Session.get("getProjectUpdatesRefreshFlag"));
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editProjectUpdateDocument;                                                                                //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
_templating.Template.TprojectUpdateForm.helpers({                                                                  // 66
	'projectName': function () {                                                                                      // 67
		function projectName() {                                                                                         // 67
			return Session.get("pmProjectName");                                                                            // 68
		}                                                                                                                //
                                                                                                                   //
		return projectName;                                                                                              //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
                                                                                                                   //
_templating.Template.TprojectUpdates.helpers({                                                                     // 74
	projectIdSelector: function () {                                                                                  // 75
		function projectIdSelector() {                                                                                   // 75
			return { projectId: Session.get("pmProjectId") }; // this could be pulled from a Session var or something that is reactive
		}                                                                                                                // 75
                                                                                                                   //
		return projectIdSelector;                                                                                        //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
AutoForm.addHooks(['addNewProjectUpdate'], {                                                                       // 81
	onSuccess: function () {                                                                                          // 82
		function onSuccess(operation, result, template) {                                                                // 82
			$("#createProjectUpdateModal").modal('hide');                                                                   // 83
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
AutoForm.addHooks(['editProjectUpdateForm'], {                                                                     // 86
	onSuccess: function () {                                                                                          // 87
		function onSuccess(operation, result, template) {                                                                // 87
			$("#editProjectUpdateModal").modal('hide');                                                                     // 88
			Session.set("getProjectUpdatesRefreshFlag", new Date());                                                        // 89
		}                                                                                                                //
                                                                                                                   //
		return onSuccess;                                                                                                //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.TprojectUpdateForm.helpers({                                                                  // 92
	defaultValues: function () {                                                                                      // 93
		function defaultValues() {                                                                                       // 93
			return { projectId: Session.get('pmProjectId') };                                                               // 94
		}                                                                                                                //
                                                                                                                   //
		return defaultValues;                                                                                            //
	}()                                                                                                               //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"credentials":{"template.credentialTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/credentials/template.credentialTemplate.js                                                               //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
                                                                                                                   // 1
Template.__checkName("credential_Section");                                                                        // 2
Template["credential_Section"] = new Template("Template.credential_Section", (function() {                         // 3
  var view = this;                                                                                                 // 4
  return [ HTML.Raw('<div class="">\n		<h4 class="text-center" id="loginModalLabel">Sign In/Login</h4>\n	</div>\n		'), HTML.DIV({
    "class": "container jumbotron"                                                                                 // 6
  }, "\n			", HTML.DIV({                                                                                           // 7
    "class": "text-center"                                                                                         // 8
  }, "\n				", HTML.DIV({                                                                                          // 9
    role: "tabpanel",                                                                                              // 10
    "class": "login-tab"                                                                                           // 11
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="signin-taba" data-target="#login" data-toggle="tab">Sign In</a></li>\n						<li role="presentation"><a id="forgetpass-taba" data-target="#forget_password" data-toggle="tab">Forgot Password</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                         // 13
  }, "\n						", HTML.DIV({                                                                                        // 14
    role: "tabpanel",                                                                                              // 15
    "class": "tab-pane active text-center",                                                                        // 16
    id: "login"                                                                                                    // 17
  }, "\n							", Spacebars.include(view.lookupTemplate("login")), "\n						"), "\n						", HTML.DIV({             // 18
    role: "tabpanel",                                                                                              // 19
    "class": "tab-pane text-center",                                                                               // 20
    id: "forget_password"                                                                                          // 21
  }, "\n							", Spacebars.include(view.lookupTemplate("password_recovery")), "							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		") ];
}));                                                                                                               // 23
                                                                                                                   // 24
Template.__checkName("login");                                                                                     // 25
Template["login"] = new Template("Template.login", (function() {                                                   // 26
  var view = this;                                                                                                 // 27
  return HTML.Raw('<div>\n				&nbsp;&nbsp;\n				<span id="login_fail" class="response_error" style="display: none;">Loggin failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="loginForm">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control required email" name="email" id="remail" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n							<input type="password" class="form-control required" name="password" id="password" placeholder="Password">\n						</div>\n						<span class="help-block has-error" id="password-error"></span>\n					</div>\n					<button type="sumbit" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In....">Login</button>\n					<div class="clearfix"></div>\n				</form>\n			</div>');
}));                                                                                                               // 29
                                                                                                                   // 30
Template.__checkName("registration");                                                                              // 31
Template["registration"] = new Template("Template.registration", (function() {                                     // 32
  var view = this;                                                                                                 // 33
  return HTML.Raw('<span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="registration_form">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control" id="remail" name="email" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n								<input type="password" class="form-control" id="password" placeholder="Password">\n							</div>\n					</div>\n					<button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Register</button>\n					<div class="clearfix"></div>\n					<div class="login-modal-footer">\n					</div>\n				</form>');
}));                                                                                                               // 35
                                                                                                                   // 36
Template.__checkName("password_recovery");                                                                         // 37
Template["password_recovery"] = new Template("Template.password_recovery", (function() {                           // 38
  var view = this;                                                                                                 // 39
  return HTML.Raw('<span id="reset_fail" class="response_error" style="display: none;">Recovery Failed, check entered email</span>\n					<div class="clearfix"></div>\n					<form id="password_recovery_form">\n						<div class="form-group">\n							<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-user"></i></div>\n								<input type="text" class="form-control required email" id="femail" name="email" placeholder="Email">\n							</div>\n							<span class="help-block has-error" data-error="0" id="femail-error"></span>\n						</div>\n						\n						<button type="submit" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait....">Recover Password</button>\n						<div class="clearfix"></div>\n					</form>');
}));                                                                                                               // 41
                                                                                                                   // 42
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"main.html":["./template.main.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/main.html                                                                                                //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
module.exports = require("./template.main.js");                                                                    // 1
                                                                                                                   // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.main.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/template.main.js                                                                                         //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
                                                                                                                   // 1
Template.body.addContent((function() {                                                                             // 2
  var view = this;                                                                                                 // 3
  return "";                                                                                                       // 4
}));                                                                                                               // 5
Meteor.startup(Template.body.renderToDocument);                                                                    // 6
                                                                                                                   // 7
Template.__checkName("ApplicationLayout");                                                                         // 8
Template["ApplicationLayout"] = new Template("Template.ApplicationLayout", (function() {                           // 9
  var view = this;                                                                                                 // 10
  return HTML.DIV({                                                                                                // 11
    id: "site-wrapper"                                                                                             // 12
  }, "\n   ", HTML.DIV({                                                                                           // 13
    id: "site-canvas"                                                                                              // 14
  }, "	\n	", HTML.DIV({                                                                                            // 15
    id: "site-menu"                                                                                                // 16
  }, "\n      ", Spacebars.include(view.lookupTemplate("left_navbar")), "\n     "), "\n	", HTML.DIV({              // 17
    "class": ""                                                                                                    // 18
  }, "\n		", Spacebars.include(view.lookupTemplate("branding_nav")), "\n		", HTML.DIV("\n				", Blaze._TemplateWith(function() {
    return "main";                                                                                                 // 20
  }, function() {                                                                                                  // 21
    return Spacebars.include(view.lookupTemplate("yield"));                                                        // 22
  }), "\n		"), "\n	"), "    \n  "), "\n");                                                                         // 23
}));                                                                                                               // 24
                                                                                                                   // 25
Template.__checkName("left_navbar");                                                                               // 26
Template["left_navbar"] = new Template("Template.left_navbar", (function() {                                       // 27
  var view = this;                                                                                                 // 28
  return [ HTML.Raw('<a href="#" class="js-toggle-leftnav" style="color: pink; font-size: 20px;"><i class="fa fa-times"></i></a>\n       '), HTML.NAV({
    "class": "cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left",                                                     // 30
    id: "cbp-spmenu-s1"                                                                                            // 31
  }, "	  \n			", HTML.Raw('<span style="display:inline"><h3>Aparto</h3></span>'), "\n			", HTML.Raw('<a href="/" class="js-toggle-leftnav">Project Details</a>'), "\n			", HTML.Raw('<a href="#" class="js-toggle-leftnav">Project Status</a>'), "\n			", Blaze.If(function() {
    return Spacebars.call(view.lookup("currentUser"));                                                             // 33
  }, function() {                                                                                                  // 34
    return [ "\n			", HTML.A({                                                                                     // 35
      href: "#"                                                                                                    // 36
    }, "My Project"), "\n			", HTML.A({                                                                            // 37
      href: "#"                                                                                                    // 38
    }, "Account Statement"), "\n			", HTML.A({                                                                     // 39
      href: "#"                                                                                                    // 40
    }, "Documents"), "\n			", HTML.A({                                                                             // 41
      href: "#"                                                                                                    // 42
    }, "Service Requests"), "\n			", HTML.A({                                                                      // 43
      href: "#"                                                                                                    // 44
    }, "Refer a friend"), "			\n			", HTML.A({                                                                     // 45
      href: "#"                                                                                                    // 46
    }, "My Profile"), "			\n			", HTML.A({                                                                         // 47
      href: "#",                                                                                                   // 48
      "class": "js-signout"                                                                                        // 49
    }, "Signout"), "\n			" ];                                                                                      // 50
  }), "		\n		") ];                                                                                                 // 51
}));                                                                                                               // 52
                                                                                                                   // 53
Template.__checkName("branding");                                                                                  // 54
Template["branding"] = new Template("Template.branding", (function() {                                             // 55
  var view = this;                                                                                                 // 56
  return HTML.Raw('<div>\n		 <button class="toggle-nav btn btn-lg btn-success js-toggle-leftnav"><i class="fa fa-bars"></i></button>\n	</div>');
}));                                                                                                               // 58
                                                                                                                   // 59
Template.__checkName("branding_nav");                                                                              // 60
Template["branding_nav"] = new Template("Template.branding_nav", (function() {                                     // 61
  var view = this;                                                                                                 // 62
  return HTML.DIV({                                                                                                // 63
    "class": "row",                                                                                                // 64
    style: "background:#47a3da"                                                                                    // 65
  }, "\n			", HTML.DIV({                                                                                           // 66
    "class": "col-md-2"                                                                                            // 67
  }, "\n				", Spacebars.include(view.lookupTemplate("branding")), "\n			"), HTML.Raw('			\n			<div class="col-md-9">\n				<h4 style="align:center"> Pushpam Group</h4>\n			</div>	\n				'), HTML.DIV({
    "class": "col-md-1",                                                                                           // 69
    style: "float:right"                                                                                           // 70
  }, "\n					", Blaze.Unless(function() {                                                                          // 71
    return Spacebars.call(view.lookup("currentUser"));                                                             // 72
  }, function() {                                                                                                  // 73
    return [ "\n					", HTML.A({                                                                                   // 74
      href: "/login",                                                                                              // 75
      "class": "btn btn-warning btn-lg",                                                                           // 76
      style: "vertical-align:middle"                                                                               // 77
    }, "Login"), "\n					" ];                                                                                      // 78
  }), "\n				"), "			\n		");                                                                                       // 79
}));                                                                                                               // 80
                                                                                                                   // 81
Template.__checkName("home");                                                                                      // 82
Template["home"] = new Template("Template.home", (function() {                                                     // 83
  var view = this;                                                                                                 // 84
  return HTML.Raw('<div>\n	<div style=";color:white">\n		<div class="">\n		  <h1>Pushpam Group</h1> \n		  <p>Pushpam Group is a multinational organization dealing with the diversified areas of Real Estate, Agro business, Floriculture, International Trading, Wellness, Hospitality, Healthcare and Education while creating meaningful living spaces, improving human well-being and empowerment of mankind through power of knowledge, health and happiness</p> \n		</div>\n		<div class="row">\n			<br>\n			<div class=" col-md-4 ">\n			  <h2>Vision</h2> \n			  <p>Play a pioneering role in Realty Sector and create landmarks that are sustainable &amp; add value to the environment, thus transforming the way we build, live and work.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Mission</h2> \n			  <p>Develop safe, eco-friendly and self sustainable living communities.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Quality Policy</h2> \n			  <p>Timely execution with 100 % compliance to agreed set of commitments.</p> \n			</div>\n		</div>\n	</div>\n</div>');
}));                                                                                                               // 86
                                                                                                                   // 87
Template.__checkName("construction_status");                                                                       // 88
Template["construction_status"] = new Template("Template.construction_status", (function() {                       // 89
  var view = this;                                                                                                 // 90
  return HTML.Raw("<div>\n		\n	</div>");                                                                           // 91
}));                                                                                                               // 92
Meteor.startup(function() {                                                                                        // 93
  var attrs = {"style":"background-image:url('projectsmain.jpg');"};                                               // 94
  for (var prop in attrs) {                                                                                        // 95
    document.body.setAttribute(prop, attrs[prop]);                                                                 // 96
  }                                                                                                                // 97
});                                                                                                                // 98
                                                                                                                   // 99
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["meteor/templating","meteor/reactive-var","./main.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// client/main.js                                                                                                  //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
var _templating = require('meteor/templating');                                                                    // 1
                                                                                                                   //
var _reactiveVar = require('meteor/reactive-var');                                                                 // 2
                                                                                                                   //
require('./main.html');                                                                                            // 4
                                                                                                                   //
Router.configure({                                                                                                 // 7
	layoutTemplate: 'ApplicationLayout'                                                                               // 8
});                                                                                                                //
                                                                                                                   //
Router.route('/', function () {                                                                                    // 11
	this.render('home', {                                                                                             // 12
		to: 'main'                                                                                                       // 13
	});                                                                                                               //
});                                                                                                                //
                                                                                                                   //
Router.route('/login', function () {                                                                               // 17
	this.render('credential_Section', {                                                                               // 18
		to: 'main'                                                                                                       // 19
	});                                                                                                               //
});                                                                                                                //
Router.route('/admin', function () {                                                                               // 22
	this.render('admin_section', {                                                                                    // 23
		to: 'main'                                                                                                       // 24
	});                                                                                                               //
});                                                                                                                //
                                                                                                                   //
function toggleNav() {                                                                                             // 29
	if ($('#site-wrapper').hasClass('show-nav')) {                                                                    // 30
		// Do things on Nav Close                                                                                        //
		$('#site-wrapper').removeClass('show-nav');                                                                      // 32
	} else {                                                                                                          //
		// Do things on Nav Open                                                                                         //
		$('#site-wrapper').addClass('show-nav');                                                                         // 35
	}                                                                                                                 //
                                                                                                                   //
	//$('#site-wrapper').toggleClass('show-nav');                                                                     //
}                                                                                                                  // 29
                                                                                                                   //
_templating.Template.branding.events({                                                                             // 42
	'click .js-toggle-leftnav': function () {                                                                         // 43
		function clickJsToggleLeftnav(event) {                                                                           // 43
			console.log('clicked nav');                                                                                     // 44
			toggleNav();                                                                                                    // 45
		}                                                                                                                //
                                                                                                                   //
		return clickJsToggleLeftnav;                                                                                     //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.left_navbar.events({                                                                          // 49
	'click .js-toggle-leftnav': function () {                                                                         // 50
		function clickJsToggleLeftnav(event) {                                                                           // 50
			console.log('clicked nav');                                                                                     // 51
			toggleNav();                                                                                                    // 52
		}                                                                                                                //
                                                                                                                   //
		return clickJsToggleLeftnav;                                                                                     //
	}(),                                                                                                              //
	'click .js-signout': function () {                                                                                // 54
		function clickJsSignout(event) {                                                                                 // 54
			event.preventDefault();                                                                                         // 55
			console.log('Logging out');                                                                                     // 56
			Meteor.logout();                                                                                                // 57
			Router.go('/login');                                                                                            // 58
		}                                                                                                                //
                                                                                                                   //
		return clickJsSignout;                                                                                           //
	}()                                                                                                               //
});                                                                                                                //
_templating.Template.branding_nav.events({});                                                                      // 61
                                                                                                                   //
_templating.Template.login.events({                                                                                // 65
	'submit form': function () {                                                                                      // 66
		function submitForm(event) {                                                                                     // 66
			event.preventDefault();                                                                                         // 67
			$("#login_fail").css('display', 'none');                                                                        // 68
			var emailVar = event.target.email.value;                                                                        // 69
			var passwordVar = event.target.password.value;                                                                  // 70
			if (validateEmail(emailVar) && password != null && passwordVar.trim() != "") {                                  // 71
				Meteor.loginWithPassword(emailVar, passwordVar, function (err) {                                               // 72
					if (err) {                                                                                                    // 73
						console.log("Login Erro");                                                                                   // 74
						console.log(err);                                                                                            // 75
						console.log("Login Err End");                                                                                // 76
						$("#login_fail").css('display', 'block');                                                                    // 77
					} else {                                                                                                      //
						Router.go('/');                                                                                              // 80
						console.log("Routing Home");                                                                                 // 81
					}                                                                                                             //
				});                                                                                                            //
			} else {                                                                                                        //
				$("#login_fail").css('display', 'block');                                                                      // 86
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return submitForm;                                                                                               //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
_templating.Template.password_recovery.events({                                                                    // 91
	'submit form': function () {                                                                                      // 92
		function submitForm(event) {                                                                                     // 92
			event.preventDefault();                                                                                         // 93
			console.log("recovery requested");                                                                              // 94
			$("#reset_fail").css('display', 'none');                                                                        // 95
			var emailVar = event.target.email.value;                                                                        // 96
			if (validateEmail(emailVar)) {                                                                                  // 97
				Accounts.forgotPassword({ email: emailVar }, function (err) {                                                  // 98
					if (err) {                                                                                                    // 99
						$("#reset_fail").css('display', 'block');                                                                    // 100
						console.log(err);                                                                                            // 101
					} else {                                                                                                      //
						$("#reset_fail").css('display', 'none');                                                                     // 105
						alert("Password recovery link sent you your email");                                                         // 106
					}                                                                                                             //
				});                                                                                                            //
			} else {                                                                                                        //
				$("#reset_fail").css('display', 'block');                                                                      // 111
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return submitForm;                                                                                               //
	}()                                                                                                               //
});                                                                                                                //
                                                                                                                   //
_templating.Template.login.rendered = function () {                                                                // 116
	$("#login_form").validate();                                                                                      // 117
};                                                                                                                 //
                                                                                                                   //
_templating.Template.registration.rendered = function () {                                                         // 120
	$("#registration_form").validate();                                                                               // 121
};                                                                                                                 //
                                                                                                                   //
_templating.Template.password_recovery.rendered = function () {                                                    // 124
	$("#password_recovery_form").validate();                                                                          // 125
};                                                                                                                 //
                                                                                                                   //
function validateEmail(email) {                                                                                    // 128
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);                                                                                            // 130
}                                                                                                                  //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"lib":{"dbOperations.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// lib/dbOperations.js                                                                                             //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
Projects = new Mongo.Collection('project');                                                                        // 1
ProjectSchemas = {};                                                                                               // 2
ProjectSchemas.Projects = new SimpleSchema({                                                                       // 3
	projectName: {                                                                                                    // 4
		type: String,                                                                                                    // 5
		label: 'Project Name'                                                                                            // 6
	},                                                                                                                //
	projectDescription: {                                                                                             // 8
		type: String,                                                                                                    // 9
		label: 'Project Description'                                                                                     // 10
	},                                                                                                                //
	projectAddress: {                                                                                                 // 12
		type: String,                                                                                                    // 13
		label: 'Project Address'                                                                                         // 14
	},                                                                                                                //
	projectAdditionalInfo: {                                                                                          // 16
		type: String,                                                                                                    // 17
		label: 'Project Info'                                                                                            // 18
	}                                                                                                                 //
});                                                                                                                //
Projects.attachSchema(ProjectSchemas.Projects);                                                                    // 21
TabularTables = {};                                                                                                // 22
                                                                                                                   //
TabularTables.Projects = new Tabular.Table({                                                                       // 24
	name: "Projects",                                                                                                 // 25
	collection: Projects,                                                                                             // 26
	columns: [{ data: "projectName", title: "Name" }, { data: "projectDescription", title: "Description" }, { data: "projectAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editProjectSection                                                             // 32
	}],                                                                                                               //
	extraFields: ['_id']                                                                                              // 35
});                                                                                                                //
Meteor.methods({                                                                                                   // 37
	'createProject': function () {                                                                                    // 38
		function createProject(project) {                                                                                // 38
			var currentUserId = Meteor.userId();                                                                            // 39
			if (currentUserId) {                                                                                            // 40
				project.createdOn = new Date();                                                                                // 41
				Projects.insert(project, function (err, data) {                                                                // 42
					if (data) {                                                                                                   // 43
						console.log("Success project insertion");                                                                    // 44
					}                                                                                                             //
				});                                                                                                            //
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return createProject;                                                                                            //
	}(),                                                                                                              //
	'editProject': function () {                                                                                      // 50
		function editProject(project, val) {                                                                             // 50
			var currentUserId = Meteor.userId();                                                                            // 51
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                     // 52
			if (currentUserId) {                                                                                            // 53
				Projects.update({ _id: project._id }, project.modifier, function (err, data) {});                              // 54
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editProject;                                                                                              //
	}(),                                                                                                              //
	'getAllProjects': function () {                                                                                   // 58
		function getAllProjects(refreshFlag) {                                                                           // 58
			return Projects.find().fetch();                                                                                 // 59
		}                                                                                                                //
                                                                                                                   //
		return getAllProjects;                                                                                           //
	}(),                                                                                                              //
	'getProjectByProjectId': function () {                                                                            // 61
		function getProjectByProjectId(projectId, refreshFlag) {                                                         // 61
			return Projects.findOne({ _id: projectId });                                                                    // 62
		}                                                                                                                //
                                                                                                                   //
		return getProjectByProjectId;                                                                                    //
	}(),                                                                                                              //
	'removeProject': function () {                                                                                    // 65
		function removeProject(projectId) {                                                                              // 65
			console.log("Removing Project" + projectId);                                                                    // 66
			return Projects.remove(projectId);                                                                              // 67
		}                                                                                                                //
                                                                                                                   //
		return removeProject;                                                                                            //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_apartments.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// lib/dbo_apartments.js                                                                                           //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
Apartments = new Mongo.Collection('apartment');                                                                    // 1
ApartmentSchemas = {};                                                                                             // 2
ApartmentSchemas.Apartments = new SimpleSchema({                                                                   // 3
	projectId: {                                                                                                      // 4
		type: String,                                                                                                    // 5
		autoform: {                                                                                                      // 6
			options: function () {                                                                                          // 7
				function options() {                                                                                           // 7
					return Projects.find().map(function (c) {                                                                     // 8
						return { label: c.projectName, value: c._id };                                                               // 9
					});                                                                                                           //
				}                                                                                                              //
                                                                                                                   //
				return options;                                                                                                //
			}()                                                                                                             //
		}                                                                                                                //
	},                                                                                                                //
	flatNumber: {                                                                                                     // 14
		type: String,                                                                                                    // 15
		label: 'Flat Number'                                                                                             // 16
	},                                                                                                                //
	flatBlock: {                                                                                                      // 18
		type: String,                                                                                                    // 19
		label: 'Block'                                                                                                   // 20
	},                                                                                                                //
	flatArea: {                                                                                                       // 22
		type: String,                                                                                                    // 23
		label: 'Area (Sqft.)'                                                                                            // 24
	},                                                                                                                //
	flatFloor: {                                                                                                      // 26
		type: Number,                                                                                                    // 27
		label: 'Floor'                                                                                                   // 28
	},                                                                                                                //
	flatStatusId: {                                                                                                   // 30
		type: Number,                                                                                                    // 31
		label: 'Status'                                                                                                  // 32
	},                                                                                                                //
	flatDescription: {                                                                                                // 34
		type: String,                                                                                                    // 35
		label: 'Info'                                                                                                    // 36
	},                                                                                                                //
	twoWheelerParking: {                                                                                              // 38
		type: Number,                                                                                                    // 39
		label: '2 Wheeler Spots'                                                                                         // 40
	},                                                                                                                //
	fourWheelerSpots: {                                                                                               // 42
		type: Number,                                                                                                    // 43
		label: '4 Wheeler Spots'                                                                                         // 44
	}                                                                                                                 //
});                                                                                                                //
Apartments.attachSchema(ApartmentSchemas.Apartments);                                                              // 47
TabularTables = {};                                                                                                // 48
                                                                                                                   //
TabularTables.Apartments = new Tabular.Table({                                                                     // 50
	name: "Apartments",                                                                                               // 51
	collection: Apartments,                                                                                           // 52
	columns: [{ data: "flatNumber", title: "Number" }, { data: "flatBlock", title: "Block" }, { data: "flatFloor", title: "Floor" }, { data: "flatArea", title: "Area" }, { data: "flatDescription", title: "Info" }, {
		tmpl: Meteor.isClient && Template.editApartmentSection                                                           // 60
	}],                                                                                                               //
	extraFields: ["_id"]                                                                                              // 63
});                                                                                                                //
Meteor.methods({                                                                                                   // 65
	'createApartment': function () {                                                                                  // 66
		function createApartment(apartment) {                                                                            // 66
			var currentUserId = Meteor.userId();                                                                            // 67
			if (currentUserId) {                                                                                            // 68
				apartment.createdOn = new Date();                                                                              // 69
				Apartments.insert(apartment, function (err, data) {                                                            // 70
					if (data) {                                                                                                   // 71
						console.log("Success Apartment insertion");                                                                  // 72
					}                                                                                                             //
				});                                                                                                            //
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return createApartment;                                                                                          //
	}(),                                                                                                              //
	'editApartment': function () {                                                                                    // 78
		function editApartment(apartment, val) {                                                                         // 78
			var currentUserId = Meteor.userId();                                                                            // 79
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                     // 80
			console.log(apartment.modifier);                                                                                // 81
			if (currentUserId) {                                                                                            // 82
				Apartments.update({ _id: apartment._id }, apartment.modifier, function (err, data) {});                        // 83
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editApartment;                                                                                            //
	}(),                                                                                                              //
	'getAllApartments': function () {                                                                                 // 87
		function getAllApartments(refreshFlag) {                                                                         // 87
			return Apartments.find().fetch();                                                                               // 88
		}                                                                                                                //
                                                                                                                   //
		return getAllApartments;                                                                                         //
	}(),                                                                                                              //
	'getApartmentByProjectId': function () {                                                                          // 90
		function getApartmentByProjectId(projectId, refreshFlag) {                                                       // 90
			return Apartments.find({ projectId: projectId }).fetch();                                                       // 91
		}                                                                                                                //
                                                                                                                   //
		return getApartmentByProjectId;                                                                                  //
	}(),                                                                                                              //
	'getApartmentByApartmentId': function () {                                                                        // 93
		function getApartmentByApartmentId(apartmentId, refreshFlag) {                                                   // 93
			return Apartments.findOne({ _id: apartmentId });                                                                // 94
		}                                                                                                                //
                                                                                                                   //
		return getApartmentByApartmentId;                                                                                //
	}(),                                                                                                              //
	'removeApartment': function () {                                                                                  // 97
		function removeApartment(apartmentId) {                                                                          // 97
			return Apartments.remove(apartmentId);                                                                          // 98
		}                                                                                                                //
                                                                                                                   //
		return removeApartment;                                                                                          //
	}(),                                                                                                              //
	'addAssignment': function () {                                                                                    // 100
		function addAssignment(apartmentId, userId) {                                                                    // 100
			UserApartment.insert({                                                                                          // 101
				userId: userId,                                                                                                // 102
				apartmentId: apartmentId,                                                                                      // 103
				assignmentStartDate: new Date()                                                                                // 104
			});                                                                                                             //
		}                                                                                                                //
                                                                                                                   //
		return addAssignment;                                                                                            //
	}(),                                                                                                              //
	'removeAssignment': function () {                                                                                 // 107
		function removeAssignment(apartmentAssignementId) {                                                              // 107
			UserApartment.remove(apartmentAssignementId);                                                                   // 108
		}                                                                                                                //
                                                                                                                   //
		return removeAssignment;                                                                                         //
	}()                                                                                                               //
                                                                                                                   //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_projectUpdates.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// lib/dbo_projectUpdates.js                                                                                       //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
ProjectUpdates = new Mongo.Collection('project_update');                                                           // 1
Schema = {};                                                                                                       // 2
                                                                                                                   //
Schema.ProjectUpdates = new SimpleSchema({                                                                         // 4
  projectId: {                                                                                                     // 5
    type: String,                                                                                                  // 6
    autoform: {                                                                                                    // 7
      type: "hidden",                                                                                              // 8
      label: false                                                                                                 // 9
    }                                                                                                              //
  },                                                                                                               //
  updateType: {                                                                                                    // 12
    type: String,                                                                                                  // 13
    autoform: {                                                                                                    // 14
      options: {                                                                                                   // 15
        recreation: "Recreation",                                                                                  // 16
        building: "Building",                                                                                      // 17
        amenities: "Amenities"                                                                                     // 18
      }                                                                                                            //
    }                                                                                                              //
  },                                                                                                               //
  updateDescription: {                                                                                             // 22
    type: String                                                                                                   // 23
  }                                                                                                                //
});                                                                                                                //
ProjectUpdates.attachSchema(Schema.ProjectUpdates);                                                                // 26
                                                                                                                   //
TabularTables.ProjectUpdates = new Tabular.Table({                                                                 // 28
  name: "ProjectUpdates",                                                                                          // 29
  collection: ProjectUpdates,                                                                                      // 30
  columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
    tmpl: Meteor.isClient && Template.TeditProjectUpdatesSection                                                   // 36
  }],                                                                                                              //
  extraFields: ['_id', 'projectId']                                                                                // 39
});                                                                                                                //
                                                                                                                   //
Meteor.methods({                                                                                                   // 42
  'createProjectUpdate': function () {                                                                             // 43
    function createProjectUpdate(projectUpdate) {                                                                  // 43
      var currentUserId = Meteor.userId();                                                                         // 44
      if (currentUserId || true) {                                                                                 // 45
        projectUpdate.createdOn = new Date();                                                                      // 46
        ProjectUpdates.insert(projectUpdate, function (err, data) {});                                             // 47
      }                                                                                                            //
    }                                                                                                              //
                                                                                                                   //
    return createProjectUpdate;                                                                                    //
  }(),                                                                                                             //
  'editProjectUpdate': function () {                                                                               // 51
    function editProjectUpdate(projectUpdate) {                                                                    // 51
      var currentUserId = Meteor.userId();                                                                         // 52
      currentUser = Meteor.users.findOne({ _id: currentUserId });                                                  // 53
      if (currentUserId || true) {                                                                                 // 54
        ProjectUpdates.update({ _id: projectUpdate._id }, projectUpdate.modifier, function (err, data) {});        // 55
      }                                                                                                            //
    }                                                                                                              //
                                                                                                                   //
    return editProjectUpdate;                                                                                      //
  }(),                                                                                                             //
  'getProjectUpdateByProjectUpdateId': function () {                                                               // 59
    function getProjectUpdateByProjectUpdateId(projectUpdateId) {                                                  // 59
      return ProjectUpdates.findOne({ _id: projectUpdateId });                                                     // 60
    }                                                                                                              //
                                                                                                                   //
    return getProjectUpdateByProjectUpdateId;                                                                      //
  }()                                                                                                              //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_users.js":function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                 //
// lib/dbo_users.js                                                                                                //
//                                                                                                                 //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                   //
UserApartment = new Mongo.Collection('user_apartment');                                                            // 1
Schema = {};                                                                                                       // 2
                                                                                                                   //
Schema.UserProfile = new SimpleSchema({                                                                            // 4
	firstName: {                                                                                                      // 5
		type: String                                                                                                     // 6
	},                                                                                                                //
	lastName: {                                                                                                       // 8
		type: String                                                                                                     // 9
	},                                                                                                                //
	dataOfBirth: {                                                                                                    // 11
		type: Date                                                                                                       // 12
	},                                                                                                                //
	sex: {                                                                                                            // 14
		type: String,                                                                                                    // 15
		autoform: {                                                                                                      // 16
			options: {                                                                                                      // 17
				male: "Male",                                                                                                  // 18
				female: "Female"                                                                                               // 19
			}                                                                                                               //
		}                                                                                                                //
	},                                                                                                                //
	photoUrl: {                                                                                                       // 23
		type: String                                                                                                     // 24
	},                                                                                                                //
	userStatus: {                                                                                                     // 26
		type: Number                                                                                                     // 27
	},                                                                                                                //
	primaryPhone: {                                                                                                   // 29
		type: Number                                                                                                     // 30
	},                                                                                                                //
	secondaryPhone: {                                                                                                 // 32
		type: Number                                                                                                     // 33
	},                                                                                                                //
	permanentAddress: {                                                                                               // 35
		type: String                                                                                                     // 36
	}                                                                                                                 //
});                                                                                                                //
                                                                                                                   //
Schema.User = new SimpleSchema({                                                                                   // 40
	emails: {                                                                                                         // 41
		type: Array,                                                                                                     // 42
		// For accounts-password, either emails or username is required, but not both. It is OK to make this             //
		// optional here because the accounts-password package does its own validation.                                  //
		// Third-party login packages may not require either. Adjust this schema as necessary for your usage.            //
		optional: true                                                                                                   // 46
	},                                                                                                                //
	"emails.$": {                                                                                                     // 48
		type: Object                                                                                                     // 49
	},                                                                                                                //
	"emails.$.address": {                                                                                             // 51
		type: String,                                                                                                    // 52
		regEx: SimpleSchema.RegEx.Email                                                                                  // 53
	},                                                                                                                //
	"emails.$.verified": {                                                                                            // 55
		type: Boolean                                                                                                    // 56
	},                                                                                                                //
                                                                                                                   //
	profile: {                                                                                                        // 60
		type: Schema.UserProfile,                                                                                        // 61
		optional: true                                                                                                   // 62
	}                                                                                                                 //
});                                                                                                                //
                                                                                                                   //
Meteor.users.attachSchema(Schema.User);                                                                            // 66
                                                                                                                   //
TabularTables.Users = new Tabular.Table({                                                                          // 69
	name: "Users",                                                                                                    // 70
	collection: Meteor.users,                                                                                         // 71
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editUserSection                                                                // 78
	}],                                                                                                               //
	extraFields: ["_id"]                                                                                              // 81
});                                                                                                                //
                                                                                                                   //
TabularTables.UserList = new Tabular.Table({                                                                       // 85
	name: "UserList",                                                                                                 // 86
	collection: Meteor.users,                                                                                         // 87
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.assignUser                                                                     // 94
	}],                                                                                                               //
	extraFields: ["_id"]                                                                                              // 97
});                                                                                                                //
                                                                                                                   //
TabularTables.AssignedUserList = new Tabular.Table({                                                               // 100
	name: "AssignedUserList",                                                                                         // 101
	collection: Meteor.users,                                                                                         // 102
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.removeUserAssignSection                                                        // 109
	}],                                                                                                               //
	extraFields: ["_id"]                                                                                              // 112
});                                                                                                                //
                                                                                                                   //
Meteor.methods({                                                                                                   // 117
	'addUser': function () {                                                                                          // 118
		function addUser(user) {                                                                                         // 118
			var currentUserId = Meteor.userId();                                                                            // 119
			user.password = "shiva";                                                                                        // 120
			console.log(user);                                                                                              // 121
			user.email = user.emails[0].address;                                                                            // 122
			if (currentUserId) {                                                                                            // 123
				Accounts.createUser(user);                                                                                     // 124
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return addUser;                                                                                                  //
	}(),                                                                                                              //
	'editUser': function () {                                                                                         // 128
		function editUser(user, val) {                                                                                   // 128
			var currentUserId = Meteor.userId();                                                                            // 129
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                     // 130
			if (currentUserId) {                                                                                            // 131
				Meteor.users.update({ _id: user._id }, user.modifier, function (err, data) {});                                // 132
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return editUser;                                                                                                 //
	}(),                                                                                                              //
	'getAllUsers': function () {                                                                                      // 136
		function getAllUsers(refreshFlag) {                                                                              // 136
			return Meteor.users.find().fetch();                                                                             // 137
		}                                                                                                                //
                                                                                                                   //
		return getAllUsers;                                                                                              //
	}(),                                                                                                              //
	'getUserByUserId': function () {                                                                                  // 139
		function getUserByUserId(userId, refreshFlag) {                                                                  // 139
			return Meteor.users.findOne({ _id: userId });                                                                   // 140
		}                                                                                                                //
                                                                                                                   //
		return getUserByUserId;                                                                                          //
	}(),                                                                                                              //
	'removeUser': function () {                                                                                       // 143
		function removeUser(userId) {                                                                                    // 143
			return Meteor.users.remove(userId);                                                                             // 144
		}                                                                                                                //
                                                                                                                   //
		return removeUser;                                                                                               //
	}(),                                                                                                              //
	'getUsersByApartmentId': function () {                                                                            // 146
		function getUsersByApartmentId(apartmentId, refreshFlag) {                                                       // 146
			u = UserApartment.find({ "apartmentId": apartmentId });                                                         // 147
			idlist = [];                                                                                                    // 148
			u.forEach(function (myDoc) {                                                                                    // 149
				idlist.push(myDoc.userId);                                                                                     // 150
			});                                                                                                             //
			return idlist;                                                                                                  // 152
			//console.log(Meteor.users.find({"_id": {$in : idlist} }).fetch());                                             //
			//return Meteor.users.find({"_id": {$in : idlist} });                                                           //
		}                                                                                                                // 146
                                                                                                                   //
		return getUsersByApartmentId;                                                                                    //
	}(),                                                                                                              //
	'addUserApartment': function () {                                                                                 // 156
		function addUserApartment(userId, apartmentId) {                                                                 // 156
			if (userId != null && apartmentId != null) {                                                                    // 157
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                        // 158
				if (assignmentRecord.count() == 0) {                                                                           // 159
					UserApartment.insert({                                                                                        // 160
						userId: userId,                                                                                              // 161
						apartmentId: apartmentId,                                                                                    // 162
						assignmentStartDate: new Date()                                                                              // 163
					});                                                                                                           //
				}                                                                                                              //
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return addUserApartment;                                                                                         //
	}(),                                                                                                              //
	'removeUserAssignment': function () {                                                                             // 169
		function removeUserAssignment(userId, apartmentId) {                                                             // 169
			if (userId != null && apartmentId != null) {                                                                    // 170
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                        // 171
				UserApartment.remove(assignmentRecord);                                                                        // 172
			}                                                                                                               //
		}                                                                                                                //
                                                                                                                   //
		return removeUserAssignment;                                                                                     //
	}()                                                                                                               //
});                                                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{"extensions":[".js",".json",".html",".css"]});
require("./client/admin/template.adminTemplate.js");
require("./client/admin/template.apartmentManagement.js");
require("./client/admin/template.projectManagement.js");
require("./client/credentials/template.credentialTemplate.js");
require("./client/template.main.js");
require("./lib/dbOperations.js");
require("./lib/dbo_apartments.js");
require("./lib/dbo_projectUpdates.js");
require("./lib/dbo_users.js");
require("./client/admin/adminFunction.js");
require("./client/admin/apartmentManagementFunction.js");
require("./client/admin/projectManagementFunction.js");
require("./client/main.js");
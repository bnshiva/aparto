var require = meteorInstall({"lib":{"dbOperations.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbOperations.js                                                                                              //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
Projects = new Mongo.Collection('project');                                                                         // 1
ProjectSchemas = {};                                                                                                // 2
ProjectSchemas.Projects = new SimpleSchema({                                                                        // 3
	projectName: {                                                                                                     // 4
		type: String,                                                                                                     // 5
		label: 'Project Name'                                                                                             // 6
	},                                                                                                                 //
	projectDescription: {                                                                                              // 8
		type: String,                                                                                                     // 9
		label: 'Project Description'                                                                                      // 10
	},                                                                                                                 //
	projectAddress: {                                                                                                  // 12
		type: String,                                                                                                     // 13
		label: 'Project Address'                                                                                          // 14
	},                                                                                                                 //
	projectAdditionalInfo: {                                                                                           // 16
		type: String,                                                                                                     // 17
		label: 'Project Info'                                                                                             // 18
	}                                                                                                                  //
});                                                                                                                 //
Projects.attachSchema(ProjectSchemas.Projects);                                                                     // 21
TabularTables = {};                                                                                                 // 22
                                                                                                                    //
TabularTables.Projects = new Tabular.Table({                                                                        // 24
	name: "Projects",                                                                                                  // 25
	collection: Projects,                                                                                              // 26
	columns: [{ data: "projectName", title: "Name" }, { data: "projectDescription", title: "Description" }, { data: "projectAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editProjectSection                                                              // 32
	}],                                                                                                                //
	extraFields: ['_id']                                                                                               // 35
});                                                                                                                 //
Meteor.methods({                                                                                                    // 37
	'createProject': function () {                                                                                     // 38
		function createProject(project) {                                                                                 // 38
			var currentUserId = Meteor.userId();                                                                             // 39
			if (currentUserId) {                                                                                             // 40
				project.createdOn = new Date();                                                                                 // 41
				Projects.insert(project, function (err, data) {                                                                 // 42
					if (data) {                                                                                                    // 43
						console.log("Success project insertion");                                                                     // 44
					}                                                                                                              //
				});                                                                                                             //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createProject;                                                                                             //
	}(),                                                                                                               //
	'editProject': function () {                                                                                       // 50
		function editProject(project, val) {                                                                              // 50
			var currentUserId = Meteor.userId();                                                                             // 51
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 52
			if (currentUserId) {                                                                                             // 53
				Projects.update({ _id: project._id }, project.modifier, function (err, data) {});                               // 54
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editProject;                                                                                               //
	}(),                                                                                                               //
	'getAllProjects': function () {                                                                                    // 58
		function getAllProjects(refreshFlag) {                                                                            // 58
			return Projects.find().fetch();                                                                                  // 59
		}                                                                                                                 //
                                                                                                                    //
		return getAllProjects;                                                                                            //
	}(),                                                                                                               //
	'getProjectByProjectId': function () {                                                                             // 61
		function getProjectByProjectId(projectId, refreshFlag) {                                                          // 61
			return Projects.findOne({ _id: projectId });                                                                     // 62
		}                                                                                                                 //
                                                                                                                    //
		return getProjectByProjectId;                                                                                     //
	}(),                                                                                                               //
	'removeProject': function () {                                                                                     // 65
		function removeProject(projectId) {                                                                               // 65
			console.log("Removing Project" + projectId);                                                                     // 66
			return Projects.remove(projectId);                                                                               // 67
		}                                                                                                                 //
                                                                                                                    //
		return removeProject;                                                                                             //
	}(),                                                                                                               //
	'isAuthorized': function () {                                                                                      // 69
		function isAuthorized(userId, document) {                                                                         // 69
			if (userId) return true;                                                                                         // 70
			return false;                                                                                                    // 71
		}                                                                                                                 //
                                                                                                                    //
		return isAuthorized;                                                                                              //
	}()                                                                                                                //
                                                                                                                    //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_apartmentManagement.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbo_apartmentManagement.js                                                                                   //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
ApartmentDocuments = new Mongo.Collection('apartment_documents');                                                   // 1
ApartmentUpdates = new Mongo.Collection('apartment_updates');                                                       // 2
ApartmentDocumentFiles = new Mongo.Collection('apartment_document_file');                                           // 3
ApartmentUpdateFiles = new Mongo.Collection('apartment_update_file');                                               // 4
ApartmentPayment = new Mongo.Collection('apartment_payment');                                                       // 5
ApartmentPaymentFiles = new Mongo.Collection('apartment_payment_file');                                             // 6
ApartmentServiceRequests = new Mongo.Collection('apartment_service_request');                                       // 7
                                                                                                                    //
Schema = {};                                                                                                        // 9
                                                                                                                    //
Schema.ApartmentUpdates = new SimpleSchema({                                                                        // 11
	apartmentId: {                                                                                                     // 12
		type: String,                                                                                                     // 13
		autoform: {                                                                                                       // 14
			type: "hidden",                                                                                                  // 15
			label: false                                                                                                     // 16
		}                                                                                                                 //
	},                                                                                                                 //
	updateType: {                                                                                                      // 19
		type: String,                                                                                                     // 20
		autoform: {                                                                                                       // 21
			options: {                                                                                                       // 22
				living: "Living Area",                                                                                          // 23
				utility: "Utilities",                                                                                           // 24
				bathroom: "Bathroom"                                                                                            // 25
			}                                                                                                                //
		}                                                                                                                 //
	},                                                                                                                 //
	updateDescription: {                                                                                               // 29
		type: String                                                                                                      // 30
	}                                                                                                                  //
});                                                                                                                 //
ApartmentUpdates.attachSchema(Schema.ApartmentUpdates);                                                             // 33
Schema.ApartmentDocuments = new SimpleSchema({                                                                      // 34
	apartmentId: {                                                                                                     // 35
		type: String,                                                                                                     // 36
		autoform: {                                                                                                       // 37
			type: "hidden",                                                                                                  // 38
			label: false                                                                                                     // 39
		}                                                                                                                 //
	},                                                                                                                 //
	documentNumber: {                                                                                                  // 42
		type: String,                                                                                                     // 43
		label: 'Document Number'                                                                                          // 44
	},                                                                                                                 //
	documentDescription: {                                                                                             // 46
		type: String,                                                                                                     // 47
		label: 'Description'                                                                                              // 48
	},                                                                                                                 //
	documentType: {                                                                                                    // 50
		type: String,                                                                                                     // 51
		autoform: {                                                                                                       // 52
			options: {                                                                                                       // 53
				loan: "Loan",                                                                                                   // 54
				property: "Property"                                                                                            // 55
			},                                                                                                               //
			label: 'Document Type'                                                                                           // 57
		}                                                                                                                 //
	},                                                                                                                 //
	issueAuthority: {                                                                                                  // 60
		type: String,                                                                                                     // 61
		label: 'Issuing Authority'                                                                                        // 62
	},                                                                                                                 //
	issueDate: {                                                                                                       // 64
		type: String,                                                                                                     // 65
		label: 'Issue Date'                                                                                               // 66
	},                                                                                                                 //
	expiryDate: {                                                                                                      // 68
		type: String,                                                                                                     // 69
		label: 'Expiry Date'                                                                                              // 70
	}                                                                                                                  //
});                                                                                                                 //
ApartmentDocuments.attachSchema(Schema.ApartmentDocuments);                                                         // 73
                                                                                                                    //
Schema.ApartmentPayment = new SimpleSchema({                                                                        // 75
	apartmentId: {                                                                                                     // 76
		type: String,                                                                                                     // 77
		autoform: {                                                                                                       // 78
			type: "hidden",                                                                                                  // 79
			label: false                                                                                                     // 80
		}                                                                                                                 //
	},                                                                                                                 //
	paymentNumber: {                                                                                                   // 83
		type: String,                                                                                                     // 84
		label: 'Ref. Number'                                                                                              // 85
	},                                                                                                                 //
	paymentAmount: {                                                                                                   // 87
		type: Number,                                                                                                     // 88
		decimal: true,                                                                                                    // 89
		label: 'Amount'                                                                                                   // 90
	},                                                                                                                 //
	paymentType: {                                                                                                     // 92
		type: String,                                                                                                     // 93
		autoform: {                                                                                                       // 94
			options: {                                                                                                       // 95
				Loan: "Loan",                                                                                                   // 96
				Cheque: "Cheque",                                                                                               // 97
				Cash: "Cash",                                                                                                   // 98
				BankersCheque: "Banker's Cheque",                                                                               // 99
				ElectronicTransfer: "Electronic Transfer"                                                                       // 100
			},                                                                                                               //
			label: 'Payment Type'                                                                                            // 102
		}                                                                                                                 //
	},                                                                                                                 //
	paymentDescription: {                                                                                              // 105
		type: String,                                                                                                     // 106
		label: 'Description'                                                                                              // 107
	},                                                                                                                 //
	paymentStatus: {                                                                                                   // 109
		type: String,                                                                                                     // 110
		autoform: {                                                                                                       // 111
			options: {                                                                                                       // 112
				Pending: "Pending",                                                                                             // 113
				Rejected: "Rejected",                                                                                           // 114
				Completed: "Completed",                                                                                         // 115
				Processing: "Processing"                                                                                        // 116
			},                                                                                                               //
			label: 'Payment Status'                                                                                          // 118
		}                                                                                                                 //
	},                                                                                                                 //
	paymentDate: {                                                                                                     // 121
		type: String,                                                                                                     // 122
		label: 'Payment Date'                                                                                             // 123
	}                                                                                                                  //
});                                                                                                                 //
ApartmentPayment.attachSchema(Schema.ApartmentPayment);                                                             // 126
Schema.ApartmentServiceRequests = new SimpleSchema({                                                                // 127
	apartmentId: {                                                                                                     // 128
		type: String,                                                                                                     // 129
		autoform: {                                                                                                       // 130
			type: "hidden",                                                                                                  // 131
			label: false                                                                                                     // 132
		}                                                                                                                 //
	},                                                                                                                 //
	requestType: {                                                                                                     // 135
		type: String,                                                                                                     // 136
		autoform: {                                                                                                       // 137
			options: {                                                                                                       // 138
				utilities: "Utilities",                                                                                         // 139
				lockedOut: "Locked Out",                                                                                        // 140
				others: "Others"                                                                                                // 141
			},                                                                                                               //
			label: 'Type'                                                                                                    // 143
		}                                                                                                                 //
	},                                                                                                                 //
	requestDescription: {                                                                                              // 146
		type: String,                                                                                                     // 147
		label: 'Description',                                                                                             // 148
		autoform: {                                                                                                       // 149
			rows: '3'                                                                                                        // 150
		}                                                                                                                 //
	},                                                                                                                 //
	requestContactNumber: {                                                                                            // 153
		type: String,                                                                                                     // 154
		label: 'Phone No.'                                                                                                // 155
	},                                                                                                                 //
	requestStatus: {                                                                                                   // 157
		type: String,                                                                                                     // 158
		autoform: {                                                                                                       // 159
			options: {                                                                                                       // 160
				Pending: "Pending",                                                                                             // 161
				Rejected: "Rejected",                                                                                           // 162
				Completed: "Completed",                                                                                         // 163
				Processing: "In Progress"                                                                                       // 164
			},                                                                                                               //
			label: 'Status'                                                                                                  // 166
		}                                                                                                                 //
	},                                                                                                                 //
	requestResolvedDate: {                                                                                             // 169
		type: String,                                                                                                     // 170
		label: 'Resolved On',                                                                                             // 171
		optional: true                                                                                                    // 172
	}                                                                                                                  //
                                                                                                                    //
});                                                                                                                 //
ApartmentServiceRequests.attachSchema(Schema.ApartmentServiceRequests);                                             // 176
                                                                                                                    //
TabularTables.ApartmentDocuments = new Tabular.Table({                                                              // 178
	name: "ApartmentDocuments",                                                                                        // 179
	collection: ApartmentDocuments,                                                                                    // 180
	columns: [{ data: "documentNumber", title: "Number" }, { data: "documentDescription", title: "Description" }, { data: "documentType", title: "Type" }, { data: "issueAuthority", title: "Issued By" }, { data: "issueDate", title: "Issued On" }, { data: "expiryDate", title: "Expiry On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentDocumentsSection                                                  // 189
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 192
});                                                                                                                 //
                                                                                                                    //
TabularTables.Client_ApartmentDocuments = new Tabular.Table({                                                       // 195
	name: "ApartmentDocuments",                                                                                        // 196
	collection: ApartmentDocuments,                                                                                    // 197
	columns: [{ data: "documentNumber", title: "Number" }, { data: "documentDescription", title: "Description" }, { data: "documentType", title: "Type" }, { data: "issueAuthority", title: "Issued By" }, { data: "issueDate", title: "Issued On" }, { data: "expiryDate", title: "Expiry On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentDocumentsSection                                           // 206
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 209
});                                                                                                                 //
                                                                                                                    //
TabularTables.ApartmentPayment = new Tabular.Table({                                                                // 212
	name: "ApartmentPayment",                                                                                          // 213
	collection: ApartmentPayment,                                                                                      // 214
	columns: [{ data: "paymentNumber", title: "Number" }, { data: "paymentAmount", title: "Amount" }, { data: "paymentType", title: "Type" }, { data: "paymentDescription", title: "Description" }, { data: "paymentStatus", title: "Status" }, { data: "paymentDate", title: "Paid On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentPaymentSection                                                    // 223
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 226
});                                                                                                                 //
                                                                                                                    //
TabularTables.Client_ApartmentPayment = new Tabular.Table({                                                         // 229
	name: "Client_ApartmentPayment",                                                                                   // 230
	collection: ApartmentPayment,                                                                                      // 231
	columns: [{ data: "paymentNumber", title: "Number" }, { data: "paymentAmount", title: "Amount" }, { data: "paymentType", title: "Type" }, { data: "paymentDescription", title: "Description" }, { data: "paymentStatus", title: "Status" }, { data: "paymentDate", title: "Paid On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentPaymentSection                                             // 240
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 243
});                                                                                                                 //
                                                                                                                    //
TabularTables.ApartmentUpdates = new Tabular.Table({                                                                // 246
	name: "ApartmentUpdates",                                                                                          // 247
	collection: ApartmentUpdates,                                                                                      // 248
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentUpdatesSection                                                    // 254
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 257
});                                                                                                                 //
                                                                                                                    //
TabularTables.Client_ApartmentUpdates = new Tabular.Table({                                                         // 260
	name: "Client_ApartmentUpdates",                                                                                   // 261
	collection: ApartmentUpdates,                                                                                      // 262
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentUpdatesSection                                             // 268
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 271
});                                                                                                                 //
                                                                                                                    //
TabularTables.ApartmentServiceRequests = new Tabular.Table({                                                        // 274
	name: "ApartmentServiceRequests",                                                                                  // 275
	collection: ApartmentServiceRequests,                                                                              // 276
	columns: [{ data: "requestType", title: "Type" }, { data: "requestDescription", title: "Description" }, { data: "contactNumber", title: "Updated On" }, { data: "requestStatus", title: "Status" }, { data: "requestResolvedDate", title: "Resolved On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentServiceRequestSection                                             // 284
	}],                                                                                                                //
	extraFields: ['_id', 'apartmentId']                                                                                // 287
});                                                                                                                 //
Meteor.methods({                                                                                                    // 289
	'createApartmentDocument': function () {                                                                           // 290
		function createApartmentDocument(apartmentDocument) {                                                             // 290
			var currentUserId = Meteor.userId();                                                                             // 291
			if (currentUserId && apartmentDocument.apartmentId != null) {                                                    // 292
				apartmentDocument.createdOn = new Date();                                                                       // 293
				ApartmentDocuments.insert(apartmentDocument, function (err, data) {});                                          // 294
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentDocument;                                                                                   //
	}(),                                                                                                               //
	'editApartmentDocument': function () {                                                                             // 298
		function editApartmentDocument(apartmentDocument) {                                                               // 298
			var currentUserId = Meteor.userId();                                                                             // 299
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 300
			if (currentUserId) {                                                                                             // 301
				ApartmentDocuments.update({ _id: apartmentDocument._id }, apartmentDocument.modifier, function (err, data) {});
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editApartmentDocument;                                                                                     //
	}(),                                                                                                               //
	'getApartmentDocumentByApartmentDocumentId': function () {                                                         // 306
		function getApartmentDocumentByApartmentDocumentId(apartmentDocumentId, refreshFlag) {                            // 306
			return ApartmentDocuments.findOne({ _id: apartmentDocumentId });                                                 // 307
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentDocumentByApartmentDocumentId;                                                                 //
	}(),                                                                                                               //
	'createApartmentDocumentFile': function () {                                                                       // 309
		function createApartmentDocumentFile(apartmentDocumentFile) {                                                     // 309
			var currentUserId = Meteor.userId();                                                                             // 310
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 311
			if (currentUserId) {                                                                                             // 312
				if (apartmentDocumentFile.apartmentId != null || apartmentDocumentFile.apartmentDocumentId != null) {           // 313
					apartmentDocumentFile.createdOn = new Date();                                                                  // 314
					ApartmentDocumentFiles.insert(apartmentDocumentFile, function (err, data) {});                                 // 315
				} else {                                                                                                        //
					throw new Meteor.Error(500, 'Apartment/Document Not selected');                                                // 319
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentDocumentFile;                                                                               //
	}(),                                                                                                               //
	'getApartmentDocumentsFilesByDocumentId': function () {                                                            // 324
		function getApartmentDocumentsFilesByDocumentId(apartmentDocumentId, refreshFlag) {                               // 324
			if (apartmentDocumentId != null) {                                                                               // 325
				return ApartmentDocumentFiles.find({ apartmentDocumentId: apartmentDocumentId }).fetch();                       // 326
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentDocumentsFilesByDocumentId;                                                                    //
	}(),                                                                                                               //
	'removeApartmentDocumentFile': function () {                                                                       // 329
		function removeApartmentDocumentFile(id) {                                                                        // 329
			ApartmentDocumentFiles.remove({ _id: id });                                                                      // 330
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentDocumentFile;                                                                               //
	}(),                                                                                                               //
                                                                                                                    //
	// Apartment Update Methods                                                                                        //
	'createApartmentUpdate': function () {                                                                             // 334
		function createApartmentUpdate(apartmentUpdate) {                                                                 // 334
			var currentUserId = Meteor.userId();                                                                             // 335
			if (currentUserId) {                                                                                             // 336
				apartmentUpdate.createdOn = new Date();                                                                         // 337
				ApartmentUpdates.insert(apartmentUpdate, function (err, data) {});                                              // 338
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentUpdate;                                                                                     //
	}(),                                                                                                               //
	'editApartmentUpdate': function () {                                                                               // 342
		function editApartmentUpdate(apartmentUpdate) {                                                                   // 342
			var currentUserId = Meteor.userId();                                                                             // 343
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 344
			if (currentUserId) {                                                                                             // 345
				ApartmentUpdates.update({ _id: apartmentUpdate._id }, apartmentUpdate.modifier, function (err, data) {});       // 346
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editApartmentUpdate;                                                                                       //
	}(),                                                                                                               //
	'removeApartmentUpdate': function () {                                                                             // 350
		function removeApartmentUpdate(id) {                                                                              // 350
			if (id != null) {                                                                                                // 351
				ApartmentUpdates.remove({ _id: id });                                                                           // 352
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentUpdate;                                                                                     //
	}(),                                                                                                               //
	'getApartmentUpdateByApartmentUpdateId': function () {                                                             // 356
		function getApartmentUpdateByApartmentUpdateId(apartmentUpdateId, refreshFlag) {                                  // 356
			console.log("Finding" + apartmentUpdateId);                                                                      // 357
			console.log(ApartmentUpdates.findOne({ _id: apartmentUpdateId }));                                               // 358
			return ApartmentUpdates.findOne({ _id: apartmentUpdateId });                                                     // 359
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentUpdateByApartmentUpdateId;                                                                     //
	}(),                                                                                                               //
	'createApartmentUpdateImage': function () {                                                                        // 361
		function createApartmentUpdateImage(apartmentUpdateImage) {                                                       // 361
			var currentUserId = Meteor.userId();                                                                             // 362
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 363
			if (currentUserId) {                                                                                             // 364
				if (apartmentUpdateImage.apartmentId != null || apartmentUpdateImage.apartmentUpdateId != null) {               // 365
					apartmentUpdateImage.createdOn = new Date();                                                                   // 366
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function (err, data) {});                                    // 367
				} else {                                                                                                        //
					throw new Meteor.Error(500, 'Project/Update Not selected');                                                    // 371
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentUpdateImage;                                                                                //
	}(),                                                                                                               //
	'getApartmentUpdateImageByUpdateId': function () {                                                                 // 376
		function getApartmentUpdateImageByUpdateId(apartmentUpdateId, refreshFlag) {                                      // 376
			if (apartmentUpdateId != null) {                                                                                 // 377
				console.log("Fetching Update Files");                                                                           // 378
				return ApartmentUpdateFiles.find({ apartmentUpdateId: apartmentUpdateId }).fetch();                             // 379
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentUpdateImageByUpdateId;                                                                         //
	}(),                                                                                                               //
	'removeApartmentUpdateImage': function () {                                                                        // 382
		function removeApartmentUpdateImage(id) {                                                                         // 382
			ApartmentUpdateFiles.remove({ _id: id });                                                                        // 383
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentUpdateImage;                                                                                //
	}(),                                                                                                               //
                                                                                                                    //
	//End Apartment Update                                                                                             //
                                                                                                                    //
	//Apartment Payment Section                                                                                        //
	'createApartmentPayment': function () {                                                                            // 389
		function createApartmentPayment(apartmentPayment) {                                                               // 389
			var currentUserId = Meteor.userId();                                                                             // 390
			if (currentUserId) {                                                                                             // 391
				apartmentPayment.createdOn = new Date();                                                                        // 392
				ApartmentPayment.insert(apartmentPayment, function (err, data) {});                                             // 393
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentPayment;                                                                                    //
	}(),                                                                                                               //
	'editApartmentPayment': function () {                                                                              // 397
		function editApartmentPayment(apartmentPayment) {                                                                 // 397
			var currentUserId = Meteor.userId();                                                                             // 398
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 399
			if (currentUserId) {                                                                                             // 400
				ApartmentPayment.update({ _id: apartmentPayment._id }, apartmentPayment.modifier, function (err, data) {});     // 401
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editApartmentPayment;                                                                                      //
	}(),                                                                                                               //
	'removeApartmentPayment': function () {                                                                            // 405
		function removeApartmentPayment(id) {                                                                             // 405
			if (id != null) {                                                                                                // 406
				ApartmentPayment.remove({ _id: id });                                                                           // 407
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentPayment;                                                                                    //
	}(),                                                                                                               //
	'getApartmentPaymentByApartmentPaymentId': function () {                                                           // 411
		function getApartmentPaymentByApartmentPaymentId(apartmentPaymentId, refreshFlag) {                               // 411
			return ApartmentPayment.findOne({ _id: apartmentPaymentId });                                                    // 412
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentPaymentByApartmentPaymentId;                                                                   //
	}(),                                                                                                               //
	'createApartmentPaymentFile': function () {                                                                        // 414
		function createApartmentPaymentFile(apartmentPaymentFile) {                                                       // 414
			var currentUserId = Meteor.userId();                                                                             // 415
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 416
			if (currentUserId) {                                                                                             // 417
				if (apartmentPaymentFile.apartmentId != null || apartmentPaymentFile.apartmentPaymentId != null) {              // 418
					apartmentPaymentFile.createdOn = new Date();                                                                   // 419
					ApartmentPaymentFiles.insert(apartmentPaymentFile, function (err, data) {});                                   // 420
				} else {                                                                                                        //
					throw new Meteor.Error(500, 'Project/Payment Not selected');                                                   // 424
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentPaymentFile;                                                                                //
	}(),                                                                                                               //
	'getApartmentPaymentFileByPaymentId': function () {                                                                // 429
		function getApartmentPaymentFileByPaymentId(apartmentPaymentId, refreshFlag) {                                    // 429
			if (apartmentPaymentId != null) {                                                                                // 430
				return ApartmentPaymentFiles.find({ apartmentPaymentId: apartmentPaymentId }).fetch();                          // 431
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentPaymentFileByPaymentId;                                                                        //
	}(),                                                                                                               //
	'removeApartmentPaymentFile': function () {                                                                        // 434
		function removeApartmentPaymentFile(id) {                                                                         // 434
			ApartmentPaymentFiles.remove({ _id: id });                                                                       // 435
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentPaymentFile;                                                                                //
	}(),                                                                                                               //
                                                                                                                    //
	// Apartment Service Request                                                                                       //
                                                                                                                    //
	'createApartmentServiceRequest': function () {                                                                     // 440
		function createApartmentServiceRequest(apartmentServiceRequest) {                                                 // 440
			var currentUserId = Meteor.userId();                                                                             // 441
			if (currentUserId) {                                                                                             // 442
				apartmentServiceRequest.createdOn = new Date();                                                                 // 443
				ApartmentServiceRequests.insert(apartmentServiceRequest, function (err, data) {});                              // 444
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentServiceRequest;                                                                             //
	}(),                                                                                                               //
	'editApartmentServiceRequest': function () {                                                                       // 448
		function editApartmentServiceRequest(apartmentServiceRequest) {                                                   // 448
			var currentUserId = Meteor.userId();                                                                             // 449
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 450
			if (currentUserId) {                                                                                             // 451
				ApartmentServiceRequests.update({ _id: apartmentServiceRequest._id }, apartmentServiceRequest.modifier, function (err, data) {});
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editApartmentServiceRequest;                                                                               //
	}(),                                                                                                               //
	'removeApartmentServiceRequest': function () {                                                                     // 456
		function removeApartmentServiceRequest(id) {                                                                      // 456
			if (id != null) {                                                                                                // 457
				ApartmentServiceRequests.remove({ _id: id });                                                                   // 458
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentServiceRequest;                                                                             //
	}(),                                                                                                               //
	'getApartmentServiceRequestByApartmentServiceRequestId': function () {                                             // 461
		function getApartmentServiceRequestByApartmentServiceRequestId(apartmentServiceRequestId, refreshFlag) {          // 461
			return ApartmentServiceRequests.findOne({ _id: apartmentServiceRequestId });                                     // 462
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentServiceRequestByApartmentServiceRequestId;                                                     //
	}(),                                                                                                               //
                                                                                                                    //
	//Client Apartmemt Update section                                                                                  //
                                                                                                                    //
	'getApartmentUpdateByApartmentUpdateId': function () {                                                             // 468
		function getApartmentUpdateByApartmentUpdateId(apartmentUpdateId) {                                               // 468
			return ApartmentUpdates.findOne({ _id: apartmentUpdateId });                                                     // 469
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentUpdateByApartmentUpdateId;                                                                     //
	}(),                                                                                                               //
	'createApartmentUpdateFile': function () {                                                                         // 471
		function createApartmentUpdateFile(apartmentUpdateImage) {                                                        // 471
			var currentUserId = Meteor.userId();                                                                             // 472
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 473
			if (currentUserId) {                                                                                             // 474
				if (apartmentUpdateImage.apartmentId != null || apartmentUpdateImage.apartmentUpdateId != null) {               // 475
					console.log("Creating a new Apartment Image");                                                                 // 476
					apartmentUpdateImage.createdOn = new Date();                                                                   // 477
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function (err, data) {});                                    // 478
				} else {                                                                                                        //
					throw new Meteor.Error(500, 'Apartment/Update Not selected');                                                  // 482
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartmentUpdateFile;                                                                                 //
	}(),                                                                                                               //
	'getApartmentUpdateImageByUpdateId': function () {                                                                 // 487
		function getApartmentUpdateImageByUpdateId(apartmentUpdateId, refreshFlag) {                                      // 487
			if (apartmentUpdateId != null) {                                                                                 // 488
				return ApartmentUpdateFiles.find({ apartmentUpdateId: apartmentUpdateId }).fetch();                             // 489
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentUpdateImageByUpdateId;                                                                         //
	}(),                                                                                                               //
	'removeApartmentUpdateFile': function () {                                                                         // 492
		function removeApartmentUpdateFile(id) {                                                                          // 492
			ApartmentUpdateFiles.remove({ _id: id });                                                                        // 493
		}                                                                                                                 //
                                                                                                                    //
		return removeApartmentUpdateFile;                                                                                 //
	}()                                                                                                                //
                                                                                                                    //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_apartments.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbo_apartments.js                                                                                            //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
Apartments = new Mongo.Collection('apartment');                                                                     // 1
ApartmentSchemas = {};                                                                                              // 2
ApartmentSchemas.Apartments = new SimpleSchema({                                                                    // 3
	projectId: {                                                                                                       // 4
		type: String,                                                                                                     // 5
		autoform: {                                                                                                       // 6
			options: function () {                                                                                           // 7
				function options() {                                                                                            // 7
					return Projects.find().map(function (c) {                                                                      // 8
						return { label: c.projectName, value: c._id };                                                                // 9
					});                                                                                                            //
				}                                                                                                               //
                                                                                                                    //
				return options;                                                                                                 //
			}()                                                                                                              //
		}                                                                                                                 //
	},                                                                                                                 //
	flatNumber: {                                                                                                      // 14
		type: String,                                                                                                     // 15
		label: 'Flat Number'                                                                                              // 16
	},                                                                                                                 //
	flatBlock: {                                                                                                       // 18
		type: String,                                                                                                     // 19
		label: 'Block'                                                                                                    // 20
	},                                                                                                                 //
	flatArea: {                                                                                                        // 22
		type: String,                                                                                                     // 23
		label: 'Area (Sqft.)'                                                                                             // 24
	},                                                                                                                 //
	flatFloor: {                                                                                                       // 26
		type: Number,                                                                                                     // 27
		label: 'Floor'                                                                                                    // 28
	},                                                                                                                 //
	flatStatusId: {                                                                                                    // 30
		type: Number,                                                                                                     // 31
		label: 'Status'                                                                                                   // 32
	},                                                                                                                 //
	flatDescription: {                                                                                                 // 34
		type: String,                                                                                                     // 35
		label: 'Info'                                                                                                     // 36
	},                                                                                                                 //
	twoWheelerParking: {                                                                                               // 38
		type: Number,                                                                                                     // 39
		label: '2 Wheeler Spots'                                                                                          // 40
	},                                                                                                                 //
	fourWheelerSpots: {                                                                                                // 42
		type: Number,                                                                                                     // 43
		label: '4 Wheeler Spots'                                                                                          // 44
	}                                                                                                                  //
});                                                                                                                 //
Apartments.attachSchema(ApartmentSchemas.Apartments);                                                               // 47
TabularTables = {};                                                                                                 // 48
                                                                                                                    //
TabularTables.Apartments = new Tabular.Table({                                                                      // 50
	name: "Apartments",                                                                                                // 51
	collection: Apartments,                                                                                            // 52
	columns: [{ data: "flatNumber", title: "Number" }, { data: "flatBlock", title: "Block" }, { data: "flatFloor", title: "Floor" }, { data: "flatArea", title: "Area" }, { data: "flatDescription", title: "Info" }, {
		tmpl: Meteor.isClient && Template.editApartmentSection                                                            // 60
	}],                                                                                                                //
	extraFields: ["_id"]                                                                                               // 63
});                                                                                                                 //
Meteor.methods({                                                                                                    // 65
	'createApartment': function () {                                                                                   // 66
		function createApartment(apartment) {                                                                             // 66
			var currentUserId = Meteor.userId();                                                                             // 67
			if (currentUserId) {                                                                                             // 68
				apartment.createdOn = new Date();                                                                               // 69
				Apartments.insert(apartment, function (err, data) {                                                             // 70
					if (data) {                                                                                                    // 71
						console.log("Success Apartment insertion");                                                                   // 72
					}                                                                                                              //
				});                                                                                                             //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createApartment;                                                                                           //
	}(),                                                                                                               //
	'editApartment': function () {                                                                                     // 78
		function editApartment(apartment, val) {                                                                          // 78
			var currentUserId = Meteor.userId();                                                                             // 79
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 80
			console.log(apartment.modifier);                                                                                 // 81
			if (currentUserId) {                                                                                             // 82
				Apartments.update({ _id: apartment._id }, apartment.modifier, function (err, data) {});                         // 83
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editApartment;                                                                                             //
	}(),                                                                                                               //
	'getAllApartments': function () {                                                                                  // 87
		function getAllApartments(refreshFlag) {                                                                          // 87
			return Apartments.find().fetch();                                                                                // 88
		}                                                                                                                 //
                                                                                                                    //
		return getAllApartments;                                                                                          //
	}(),                                                                                                               //
	'getApartmentByProjectId': function () {                                                                           // 90
		function getApartmentByProjectId(projectId, refreshFlag) {                                                        // 90
			return Apartments.find({ projectId: projectId }).fetch();                                                        // 91
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentByProjectId;                                                                                   //
	}(),                                                                                                               //
	'getApartmentByApartmentId': function () {                                                                         // 93
		function getApartmentByApartmentId(apartmentId, refreshFlag) {                                                    // 93
			return Apartments.findOne({ _id: apartmentId });                                                                 // 94
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentByApartmentId;                                                                                 //
	}(),                                                                                                               //
	'removeApartment': function () {                                                                                   // 97
		function removeApartment(apartmentId) {                                                                           // 97
			return Apartments.remove(apartmentId);                                                                           // 98
		}                                                                                                                 //
                                                                                                                    //
		return removeApartment;                                                                                           //
	}(),                                                                                                               //
	'addAssignment': function () {                                                                                     // 100
		function addAssignment(apartmentId, userId) {                                                                     // 100
			UserApartment.insert({                                                                                           // 101
				userId: userId,                                                                                                 // 102
				apartmentId: apartmentId,                                                                                       // 103
				assignmentStartDate: new Date()                                                                                 // 104
			});                                                                                                              //
		}                                                                                                                 //
                                                                                                                    //
		return addAssignment;                                                                                             //
	}(),                                                                                                               //
	'removeAssignment': function () {                                                                                  // 107
		function removeAssignment(apartmentAssignementId) {                                                               // 107
			UserApartment.remove(apartmentAssignementId);                                                                    // 108
		}                                                                                                                 //
                                                                                                                    //
		return removeAssignment;                                                                                          //
	}()                                                                                                                //
                                                                                                                    //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_customerManagement.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbo_customerManagement.js                                                                                    //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
                                                                                                                    //
Meteor.methods({                                                                                                    // 2
	'getProjectIdByUserId': function () {                                                                              // 3
		function getProjectIdByUserId(userId) {                                                                           // 3
			console.log("Apt Id in Project Updates for user" + userId);                                                      // 4
			userApartmentMap = UserApartment.findOne({ userId: userId });                                                    // 5
			console.log(userApartmentMap);                                                                                   // 6
			apartmentId = userApartmentMap['apartmentId'];                                                                   // 7
			console.log(apartmentId);                                                                                        // 8
			apartment = Apartments.findOne({ _id: apartmentId });                                                            // 9
			console.log(apartment);                                                                                          // 10
			projectId = apartment['projectId'];                                                                              // 11
			console.log("projectId is " + projectId);                                                                        // 12
			return projectId;                                                                                                // 13
		}                                                                                                                 //
                                                                                                                    //
		return getProjectIdByUserId;                                                                                      //
	}(),                                                                                                               //
	'getApartmentIdByUserId': function () {                                                                            // 15
		function getApartmentIdByUserId(userId) {                                                                         // 15
			console.log("Findin out " + userId);                                                                             // 16
			console.log("Apt Id for user" + userId);                                                                         // 17
			userApartmentMap = UserApartment.findOne({ userId: userId });                                                    // 18
			console.log(userApartmentMap);                                                                                   // 19
			apartmentId = userApartmentMap['apartmentId'];                                                                   // 20
			return apartmentId;                                                                                              // 21
		}                                                                                                                 //
                                                                                                                    //
		return getApartmentIdByUserId;                                                                                    //
	}()                                                                                                                //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_projectUpdates.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbo_projectUpdates.js                                                                                        //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
ProjectUpdates = new Mongo.Collection('project_update');                                                            // 1
ProjectUpdateImages = new Mongo.Collection('product_update_image');                                                 // 2
Schema = {};                                                                                                        // 3
                                                                                                                    //
Schema.ProjectUpdates = new SimpleSchema({                                                                          // 5
	projectId: {                                                                                                       // 6
		type: String,                                                                                                     // 7
		autoform: {                                                                                                       // 8
			type: "hidden",                                                                                                  // 9
			label: false                                                                                                     // 10
		}                                                                                                                 //
	},                                                                                                                 //
	updateType: {                                                                                                      // 13
		type: String,                                                                                                     // 14
		autoform: {                                                                                                       // 15
			options: {                                                                                                       // 16
				recreation: "Recreation",                                                                                       // 17
				building: "Building",                                                                                           // 18
				amenities: "Amenities"                                                                                          // 19
			}                                                                                                                //
		}                                                                                                                 //
	},                                                                                                                 //
	updateDescription: {                                                                                               // 23
		type: String                                                                                                      // 24
	}                                                                                                                  //
});                                                                                                                 //
ProjectUpdates.attachSchema(Schema.ProjectUpdates);                                                                 // 27
                                                                                                                    //
TabularTables.ProjectUpdates = new Tabular.Table({                                                                  // 29
	name: "ProjectUpdates",                                                                                            // 30
	collection: ProjectUpdates,                                                                                        // 31
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TeditProjectUpdatesSection                                                      // 37
	}],                                                                                                                //
	extraFields: ['_id', 'projectId']                                                                                  // 40
});                                                                                                                 //
                                                                                                                    //
TabularTables.Client_ProjectUpdates = new Tabular.Table({                                                           // 43
	name: "Client_ProjectUpdates",                                                                                     // 44
	collection: ProjectUpdates,                                                                                        // 45
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewProjectUpdatesSection                                               // 51
	}],                                                                                                                //
	extraFields: ['_id', 'projectId']                                                                                  // 54
});                                                                                                                 //
                                                                                                                    //
Meteor.methods({                                                                                                    // 57
	'createProjectUpdate': function () {                                                                               // 58
		function createProjectUpdate(projectUpdate) {                                                                     // 58
			var currentUserId = Meteor.userId();                                                                             // 59
			if (currentUserId) {                                                                                             // 60
				projectUpdate.createdOn = new Date();                                                                           // 61
				ProjectUpdates.insert(projectUpdate, function (err, data) {});                                                  // 62
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createProjectUpdate;                                                                                       //
	}(),                                                                                                               //
	'editProjectUpdate': function () {                                                                                 // 66
		function editProjectUpdate(projectUpdate) {                                                                       // 66
			var currentUserId = Meteor.userId();                                                                             // 67
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 68
			if (currentUserId) {                                                                                             // 69
				ProjectUpdates.update({ _id: projectUpdate._id }, projectUpdate.modifier, function (err, data) {});             // 70
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editProjectUpdate;                                                                                         //
	}(),                                                                                                               //
	'getProjectUpdateByProjectUpdateId': function () {                                                                 // 74
		function getProjectUpdateByProjectUpdateId(projectUpdateId) {                                                     // 74
			return ProjectUpdates.findOne({ _id: projectUpdateId });                                                         // 75
		}                                                                                                                 //
                                                                                                                    //
		return getProjectUpdateByProjectUpdateId;                                                                         //
	}(),                                                                                                               //
	'createProjectUpdateImage': function () {                                                                          // 77
		function createProjectUpdateImage(projectUpdateImage) {                                                           // 77
			var currentUserId = Meteor.userId();                                                                             // 78
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 79
			if (currentUserId) {                                                                                             // 80
				if (projectUpdateImage.projectId != null || projectUpdateImage.projectUpdateId != null) {                       // 81
					projectUpdateImage.createdOn = new Date();                                                                     // 82
					ProjectUpdateImages.insert(projectUpdateImage, function (err, data) {});                                       // 83
				} else {                                                                                                        //
					throw new Meteor.Error(500, 'Project/Update Not selected');                                                    // 87
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return createProjectUpdateImage;                                                                                  //
	}(),                                                                                                               //
	'getProjectUpdateImageByUpdateId': function () {                                                                   // 92
		function getProjectUpdateImageByUpdateId(projectUpdateId, refreshFlag) {                                          // 92
			//return [{'url':'google.com'},{'url':'faceobooj.com'}];                                                         //
			if (projectUpdateId != null) {                                                                                   // 94
				return ProjectUpdateImages.find({ projectUpdateId: projectUpdateId }).fetch();                                  // 95
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return getProjectUpdateImageByUpdateId;                                                                           //
	}(),                                                                                                               //
	'removeProjectUpdateImage': function () {                                                                          // 98
		function removeProjectUpdateImage(id) {                                                                           // 98
			ProjectUpdateImages.remove({ _id: id });                                                                         // 99
		}                                                                                                                 //
                                                                                                                    //
		return removeProjectUpdateImage;                                                                                  //
	}()                                                                                                                //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_users.js":function(require){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// lib/dbo_users.js                                                                                                 //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
UserApartment = new Mongo.Collection('user_apartment');                                                             // 1
Schema = {};                                                                                                        // 2
                                                                                                                    //
Schema.UserProfile = new SimpleSchema({                                                                             // 4
	firstName: {                                                                                                       // 5
		type: String                                                                                                      // 6
	},                                                                                                                 //
	lastName: {                                                                                                        // 8
		type: String                                                                                                      // 9
	},                                                                                                                 //
	dataOfBirth: {                                                                                                     // 11
		type: Date                                                                                                        // 12
	},                                                                                                                 //
	sex: {                                                                                                             // 14
		type: String,                                                                                                     // 15
		autoform: {                                                                                                       // 16
			options: {                                                                                                       // 17
				male: "Male",                                                                                                   // 18
				female: "Female"                                                                                                // 19
			}                                                                                                                //
		}                                                                                                                 //
	},                                                                                                                 //
	photoUrl: {                                                                                                        // 23
		type: String                                                                                                      // 24
	},                                                                                                                 //
	userStatus: {                                                                                                      // 26
		type: Number                                                                                                      // 27
	},                                                                                                                 //
	primaryPhone: {                                                                                                    // 29
		type: Number                                                                                                      // 30
	},                                                                                                                 //
	secondaryPhone: {                                                                                                  // 32
		type: Number                                                                                                      // 33
	},                                                                                                                 //
	permanentAddress: {                                                                                                // 35
		type: String                                                                                                      // 36
	}                                                                                                                  //
});                                                                                                                 //
                                                                                                                    //
Schema.User = new SimpleSchema({                                                                                    // 40
	emails: {                                                                                                          // 41
		type: Array,                                                                                                      // 42
		// For accounts-password, either emails or username is required, but not both. It is OK to make this              //
		// optional here because the accounts-password package does its own validation.                                   //
		// Third-party login packages may not require either. Adjust this schema as necessary for your usage.             //
		optional: true                                                                                                    // 46
	},                                                                                                                 //
	"emails.$": {                                                                                                      // 48
		type: Object                                                                                                      // 49
	},                                                                                                                 //
	"emails.$.address": {                                                                                              // 51
		type: String,                                                                                                     // 52
		regEx: SimpleSchema.RegEx.Email                                                                                   // 53
	},                                                                                                                 //
	"emails.$.verified": {                                                                                             // 55
		type: Boolean                                                                                                     // 56
	},                                                                                                                 //
	services: {                                                                                                        // 58
		type: Object,                                                                                                     // 59
		optional: true,                                                                                                   // 60
		blackbox: true,                                                                                                   // 61
		autoform: {                                                                                                       // 62
			type: "hidden",                                                                                                  // 63
			label: false                                                                                                     // 64
		}                                                                                                                 //
	},                                                                                                                 //
                                                                                                                    //
	profile: {                                                                                                         // 68
		type: Schema.UserProfile,                                                                                         // 69
		optional: true                                                                                                    // 70
	}                                                                                                                  //
});                                                                                                                 //
                                                                                                                    //
Meteor.users.attachSchema(Schema.User);                                                                             // 74
                                                                                                                    //
TabularTables.Users = new Tabular.Table({                                                                           // 77
	name: "Users",                                                                                                     // 78
	collection: Meteor.users,                                                                                          // 79
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editUserSection                                                                 // 86
	}],                                                                                                                //
	extraFields: ["_id"]                                                                                               // 89
});                                                                                                                 //
                                                                                                                    //
TabularTables.UserList = new Tabular.Table({                                                                        // 93
	name: "UserList",                                                                                                  // 94
	collection: Meteor.users,                                                                                          // 95
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.assignUser                                                                      // 102
	}],                                                                                                                //
	extraFields: ["_id"]                                                                                               // 105
});                                                                                                                 //
                                                                                                                    //
TabularTables.AssignedUserList = new Tabular.Table({                                                                // 108
	name: "AssignedUserList",                                                                                          // 109
	collection: Meteor.users,                                                                                          // 110
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.removeUserAssignSection                                                         // 117
	}],                                                                                                                //
	extraFields: ["_id"]                                                                                               // 120
});                                                                                                                 //
                                                                                                                    //
Meteor.methods({                                                                                                    // 125
	'addUser': function () {                                                                                           // 126
		function addUser(user) {                                                                                          // 126
			var currentUserId = Meteor.userId();                                                                             // 127
			user.password = "shiva";                                                                                         // 128
			console.log(user);                                                                                               // 129
			user.email = user.emails[0].address;                                                                             // 130
			if (currentUserId) {                                                                                             // 131
				Accounts.createUser(user);                                                                                      // 132
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return addUser;                                                                                                   //
	}(),                                                                                                               //
	'editUser': function () {                                                                                          // 136
		function editUser(user, val) {                                                                                    // 136
			var currentUserId = Meteor.userId();                                                                             // 137
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                      // 138
			user.email = user.emails[0].address;                                                                             // 139
			if (currentUserId) {                                                                                             // 140
				Meteor.users.update({ _id: user._id }, user.modifier, function (err, data) {});                                 // 141
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return editUser;                                                                                                  //
	}(),                                                                                                               //
	'getAllUsers': function () {                                                                                       // 145
		function getAllUsers(refreshFlag) {                                                                               // 145
			return Meteor.users.find().fetch();                                                                              // 146
		}                                                                                                                 //
                                                                                                                    //
		return getAllUsers;                                                                                               //
	}(),                                                                                                               //
	'getUserByUserId': function () {                                                                                   // 148
		function getUserByUserId(userId, refreshFlag) {                                                                   // 148
			return Meteor.users.findOne({ _id: userId });                                                                    // 149
		}                                                                                                                 //
                                                                                                                    //
		return getUserByUserId;                                                                                           //
	}(),                                                                                                               //
	'removeUser': function () {                                                                                        // 152
		function removeUser(userId) {                                                                                     // 152
			return Meteor.users.remove(userId);                                                                              // 153
		}                                                                                                                 //
                                                                                                                    //
		return removeUser;                                                                                                //
	}(),                                                                                                               //
	'getUsersByApartmentId': function () {                                                                             // 155
		function getUsersByApartmentId(apartmentId, refreshFlag) {                                                        // 155
			u = UserApartment.find({ "apartmentId": apartmentId });                                                          // 156
			idlist = [];                                                                                                     // 157
			u.forEach(function (myDoc) {                                                                                     // 158
				idlist.push(myDoc.userId);                                                                                      // 159
			});                                                                                                              //
			return idlist;                                                                                                   // 161
			//console.log(Meteor.users.find({"_id": {$in : idlist} }).fetch());                                              //
			//return Meteor.users.find({"_id": {$in : idlist} });                                                            //
		}                                                                                                                 // 155
                                                                                                                    //
		return getUsersByApartmentId;                                                                                     //
	}(),                                                                                                               //
	'addUserApartment': function () {                                                                                  // 165
		function addUserApartment(userId, apartmentId) {                                                                  // 165
			if (userId != null && apartmentId != null) {                                                                     // 166
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                         // 167
				if (assignmentRecord == null || assignmentRecord.count() == 0) {                                                // 168
					UserApartment.insert({                                                                                         // 169
						userId: userId,                                                                                               // 170
						apartmentId: apartmentId,                                                                                     // 171
						assignmentStartDate: new Date()                                                                               // 172
					});                                                                                                            //
				}                                                                                                               //
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return addUserApartment;                                                                                          //
	}(),                                                                                                               //
	'removeUserAssignment': function () {                                                                              // 178
		function removeUserAssignment(userId, apartmentId) {                                                              // 178
			if (userId != null && apartmentId != null) {                                                                     // 179
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                         // 180
				UserApartment.remove(assignmentRecord);                                                                         // 181
			}                                                                                                                //
		}                                                                                                                 //
                                                                                                                    //
		return removeUserAssignment;                                                                                      //
	}()                                                                                                                //
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"server":{"ServerConfig.js":function(){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// server/ServerConfig.js                                                                                           //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
Cloudinary.config({                                                                                                 // 1
  cloud_name: 'aparto',                                                                                             // 2
  api_key: '572689295312982',                                                                                       // 3
  api_secret: 'rejB7jCjCzi9r_IkK8QHeUV2Ijg'                                                                         // 4
});                                                                                                                 //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["meteor/meteor","meteor/templating","meteor/reactive-var",function(require){

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                  //
// server/main.js                                                                                                   //
//                                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                    //
var _meteor = require('meteor/meteor');                                                                             // 1
                                                                                                                    //
var _templating = require('meteor/templating');                                                                     // 2
                                                                                                                    //
var _reactiveVar = require('meteor/reactive-var');                                                                  // 3
                                                                                                                    //
//Meteor.startup(() => {                                                                                            //
//Projects =  new Mongo.Collection('project');                                                                      //
/**	var Schemas={};                                                                                                 //
Schemas.Projects = new SimpleSchema({                                                                               //
			projectName:{                                                                                                    //
				type:String,                                                                                                    //
				label:'Project Name'                                                                                            //
			},                                                                                                               //
			projectDescription:{                                                                                             //
				type:String,                                                                                                    //
				label:'Project Description'                                                                                     //
			},                                                                                                               //
			projectAddress:{                                                                                                 //
				type:String,                                                                                                    //
				label:'Project Address'                                                                                         //
			},                                                                                                               //
			projectAdditionalInfo:{                                                                                          //
				type:String,                                                                                                    //
				label:'Project Info'                                                                                            //
			}                                                                                                                //
		});                                                                                                               //
		Projects.attachSchema(Schemas.Projects); **/                                                                      //
console.log("attached schema");                                                                                     // 28
// code to run on server at startup                                                                                 //
                                                                                                                    //
//  });                                                                                                             //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]}},{"extensions":[".js",".json"]});
require("./lib/dbOperations.js");
require("./lib/dbo_apartmentManagement.js");
require("./lib/dbo_apartments.js");
require("./lib/dbo_customerManagement.js");
require("./lib/dbo_projectUpdates.js");
require("./lib/dbo_users.js");
require("./server/ServerConfig.js");
require("./server/main.js");
//# sourceMappingURL=app.js.map

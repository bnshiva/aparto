var require = meteorInstall({"client":{"admin":{"adminTemplate.html":["./template.adminTemplate.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/adminTemplate.html                                                                                     //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.adminTemplate.js");                                                               // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.adminTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/template.adminTemplate.js                                                                              //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("admin_section");                                                                                 // 2
Template["admin_section"] = new Template("Template.admin_section", (function() {                                       // 3
  var view = this;                                                                                                     // 4
  return HTML.DIV("\n		", HTML.DIV({                                                                                   // 5
    "class": "row"                                                                                                     // 6
  }, "\n			", HTML.DIV({                                                                                               // 7
    "class": "jumbotron container"                                                                                     // 8
  }, "			\n			", HTML.DIV({                                                                                            // 9
    "class": "text-center"                                                                                             // 10
  }, "\n				", HTML.DIV({                                                                                              // 11
    role: "tabpanel"                                                                                                   // 12
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="project-tab" data-target="#project" data-toggle="tab">Project Management</a></li>\n						<li role="presentation"><a id="apartment-tab" data-target="#apartment" data-toggle="tab">Apartment Management</a></li>\n						<li role="presentation"><a id="user-tab" data-target="#user" data-toggle="tab">User Management</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                             // 14
  }, "					\n						", HTML.DIV({                                                                                       // 15
    role: "tabpanel",                                                                                                  // 16
    "class": "tab-pane active text-center",                                                                            // 17
    id: "project"                                                                                                      // 18
  }, "\n							", HTML.DIV({                                                                                           // 19
    "class": "row"                                                                                                     // 20
  }, "\n								", HTML.DIV({                                                                                          // 21
    "class": "col-md-12"                                                                                               // 22
  }, "\n									", Spacebars.include(view.lookupTemplate("projectForm")), "\n									", Spacebars.include(view.lookupTemplate("editProjectForm")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row "                                                                                                    // 24
  }, "\n								", HTML.DIV({                                                                                          // 25
    "class": "col-md-12"                                                                                               // 26
  }, "\n									", HTML.DIV({                                                                                         // 27
    "class": "table-responsive"                                                                                        // 28
  }, "\n										", Spacebars.include(view.lookupTemplate("projectList")), "\n									"), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row "                                                                                                    // 30
  }, "\n								", HTML.DIV({                                                                                          // 31
    "class": "col-md-12"                                                                                               // 32
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectManagement")), " \n								"), "\n							"), "\n														\n						"), "\n						", HTML.DIV({
    role: "tabpanel",                                                                                                  // 34
    "class": "tab-pane text-center",                                                                                   // 35
    id: "apartment"                                                                                                    // 36
  }, "\n							", HTML.DIV({                                                                                           // 37
    "class": "row"                                                                                                     // 38
  }, "\n								", HTML.DIV({                                                                                          // 39
    "class": "col-md-12"                                                                                               // 40
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentManagementMain")), "						\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                     // 42
  }, "\n								", HTML.DIV({                                                                                          // 43
    "class": "col-md-12"                                                                                               // 44
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentForm")), "	\n								", Spacebars.include(view.lookupTemplate("editApartmentForm")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                     // 46
  }, "\n								", HTML.DIV({                                                                                          // 47
    "class": "col-md-12"                                                                                               // 48
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentList")), "						\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                     // 50
  }, "\n								", HTML.DIV({                                                                                          // 51
    "class": "col-md-12"                                                                                               // 52
  }, "\n								", Spacebars.include(view.lookupTemplate("apartmentManagement")), "						\n								"), "\n							"), "	\n						"), "\n						", HTML.DIV({
    role: "tabpanel",                                                                                                  // 54
    "class": "tab-pane text-center",                                                                                   // 55
    id: "user"                                                                                                         // 56
  }, "\n							", HTML.DIV({                                                                                           // 57
    "class": "row"                                                                                                     // 58
  }, "\n								", HTML.DIV({                                                                                          // 59
    "class": "col-md-12"                                                                                               // 60
  }, "\n								", Spacebars.include(view.lookupTemplate("userForm")), "	\n								", Spacebars.include(view.lookupTemplate("editUserForm")), "\n								"), "\n								", HTML.DIV({
    "class": "row"                                                                                                     // 62
  }, "\n								", HTML.DIV({                                                                                          // 63
    "class": "col-md-12"                                                                                               // 64
  }, "\n								", Spacebars.include(view.lookupTemplate("userList")), "						\n								"), "\n							"), "\n							"), "	\n							"), "\n\n							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		"), "\n	");
}));                                                                                                                   // 66
                                                                                                                       // 67
Template.__checkName("apartmentManagementMain");                                                                       // 68
Template["apartmentManagementMain"] = new Template("Template.apartmentManagementMain", (function() {                   // 69
  var view = this;                                                                                                     // 70
  return Spacebars.include(view.lookupTemplate("projectListOptions"));                                                 // 71
}));                                                                                                                   // 72
                                                                                                                       // 73
Template.__checkName("projectListOptions");                                                                            // 74
Template["projectListOptions"] = new Template("Template.projectListOptions", (function() {                             // 75
  var view = this;                                                                                                     // 76
  return HTML.SELECT({                                                                                                 // 77
    id: "projectNameDropDown",                                                                                         // 78
    "class": "projectNameDropDown"                                                                                     // 79
  }, HTML.Raw('\n		<option disabled="disabled" selected="selected">Please Select</option> \n		'), Blaze.Each(function() {
    return Spacebars.call(view.lookup("projects"));                                                                    // 81
  }, function() {                                                                                                      // 82
    return [ "\n			", HTML.OPTION({                                                                                    // 83
      value: function() {                                                                                              // 84
        return Spacebars.mustache(view.lookup("_id"));                                                                 // 85
      }                                                                                                                // 86
    }, Blaze.View("lookup:projectName", function() {                                                                   // 87
      return Spacebars.mustache(view.lookup("projectName"));                                                           // 88
    })), "\n		" ];                                                                                                     // 89
  }), "\n	");                                                                                                          // 90
}));                                                                                                                   // 91
                                                                                                                       // 92
Template.__checkName("projectForm");                                                                                   // 93
Template["projectForm"] = new Template("Template.projectForm", (function() {                                           // 94
  var view = this;                                                                                                     // 95
  return [ HTML.Raw('<button class="btn btn-primary js-createProject">Create Project</button>\n	'), HTML.DIV({         // 96
    "class": "modal fade",                                                                                             // 97
    tabindex: "-1",                                                                                                    // 98
    id: "createProjectModal",                                                                                          // 99
    role: "dialog"                                                                                                     // 100
  }, "\n	  ", HTML.DIV({                                                                                               // 101
    "class": "modal-dialog"                                                                                            // 102
  }, "\n		", HTML.DIV({                                                                                                // 103
    "class": "modal-content"                                                                                           // 104
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add/Edit Projects</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 106
  }, "\n			 ", HTML.Raw('<span id="project_fail" class="response_error" style="display: none;">Project Save Failed!</span>'), "\n			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                           // 108
      collection: Spacebars.call("Projects"),                                                                          // 109
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 110
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 111
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 112
      id: Spacebars.call("addNewProject"),                                                                             // 113
      type: Spacebars.call("method"),                                                                                  // 114
      meteormethod: Spacebars.call("createProject"),                                                                   // 115
      resetOnSuccess: Spacebars.call(true)                                                                             // 116
    };                                                                                                                 // 117
  }, function() {                                                                                                      // 118
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 119
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 121
                                                                                                                       // 122
Template.__checkName("editProjectForm");                                                                               // 123
Template["editProjectForm"] = new Template("Template.editProjectForm", (function() {                                   // 124
  var view = this;                                                                                                     // 125
  return [ HTML.DIV({                                                                                                  // 126
    "class": "modal fade",                                                                                             // 127
    tabindex: "-1",                                                                                                    // 128
    id: "editProjectModal",                                                                                            // 129
    role: "dialog"                                                                                                     // 130
  }, "\n	  ", HTML.DIV({                                                                                               // 131
    "class": "modal-dialog"                                                                                            // 132
  }, "\n		", HTML.DIV({                                                                                                // 133
    "class": "modal-content"                                                                                           // 134
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Project</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 136
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 137
    return {                                                                                                           // 138
      collection: Spacebars.call("Projects"),                                                                          // 139
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 140
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 141
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 142
      id: Spacebars.call("editProjectForm"),                                                                           // 143
      type: Spacebars.call("method-update"),                                                                           // 144
      meteormethod: Spacebars.call("editProject"),                                                                     // 145
      resetOnSuccess: Spacebars.call(true),                                                                            // 146
      doc: Spacebars.call(view.lookup("editProjectDocument")),                                                         // 147
      singleMethodArgument: Spacebars.call(true)                                                                       // 148
    };                                                                                                                 // 149
  }, function() {                                                                                                      // 150
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 151
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 153
                                                                                                                       // 154
Template.__checkName("editProjectSection");                                                                            // 155
Template["editProjectSection"] = new Template("Template.editProjectSection", (function() {                             // 156
  var view = this;                                                                                                     // 157
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editProject">Edit</button>\n  <button type="button" class="btn btn-xs js_removeProject btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteProjectDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Project? The changes cannot be undone!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveProject">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveProject btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                                   // 159
                                                                                                                       // 160
Template.__checkName("projectList");                                                                                   // 161
Template["projectList"] = new Template("Template.projectList", (function() {                                           // 162
  var view = this;                                                                                                     // 163
  return Blaze._TemplateWith(function() {                                                                              // 164
    return {                                                                                                           // 165
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Projects")),                                  // 166
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 167
      id: Spacebars.call("projectList")                                                                                // 168
    };                                                                                                                 // 169
  }, function() {                                                                                                      // 170
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 171
  });                                                                                                                  // 172
}));                                                                                                                   // 173
                                                                                                                       // 174
Template.__checkName("apartmentList");                                                                                 // 175
Template["apartmentList"] = new Template("Template.apartmentList", (function() {                                       // 176
  var view = this;                                                                                                     // 177
  return Blaze._TemplateWith(function() {                                                                              // 178
    return {                                                                                                           // 179
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Apartments")),                                // 180
      selector: Spacebars.call(view.lookup("projectIdSelector")),                                                      // 181
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 182
      id: Spacebars.call("apartmentList")                                                                              // 183
    };                                                                                                                 // 184
  }, function() {                                                                                                      // 185
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 186
  });                                                                                                                  // 187
}));                                                                                                                   // 188
                                                                                                                       // 189
Template.__checkName("userList");                                                                                      // 190
Template["userList"] = new Template("Template.userList", (function() {                                                 // 191
  var view = this;                                                                                                     // 192
  return Blaze._TemplateWith(function() {                                                                              // 193
    return {                                                                                                           // 194
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Users")),                                     // 195
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 196
      id: Spacebars.call("userList")                                                                                   // 197
    };                                                                                                                 // 198
  }, function() {                                                                                                      // 199
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 200
  });                                                                                                                  // 201
}));                                                                                                                   // 202
                                                                                                                       // 203
Template.__checkName("editApartmentSection");                                                                          // 204
Template["editApartmentSection"] = new Template("Template.editApartmentSection", (function() {                         // 205
  var view = this;                                                                                                     // 206
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editApartment">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartment btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteApartmentDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete!</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Apartment? The changes cannot be undone!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveApartment">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveApartment btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                                   // 208
                                                                                                                       // 209
Template.__checkName("apartmentForm");                                                                                 // 210
Template["apartmentForm"] = new Template("Template.apartmentForm", (function() {                                       // 211
  var view = this;                                                                                                     // 212
  return [ HTML.Raw('<button class="btn btn-primary js-createApartment">Create Apartment</button>\n	'), HTML.DIV({     // 213
    "class": "modal fade",                                                                                             // 214
    tabindex: "-1",                                                                                                    // 215
    id: "createApartmentModal",                                                                                        // 216
    role: "dialog"                                                                                                     // 217
  }, "\n	  ", HTML.DIV({                                                                                               // 218
    "class": "modal-dialog"                                                                                            // 219
  }, "\n		", HTML.DIV({                                                                                                // 220
    "class": "modal-content"                                                                                           // 221
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Apartment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 223
  }, "\n			 ", HTML.Raw('<span id="apartment_fail" class="response_error" style="display: none;">Apartment Save Failed!</span>'), "\n			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                           // 225
      collection: Spacebars.call("Apartments"),                                                                        // 226
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 227
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 228
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 229
      id: Spacebars.call("addNewApartment"),                                                                           // 230
      type: Spacebars.call("method"),                                                                                  // 231
      meteormethod: Spacebars.call("createApartment"),                                                                 // 232
      resetOnSuccess: Spacebars.call(true)                                                                             // 233
    };                                                                                                                 // 234
  }, function() {                                                                                                      // 235
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 236
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 238
                                                                                                                       // 239
Template.__checkName("editApartmentForm");                                                                             // 240
Template["editApartmentForm"] = new Template("Template.editApartmentForm", (function() {                               // 241
  var view = this;                                                                                                     // 242
  return [ HTML.DIV({                                                                                                  // 243
    "class": "modal fade",                                                                                             // 244
    tabindex: "-1",                                                                                                    // 245
    id: "editApartmentModal",                                                                                          // 246
    role: "dialog"                                                                                                     // 247
  }, "\n	  ", HTML.DIV({                                                                                               // 248
    "class": "modal-dialog"                                                                                            // 249
  }, "\n		", HTML.DIV({                                                                                                // 250
    "class": "modal-content"                                                                                           // 251
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Apartment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 253
  }, "\n			 ", HTML.Raw('<span id="apartment_edit_fail" class="response_error" style="display: none;">Apartment Edit Failed!</span>'), "			 \n			   ", Blaze._TemplateWith(function() {
    return {                                                                                                           // 255
      collection: Spacebars.call("Apartments"),                                                                        // 256
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 257
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 258
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 259
      id: Spacebars.call("editApartmentForm"),                                                                         // 260
      type: Spacebars.call("method-update"),                                                                           // 261
      meteormethod: Spacebars.call("editApartment"),                                                                   // 262
      resetOnSuccess: Spacebars.call(true),                                                                            // 263
      doc: Spacebars.call(view.lookup("editApartmentDocument")),                                                       // 264
      singleMethodArgument: Spacebars.call(true)                                                                       // 265
    };                                                                                                                 // 266
  }, function() {                                                                                                      // 267
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 268
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 270
                                                                                                                       // 271
Template.__checkName("userForm");                                                                                      // 272
Template["userForm"] = new Template("Template.userForm", (function() {                                                 // 273
  var view = this;                                                                                                     // 274
  return [ HTML.Raw('<button class="btn btn-primary js-createUser">Create User</button>\n	'), HTML.DIV({               // 275
    "class": "modal fade",                                                                                             // 276
    tabindex: "-1",                                                                                                    // 277
    id: "createUserModal",                                                                                             // 278
    role: "dialog"                                                                                                     // 279
  }, "\n	  ", HTML.DIV({                                                                                               // 280
    "class": "modal-dialog"                                                                                            // 281
  }, "\n		", HTML.DIV({                                                                                                // 282
    "class": "modal-content"                                                                                           // 283
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 285
  }, "\n			 \n			   ", Blaze._TemplateWith(function() {                                                                // 286
    return {                                                                                                           // 287
      collection: Spacebars.call("Meteor.users"),                                                                      // 288
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 289
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 290
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 291
      id: Spacebars.call("addNewUser"),                                                                                // 292
      type: Spacebars.call("method"),                                                                                  // 293
      meteormethod: Spacebars.call("addUser"),                                                                         // 294
      resetOnSuccess: Spacebars.call(true)                                                                             // 295
    };                                                                                                                 // 296
  }, function() {                                                                                                      // 297
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 298
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 300
                                                                                                                       // 301
Template.__checkName("editUserForm");                                                                                  // 302
Template["editUserForm"] = new Template("Template.editUserForm", (function() {                                         // 303
  var view = this;                                                                                                     // 304
  return [ HTML.DIV({                                                                                                  // 305
    "class": "modal fade",                                                                                             // 306
    tabindex: "-1",                                                                                                    // 307
    id: "editUserModal",                                                                                               // 308
    role: "dialog"                                                                                                     // 309
  }, "\n	  ", HTML.DIV({                                                                                               // 310
    "class": "modal-dialog"                                                                                            // 311
  }, "\n		", HTML.DIV({                                                                                                // 312
    "class": "modal-content"                                                                                           // 313
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 315
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 316
    return {                                                                                                           // 317
      collection: Spacebars.call("Meteor.users"),                                                                      // 318
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 319
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 320
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 321
      id: Spacebars.call("editUserForm"),                                                                              // 322
      type: Spacebars.call("method-update"),                                                                           // 323
      meteormethod: Spacebars.call("editUser"),                                                                        // 324
      resetOnSuccess: Spacebars.call(true),                                                                            // 325
      doc: Spacebars.call(view.lookup("editUserDocument")),                                                            // 326
      singleMethodArgument: Spacebars.call(true)                                                                       // 327
    };                                                                                                                 // 328
  }, function() {                                                                                                      // 329
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 330
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 332
                                                                                                                       // 333
Template.__checkName("editUserSection");                                                                               // 334
Template["editUserSection"] = new Template("Template.editUserSection", (function() {                                   // 335
  var view = this;                                                                                                     // 336
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editUser">Edit</button>\n  <button type="button" class="btn btn-xs js_removeUser btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteUserDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the User? He may be assigned to an apartment!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveUser">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveUser btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                                   // 338
                                                                                                                       // 339
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"apartmentManagement.html":["./template.apartmentManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/apartmentManagement.html                                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.apartmentManagement.js");                                                         // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.apartmentManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/template.apartmentManagement.js                                                                        //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("apartmentManagement");                                                                           // 2
Template["apartmentManagement"] = new Template("Template.apartmentManagement", (function() {                           // 3
  var view = this;                                                                                                     // 4
  return HTML.DIV({                                                                                                    // 5
    "class": "row"                                                                                                     // 6
  }, "\n	", HTML.DIV({                                                                                                 // 7
    "class": "jumbotron container"                                                                                     // 8
  }, "	\n		", HTML.DIV({                                                                                               // 9
    role: "tabpanel"                                                                                                   // 10
  }, "\n							", HTML.Raw("<!-- Nav tabs -->"), "\n							", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n								<li role="presentation" class="active"><a id="update-tab" data-target="#updateManagement" data-toggle="tab">Updates</a></li>\n								\n								<li role="presentation"><a id="documents-tab" data-target="#documentManagement" data-toggle="tab">Documents</a></li>\n								<li role="presentation"><a id="payment-tab" data-target="#paymentManagement" data-toggle="tab">Payments</a></li>\n								<li role="presentation"><a id="service-tab" data-target="#serviceManagement" data-toggle="tab">Service Requests</a></li>\n								<li role="presentation"><a id="userManagement-tab" data-target="#userManagement" data-toggle="tab">User Management</a></li>\n							</ul>'), "\n						\n							", HTML.Raw("<!-- Tab panes -->"), "\n							", HTML.DIV({
    "class": "tab-content"                                                                                             // 12
  }, "						\n								", HTML.DIV({                                                                                    // 13
    role: "tabpanel",                                                                                                  // 14
    "class": "tab-pane active text-center",                                                                            // 15
    id: "updateManagement"                                                                                             // 16
  }, "\n									", HTML.DIV({                                                                                         // 17
    "class": "row"                                                                                                     // 18
  }, "\n										", HTML.DIV({                                                                                        // 19
    "class": "col-md-12"                                                                                               // 20
  }, "										\n											", Spacebars.include(view.lookupTemplate("TapartmentUpdateForm")), "\n										"), "\n									"), "\n									", HTML.DIV({
    "class": "row"                                                                                                     // 22
  }, "\n										", HTML.DIV({                                                                                        // 23
    "class": "col-md-12"                                                                                               // 24
  }, "\n											", Spacebars.include(view.lookupTemplate("TapartmentUpdates")), "\n											", Spacebars.include(view.lookupTemplate("TeditApartmentUpdateForm")), "\n										"), "\n									"), "			\n									\n								"), "\n								", HTML.DIV({
    role: "tabpanel",                                                                                                  // 26
    "class": "tab-pane text-center",                                                                                   // 27
    id: "userManagement"                                                                                               // 28
  }, "\n									", HTML.DIV({                                                                                         // 29
    "class": "row"                                                                                                     // 30
  }, "\n										", HTML.DIV({                                                                                        // 31
    "class": "col-md-12"                                                                                               // 32
  }, "												\n											", Spacebars.include(view.lookupTemplate("userAssignment")), "\n										"), "\n									"), "	\n									", HTML.DIV({
    "class": "row"                                                                                                     // 34
  }, "\n										", HTML.DIV({                                                                                        // 35
    "class": "col-md-12"                                                                                               // 36
  }, "\n											", Spacebars.include(view.lookupTemplate("assignedUsers")), "\n										"), "\n									"), "									\n								"), "\n								", HTML.DIV({
    role: "tabpanel",                                                                                                  // 38
    "class": "tab-pane text-center",                                                                                   // 39
    id: "documentManagement"                                                                                           // 40
  }, "\n									", HTML.DIV({                                                                                         // 41
    "class": "row"                                                                                                     // 42
  }, "\n										", HTML.DIV({                                                                                        // 43
    "class": "col-md-12"                                                                                               // 44
  }, "\n										 \n											", Spacebars.include(view.lookupTemplate("TCreateApartmentDocument")), "\n											 ", Spacebars.include(view.lookupTemplate("TApartmentDocumentFiles")), "\n											\n										"), "\n									"), "\n									", HTML.DIV({
    "class": "row"                                                                                                     // 46
  }, "\n										", HTML.DIV({                                                                                        // 47
    "class": "col-md-12"                                                                                               // 48
  }, "\n											", HTML.DIV({                                                                                       // 49
    "class": "table-responsive"                                                                                        // 50
  }, "\n												", Spacebars.include(view.lookupTemplate("TapartmentDocuments")), "																			\n											"), "\n											", Spacebars.include(view.lookupTemplate("TeditApartmentDocumentForm")), "\n										"), "\n									"), "							\n								"), "						\n								", HTML.DIV({
    role: "tabpanel",                                                                                                  // 52
    "class": "tab-pane  text-center",                                                                                  // 53
    id: "serviceManagement"                                                                                            // 54
  }, "\n									", HTML.DIV({                                                                                         // 55
    "class": "row"                                                                                                     // 56
  }, "\n										", HTML.DIV({                                                                                        // 57
    "class": "col-md-12"                                                                                               // 58
  }, "										 \n											", Spacebars.include(view.lookupTemplate("TapartmentServiceRequestForm")), "\n											", HTML.Raw("<!-- {{> TApartmentPaymentFiles}} -->"), "\n											\n										"), "\n									"), "\n									", HTML.DIV({
    "class": "row"                                                                                                     // 60
  }, "\n										", HTML.DIV({                                                                                        // 61
    "class": "col-md-12"                                                                                               // 62
  }, "\n											", HTML.DIV({                                                                                       // 63
    "class": "table-responsive"                                                                                        // 64
  }, "\n												", Spacebars.include(view.lookupTemplate("TapartmentServiceRequests")), "																			\n											"), "\n											", Spacebars.include(view.lookupTemplate("TeditApartmentServiceRequestForm")), "\n										"), "\n									"), "															\n								"), "\n								", HTML.DIV({
    role: "tabpanel",                                                                                                  // 66
    "class": "tab-pane  text-center",                                                                                  // 67
    id: "paymentManagement"                                                                                            // 68
  }, "\n									", HTML.DIV({                                                                                         // 69
    "class": "row"                                                                                                     // 70
  }, "\n										", HTML.DIV({                                                                                        // 71
    "class": "col-md-12"                                                                                               // 72
  }, "										 \n											", Spacebars.include(view.lookupTemplate("TapartmentPaymentForm")), "\n											", HTML.Raw("<!-- {{> TApartmentPaymentFiles}} -->"), "\n											\n										"), "\n									"), "\n									", HTML.DIV({
    "class": "row"                                                                                                     // 74
  }, "\n										", HTML.DIV({                                                                                        // 75
    "class": "col-md-12"                                                                                               // 76
  }, "\n											", HTML.DIV({                                                                                       // 77
    "class": "table-responsive"                                                                                        // 78
  }, "\n												", Spacebars.include(view.lookupTemplate("TapartmentPayments")), "																			\n											"), "\n											", Spacebars.include(view.lookupTemplate("TeditApartmentPaymentForm")), "\n										"), "\n									"), "\n								"), "\n					"), "\n		"), "\n	"), "\n");
}));                                                                                                                   // 80
                                                                                                                       // 81
Template.__checkName("userAssignment");                                                                                // 82
Template["userAssignment"] = new Template("Template.userAssignment", (function() {                                     // 83
  var view = this;                                                                                                     // 84
  return [ HTML.Raw('<button class="btn btn-primary js-assignOwners">Assign Owners</button>\n  '), HTML.DIV({          // 85
    "class": "modal fade",                                                                                             // 86
    tabindex: "-1",                                                                                                    // 87
    id: "assignUserModal",                                                                                             // 88
    role: "dialog"                                                                                                     // 89
  }, "\n	  ", HTML.DIV({                                                                                               // 90
    "class": "modal-dialog"                                                                                            // 91
  }, "\n		", HTML.DIV({                                                                                                // 92
    "class": "modal-content"                                                                                           // 93
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Assign User</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 95
  }, "\n			 ", Spacebars.include(view.lookupTemplate("assignUsers")), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n			<button type="button" class="btn btn-xs btn-warning js_closeAssignUserModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 97
                                                                                                                       // 98
Template.__checkName("assignUsers");                                                                                   // 99
Template["assignUsers"] = new Template("Template.assignUsers", (function() {                                           // 100
  var view = this;                                                                                                     // 101
  return Blaze._TemplateWith(function() {                                                                              // 102
    return {                                                                                                           // 103
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "UserList")),                                  // 104
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 105
      id: Spacebars.call("assignUsersList")                                                                            // 106
    };                                                                                                                 // 107
  }, function() {                                                                                                      // 108
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 109
  });                                                                                                                  // 110
}));                                                                                                                   // 111
                                                                                                                       // 112
Template.__checkName("assignedUsers");                                                                                 // 113
Template["assignedUsers"] = new Template("Template.assignedUsers", (function() {                                       // 114
  var view = this;                                                                                                     // 115
  return [ Blaze._TemplateWith(function() {                                                                            // 116
    return {                                                                                                           // 117
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "AssignedUserList")),                          // 118
      selector: Spacebars.call(view.lookup("userIdSelector")),                                                         // 119
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 120
      id: Spacebars.call("assignedUserList")                                                                           // 121
    };                                                                                                                 // 122
  }, function() {                                                                                                      // 123
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 124
  }), "\n				", Spacebars.include(view.lookupTemplate("removeAssignmentModal")) ];                                     // 125
}));                                                                                                                   // 126
                                                                                                                       // 127
Template.__checkName("removeUserAssignSection");                                                                       // 128
Template["removeUserAssignSection"] = new Template("Template.removeUserAssignSection", (function() {                   // 129
  var view = this;                                                                                                     // 130
  return HTML.Raw('<button type="button" class="btn btn-xs btn-danger js_removeUserAssignment">Remove</button>');      // 131
}));                                                                                                                   // 132
                                                                                                                       // 133
Template.__checkName("assignUser");                                                                                    // 134
Template["assignUser"] = new Template("Template.assignUser", (function() {                                             // 135
  var view = this;                                                                                                     // 136
  return HTML.Raw('<button type="button" class="btn btn-xs btn-danger js_assignUserToApartment">Assign</button>');     // 137
}));                                                                                                                   // 138
                                                                                                                       // 139
Template.__checkName("removeAssignmentModal");                                                                         // 140
Template["removeAssignmentModal"] = new Template("Template.removeAssignmentModal", (function() {                       // 141
  var view = this;                                                                                                     // 142
  return HTML.Raw('<div class="modal fade" tabindex="-1" id="confirmRemoveUserAssignmentModal" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete?</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to remove the Assignment? He will lose all the updates!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveUserAssignment">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveUserAssignment btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div><!-- /.modal -->');
}));                                                                                                                   // 144
                                                                                                                       // 145
Template.__checkName("TeditApartmentDocumentsSection");                                                                // 146
Template["TeditApartmentDocumentsSection"] = new Template("Template.TeditApartmentDocumentsSection", (function() {     // 147
  var view = this;                                                                                                     // 148
  return HTML.Raw('<button type="button" class="btn btn-xs btn-success js_manageApartmentDocumentFiles">Files</button>\n  <button type="button" class="btn btn-xs btn-warning js_editApartmentDocument">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartmentDocument btn-danger">Delete</button>');
}));                                                                                                                   // 150
                                                                                                                       // 151
Template.__checkName("TapartmentDocuments");                                                                           // 152
Template["TapartmentDocuments"] = new Template("Template.TapartmentDocuments", (function() {                           // 153
  var view = this;                                                                                                     // 154
  return Blaze._TemplateWith(function() {                                                                              // 155
    return {                                                                                                           // 156
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ApartmentDocuments")),                        // 157
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 158
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 159
      id: Spacebars.call("apartmentDocumentsTable")                                                                    // 160
    };                                                                                                                 // 161
  }, function() {                                                                                                      // 162
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 163
  });                                                                                                                  // 164
}));                                                                                                                   // 165
                                                                                                                       // 166
Template.__checkName("TCreateApartmentDocument");                                                                      // 167
Template["TCreateApartmentDocument"] = new Template("Template.TCreateApartmentDocument", (function() {                 // 168
  var view = this;                                                                                                     // 169
  return [ HTML.Raw('<button class="btn btn-primary js-createApartmentDocument">Add Document</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                             // 171
    tabindex: "-1",                                                                                                    // 172
    id: "createApartmentDocumentModal",                                                                                // 173
    role: "dialog"                                                                                                     // 174
  }, "\n	  ", HTML.DIV({                                                                                               // 175
    "class": "modal-dialog"                                                                                            // 176
  }, "\n		", HTML.DIV({                                                                                                // 177
    "class": "modal-content"                                                                                           // 178
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Document</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 180
  }, ">\n			 ", HTML.DIV({                                                                                             // 181
    "class": "row"                                                                                                     // 182
  }, "\n				", HTML.DIV({                                                                                              // 183
    "class": "col-md-12"                                                                                               // 184
  }, "\n					", Blaze._TemplateWith(function() {                                                                       // 185
    return {                                                                                                           // 186
      collection: Spacebars.call("ApartmentDocuments"),                                                                // 187
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 188
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 189
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 190
      id: Spacebars.call("addNewApartmentDocumentForm"),                                                               // 191
      type: Spacebars.call("method"),                                                                                  // 192
      meteormethod: Spacebars.call("createApartmentDocument"),                                                         // 193
      doc: Spacebars.call(view.lookup("defaultApartmentDocument")),                                                    // 194
      resetOnSuccess: Spacebars.call(true)                                                                             // 195
    };                                                                                                                 // 196
  }, function() {                                                                                                      // 197
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 198
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 200
                                                                                                                       // 201
Template.__checkName("TeditApartmentDocumentForm");                                                                    // 202
Template["TeditApartmentDocumentForm"] = new Template("Template.TeditApartmentDocumentForm", (function() {             // 203
  var view = this;                                                                                                     // 204
  return HTML.DIV({                                                                                                    // 205
    "class": "modal fade",                                                                                             // 206
    tabindex: "-1",                                                                                                    // 207
    id: "editApartmentDocumentModal",                                                                                  // 208
    role: "dialog"                                                                                                     // 209
  }, "\n	 ", HTML.DIV({                                                                                                // 210
    "class": "modal-dialog"                                                                                            // 211
  }, "\n		", HTML.DIV({                                                                                                // 212
    "class": "modal-content"                                                                                           // 213
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Document</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 215
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 216
    return {                                                                                                           // 217
      collection: Spacebars.call("ApartmentDocuments"),                                                                // 218
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 219
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 220
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 221
      id: Spacebars.call("editApartmentDocumentForm"),                                                                 // 222
      type: Spacebars.call("method-update"),                                                                           // 223
      meteormethod: Spacebars.call("editApartmentDocument"),                                                           // 224
      resetOnSuccess: Spacebars.call(true),                                                                            // 225
      doc: Spacebars.call(view.lookup("editApartmentUpdateDocument")),                                                 // 226
      singleMethodArgument: Spacebars.call(true)                                                                       // 227
    };                                                                                                                 // 228
  }, function() {                                                                                                      // 229
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 230
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), "\n	  "), " \n	");             // 231
}));                                                                                                                   // 232
                                                                                                                       // 233
Template.__checkName("TapartmentUpdates");                                                                             // 234
Template["TapartmentUpdates"] = new Template("Template.TapartmentUpdates", (function() {                               // 235
  var view = this;                                                                                                     // 236
  return [ Blaze._TemplateWith(function() {                                                                            // 237
    return {                                                                                                           // 238
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ApartmentUpdates")),                          // 239
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 240
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 241
      id: Spacebars.call("apartmentUpdatesTable")                                                                      // 242
    };                                                                                                                 // 243
  }, function() {                                                                                                      // 244
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 245
  }), "\n	", Spacebars.include(view.lookupTemplate("TapartmentUpdateFiles")) ];                                        // 246
}));                                                                                                                   // 247
                                                                                                                       // 248
Template.__checkName("TeditApartmentUpdatesSection");                                                                  // 249
Template["TeditApartmentUpdatesSection"] = new Template("Template.TeditApartmentUpdatesSection", (function() {         // 250
  var view = this;                                                                                                     // 251
  return HTML.Raw('<button type="button" class="btn btn-xs btn-success js_manageApartmentUpdateFiles">Files</button>\n  <button type="button" class="btn btn-xs btn-warning js_editApartmentUpdate">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartmentUpdate btn-danger">Delete</button>');
}));                                                                                                                   // 253
                                                                                                                       // 254
Template.__checkName("TapartmentUpdateForm");                                                                          // 255
Template["TapartmentUpdateForm"] = new Template("Template.TapartmentUpdateForm", (function() {                         // 256
  var view = this;                                                                                                     // 257
  return [ HTML.Raw('<button class="btn btn-primary js-createApartmentUpdate">Add Apartment Update</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                             // 259
    tabindex: "-1",                                                                                                    // 260
    id: "createApartmentUpdateModal",                                                                                  // 261
    role: "dialog"                                                                                                     // 262
  }, "\n	  ", HTML.DIV({                                                                                               // 263
    "class": "modal-dialog"                                                                                            // 264
  }, "\n		", HTML.DIV({                                                                                                // 265
    "class": "modal-content"                                                                                           // 266
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 268
  }, "\n			 ", HTML.Raw('<div class="row">\n				<div class="col-md-12">\n				<!--<h4> Apartment Name: {{projectName}}</h4>-->\n				</div>\n			 </div>'), "\n			 ", HTML.DIV({
    "class": "row"                                                                                                     // 270
  }, "\n				", HTML.DIV({                                                                                              // 271
    "class": "col-md-12"                                                                                               // 272
  }, "\n					", Blaze._TemplateWith(function() {                                                                       // 273
    return {                                                                                                           // 274
      collection: Spacebars.call("ApartmentUpdates"),                                                                  // 275
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 276
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 277
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 278
      id: Spacebars.call("addNewApartmentUpdateForm"),                                                                 // 279
      type: Spacebars.call("method"),                                                                                  // 280
      meteormethod: Spacebars.call("createApartmentUpdate"),                                                           // 281
      doc: Spacebars.call(view.lookup("defaultApartmentUpdate")),                                                      // 282
      resetOnSuccess: Spacebars.call(true)                                                                             // 283
    };                                                                                                                 // 284
  }, function() {                                                                                                      // 285
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 286
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 288
                                                                                                                       // 289
Template.__checkName("TeditApartmentUpdateForm");                                                                      // 290
Template["TeditApartmentUpdateForm"] = new Template("Template.TeditApartmentUpdateForm", (function() {                 // 291
  var view = this;                                                                                                     // 292
  return [ HTML.DIV({                                                                                                  // 293
    "class": "modal fade",                                                                                             // 294
    tabindex: "-1",                                                                                                    // 295
    id: "editApartmentUpdateModal",                                                                                    // 296
    role: "dialog"                                                                                                     // 297
  }, "\n	  ", HTML.DIV({                                                                                               // 298
    "class": "modal-dialog"                                                                                            // 299
  }, "\n		", HTML.DIV({                                                                                                // 300
    "class": "modal-content"                                                                                           // 301
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 303
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 304
    return {                                                                                                           // 305
      collection: Spacebars.call("ApartmentUpdates"),                                                                  // 306
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 307
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 308
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 309
      id: Spacebars.call("editApartmentUpdateForm"),                                                                   // 310
      type: Spacebars.call("method-update"),                                                                           // 311
      meteormethod: Spacebars.call("editApartmentUpdate"),                                                             // 312
      resetOnSuccess: Spacebars.call(true),                                                                            // 313
      doc: Spacebars.call(view.lookup("editApartmentUpdate")),                                                         // 314
      singleMethodArgument: Spacebars.call(true)                                                                       // 315
    };                                                                                                                 // 316
  }, function() {                                                                                                      // 317
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 318
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 320
                                                                                                                       // 321
Template.__checkName("TapartmentPayments");                                                                            // 322
Template["TapartmentPayments"] = new Template("Template.TapartmentPayments", (function() {                             // 323
  var view = this;                                                                                                     // 324
  return Blaze._TemplateWith(function() {                                                                              // 325
    return {                                                                                                           // 326
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ApartmentPayment")),                          // 327
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 328
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 329
      id: Spacebars.call("apartmentPaymentTable")                                                                      // 330
    };                                                                                                                 // 331
  }, function() {                                                                                                      // 332
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 333
  });                                                                                                                  // 334
}));                                                                                                                   // 335
                                                                                                                       // 336
Template.__checkName("TeditApartmentPaymentSection");                                                                  // 337
Template["TeditApartmentPaymentSection"] = new Template("Template.TeditApartmentPaymentSection", (function() {         // 338
  var view = this;                                                                                                     // 339
  return HTML.Raw('<button type="button" class="btn btn-xs btn-success js_manageApartmentPaymentFiles">Files</button>\n  <button type="button" class="btn btn-xs btn-warning js_editApartmentPayment">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartmentPayment btn-danger">Delete</button>');
}));                                                                                                                   // 341
                                                                                                                       // 342
Template.__checkName("TapartmentPaymentForm");                                                                         // 343
Template["TapartmentPaymentForm"] = new Template("Template.TapartmentPaymentForm", (function() {                       // 344
  var view = this;                                                                                                     // 345
  return [ HTML.Raw('<button class="btn btn-primary js-createApartmentPayment">Add Apartment Payment</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                             // 347
    tabindex: "-1",                                                                                                    // 348
    id: "createApartmentPaymentModal",                                                                                 // 349
    role: "dialog"                                                                                                     // 350
  }, "\n	  ", HTML.DIV({                                                                                               // 351
    "class": "modal-dialog"                                                                                            // 352
  }, "\n		", HTML.DIV({                                                                                                // 353
    "class": "modal-content"                                                                                           // 354
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Payment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 356
  }, "\n			 ", HTML.Raw('<div class="row">\n				<div class="col-md-12">\n				<!--<h4> Apartment Name: {{projectName}}</h4>-->\n				</div>\n			 </div>'), "\n			 ", HTML.DIV({
    "class": "row"                                                                                                     // 358
  }, "\n				", HTML.DIV({                                                                                              // 359
    "class": "col-md-12"                                                                                               // 360
  }, "\n					", Blaze._TemplateWith(function() {                                                                       // 361
    return {                                                                                                           // 362
      collection: Spacebars.call("ApartmentPayment"),                                                                  // 363
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 364
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 365
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 366
      id: Spacebars.call("addNewApartmentPaymentForm"),                                                                // 367
      type: Spacebars.call("method"),                                                                                  // 368
      meteormethod: Spacebars.call("createApartmentPayment"),                                                          // 369
      doc: Spacebars.call(view.lookup("defaultApartmentPayment")),                                                     // 370
      resetOnSuccess: Spacebars.call(true)                                                                             // 371
    };                                                                                                                 // 372
  }, function() {                                                                                                      // 373
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 374
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 376
                                                                                                                       // 377
Template.__checkName("TeditApartmentPaymentForm");                                                                     // 378
Template["TeditApartmentPaymentForm"] = new Template("Template.TeditApartmentPaymentForm", (function() {               // 379
  var view = this;                                                                                                     // 380
  return [ HTML.DIV({                                                                                                  // 381
    "class": "modal fade",                                                                                             // 382
    tabindex: "-1",                                                                                                    // 383
    id: "editApartmentPaymentModal",                                                                                   // 384
    role: "dialog"                                                                                                     // 385
  }, "\n	  ", HTML.DIV({                                                                                               // 386
    "class": "modal-dialog"                                                                                            // 387
  }, "\n		", HTML.DIV({                                                                                                // 388
    "class": "modal-content"                                                                                           // 389
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Payment</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 391
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 392
    return {                                                                                                           // 393
      collection: Spacebars.call("ApartmentPayment"),                                                                  // 394
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 395
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 396
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 397
      id: Spacebars.call("editApartmentPaymentForm"),                                                                  // 398
      type: Spacebars.call("method-update"),                                                                           // 399
      meteormethod: Spacebars.call("editApartmentPayment"),                                                            // 400
      resetOnSuccess: Spacebars.call(true),                                                                            // 401
      doc: Spacebars.call(view.lookup("editApartmentPayment")),                                                        // 402
      singleMethodArgument: Spacebars.call(true)                                                                       // 403
    };                                                                                                                 // 404
  }, function() {                                                                                                      // 405
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 406
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 408
                                                                                                                       // 409
Template.__checkName("TapartmentServiceRequests");                                                                     // 410
Template["TapartmentServiceRequests"] = new Template("Template.TapartmentServiceRequests", (function() {               // 411
  var view = this;                                                                                                     // 412
  return Blaze._TemplateWith(function() {                                                                              // 413
    return {                                                                                                           // 414
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ApartmentServiceRequests")),                  // 415
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 416
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 417
      id: Spacebars.call("apartmentServiceRequestsTable")                                                              // 418
    };                                                                                                                 // 419
  }, function() {                                                                                                      // 420
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 421
  });                                                                                                                  // 422
}));                                                                                                                   // 423
                                                                                                                       // 424
Template.__checkName("TeditApartmentServiceRequestSection");                                                           // 425
Template["TeditApartmentServiceRequestSection"] = new Template("Template.TeditApartmentServiceRequestSection", (function() {
  var view = this;                                                                                                     // 427
  return HTML.Raw('<button type="button" class="btn btn-xs btn-warning js_editApartmentServiceRequest">Edit</button>\n  <button type="button" class="btn btn-xs js_removeApartmentServiceRequest btn-danger">Delete</button>');
}));                                                                                                                   // 429
                                                                                                                       // 430
Template.__checkName("TapartmentServiceRequestForm");                                                                  // 431
Template["TapartmentServiceRequestForm"] = new Template("Template.TapartmentServiceRequestForm", (function() {         // 432
  var view = this;                                                                                                     // 433
  return [ HTML.Raw('<button class="btn btn-primary js-createApartmentServiceRequest">Add Service Request</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                             // 435
    tabindex: "-1",                                                                                                    // 436
    id: "createApartmentServiceRequestModal",                                                                          // 437
    role: "dialog"                                                                                                     // 438
  }, "\n	  ", HTML.DIV({                                                                                               // 439
    "class": "modal-dialog"                                                                                            // 440
  }, "\n		", HTML.DIV({                                                                                                // 441
    "class": "modal-content"                                                                                           // 442
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Service Request</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 444
  }, "\n			 ", HTML.Raw('<div class="row">\n				<div class="col-md-12">\n				<!--<h4> Apartment Name: {{projectName}}</h4>-->\n				</div>\n			 </div>'), "\n			 ", HTML.DIV({
    "class": "row"                                                                                                     // 446
  }, "\n				", HTML.DIV({                                                                                              // 447
    "class": "col-md-12"                                                                                               // 448
  }, "\n					", Blaze._TemplateWith(function() {                                                                       // 449
    return {                                                                                                           // 450
      collection: Spacebars.call("ApartmentServiceRequests"),                                                          // 451
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 452
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 453
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 454
      id: Spacebars.call("addNewApartmentServiceRequestForm"),                                                         // 455
      type: Spacebars.call("method"),                                                                                  // 456
      meteormethod: Spacebars.call("createApartmentServiceRequest"),                                                   // 457
      doc: Spacebars.call(view.lookup("defaultApartmentServiceRequest")),                                              // 458
      resetOnSuccess: Spacebars.call(true)                                                                             // 459
    };                                                                                                                 // 460
  }, function() {                                                                                                      // 461
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 462
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 464
                                                                                                                       // 465
Template.__checkName("TeditApartmentServiceRequestForm");                                                              // 466
Template["TeditApartmentServiceRequestForm"] = new Template("Template.TeditApartmentServiceRequestForm", (function() {
  var view = this;                                                                                                     // 468
  return [ HTML.DIV({                                                                                                  // 469
    "class": "modal fade",                                                                                             // 470
    tabindex: "-1",                                                                                                    // 471
    id: "editApartmentServiceRequestModal",                                                                            // 472
    role: "dialog"                                                                                                     // 473
  }, "\n	  ", HTML.DIV({                                                                                               // 474
    "class": "modal-dialog"                                                                                            // 475
  }, "\n		", HTML.DIV({                                                                                                // 476
    "class": "modal-content"                                                                                           // 477
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Request</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 479
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 480
    return {                                                                                                           // 481
      collection: Spacebars.call("ApartmentServiceRequests"),                                                          // 482
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 483
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 484
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 485
      id: Spacebars.call("editApartmentServiceRequestForm"),                                                           // 486
      type: Spacebars.call("method-update"),                                                                           // 487
      meteormethod: Spacebars.call("editApartmentServiceRequest"),                                                     // 488
      resetOnSuccess: Spacebars.call(true),                                                                            // 489
      doc: Spacebars.call(view.lookup("editApartmentServiceRequest")),                                                 // 490
      singleMethodArgument: Spacebars.call(true)                                                                       // 491
    };                                                                                                                 // 492
  }, function() {                                                                                                      // 493
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 494
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 496
                                                                                                                       // 497
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"fileManagement.html":["./template.fileManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/fileManagement.html                                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.fileManagement.js");                                                              // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.fileManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/template.fileManagement.js                                                                             //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("TprojectUpdateFiles");                                                                           // 2
Template["TprojectUpdateFiles"] = new Template("Template.TprojectUpdateFiles", (function() {                           // 3
  var view = this;                                                                                                     // 4
  return HTML.DIV({                                                                                                    // 5
    "class": "modal fade",                                                                                             // 6
    tabindex: "-1",                                                                                                    // 7
    id: "projectUpdateFilesModal",                                                                                     // 8
    role: "dialog"                                                                                                     // 9
  }, "\n	  ", HTML.DIV({                                                                                               // 10
    "class": "modal-dialog"                                                                                            // 11
  }, "\n		", HTML.DIV({                                                                                                // 12
    "class": "modal-content"                                                                                           // 13
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 15
  }, "\n			", HTML.Raw('<div id="projectUpdateFilesError">\n				<p style="color:red">Error Uploading File, check selections or allowable file formats</p>\n			</div>'), "\n			", HTML.Raw('<div class="row">\n				<input type="file" accept="image/*;capture=camera" id="projectUpdateImage">\n				<button class="btn btn-primary js_uploadImage">Upload</button>\n			</div>'), "				\n			", HTML.DIV({
    "class": "row"                                                                                                     // 17
  }, "\n			", Spacebars.include(view.lookupTemplate("TProjectUpdateFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeProjectUpdateFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->\n	"));
}));                                                                                                                   // 19
                                                                                                                       // 20
Template.__checkName("TProjectUpdateFileGallery");                                                                     // 21
Template["TProjectUpdateFileGallery"] = new Template("Template.TProjectUpdateFileGallery", (function() {               // 22
  var view = this;                                                                                                     // 23
  return [ HTML.DIV({                                                                                                  // 24
    "class": "container"                                                                                               // 25
  }, "\n  ", HTML.DIV({                                                                                                // 26
    "class": "row"                                                                                                     // 27
  }, "\n    ", HTML.DIV({                                                                                              // 28
    "class": "row"                                                                                                     // 29
  }, "\n	", Blaze.Each(function() {                                                                                    // 30
    return Spacebars.call(view.lookup("projectUpdateFiles"));                                                          // 31
  }, function() {                                                                                                      // 32
    return [ "\n		", Blaze.If(function() {                                                                             // 33
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 34
    }, function() {                                                                                                    // 35
      return [ "\n		  ", HTML.DIV({                                                                                    // 36
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 37
      }, "\n		  ", HTML.A({                                                                                            // 38
        href: "#",                                                                                                     // 39
        title: "Project Updates"                                                                                       // 40
      }, HTML.IMG({                                                                                                    // 41
        src: function() {                                                                                              // 42
          return Spacebars.mustache(view.lookup("url"));                                                               // 43
        },                                                                                                             // 44
        "class": "js_imageThumbnail thumbnail img-responsive"                                                          // 45
      })), "\n		  ", HTML.BUTTON({                                                                                     // 46
        "class": "btn btn-xs btn-danger js_projectUpdateDeleteFile"                                                    // 47
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 48
    }, function() {                                                                                                    // 49
      return [ "\n		  ", HTML.DIV({                                                                                    // 50
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 51
      }, "\n		  ", HTML.A({                                                                                            // 52
        href: "#",                                                                                                     // 53
        title: "Project Updates"                                                                                       // 54
      }, HTML.IMG({                                                                                                    // 55
        src: "no_preview.png",                                                                                         // 56
        "class": "js_imageThumbnail thumbnail img-responsive"                                                          // 57
      })), "\n		  ", HTML.BUTTON({                                                                                     // 58
        "class": "btn btn-xs btn-danger js_projectUpdateDeleteFile"                                                    // 59
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 60
    }), "\n		\n	" ];                                                                                                   // 61
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="projectUpdateFileGalleryModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>\n<div id="projectFileLoadModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="fileLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 63
                                                                                                                       // 64
Template.__checkName("TapartmentUpdateFiles");                                                                         // 65
Template["TapartmentUpdateFiles"] = new Template("Template.TapartmentUpdateFiles", (function() {                       // 66
  var view = this;                                                                                                     // 67
  return HTML.DIV({                                                                                                    // 68
    "class": "modal fade",                                                                                             // 69
    tabindex: "-1",                                                                                                    // 70
    id: "apartmentUpdatesFilesModal",                                                                                  // 71
    role: "dialog"                                                                                                     // 72
  }, "\n	  ", HTML.DIV({                                                                                               // 73
    "class": "modal-dialog"                                                                                            // 74
  }, "\n		", HTML.DIV({                                                                                                // 75
    "class": "modal-content"                                                                                           // 76
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 78
  }, "\n			", HTML.Raw('<div id="projectUpdateFilesError">\n				<p style="color:red">Error Uploading File, check selections or allowable file formats</p>\n			</div>'), "\n			", HTML.Raw('<div class="row">\n				<input type="file" accept="image/*;capture=camera" id="apartmentUpdateFile">\n				<button class="btn btn-primary js_uploadImage">Upload</button>\n			</div>'), "				\n			", HTML.DIV({
    "class": "row"                                                                                                     // 80
  }, "\n			", Spacebars.include(view.lookupTemplate("TApartmentUpdateFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeProjectUpdateFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->\n	"));
}));                                                                                                                   // 82
                                                                                                                       // 83
Template.__checkName("TApartmentUpdateFileGallery");                                                                   // 84
Template["TApartmentUpdateFileGallery"] = new Template("Template.TApartmentUpdateFileGallery", (function() {           // 85
  var view = this;                                                                                                     // 86
  return [ HTML.DIV({                                                                                                  // 87
    "class": "container"                                                                                               // 88
  }, "\n  ", HTML.DIV({                                                                                                // 89
    "class": "row"                                                                                                     // 90
  }, "\n    ", HTML.DIV({                                                                                              // 91
    "class": "row"                                                                                                     // 92
  }, "\n	", Blaze.Each(function() {                                                                                    // 93
    return Spacebars.call(view.lookup("apartmentUpdateFiles"));                                                        // 94
  }, function() {                                                                                                      // 95
    return [ "\n		", Blaze.If(function() {                                                                             // 96
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 97
    }, function() {                                                                                                    // 98
      return [ "\n		  ", HTML.DIV({                                                                                    // 99
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 100
      }, "\n		  ", HTML.A({                                                                                            // 101
        href: "#",                                                                                                     // 102
        title: "Apartment Updates"                                                                                     // 103
      }, HTML.IMG({                                                                                                    // 104
        src: function() {                                                                                              // 105
          return Spacebars.mustache(view.lookup("url"));                                                               // 106
        },                                                                                                             // 107
        "class": "js_imageThumbnail thumbnail img-responsive"                                                          // 108
      })), "\n		  ", HTML.BUTTON({                                                                                     // 109
        "class": "btn btn-xs btn-danger js_apartmentUpdateDeleteFile"                                                  // 110
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 111
    }, function() {                                                                                                    // 112
      return [ "\n		  ", HTML.DIV({                                                                                    // 113
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 114
      }, "\n		  ", HTML.A({                                                                                            // 115
        href: "#",                                                                                                     // 116
        title: "Project Updates"                                                                                       // 117
      }, HTML.IMG({                                                                                                    // 118
        src: "no_preview.png",                                                                                         // 119
        "class": "js_imageThumbnail thumbnail img-responsive"                                                          // 120
      })), "\n		  ", HTML.BUTTON({                                                                                     // 121
        "class": "btn btn-xs btn-danger js_apartmentUpdateDeleteFile"                                                  // 122
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 123
    }), "\n		\n	" ];                                                                                                   // 124
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="apartmentFileLoadModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="apartment_fileLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 126
                                                                                                                       // 127
Template.__checkName("TApartmentDocumentFiles");                                                                       // 128
Template["TApartmentDocumentFiles"] = new Template("Template.TApartmentDocumentFiles", (function() {                   // 129
  var view = this;                                                                                                     // 130
  return HTML.DIV({                                                                                                    // 131
    "class": "modal fade",                                                                                             // 132
    tabindex: "-1",                                                                                                    // 133
    id: "apartmentDocumentFilesModal",                                                                                 // 134
    role: "dialog"                                                                                                     // 135
  }, "\n	  ", HTML.DIV({                                                                                               // 136
    "class": "modal-dialog"                                                                                            // 137
  }, "\n		", HTML.DIV({                                                                                                // 138
    "class": "modal-content"                                                                                           // 139
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 141
  }, "\n			", HTML.Raw('<div id="apartmentDocumentUploadFilesError">\n				<p style="color:red;display:none">Error Uploading File, check selections or allowable file formats</p>\n			</div>'), "\n			", HTML.Raw('<div class="row">\n				<input type="file" accept="image/*;capture=camera" id="uploadApartmentDocument">\n				<button class="btn btn-primary js_uploadApartmentDocument">Upload</button>\n			</div>'), "				\n			", HTML.DIV({
    "class": "row"                                                                                                     // 143
  }, "\n			", Spacebars.include(view.lookupTemplate("TApartmentDocumentsFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeProjectUpdateFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->\n	"));
}));                                                                                                                   // 145
                                                                                                                       // 146
Template.__checkName("TApartmentDocumentsFileGallery");                                                                // 147
Template["TApartmentDocumentsFileGallery"] = new Template("Template.TApartmentDocumentsFileGallery", (function() {     // 148
  var view = this;                                                                                                     // 149
  return [ HTML.DIV({                                                                                                  // 150
    "class": "container"                                                                                               // 151
  }, "\n  ", HTML.DIV({                                                                                                // 152
    "class": "row"                                                                                                     // 153
  }, "\n    ", HTML.DIV({                                                                                              // 154
    "class": "row"                                                                                                     // 155
  }, "\n	", Blaze.Each(function() {                                                                                    // 156
    return Spacebars.call(view.lookup("apartmentDocumentFiles"));                                                      // 157
  }, function() {                                                                                                      // 158
    return [ "\n		", Blaze.If(function() {                                                                             // 159
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 160
    }, function() {                                                                                                    // 161
      return [ "\n		  ", HTML.DIV({                                                                                    // 162
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 163
      }, "\n		  ", HTML.A({                                                                                            // 164
        href: "#",                                                                                                     // 165
        title: "Project Updates"                                                                                       // 166
      }, HTML.IMG({                                                                                                    // 167
        src: function() {                                                                                              // 168
          return Spacebars.mustache(view.lookup("url"));                                                               // 169
        },                                                                                                             // 170
        "class": "js_apartmentDocumentThumbnail thumbnail img-responsive"                                              // 171
      })), "\n		  ", HTML.BUTTON({                                                                                     // 172
        "class": "btn btn-xs btn-danger js_apartmentDocumentFileDelete"                                                // 173
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 174
    }, function() {                                                                                                    // 175
      return [ "\n		  ", HTML.DIV({                                                                                    // 176
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 177
      }, "\n		  ", HTML.A({                                                                                            // 178
        href: "#",                                                                                                     // 179
        title: "Project Updates"                                                                                       // 180
      }, HTML.IMG({                                                                                                    // 181
        src: "no_preview.png",                                                                                         // 182
        "class": "js_apartmentDocumentThumbnail thumbnail img-responsive"                                              // 183
      })), "\n		  ", HTML.BUTTON({                                                                                     // 184
        "class": "btn btn-xs btn-danger js_apartmentDocumentFileDelete"                                                // 185
      }, " Delete "), "\n		  "), "\n		" ];                                                                             // 186
    }), "\n		\n	" ];                                                                                                   // 187
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="apartmentDocumentFileGalleryModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="apartmentDocumentLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 189
                                                                                                                       // 190
Template.__checkName("TClient_projectUpdateFileGallery");                                                              // 191
Template["TClient_projectUpdateFileGallery"] = new Template("Template.TClient_projectUpdateFileGallery", (function() {
  var view = this;                                                                                                     // 193
  return [ HTML.DIV({                                                                                                  // 194
    "class": "container"                                                                                               // 195
  }, "\n  ", HTML.DIV({                                                                                                // 196
    "class": "row"                                                                                                     // 197
  }, "\n    ", HTML.DIV({                                                                                              // 198
    "class": "row"                                                                                                     // 199
  }, "\n	", Blaze.Each(function() {                                                                                    // 200
    return Spacebars.call(view.lookup("projectUpdateFiles"));                                                          // 201
  }, function() {                                                                                                      // 202
    return [ "\n		", Blaze.If(function() {                                                                             // 203
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 204
    }, function() {                                                                                                    // 205
      return [ "\n		  ", HTML.DIV({                                                                                    // 206
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 207
      }, "\n		  ", HTML.A({                                                                                            // 208
        href: "#",                                                                                                     // 209
        title: "Project Updates"                                                                                       // 210
      }, HTML.IMG({                                                                                                    // 211
        src: function() {                                                                                              // 212
          return Spacebars.mustache(view.lookup("url"));                                                               // 213
        },                                                                                                             // 214
        "class": "js_projectFileThumbnail thumbnail img-responsive"                                                    // 215
      })), "\n		  "), "\n		" ];                                                                                        // 216
    }, function() {                                                                                                    // 217
      return [ "\n		  ", HTML.DIV({                                                                                    // 218
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 219
      }, "\n		  ", HTML.A({                                                                                            // 220
        href: "#",                                                                                                     // 221
        title: "Project Updates"                                                                                       // 222
      }, HTML.IMG({                                                                                                    // 223
        src: "no_preview.png",                                                                                         // 224
        "class": "js_projectFileThumbnail thumbnail img-responsive"                                                    // 225
      })), "\n		  "), "\n		" ];                                                                                        // 226
    }), "\n		\n	" ];                                                                                                   // 227
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="TClient_projectUpdateFilesModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="Client_projectUpdateFilesLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 229
                                                                                                                       // 230
Template.__checkName("TClient_apartmentPaymentFileGallery");                                                           // 231
Template["TClient_apartmentPaymentFileGallery"] = new Template("Template.TClient_apartmentPaymentFileGallery", (function() {
  var view = this;                                                                                                     // 233
  return [ HTML.DIV({                                                                                                  // 234
    "class": "container"                                                                                               // 235
  }, "\n  ", HTML.DIV({                                                                                                // 236
    "class": "row"                                                                                                     // 237
  }, "\n    ", HTML.DIV({                                                                                              // 238
    "class": "row"                                                                                                     // 239
  }, "\n	", Blaze.Each(function() {                                                                                    // 240
    return Spacebars.call(view.lookup("apartmentPaymentFiles"));                                                       // 241
  }, function() {                                                                                                      // 242
    return [ "\n		", Blaze.If(function() {                                                                             // 243
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 244
    }, function() {                                                                                                    // 245
      return [ "\n		  ", HTML.DIV({                                                                                    // 246
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 247
      }, "\n		  ", HTML.A({                                                                                            // 248
        href: "#",                                                                                                     // 249
        title: "Project Updates"                                                                                       // 250
      }, HTML.IMG({                                                                                                    // 251
        src: function() {                                                                                              // 252
          return Spacebars.mustache(view.lookup("url"));                                                               // 253
        },                                                                                                             // 254
        "class": "js_apartmentPaymentThumbnail thumbnail img-responsive"                                               // 255
      })), "\n		  "), "\n		" ];                                                                                        // 256
    }, function() {                                                                                                    // 257
      return [ "\n		  ", HTML.DIV({                                                                                    // 258
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 259
      }, "\n		  ", HTML.A({                                                                                            // 260
        href: "#",                                                                                                     // 261
        title: "Project Updates"                                                                                       // 262
      }, HTML.IMG({                                                                                                    // 263
        src: "no_preview.png",                                                                                         // 264
        "class": "js_apartmentPaymentThumbnail thumbnail img-responsive"                                               // 265
      })), "\n		  "), "\n		" ];                                                                                        // 266
    }), "\n		\n	" ];                                                                                                   // 267
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="Client_apartmentPaymentFilesModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="Client_apartmentPaymentFilesLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 269
                                                                                                                       // 270
Template.__checkName("TClient_apartmentUpdatesFileGallery");                                                           // 271
Template["TClient_apartmentUpdatesFileGallery"] = new Template("Template.TClient_apartmentUpdatesFileGallery", (function() {
  var view = this;                                                                                                     // 273
  return [ HTML.DIV({                                                                                                  // 274
    "class": "container"                                                                                               // 275
  }, "\n  ", HTML.DIV({                                                                                                // 276
    "class": "row"                                                                                                     // 277
  }, "\n    ", HTML.DIV({                                                                                              // 278
    "class": "row"                                                                                                     // 279
  }, "\n	", Blaze.Each(function() {                                                                                    // 280
    return Spacebars.call(view.lookup("apartmentUpdateFiles"));                                                        // 281
  }, function() {                                                                                                      // 282
    return [ "\n		", Blaze.If(function() {                                                                             // 283
      return Spacebars.dataMustache(view.lookup("isImage"), view.lookup("url"));                                       // 284
    }, function() {                                                                                                    // 285
      return [ "\n		  ", HTML.DIV({                                                                                    // 286
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 287
      }, "\n		  ", HTML.A({                                                                                            // 288
        href: "#",                                                                                                     // 289
        title: "Project Updates"                                                                                       // 290
      }, HTML.IMG({                                                                                                    // 291
        src: function() {                                                                                              // 292
          return Spacebars.mustache(view.lookup("url"));                                                               // 293
        },                                                                                                             // 294
        "class": "js_apartmentUpdatesThumbnail thumbnail img-responsive"                                               // 295
      })), "\n		  "), "\n		" ];                                                                                        // 296
    }, function() {                                                                                                    // 297
      return [ "\n		  ", HTML.DIV({                                                                                    // 298
        "class": "col-lg-3 col-sm-4 col-6"                                                                             // 299
      }, "\n		  ", HTML.A({                                                                                            // 300
        href: "#",                                                                                                     // 301
        title: "Project Updates"                                                                                       // 302
      }, HTML.IMG({                                                                                                    // 303
        src: "no_preview.png",                                                                                         // 304
        "class": "js_apartmentUpdatesThumbnail thumbnail img-responsive"                                               // 305
      })), "\n		  "), "\n		" ];                                                                                        // 306
    }), "\n		\n	" ];                                                                                                   // 307
  }), "\n    "), "\n  "), "\n"), HTML.Raw('\n<div id="Client_apartmentUpdatesFilesModal" class="modal fade" tabindex="-1" role="dialog">\n  <div class="modal-dialog">\n  <div class="modal-content">\n	<div class="modal-header">\n		<button type="button" class="close" data-dismiss="modal">×</button>\n		<h3 class="modal-title">File</h3>\n	</div>\n	<div class="modal-body">\n		<iframe src="" id="Client_apartmentUpdateFilesLoadIFrame" style="position: relative; height: 100%; width: 100%;">\n		</iframe>\n	</div>\n	<div class="modal-footer">\n		<button class="btn btn-default" data-dismiss="modal">Close</button>\n	</div>\n   </div>\n  </div>\n</div>') ];
}));                                                                                                                   // 309
                                                                                                                       // 310
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"projectManagement.html":["./template.projectManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/projectManagement.html                                                                                 //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.projectManagement.js");                                                           // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.projectManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/template.projectManagement.js                                                                          //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("TprojectUpdates");                                                                               // 2
Template["TprojectUpdates"] = new Template("Template.TprojectUpdates", (function() {                                   // 3
  var view = this;                                                                                                     // 4
  return Blaze._TemplateWith(function() {                                                                              // 5
    return {                                                                                                           // 6
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "ProjectUpdates")),                            // 7
      selector: Spacebars.call(view.lookup("projectIdSelector")),                                                      // 8
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 9
      id: Spacebars.call("projectUpdatesTable")                                                                        // 10
    };                                                                                                                 // 11
  }, function() {                                                                                                      // 12
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 13
  });                                                                                                                  // 14
}));                                                                                                                   // 15
                                                                                                                       // 16
Template.__checkName("TeditProjectUpdatesSection");                                                                    // 17
Template["TeditProjectUpdatesSection"] = new Template("Template.TeditProjectUpdatesSection", (function() {             // 18
  var view = this;                                                                                                     // 19
  return HTML.Raw('<button type="button" class="btn btn-xs btn-success js_manageProjectUpdateFiles">Files</button>\n  <button type="button" class="btn btn-xs btn-warning js_editProjectUpdate">Edit</button>\n  <button type="button" class="btn btn-xs js_removeProjectUpdate btn-danger">Delete</button>\n  <div class="modal fade" tabindex="-1" id="confirmDeleteProjectDialog" role="dialog">\n	  <div class="modal-dialog">\n		<div class="modal-content">\n		  <div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Confirm Delete</h4>\n		  </div>\n		  <div class="modal-body">\n			 <h4> Are you sure you want to delete the Project Update? You will loose all the files!</h4>\n			 <button type="button" class="btn btn-xs btn-warning js_cancelRemoveProjectUpdate">Cancel</button>\n			 <button type="button" class="btn btn-xs js_confirmRemoveProjectUpdate btn-danger">Confirm Delete</button>\n		  </div>\n		  <div class="modal-footer">\n		  </div>\n		</div><!-- /.modal-content -->\n	  </div><!-- /.modal-dialog -->\n	</div>');
}));                                                                                                                   // 21
                                                                                                                       // 22
Template.__checkName("TprojectUpdateForm");                                                                            // 23
Template["TprojectUpdateForm"] = new Template("Template.TprojectUpdateForm", (function() {                             // 24
  var view = this;                                                                                                     // 25
  return [ HTML.Raw('<button class="btn btn-primary js-createProjectUpdate">Add Project Update</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                             // 27
    tabindex: "-1",                                                                                                    // 28
    id: "createProjectUpdateModal",                                                                                    // 29
    role: "dialog"                                                                                                     // 30
  }, "\n	  ", HTML.DIV({                                                                                               // 31
    "class": "modal-dialog"                                                                                            // 32
  }, "\n		", HTML.DIV({                                                                                                // 33
    "class": "modal-content"                                                                                           // 34
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 36
  }, "\n			 ", HTML.DIV({                                                                                              // 37
    "class": "row"                                                                                                     // 38
  }, "\n				", HTML.DIV({                                                                                              // 39
    "class": "col-md-12"                                                                                               // 40
  }, "\n				", HTML.H4(" Project Name: ", Blaze.View("lookup:projectName", function() {                                // 41
    return Spacebars.mustache(view.lookup("projectName"));                                                             // 42
  })), "\n				"), "\n			 "), "\n			 ", HTML.DIV({                                                                      // 43
    "class": "row"                                                                                                     // 44
  }, "\n				", HTML.DIV({                                                                                              // 45
    "class": "col-md-12"                                                                                               // 46
  }, "\n					", Blaze._TemplateWith(function() {                                                                       // 47
    return {                                                                                                           // 48
      collection: Spacebars.call("ProjectUpdates"),                                                                    // 49
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 50
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 51
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 52
      id: Spacebars.call("addNewProjectUpdate"),                                                                       // 53
      type: Spacebars.call("method"),                                                                                  // 54
      meteormethod: Spacebars.call("createProjectUpdate"),                                                             // 55
      doc: Spacebars.call(view.lookup("defaultValues")),                                                               // 56
      resetOnSuccess: Spacebars.call(true)                                                                             // 57
    };                                                                                                                 // 58
  }, function() {                                                                                                      // 59
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 60
  }), "\n				"), "\n			 "), "\n			   \n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 62
                                                                                                                       // 63
Template.__checkName("TeditProjectUpdateForm");                                                                        // 64
Template["TeditProjectUpdateForm"] = new Template("Template.TeditProjectUpdateForm", (function() {                     // 65
  var view = this;                                                                                                     // 66
  return [ HTML.DIV({                                                                                                  // 67
    "class": "modal fade",                                                                                             // 68
    tabindex: "-1",                                                                                                    // 69
    id: "editProjectUpdateModal",                                                                                      // 70
    role: "dialog"                                                                                                     // 71
  }, "\n	  ", HTML.DIV({                                                                                               // 72
    "class": "modal-dialog"                                                                                            // 73
  }, "\n		", HTML.DIV({                                                                                                // 74
    "class": "modal-content"                                                                                           // 75
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Edit Update</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 77
  }, "		 \n			   ", Blaze._TemplateWith(function() {                                                                   // 78
    return {                                                                                                           // 79
      collection: Spacebars.call("ProjectUpdates"),                                                                    // 80
      template: Spacebars.call("bootstrap3-horizontal"),                                                               // 81
      "label-class": Spacebars.call("col-sm-2"),                                                                       // 82
      "input-col-class": Spacebars.call("col-sm-10"),                                                                  // 83
      id: Spacebars.call("editProjectUpdateForm"),                                                                     // 84
      type: Spacebars.call("method-update"),                                                                           // 85
      meteormethod: Spacebars.call("editProjectUpdate"),                                                               // 86
      resetOnSuccess: Spacebars.call(true),                                                                            // 87
      doc: Spacebars.call(view.lookup("editProjectUpdateDocument")),                                                   // 88
      singleMethodArgument: Spacebars.call(true)                                                                       // 89
    };                                                                                                                 // 90
  }, function() {                                                                                                      // 91
    return Spacebars.include(view.lookupTemplate("quickForm"));                                                        // 92
  }), "\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                                   // 94
                                                                                                                       // 95
Template.__checkName("TprojectManagement");                                                                            // 96
Template["TprojectManagement"] = new Template("Template.TprojectManagement", (function() {                             // 97
  var view = this;                                                                                                     // 98
  return HTML.DIV({                                                                                                    // 99
    "class": "row"                                                                                                     // 100
  }, "\n			", HTML.DIV({                                                                                               // 101
    "class": "jumbotron container"                                                                                     // 102
  }, "			\n			", HTML.DIV({                                                                                            // 103
    "class": "text-center"                                                                                             // 104
  }, "\n				", HTML.DIV({                                                                                              // 105
    role: "tabpanel"                                                                                                   // 106
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="project-tab" data-target="#pmProject" data-toggle="tab">Project Update</a></li>\n						<li role="presentation"><a id="pmTBD1-tab" data-target="#pmTBD1" data-toggle="tab">TBD</a></li>\n						<li role="presentation"><a id="pmTBD2-tab" data-target="#pmTBD2" data-toggle="tab">TBD</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                             // 108
  }, "					\n						", HTML.DIV({                                                                                       // 109
    role: "tabpanel",                                                                                                  // 110
    "class": "tab-pane active text-center",                                                                            // 111
    id: "pmProject"                                                                                                    // 112
  }, "\n							", HTML.DIV({                                                                                           // 113
    "class": "row"                                                                                                     // 114
  }, "\n								", HTML.DIV({                                                                                          // 115
    "class": "col-md-12"                                                                                               // 116
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectUpdateForm")), "\n									", Spacebars.include(view.lookupTemplate("TprojectUpdateFiles")), "\n								"), "\n							"), "\n							", HTML.DIV({
    "class": "row"                                                                                                     // 118
  }, "\n								", HTML.DIV({                                                                                          // 119
    "class": "col-md-12"                                                                                               // 120
  }, "\n								", HTML.DIV({                                                                                          // 121
    "class": "table-responsive"                                                                                        // 122
  }, "\n									", Spacebars.include(view.lookupTemplate("TprojectUpdates")), "\n								"), "\n									", Spacebars.include(view.lookupTemplate("TeditProjectUpdateForm")), "\n								"), "\n							"), "							\n						"), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="pmTBD1">\n							<div class="row">\n								<div class="col-md-12">						\n								</div>\n							</div>\n							\n						</div>'), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="pmTBD2">							\n							<div class="row">\n								<div class="col-md-12">\n								</div>\n							</div>\n							</div>'), "\n\n							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		");
}));                                                                                                                   // 124
                                                                                                                       // 125
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"adminFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./adminTemplate.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/adminFunction.js                                                                                       //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var _meteor = require('meteor/meteor');                                                                                // 1
                                                                                                                       //
var _templating = require('meteor/templating');                                                                        // 2
                                                                                                                       //
var _reactiveVar = require('meteor/reactive-var');                                                                     // 3
                                                                                                                       //
require('./adminTemplate.html');                                                                                       // 5
                                                                                                                       //
//Template.registerHelper("Collections", Collections);                                                                 //
_templating.Template.admin_section.events({                                                                            // 8
                                                                                                                       //
	'click .js-createProject': function () {                                                                              // 10
		function clickJsCreateProject(event) {                                                                               // 10
			$("#createProjectModal").modal();                                                                                   // 11
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateProject;                                                                                         //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.admin_section.helpers({                                                                           // 17
	'isProjectSelected': function () {                                                                                    // 18
		function isProjectSelected() {                                                                                       // 18
			return Session.get('pmProjectId');                                                                                  // 19
		}                                                                                                                    //
                                                                                                                       //
		return isProjectSelected;                                                                                            //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.projectList.events({                                                                              // 24
	'click tbody > tr': function () {                                                                                     // 25
		function clickTbodyTr(event) {                                                                                       // 25
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 26
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 27
			Session.set("pmProjectId", rowData._id);                                                                            // 28
			Session.set("pmProjectName", rowData.projectName);                                                                  // 29
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.projectListOptions.helpers({                                                                      // 35
	'projects': function () {                                                                                             // 36
		function projects() {                                                                                                // 36
			return ReactiveMethod.call('getAllProjects', Session.get("getProjectsRefreshFlag"));                                // 37
		}                                                                                                                    //
                                                                                                                       //
		return projects;                                                                                                     //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.projectListOptions.events({                                                                       // 41
	'change .projectNameDropDown': function () {                                                                          // 42
		function changeProjectNameDropDown(event) {                                                                          // 42
			var projectId = $(event.currentTarget).val();                                                                       // 43
			Session.set("selectedProjectId", projectId);                                                                        // 44
			console.log("ProjectId : " + projectId);                                                                            // 45
		}                                                                                                                    //
                                                                                                                       //
		return changeProjectNameDropDown;                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.apartmentForm.events({                                                                            // 49
	'click .js-createApartment': function () {                                                                            // 50
		function clickJsCreateApartment(event) {                                                                             // 50
			event.preventDefault();                                                                                             // 51
			$("#createApartmentModal").modal('show');                                                                           // 52
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateApartment;                                                                                       //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.apartmentList.helpers({                                                                           // 55
	projectIdSelector: function () {                                                                                      // 56
		function projectIdSelector() {                                                                                       // 56
			return { projectId: Session.get("selectedProjectId") }; // this could be pulled from a Session var or something that is reactive
		}                                                                                                                    // 56
                                                                                                                       //
		return projectIdSelector;                                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.apartmentList.events({                                                                            // 60
	'click tbody > tr': function () {                                                                                     // 61
		function clickTbodyTr(event) {                                                                                       // 61
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 62
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 63
			Session.set("selectedApartmentId", rowData._id);                                                                    // 64
			console.log("select apt" + Session.get("selectedApartmentId"));                                                     // 65
			Session.set("apartmentSelectFlag", new Date());                                                                     // 66
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.userForm.events({                                                                                 // 73
	'click .js-createUser': function () {                                                                                 // 74
		function clickJsCreateUser(event) {                                                                                  // 74
			event.preventDefault();                                                                                             // 75
			$("#createUserModal").modal('show');                                                                                // 76
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateUser;                                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.editApartmentSection.events({                                                                     // 80
	'click .js_editApartment': function () {                                                                              // 81
		function clickJs_editApartment(event) {                                                                              // 81
			event.preventDefault();                                                                                             // 82
			Session.set("getProjectsRefreshFlag", new Date());                                                                  // 83
			Session.set("editApartmentDocument", this);                                                                         // 84
			$("#editApartmentModal").modal('show');                                                                             // 85
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editApartment;                                                                                        //
	}(),                                                                                                                  //
	'click .js_removeApartment': function () {                                                                            // 87
		function clickJs_removeApartment(event) {                                                                            // 87
			event.preventDefault();                                                                                             // 88
			Session.set("getProjectsRefreshFlag", new Date());                                                                  // 89
			$("#confirmDeleteApartmentDialog").modal('show');                                                                   // 90
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeApartment;                                                                                      //
	}(),                                                                                                                  //
	'click .js_cancelRemoveApartment': function () {                                                                      // 92
		function clickJs_cancelRemoveApartment(event) {                                                                      // 92
			event.preventDefault();                                                                                             // 93
			$("#confirmDeleteApartmentDialog").modal('hide');                                                                   // 94
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_cancelRemoveApartment;                                                                                //
	}(),                                                                                                                  //
	'click .js_confirmRemoveApartment': function () {                                                                     // 96
		function clickJs_confirmRemoveApartment(event) {                                                                     // 96
			event.preventDefault();                                                                                             // 97
			$("#confirmDeleteApartmentDialog").modal('hide');                                                                   // 98
			if (Session.get("editApartmentDocument") != null) {                                                                 // 99
				_meteor.Meteor.call('removeApartment', Session.get("editApartmentDocument")._id);                                  // 100
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_confirmRemoveApartment;                                                                               //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.editProjectSection.events({                                                                       // 106
	'click .js_editProject': function () {                                                                                // 107
		function clickJs_editProject(event) {                                                                                // 107
			event.preventDefault();                                                                                             // 108
			Session.set("getProjectRefreshFlag", new Date());                                                                   // 109
			Session.set("editProjectDocument", this);                                                                           // 110
			$("#editProjectModal").modal('show');                                                                               // 111
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editProject;                                                                                          //
	}(),                                                                                                                  //
	'click .js_removeProject': function () {                                                                              // 113
		function clickJs_removeProject(event) {                                                                              // 113
			event.preventDefault();                                                                                             // 114
			Session.set("getProjectRefreshFlag", new Date());                                                                   // 115
			Session.set("editProjectDocument", this);                                                                           // 116
			$("#confirmDeleteProjectDialog").modal('show');                                                                     // 117
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeProject;                                                                                        //
	}(),                                                                                                                  //
	'click .js_cancelRemoveProject': function () {                                                                        // 119
		function clickJs_cancelRemoveProject(event) {                                                                        // 119
			event.preventDefault();                                                                                             // 120
			$("#confirmDeleteProjectDialog").modal('hide');                                                                     // 121
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_cancelRemoveProject;                                                                                  //
	}(),                                                                                                                  //
	'click .js_confirmRemoveProject': function () {                                                                       // 123
		function clickJs_confirmRemoveProject(event) {                                                                       // 123
			event.preventDefault();                                                                                             // 124
			$("#confirmDeleteProjectDialog").modal('hide');                                                                     // 125
			if (Session.get("editProjectDocument") != null) {                                                                   // 126
				_meteor.Meteor.call('removeProject', Session.get("editProjectDocument")._id);                                      // 127
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_confirmRemoveProject;                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.editApartmentForm.helpers({                                                                       // 133
	'editApartmentDocument': function () {                                                                                // 134
		function editApartmentDocument() {                                                                                   // 134
			if (Session.get("editApartmentDocument") != null) {                                                                 // 135
				return ReactiveMethod.call('getApartmentByApartmentId', Session.get("editApartmentDocument")._id, Session.get("getProjectsRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentDocument;                                                                                        //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.editProjectForm.helpers({                                                                         // 140
	'editProjectDocument': function () {                                                                                  // 141
		function editProjectDocument() {                                                                                     // 141
			if (Session.get("editProjectDocument") != null) {                                                                   // 142
				return ReactiveMethod.call('getProjectByProjectId', Session.get("editProjectDocument")._id, Session.get("getProjectsRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editProjectDocument;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.editUserForm.helpers({                                                                            // 147
	'editUserDocument': function () {                                                                                     // 148
		function editUserDocument() {                                                                                        // 148
			if (Session.get("editUserDocument") != null) {                                                                      // 149
				return ReactiveMethod.call('getUserByUserId', Session.get("editUserDocument")._id, Session.get("userRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editUserDocument;                                                                                             //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.editUserSection.events({                                                                          // 154
	'click .js_editUser': function () {                                                                                   // 155
		function clickJs_editUser(event) {                                                                                   // 155
			event.preventDefault();                                                                                             // 156
			Session.set("editUserDocument", this);                                                                              // 157
			$("#editUserModal").modal('show');                                                                                  // 158
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editUser;                                                                                             //
	}(),                                                                                                                  //
	'click .js_removeUser': function () {                                                                                 // 160
		function clickJs_removeUser(event) {                                                                                 // 160
			event.preventDefault();                                                                                             // 161
			Session.set("editUserDocument", this);                                                                              // 162
			$("#confirmDeleteUserDialog").modal('show');                                                                        // 163
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeUser;                                                                                           //
	}(),                                                                                                                  //
	'click .js_cancelRemoveUser': function () {                                                                           // 165
		function clickJs_cancelRemoveUser(event) {                                                                           // 165
			event.preventDefault();                                                                                             // 166
			$("#confirmDeleteUserDialog").modal('hide');                                                                        // 167
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_cancelRemoveUser;                                                                                     //
	}(),                                                                                                                  //
	'click .js_confirmRemoveUser': function () {                                                                          // 169
		function clickJs_confirmRemoveUser(event) {                                                                          // 169
			event.preventDefault();                                                                                             // 170
			$("#confirmDeleteUserDialog").modal('hide');                                                                        // 171
			if (Session.get("editUserDocument") != null) {                                                                      // 172
				_meteor.Meteor.call('removeProject', Session.get("editUserDocument")._id);                                         // 173
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_confirmRemoveUser;                                                                                    //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['editApartmentForm'], {                                                                             // 180
	onSuccess: function () {                                                                                              // 181
		function onSuccess(operation, result, template) {                                                                    // 181
			$("#editApartmentModal").modal('hide');                                                                             // 182
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['editProjectForm'], {                                                                               // 185
	onSuccess: function () {                                                                                              // 186
		function onSuccess(operation, result, template) {                                                                    // 186
			$("#editProjectModal").modal('hide');                                                                               // 187
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewProject'], {                                                                                 // 190
	onSuccess: function () {                                                                                              // 191
		function onSuccess(operation, result, template) {                                                                    // 191
			Session.set("getProjectsRefreshFlag", new Date());                                                                  // 192
			$("#createProjectModal").modal('hide');                                                                             // 193
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewApartment'], {                                                                               // 196
	onSuccess: function () {                                                                                              // 197
		function onSuccess(operation, result, template) {                                                                    // 197
			$("#createApartmentModal").modal('hide');                                                                           // 198
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewUser'], {                                                                                    // 201
	onSuccess: function () {                                                                                              // 202
		function onSuccess(operation, result, template) {                                                                    // 202
			$("#createUserModal").modal('hide');                                                                                // 203
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['editUserForm'], {                                                                                  // 206
	onSuccess: function () {                                                                                              // 207
		function onSuccess(operation, result, template) {                                                                    // 207
			$("#editUserModal").modal('hide');                                                                                  // 208
			Session.set("userRefreshFlag", new Date());                                                                         // 209
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"apartmentManagementFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./apartmentManagement.html","./fileManagement.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/apartmentManagementFunction.js                                                                         //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var _meteor = require('meteor/meteor');                                                                                // 1
                                                                                                                       //
var _templating = require('meteor/templating');                                                                        // 2
                                                                                                                       //
var _reactiveVar = require('meteor/reactive-var');                                                                     // 3
                                                                                                                       //
require('./apartmentManagement.html');                                                                                 // 5
                                                                                                                       //
require('./fileManagement.html');                                                                                      // 6
                                                                                                                       //
$.cloudinary.config({                                                                                                  // 8
	cloud_name: "aparto"                                                                                                  // 9
});                                                                                                                    //
                                                                                                                       //
_templating.Template.assignedUsers.helpers({                                                                           // 12
	userIdSelector: function () {                                                                                         // 13
		function userIdSelector() {                                                                                          // 13
			userIdList = ReactiveMethod.call('getUsersByApartmentId', Session.get("selectedApartmentId"), Session.get("apartmentSelectFlag"));
			return { '_id': { $in: userIdList } };                                                                              // 15
		}                                                                                                                    //
                                                                                                                       //
		return userIdSelector;                                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.userAssignment.events({                                                                           // 19
	'click .js-assignOwners': function () {                                                                               // 20
		function clickJsAssignOwners(event) {                                                                                // 20
			$("#assignUserModal").modal('show');                                                                                // 21
		}                                                                                                                    //
                                                                                                                       //
		return clickJsAssignOwners;                                                                                          //
	}(),                                                                                                                  //
	'click .js_closeAssignUserModal': function () {                                                                       // 23
		function clickJs_closeAssignUserModal(event) {                                                                       // 23
			$("#assignUserModal").modal('hide');                                                                                // 24
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_closeAssignUserModal;                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.assignUser.events({                                                                               // 27
	'click .js_assignUserToApartment': function () {                                                                      // 28
		function clickJs_assignUserToApartment(event) {                                                                      // 28
			apartmentId = Session.get("selectedApartmentId");                                                                   // 29
			userId = this._id;                                                                                                  // 30
			console.log(apartmentId + " User:" + userId);                                                                       // 31
			if (apartmentId != null && userId != null) {                                                                        // 32
				_meteor.Meteor.call('addUserApartment', userId, apartmentId);                                                      // 33
			}                                                                                                                   //
			Session.set("apartmentSelectFlag", new Date());                                                                     // 35
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_assignUserToApartment;                                                                                //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.removeUserAssignSection.events({                                                                  // 39
	'click .js_removeUserAssignment': function () {                                                                       // 40
		function clickJs_removeUserAssignment(event) {                                                                       // 40
			Session.set("selectedUserId", this._id);                                                                            // 41
			$("#confirmRemoveUserAssignmentModal").modal('show');                                                               // 42
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeUserAssignment;                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.removeAssignmentModal.events({                                                                    // 46
	'click .js_cancelRemoveUserAssignment': function () {                                                                 // 47
		function clickJs_cancelRemoveUserAssignment(event) {                                                                 // 47
			$("#confirmRemoveUserAssignmentModal").modal('hide');                                                               // 48
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_cancelRemoveUserAssignment;                                                                           //
	}(),                                                                                                                  //
	'click .js_confirmRemoveUserAssignment': function () {                                                                // 50
		function clickJs_confirmRemoveUserAssignment(event) {                                                                // 50
			userId = Session.get("selectedUserId");                                                                             // 51
			apartmentId = Session.get("selectedApartmentId");                                                                   // 52
			if (userId != null && apartmentId != null) {                                                                        // 53
				_meteor.Meteor.call('removeUserAssignment', userId, apartmentId);                                                  // 54
				Session.set("apartmentSelectFlag", new Date());                                                                    // 55
			}                                                                                                                   //
			$("#confirmRemoveUserAssignmentModal").modal('hide');                                                               // 57
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_confirmRemoveUserAssignment;                                                                          //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentDocuments.helpers({                                                                     // 62
	apartmentIdSelector: function () {                                                                                    // 63
		function apartmentIdSelector() {                                                                                     // 63
			return { apartmentId: Session.get("selectedApartmentId") };                                                         // 64
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentDocuments.events({                                                                      // 68
	'click tbody > tr': function () {                                                                                     // 69
		function clickTbodyTr(event) {                                                                                       // 69
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 70
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 71
			Session.set("am_apartmentDocumentId", rowData._id);                                                                 // 72
			Session.set("apartmentRefreshFlag", new Date());                                                                    // 73
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}(),                                                                                                                  //
	'click .sampleModalShow': function () {                                                                               // 75
		function clickSampleModalShow() {                                                                                    // 75
			$("#editApartmentDocumentModal").modal('show');                                                                     // 76
		}                                                                                                                    //
                                                                                                                       //
		return clickSampleModalShow;                                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TCreateApartmentDocument.helpers({                                                                // 80
	defaultApartmentDocument: function () {                                                                               // 81
		function defaultApartmentDocument() {                                                                                // 81
			return { apartmentId: Session.get('selectedApartmentId') };                                                         // 82
		}                                                                                                                    //
                                                                                                                       //
		return defaultApartmentDocument;                                                                                     //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['addNewApartmentDocumentForm'], {                                                                   // 86
	onSuccess: function () {                                                                                              // 87
		function onSuccess(operation, result, template) {                                                                    // 87
			$("#createApartmentDocumentModal").modal('hide');                                                                   // 88
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['editApartmentDocumentForm'], {                                                                     // 91
	onSuccess: function () {                                                                                              // 92
		function onSuccess(operation, result, template) {                                                                    // 92
			$("#editApartmentDocumentModal").modal('hide');                                                                     // 93
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TCreateApartmentDocument.events({                                                                 // 97
	'click .js-createApartmentDocument': function () {                                                                    // 98
		function clickJsCreateApartmentDocument() {                                                                          // 98
			$("#createApartmentDocumentModal").modal('show');                                                                   // 99
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateApartmentDocument;                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentDocumentForm.helpers({                                                              // 103
	'editApartmentUpdateDocument': function () {                                                                          // 104
		function editApartmentUpdateDocument() {                                                                             // 104
			if (Session.get("am_apartmentDocumentId") != null) {                                                                // 105
				console.log("Setting apt to" + Session.get("am_apartmentDocumentId"));                                             // 106
				return ReactiveMethod.call('getApartmentDocumentByApartmentDocumentId', Session.get("am_apartmentDocumentId"), Session.get("apartmentRefreshFlag"));
			}                                                                                                                   //
			return null;                                                                                                        // 109
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentUpdateDocument;                                                                                  //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TeditApartmentDocumentsSection.events({                                                           // 112
	'click .js_manageApartmentDocumentFiles': function () {                                                               // 113
		function clickJs_manageApartmentDocumentFiles() {                                                                    // 113
			$("#s").modal('show');                                                                                              // 114
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_manageApartmentDocumentFiles;                                                                         //
	}(),                                                                                                                  //
	'click .js_editApartmentDocument': function () {                                                                      // 116
		function clickJs_editApartmentDocument() {                                                                           // 116
			$("#editApartmentDocumentModal").modal('show');                                                                     // 117
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editApartmentDocument;                                                                                //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TApartmentDocumentsFileGallery.helpers({                                                          // 120
	'apartmentDocumentFiles': function () {                                                                               // 121
		function apartmentDocumentFiles() {                                                                                  // 121
			if (Session.get("am_apartmentDocumentId") != null) {                                                                // 122
				return ReactiveMethod.call('getApartmentDocumentsFilesByDocumentId', Session.get("am_apartmentDocumentId"), Session.get("apartmentRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return apartmentDocumentFiles;                                                                                       //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 126
		function isImage(url) {                                                                                              // 126
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 127
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TApartmentDocumentsFileGallery.events({                                                           // 130
	'click .js_apartmentDocumentThumbnail': function () {                                                                 // 131
		function clickJs_apartmentDocumentThumbnail() {                                                                      // 131
			$("#apartmentDocumentLoadIFrame").attr('src', this.url);                                                            // 132
			$('#apartmentDocumentFileGalleryModal').modal('show');                                                              // 133
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_apartmentDocumentThumbnail;                                                                           //
	}(),                                                                                                                  //
	'click .js_apartmentDocumentFileDelete': function () {                                                                // 135
		function clickJs_apartmentDocumentFileDelete() {                                                                     // 135
			var result = confirm("Are you sure you want to delete the document?");                                              // 136
			if (result) {                                                                                                       // 137
				//console.log(this);                                                                                               //
				Cloudinary['delete'](this.publicId);                                                                               // 139
				_meteor.Meteor.call("removeApartmentDocumentFile", this._id);                                                      // 140
				Session.set("apartmentRefreshFlag", new Date());                                                                   // 141
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_apartmentDocumentFileDelete;                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TApartmentDocumentFiles.events({                                                                  // 147
	'click .js_uploadApartmentDocument': function () {                                                                    // 148
		function clickJs_uploadApartmentDocument() {                                                                         // 148
			var ApartmentDocumentFile = {};                                                                                     // 149
			var file = $('#uploadApartmentDocument').get(0).files[0];                                                           // 150
			var result = {};                                                                                                    // 151
			console.log(file);                                                                                                  // 152
			Cloudinary._upload_file(file, { "folder": "apartmentDocuments", "allowed_formats": allowableFormats, "resource_type": "auto" }, function (err, res) {
				if (err) {                                                                                                         // 154
					console.log(err);                                                                                                 // 155
				} else {                                                                                                           //
					console.log(res);                                                                                                 // 158
					result.url = res.url;                                                                                             // 159
					result.publicId = res.public_id;                                                                                  // 160
					console.log(result);                                                                                              // 161
					apartmentId = Session.get("selectedApartmentId");                                                                 // 162
					apartmentDocumentId = Session.get("am_apartmentDocumentId");                                                      // 163
					if (apartmentId != null && apartmentDocumentId != null) {                                                         // 164
						ApartmentDocumentFile.apartmentId = apartmentId;                                                                 // 165
						ApartmentDocumentFile.apartmentDocumentId = apartmentDocumentId;                                                 // 166
						ApartmentDocumentFile.publicId = res.public_id;                                                                  // 167
						ApartmentDocumentFile.url = res.url;                                                                             // 168
						ApartmentDocumentFile.fullRes = res;                                                                             // 169
						_meteor.Meteor.call("createApartmentDocumentFile", ApartmentDocumentFile);                                       // 170
						Session.set("apartmentRefreshFlag", new Date());                                                                 // 171
					}                                                                                                                 //
				}                                                                                                                  //
			});                                                                                                                 //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_uploadApartmentDocument;                                                                              //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
//     Apartment Updates section                                                                                       //
                                                                                                                       //
_templating.Template.TapartmentUpdateFiles.events({                                                                    // 181
	'click .js_uploadImage': function () {                                                                                // 182
		function clickJs_uploadImage() {                                                                                     // 182
			var ApartmentUpdateFile = {};                                                                                       // 183
			var file = $('#apartmentUpdateFile').get(0).files[0];                                                               // 184
			console.log("Apartment File Update Started");                                                                       // 185
			var result = {};                                                                                                    // 186
			console.log(file);                                                                                                  // 187
			Cloudinary._upload_file(file, { "folder": "apartmentUpdates", "allowed_formats": allowableFormats, "resource_type": "auto" }, function (err, res) {
				if (err) {                                                                                                         // 189
					console.log(err);                                                                                                 // 190
				} else {                                                                                                           //
					console.log(res);                                                                                                 // 193
					result.url = res.url;                                                                                             // 194
					result.publicId = res.public_id;                                                                                  // 195
					console.log(result);                                                                                              // 196
					apartmentId = Session.get("selectedApartmentId");                                                                 // 197
					apartmentUpdateId = Session.get("am_apartmentUpdateId");                                                          // 198
					if (apartmentId != null && apartmentUpdateId != null) {                                                           // 199
						ApartmentUpdateFile.apartmentId = apartmentId;                                                                   // 200
						ApartmentUpdateFile.apartmentUpdateId = apartmentUpdateId;                                                       // 201
						ApartmentUpdateFile.publicId = res.public_id;                                                                    // 202
						ApartmentUpdateFile.url = res.url;                                                                               // 203
						ApartmentUpdateFile.fullRes = res;                                                                               // 204
						_meteor.Meteor.call("createApartmentUpdateFile", ApartmentUpdateFile);                                           // 205
						Session.set("apartmentRefreshFlag", new Date());                                                                 // 206
					}                                                                                                                 //
				}                                                                                                                  //
			});                                                                                                                 //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_uploadImage;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TApartmentUpdateFileGallery.helpers({                                                             // 214
	'apartmentUpdateFiles': function () {                                                                                 // 215
		function apartmentUpdateFiles() {                                                                                    // 215
			return ReactiveMethod.call('getApartmentUpdateImageByUpdateId', Session.get('am_apartmentUpdateId'), Session.get("apartmentRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return apartmentUpdateFiles;                                                                                         //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 218
		function isImage(url) {                                                                                              // 218
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 219
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TApartmentUpdateFileGallery.events({                                                              // 224
	'click .js_imageThumbnail': function () {                                                                             // 225
		function clickJs_imageThumbnail() {                                                                                  // 225
			console.log("Thumbnail Opening");                                                                                   // 226
			$("#apartment_fileLoadIFrame").attr('src', this.url);                                                               // 227
			$('#apartmentFileLoadModal').modal('show');                                                                         // 228
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_imageThumbnail;                                                                                       //
	}(),                                                                                                                  //
	'click .js_apartmentUpdateDeleteFile': function () {                                                                  // 230
		function clickJs_apartmentUpdateDeleteFile() {                                                                       // 230
			var result = confirm("Are you sure you want to delete the Update File?");                                           // 231
			if (result) {                                                                                                       // 232
				console.log(this);                                                                                                 // 233
				Cloudinary['delete'](this.publicId);                                                                               // 234
				_meteor.Meteor.call("removeApartmentUpdateImage", this._id);                                                       // 235
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_apartmentUpdateDeleteFile;                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentUpdateForm.helpers({                                                                    // 240
	'defaultApartmentUpdate': function () {                                                                               // 241
		function defaultApartmentUpdate() {                                                                                  // 241
			return { 'apartmentId': Session.get('selectedApartmentId') };                                                       // 242
		}                                                                                                                    //
                                                                                                                       //
		return defaultApartmentUpdate;                                                                                       //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TapartmentUpdateForm.events({                                                                     // 245
	'click .js-createApartmentUpdate': function () {                                                                      // 246
		function clickJsCreateApartmentUpdate() {                                                                            // 246
			$("#createApartmentUpdateModal").modal('show');                                                                     // 247
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateApartmentUpdate;                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentUpdateForm.helpers({                                                                // 253
	'editApartmentUpdate': function () {                                                                                  // 254
		function editApartmentUpdate() {                                                                                     // 254
			if (Session.get('selectedApartmentId')) {                                                                           // 255
				return ReactiveMethod.call('getApartmentUpdateByApartmentUpdateId', Session.get('am_apartmentUpdateId'), Session.get("apartmentRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentUpdate;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentUpdates.events({                                                                        // 261
	'click tbody > tr': function () {                                                                                     // 262
		function clickTbodyTr(event) {                                                                                       // 262
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 263
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 264
			Session.set("am_apartmentUpdateId", rowData._id);                                                                   // 265
			Session.set("apartmentRefreshFlag", new Date());                                                                    // 266
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentUpdates.helpers({                                                                       // 270
	apartmentIdSelector: function () {                                                                                    // 271
		function apartmentIdSelector() {                                                                                     // 271
			return { apartmentId: Session.get("selectedApartmentId") };                                                         // 272
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentUpdatesSection.events({                                                             // 276
	'click .js_manageApartmentUpdateFiles': function () {                                                                 // 277
		function clickJs_manageApartmentUpdateFiles() {                                                                      // 277
			$("#apartmentUpdatesFilesModal").modal('show');                                                                     // 278
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_manageApartmentUpdateFiles;                                                                           //
	}(),                                                                                                                  //
	'click .js_editApartmentUpdate': function () {                                                                        // 280
		function clickJs_editApartmentUpdate() {                                                                             // 280
			$("#editApartmentUpdateModal").modal('show');                                                                       // 281
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editApartmentUpdate;                                                                                  //
	}(),                                                                                                                  //
	'click .js_removeApartmentUpdate': function () {                                                                      // 283
		function clickJs_removeApartmentUpdate() {                                                                           // 283
			var result = confirm("Are you sure you want to delete the update, you will lose all the files?");                   // 284
			if (result) {                                                                                                       // 285
				_meteor.Meteor.call("removeApartmentUpdate", this._id);                                                            // 286
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeApartmentUpdate;                                                                                //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['editApartmentUpdateForm'], {                                                                       // 293
	onSuccess: function () {                                                                                              // 294
		function onSuccess(operation, result, template) {                                                                    // 294
			$("#editApartmentUpdateModal").modal('hide');                                                                       // 295
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewApartmentUpdateForm'], {                                                                     // 298
	onSuccess: function () {                                                                                              // 299
		function onSuccess(operation, result, template) {                                                                    // 299
			$("#createApartmentUpdateModal").modal('hide');                                                                     // 300
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
//End Apartment Updates                                                                                                //
                                                                                                                       //
// Apartment Payment Section                                                                                           //
                                                                                                                       //
_templating.Template.TapartmentPayments.helpers({                                                                      // 308
	apartmentIdSelector: function () {                                                                                    // 309
		function apartmentIdSelector() {                                                                                     // 309
			return { apartmentId: Session.get("selectedApartmentId") };                                                         // 310
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentPaymentForm.helpers({                                                                   // 314
	'defaultApartmentPayment': function () {                                                                              // 315
		function defaultApartmentPayment() {                                                                                 // 315
			return { 'apartmentId': Session.get('selectedApartmentId') };                                                       // 316
		}                                                                                                                    //
                                                                                                                       //
		return defaultApartmentPayment;                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TapartmentPaymentForm.events({                                                                    // 319
	'click .js-createApartmentPayment': function () {                                                                     // 320
		function clickJsCreateApartmentPayment() {                                                                           // 320
			$("#createApartmentPaymentModal").modal('show');                                                                    // 321
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateApartmentPayment;                                                                                //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentPaymentForm.helpers({                                                               // 327
	'editApartmentPayment': function () {                                                                                 // 328
		function editApartmentPayment() {                                                                                    // 328
			if (Session.get('selectedApartmentId')) {                                                                           // 329
				return ReactiveMethod.call('getApartmentPaymentByApartmentPaymentId', Session.get('am_apartmentPaymentId'), Session.get("apartmentRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentPayment;                                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentPayments.events({                                                                       // 335
	'click tbody > tr': function () {                                                                                     // 336
		function clickTbodyTr(event) {                                                                                       // 336
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 337
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 338
			Session.set("am_apartmentPaymentId", rowData._id);                                                                  // 339
			Session.set("apartmentRefreshFlag", new Date());                                                                    // 340
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentPaymentSection.events({                                                             // 344
	'click .js_manageApartmentPaymentFiles': function () {                                                                // 345
		function clickJs_manageApartmentPaymentFiles() {                                                                     // 345
			//$("#createApartmentPaymentModal").modal('show');                                                                  //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_manageApartmentPaymentFiles;                                                                          //
	}(),                                                                                                                  //
	'click .js_editApartmentPayment': function () {                                                                       // 348
		function clickJs_editApartmentPayment() {                                                                            // 348
			$("#editApartmentPaymentModal").modal('show');                                                                      // 349
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editApartmentPayment;                                                                                 //
	}(),                                                                                                                  //
	'click .js_removeApartmentPayment': function () {                                                                     // 351
		function clickJs_removeApartmentPayment() {                                                                          // 351
			var result = confirm("Are you sure you want to delete the payment, you will lose all the files?");                  // 352
			if (result) {                                                                                                       // 353
				_meteor.Meteor.call("removeApartmentPayment", this._id);                                                           // 354
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeApartmentPayment;                                                                               //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['editApartmentPaymentForm'], {                                                                      // 361
	onSuccess: function () {                                                                                              // 362
		function onSuccess(operation, result, template) {                                                                    // 362
			$("#editApartmentPaymentModal").modal('hide');                                                                      // 363
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewApartmentPaymentForm'], {                                                                    // 366
	onSuccess: function () {                                                                                              // 367
		function onSuccess(operation, result, template) {                                                                    // 367
			$("#createApartmentPaymentModal").modal('hide');                                                                    // 368
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
// Apartment Service Request Section                                                                                   //
                                                                                                                       //
_templating.Template.TapartmentServiceRequests.helpers({                                                               // 374
	apartmentIdSelector: function () {                                                                                    // 375
		function apartmentIdSelector() {                                                                                     // 375
			return { apartmentId: Session.get("selectedApartmentId") };                                                         // 376
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentServiceRequestForm.helpers({                                                            // 380
	'defaultApartmentServiceRequest': function () {                                                                       // 381
		function defaultApartmentServiceRequest() {                                                                          // 381
			return { 'apartmentId': Session.get('selectedApartmentId') };                                                       // 382
		}                                                                                                                    //
                                                                                                                       //
		return defaultApartmentServiceRequest;                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TapartmentServiceRequestForm.events({                                                             // 385
	'click .js-createApartmentServiceRequest': function () {                                                              // 386
		function clickJsCreateApartmentServiceRequest() {                                                                    // 386
			$("#createApartmentServiceRequestModal").modal('show');                                                             // 387
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateApartmentServiceRequest;                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentServiceRequestForm.helpers({                                                        // 393
	'editApartmentServiceRequest': function () {                                                                          // 394
		function editApartmentServiceRequest() {                                                                             // 394
			if (Session.get('selectedApartmentId')) {                                                                           // 395
				return ReactiveMethod.call('getApartmentServiceRequestByApartmentServiceRequestId', Session.get('am_apartmentServiceRequestId'), Session.get("apartmentRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentServiceRequest;                                                                                  //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TapartmentServiceRequests.events({                                                                // 401
	'click tbody > tr': function () {                                                                                     // 402
		function clickTbodyTr(event) {                                                                                       // 402
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 403
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 404
			Session.set("am_apartmentServiceRequestId", rowData._id);                                                           // 405
			Session.set("apartmentRefreshFlag", new Date());                                                                    // 406
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditApartmentServiceRequestSection.events({                                                      // 410
	'click .js_editApartmentServiceRequest': function () {                                                                // 411
		function clickJs_editApartmentServiceRequest() {                                                                     // 411
			$("#editApartmentServiceRequestModal").modal('show');                                                               // 412
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editApartmentServiceRequest;                                                                          //
	}(),                                                                                                                  //
	'click .js_removeApartmentPayment': function () {                                                                     // 414
		function clickJs_removeApartmentPayment() {                                                                          // 414
			var result = confirm("Are you sure you want to delete the request?");                                               // 415
			if (result) {                                                                                                       // 416
				_meteor.Meteor.call("removeApartmentServiceRequest", this._id);                                                    // 417
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_removeApartmentPayment;                                                                               //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['editApartmentServiceRequestForm'], {                                                               // 424
	onSuccess: function () {                                                                                              // 425
		function onSuccess(operation, result, template) {                                                                    // 425
			$("#editApartmentServiceRequestModal").modal('hide');                                                               // 426
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['addNewApartmentServiceRequestForm'], {                                                             // 429
	onSuccess: function () {                                                                                              // 430
		function onSuccess(operation, result, template) {                                                                    // 430
			$("#createApartmentServiceRequestModal").modal('hide');                                                             // 431
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"projectManagementFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./projectManagement.html","./fileManagement.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/admin/projectManagementFunction.js                                                                           //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var _meteor = require('meteor/meteor');                                                                                // 1
                                                                                                                       //
var _templating = require('meteor/templating');                                                                        // 2
                                                                                                                       //
var _reactiveVar = require('meteor/reactive-var');                                                                     // 3
                                                                                                                       //
require('./projectManagement.html');                                                                                   // 5
                                                                                                                       //
require('./fileManagement.html');                                                                                      // 6
                                                                                                                       //
$.cloudinary.config({                                                                                                  // 8
	cloud_name: "aparto"                                                                                                  // 9
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TprojectUpdateForm.events({                                                                       // 16
	'click .js-createProjectUpdate': function () {                                                                        // 17
		function clickJsCreateProjectUpdate(event) {                                                                         // 17
			$("#createProjectUpdateModal").modal('show');                                                                       // 18
		}                                                                                                                    //
                                                                                                                       //
		return clickJsCreateProjectUpdate;                                                                                   //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditProjectUpdatesSection.events({                                                               // 23
	'click .js_editProjectUpdate': function () {                                                                          // 24
		function clickJs_editProjectUpdate(event) {                                                                          // 24
			event.preventDefault();                                                                                             // 25
			Session.set("editProjectUpdateRecord", this);                                                                       // 26
			Session.set("getProjectUpdatesRefreshFlag", new Date());                                                            // 27
			$("#editProjectUpdateModal").modal('show');                                                                         // 28
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_editProjectUpdate;                                                                                    //
	}(),                                                                                                                  //
                                                                                                                       //
	'click .js_manageProjectUpdateFiles': function () {                                                                   // 31
		function clickJs_manageProjectUpdateFiles(event) {                                                                   // 31
			event.preventDefault();                                                                                             // 32
			console.log("Opening File Uploader");                                                                               // 33
			$("#projectUpdateFilesModal").modal('show');                                                                        // 34
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_manageProjectUpdateFiles;                                                                             //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TprojectUpdateFiles.events({                                                                      // 39
	'click .js_closeProjectUpdateFilesModal': function () {                                                               // 40
		function clickJs_closeProjectUpdateFilesModal(event) {                                                               // 40
			event.preventDefault();                                                                                             // 41
			$("#projectUpdateFilesModal").modal('hide');                                                                        // 42
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_closeProjectUpdateFilesModal;                                                                         //
	}(),                                                                                                                  //
	'click .js_uploadImage': function () {                                                                                // 44
		function clickJs_uploadImage(event) {                                                                                // 44
			var projectUpdateImage = {};                                                                                        // 45
			var file = $('#uploadedFile').get(0).files[0];                                                                      // 46
			var result = {};                                                                                                    // 47
			console.log(file);                                                                                                  // 48
			Cloudinary._upload_file(file, { "folder": "updates", "allowed_formats": allowableFormats, "resource_type": "auto" }, function (err, res) {
				if (err) {                                                                                                         // 50
					console.log(err);                                                                                                 // 51
				} else {                                                                                                           //
					console.log(res);                                                                                                 // 54
					result.url = res.url;                                                                                             // 55
					result.publicId = res.public_id;                                                                                  // 56
					console.log(result);                                                                                              // 57
					projectId = Session.get("pmProjectId");                                                                           // 58
					projectUpdateId = Session.get("pmProjectUpdateId");                                                               // 59
					if (projectId != null && projectUpdateId != null) {                                                               // 60
						projectUpdateImage.projectId = projectId;                                                                        // 61
						projectUpdateImage.projectUpdateId = projectUpdateId;                                                            // 62
						projectUpdateImage.publicId = res.public_id;                                                                     // 63
						projectUpdateImage.url = res.url;                                                                                // 64
						projectUpdateImage.fullRes = res;                                                                                // 65
						_meteor.Meteor.call("createProjectUpdateImage", projectUpdateImage);                                             // 66
					}                                                                                                                 //
				}                                                                                                                  //
			});                                                                                                                 //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_uploadImage;                                                                                          //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TprojectUpdateFiles.helpers({                                                                     // 75
	'uploadedFiles': function () {                                                                                        // 76
		function uploadedFiles(event) {                                                                                      // 76
			projectUpdateId = Session.get("pmProjectUpdateId");                                                                 // 77
			return _meteor.Meteor.call("getProjectUpdateImageByUpdateId", projectUpdateId);                                     // 78
		}                                                                                                                    //
                                                                                                                       //
		return uploadedFiles;                                                                                                //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TeditProjectUpdateForm.helpers({                                                                  // 82
	'editProjectUpdateDocument': function () {                                                                            // 83
		function editProjectUpdateDocument() {                                                                               // 83
			if (Session.get("editProjectUpdateRecord") != null) {                                                               // 84
				return ReactiveMethod.call('getProjectUpdateByProjectUpdateId', Session.get("editProjectUpdateRecord")._id, Session.get("getProjectUpdatesRefreshFlag"));
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editProjectUpdateDocument;                                                                                    //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.TprojectUpdateForm.helpers({                                                                      // 90
	'projectName': function () {                                                                                          // 91
		function projectName() {                                                                                             // 91
			return Session.get("pmProjectName");                                                                                // 92
		}                                                                                                                    //
                                                                                                                       //
		return projectName;                                                                                                  //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TprojectUpdates.helpers({                                                                         // 98
	projectIdSelector: function () {                                                                                      // 99
		function projectIdSelector() {                                                                                       // 99
			return { projectId: Session.get("pmProjectId") }; // this could be pulled from a Session var or something that is reactive
		}                                                                                                                    // 99
                                                                                                                       //
		return projectIdSelector;                                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TprojectUpdates.events({                                                                          // 103
	'click tbody > tr': function () {                                                                                     // 104
		function clickTbodyTr(event) {                                                                                       // 104
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 105
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 106
			Session.set("pmProjectUpdateId", rowData._id);                                                                      // 107
			Session.set("getProjectUpdatesRefreshFlag", new Date());                                                            // 108
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TProjectUpdateFileGallery.helpers({                                                               // 111
	'projectUpdateFiles': function () {                                                                                   // 112
		function projectUpdateFiles() {                                                                                      // 112
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId", Session.get("pmProjectUpdateId"), Session.get("getProjectUpdatesRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return projectUpdateFiles;                                                                                           //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 115
		function isImage(url) {                                                                                              // 115
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 116
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TProjectUpdateFileGallery.events({                                                                // 119
	'click .js_imageThumbnail': function () {                                                                             // 120
		function clickJs_imageThumbnail() {                                                                                  // 120
			$("#fileLoadIFrame").attr('src', this.url);                                                                         // 121
			//var title = $(this).parent('a').attr("title");                                                                    //
			//console.log(this);                                                                                                //
			//$('#projectFileLoadModal .modal-title').html(title);                                                              //
			//$($(this).parents('div').html()).appendTo('#projectFileLoadModal .modal-body');                                   //
			$('#projectFileLoadModal').modal('show');                                                                           // 120
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_imageThumbnail;                                                                                       //
	}(),                                                                                                                  //
	'click .js_projectUpdateDeleteFile': function () {                                                                    // 128
		function clickJs_projectUpdateDeleteFile() {                                                                         // 128
			var result = confirm("Are you sure you want to delete the document?");                                              // 129
			if (result) {                                                                                                       // 130
				console.log(this);                                                                                                 // 131
				Cloudinary['delete'](this.publicId);                                                                               // 132
				_meteor.Meteor.call("removeProjectUpdateImage", this._id);                                                         // 133
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_projectUpdateDeleteFile;                                                                              //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
AutoForm.addHooks(['addNewProjectUpdate'], {                                                                           // 143
	onSuccess: function () {                                                                                              // 144
		function onSuccess(operation, result, template) {                                                                    // 144
			$("#createProjectUpdateModal").modal('hide');                                                                       // 145
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
AutoForm.addHooks(['editProjectUpdateForm'], {                                                                         // 148
	onSuccess: function () {                                                                                              // 149
		function onSuccess(operation, result, template) {                                                                    // 149
			$("#editProjectUpdateModal").modal('hide');                                                                         // 150
			Session.set("getProjectUpdatesRefreshFlag", new Date());                                                            // 151
		}                                                                                                                    //
                                                                                                                       //
		return onSuccess;                                                                                                    //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TprojectUpdateForm.helpers({                                                                      // 154
	defaultValues: function () {                                                                                          // 155
		function defaultValues() {                                                                                           // 155
			return { projectId: Session.get('pmProjectId') };                                                                   // 156
		}                                                                                                                    //
                                                                                                                       //
		return defaultValues;                                                                                                //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"credentials":{"template.credentialTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/credentials/template.credentialTemplate.js                                                                   //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("credential_Section");                                                                            // 2
Template["credential_Section"] = new Template("Template.credential_Section", (function() {                             // 3
  var view = this;                                                                                                     // 4
  return [ HTML.Raw('<div class="">\n		<h4 class="text-center" id="loginModalLabel">Sign In/Login</h4>\n	</div>\n		'), HTML.DIV({
    "class": "container jumbotron"                                                                                     // 6
  }, "\n			", HTML.DIV({                                                                                               // 7
    "class": "text-center"                                                                                             // 8
  }, "\n				", HTML.DIV({                                                                                              // 9
    role: "tabpanel",                                                                                                  // 10
    "class": "login-tab"                                                                                               // 11
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="signin-taba" data-target="#login" data-toggle="tab">Sign In</a></li>\n						<li role="presentation"><a id="forgetpass-taba" data-target="#forget_password" data-toggle="tab">Forgot Password</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                             // 13
  }, "\n						", HTML.DIV({                                                                                            // 14
    role: "tabpanel",                                                                                                  // 15
    "class": "tab-pane active text-center",                                                                            // 16
    id: "login"                                                                                                        // 17
  }, "\n							", Spacebars.include(view.lookupTemplate("login")), "\n						"), "\n						", HTML.DIV({                 // 18
    role: "tabpanel",                                                                                                  // 19
    "class": "tab-pane text-center",                                                                                   // 20
    id: "forget_password"                                                                                              // 21
  }, "\n							", Spacebars.include(view.lookupTemplate("password_recovery")), "							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		") ];
}));                                                                                                                   // 23
                                                                                                                       // 24
Template.__checkName("login");                                                                                         // 25
Template["login"] = new Template("Template.login", (function() {                                                       // 26
  var view = this;                                                                                                     // 27
  return HTML.Raw('<div>\n				&nbsp;&nbsp;\n				<span id="login_fail" class="response_error" style="display: none;">Loggin failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="loginForm">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control required email" name="email" id="remail" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n							<input type="password" class="form-control required" name="password" id="password" placeholder="Password">\n						</div>\n						<span class="help-block has-error" id="password-error"></span>\n					</div>\n					<button type="sumbit" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In....">Login</button>\n					<div class="clearfix"></div>\n				</form>\n			</div>');
}));                                                                                                                   // 29
                                                                                                                       // 30
Template.__checkName("registration");                                                                                  // 31
Template["registration"] = new Template("Template.registration", (function() {                                         // 32
  var view = this;                                                                                                     // 33
  return HTML.Raw('<span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="registration_form">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control" id="remail" name="email" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n								<input type="password" class="form-control" id="password" placeholder="Password">\n							</div>\n					</div>\n					<button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Register</button>\n					<div class="clearfix"></div>\n					<div class="login-modal-footer">\n					</div>\n				</form>');
}));                                                                                                                   // 35
                                                                                                                       // 36
Template.__checkName("password_recovery");                                                                             // 37
Template["password_recovery"] = new Template("Template.password_recovery", (function() {                               // 38
  var view = this;                                                                                                     // 39
  return HTML.Raw('<span id="reset_fail" class="response_error" style="display: none;">Recovery Failed, check entered email</span>\n					<div class="clearfix"></div>\n					<form id="password_recovery_form">\n						<div class="form-group">\n							<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-user"></i></div>\n								<input type="text" class="form-control required email" id="femail" name="email" placeholder="Email">\n							</div>\n							<span class="help-block has-error" data-error="0" id="femail-error"></span>\n						</div>\n						\n						<button type="submit" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait....">Recover Password</button>\n						<div class="clearfix"></div>\n					</form>');
}));                                                                                                                   // 41
                                                                                                                       // 42
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"customer":{"customerManagement.html":["./template.customerManagement.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/customer/customerManagement.html                                                                             //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.customerManagement.js");                                                          // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.customerManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/customer/template.customerManagement.js                                                                      //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.__checkName("myProject");                                                                                     // 2
Template["myProject"] = new Template("Template.myProject", (function() {                                               // 3
  var view = this;                                                                                                     // 4
  return HTML.DIV({                                                                                                    // 5
    "class": "jumbotron container"                                                                                     // 6
  }, "\n		", Spacebars.include(view.lookupTemplate("TClient_projectUpdates")), "\n	");                                 // 7
}));                                                                                                                   // 8
                                                                                                                       // 9
Template.__checkName("myApartment");                                                                                   // 10
Template["myApartment"] = new Template("Template.myApartment", (function() {                                           // 11
  var view = this;                                                                                                     // 12
  return HTML.DIV({                                                                                                    // 13
    "class": "jumbotron container"                                                                                     // 14
  }, "\n		", Spacebars.include(view.lookupTemplate("TClient_apartmentUpdates")), "\n	");                               // 15
}));                                                                                                                   // 16
                                                                                                                       // 17
Template.__checkName("Client_payment");                                                                                // 18
Template["Client_payment"] = new Template("Template.Client_payment", (function() {                                     // 19
  var view = this;                                                                                                     // 20
  return Spacebars.include(view.lookupTemplate("TClient_apartmentPayments"));                                          // 21
}));                                                                                                                   // 22
                                                                                                                       // 23
Template.__checkName("TClient_projectUpdates");                                                                        // 24
Template["TClient_projectUpdates"] = new Template("Template.TClient_projectUpdates", (function() {                     // 25
  var view = this;                                                                                                     // 26
  return Blaze._TemplateWith(function() {                                                                              // 27
    return {                                                                                                           // 28
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Client_ProjectUpdates")),                     // 29
      selector: Spacebars.call(view.lookup("projectIdSelector")),                                                      // 30
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 31
      id: Spacebars.call("client_projectUpdatesTable")                                                                 // 32
    };                                                                                                                 // 33
  }, function() {                                                                                                      // 34
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 35
  });                                                                                                                  // 36
}));                                                                                                                   // 37
                                                                                                                       // 38
Template.__checkName("TClient_viewProjectUpdatesSection");                                                             // 39
Template["TClient_viewProjectUpdatesSection"] = new Template("Template.TClient_viewProjectUpdatesSection", (function() {
  var view = this;                                                                                                     // 41
  return [ HTML.Raw('<button type="button" class="btn btn-xs btn-success js_viewProjectUpdateFiles">Files</button>\n\n'), HTML.DIV({
    "class": "modal fade",                                                                                             // 43
    tabindex: "-1",                                                                                                    // 44
    id: "Client_projectUpdateFilesModal",                                                                              // 45
    role: "dialog"                                                                                                     // 46
  }, "\n	  ", HTML.DIV({                                                                                               // 47
    "class": "modal-dialog"                                                                                            // 48
  }, "\n		", HTML.DIV({                                                                                                // 49
    "class": "modal-content"                                                                                           // 50
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 52
  }, "			\n			", HTML.DIV({                                                                                            // 53
    "class": "row"                                                                                                     // 54
  }, "\n			", Spacebars.include(view.lookupTemplate("TClient_projectUpdateFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeProjectUpdateFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	") ];
}));                                                                                                                   // 56
                                                                                                                       // 57
Template.__checkName("TClient_apartmentUpdates");                                                                      // 58
Template["TClient_apartmentUpdates"] = new Template("Template.TClient_apartmentUpdates", (function() {                 // 59
  var view = this;                                                                                                     // 60
  return Blaze._TemplateWith(function() {                                                                              // 61
    return {                                                                                                           // 62
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Client_ApartmentUpdates")),                   // 63
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 64
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 65
      id: Spacebars.call("client_apartmentUpdatesTable")                                                               // 66
    };                                                                                                                 // 67
  }, function() {                                                                                                      // 68
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 69
  });                                                                                                                  // 70
}));                                                                                                                   // 71
                                                                                                                       // 72
Template.__checkName("TClient_viewApartmentUpdatesSection");                                                           // 73
Template["TClient_viewApartmentUpdatesSection"] = new Template("Template.TClient_viewApartmentUpdatesSection", (function() {
  var view = this;                                                                                                     // 75
  return [ HTML.Raw('<button type="button" class="btn btn-xs btn-success js_viewApartmentUpdateFiles">Files</button>\n\n'), HTML.DIV({
    "class": "modal fade",                                                                                             // 77
    tabindex: "-1",                                                                                                    // 78
    id: "Client_apartmentUpdatesFilesModal",                                                                           // 79
    role: "dialog"                                                                                                     // 80
  }, "\n	  ", HTML.DIV({                                                                                               // 81
    "class": "modal-dialog"                                                                                            // 82
  }, "\n		", HTML.DIV({                                                                                                // 83
    "class": "modal-content"                                                                                           // 84
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 86
  }, "			\n			", HTML.DIV({                                                                                            // 87
    "class": "row"                                                                                                     // 88
  }, "\n			", Spacebars.include(view.lookupTemplate("TClient_apartmentUpdatesFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeApartmentUpdatesFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	") ];
}));                                                                                                                   // 90
                                                                                                                       // 91
Template.__checkName("TClient_apartmentPayments");                                                                     // 92
Template["TClient_apartmentPayments"] = new Template("Template.TClient_apartmentPayments", (function() {               // 93
  var view = this;                                                                                                     // 94
  return Blaze._TemplateWith(function() {                                                                              // 95
    return {                                                                                                           // 96
      table: Spacebars.call(Spacebars.dot(view.lookup("TabularTables"), "Client_ApartmentPayment")),                   // 97
      selector: Spacebars.call(view.lookup("apartmentIdSelector")),                                                    // 98
      "class": Spacebars.call("table table-sm table-bordered table-condensed"),                                        // 99
      id: Spacebars.call("client_apartmentPaymentsTable")                                                              // 100
    };                                                                                                                 // 101
  }, function() {                                                                                                      // 102
    return Spacebars.include(view.lookupTemplate("tabular"));                                                          // 103
  });                                                                                                                  // 104
}));                                                                                                                   // 105
                                                                                                                       // 106
Template.__checkName("TClient_viewApartmentPaymentSection");                                                           // 107
Template["TClient_viewApartmentPaymentSection"] = new Template("Template.TClient_viewApartmentPaymentSection", (function() {
  var view = this;                                                                                                     // 109
  return [ HTML.Raw('<button type="button" class="btn btn-xs btn-success js_viewApartmentPaymentFiles">Files</button>\n\n'), HTML.DIV({
    "class": "modal fade",                                                                                             // 111
    tabindex: "-1",                                                                                                    // 112
    id: "Client_apartmentPaymentFilesModal",                                                                           // 113
    role: "dialog"                                                                                                     // 114
  }, "\n	  ", HTML.DIV({                                                                                               // 115
    "class": "modal-dialog"                                                                                            // 116
  }, "\n		", HTML.DIV({                                                                                                // 117
    "class": "modal-content"                                                                                           // 118
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Manage Files</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                              // 120
  }, "			\n			", HTML.DIV({                                                                                            // 121
    "class": "row"                                                                                                     // 122
  }, "\n			", Spacebars.include(view.lookupTemplate("TClient_apartmentDocumentFileGallery")), "\n			"), "\n		  "), "		  \n		  ", HTML.Raw('<div class="modal-footer">\n				<button type="button" class="btn btn-xs btn-warning js_closeApartmentPaymentFilesModal">Close</button>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	") ];
}));                                                                                                                   // 124
                                                                                                                       // 125
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"customerManagement.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./customerManagement.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/customer/customerManagement.js                                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var _meteor = require('meteor/meteor');                                                                                // 1
                                                                                                                       //
var _templating = require('meteor/templating');                                                                        // 2
                                                                                                                       //
var _reactiveVar = require('meteor/reactive-var');                                                                     // 3
                                                                                                                       //
require('./customerManagement.html');                                                                                  // 5
                                                                                                                       //
$.cloudinary.config({                                                                                                  // 7
	cloud_name: "aparto"                                                                                                  // 8
});                                                                                                                    //
//Client Project Updates                                                                                               //
_templating.Template.TClient_projectUpdates.helpers({                                                                  // 11
	projectIdSelector: function () {                                                                                      // 12
		function projectIdSelector() {                                                                                       // 12
			return {                                                                                                            // 13
				projectId: ReactiveMethod.call('getProjectIdByUserId', _meteor.Meteor.userId(), Session.get("cmRefreshFlag"))      // 14
			};                                                                                                                  //
		}                                                                                                                    //
                                                                                                                       //
		return projectIdSelector;                                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TClient_projectUpdates.events({                                                                   // 19
	'click tbody > tr': function () {                                                                                     // 20
		function clickTbodyTr(event) {                                                                                       // 20
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 21
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 22
			Session.set("cmProjectUpdateId", rowData._id);                                                                      // 23
			Session.get("cmRefreshFlag", new Date());                                                                           // 24
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.TClient_viewProjectUpdatesSection.events({                                                        // 28
	'click .js_viewProjectUpdateFiles': function () {                                                                     // 29
		function clickJs_viewProjectUpdateFiles(event) {                                                                     // 29
			$("#Client_projectUpdateFilesModal").modal('show');                                                                 // 30
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_viewProjectUpdateFiles;                                                                               //
	}(),                                                                                                                  //
	'click. js_closeProjectUpdateFilesModal': function () {                                                               // 32
		function clickJs_closeProjectUpdateFilesModal(event) {                                                               // 32
			$("#Client_projectUpdateFilesModal").modal('hide');                                                                 // 33
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_closeProjectUpdateFilesModal;                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TClient_projectUpdateFileGallery.events({                                                         // 36
	'click .js_projectUpdateThumbnail': function () {                                                                     // 37
		function clickJs_projectUpdateThumbnail() {                                                                          // 37
			$("#Client_projectUpdateFilesLoadIFrame").attr('src', this.url);                                                    // 38
			$('#Client_projectUpdateFilesModal').modal('show');                                                                 // 39
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_projectUpdateThumbnail;                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TClient_projectUpdateFileGallery.helpers({                                                        // 42
	'projectUpdateFiles': function () {                                                                                   // 43
		function projectUpdateFiles() {                                                                                      // 43
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId", Session.get("cmProjectUpdateId"), Session.get("cmRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return projectUpdateFiles;                                                                                           //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 46
		function isImage(url) {                                                                                              // 46
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 47
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
//End Client Project Update                                                                                            //
//Client Apartment Updates                                                                                             //
_templating.Template.TClient_apartmentUpdates.helpers({                                                                // 52
	'apartmentIdSelector': function () {                                                                                  // 53
		function apartmentIdSelector() {                                                                                     // 53
			return {                                                                                                            // 54
				apartmentId: ReactiveMethod.call('getApartmentIdByUserId', _meteor.Meteor.userId(), Session.get("cmRefreshFlag"))  // 55
			};                                                                                                                  //
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TClient_apartmentUpdates.events({                                                                 // 60
	'click tbody > tr': function () {                                                                                     // 61
		function clickTbodyTr(event) {                                                                                       // 61
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 62
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 63
			Session.set("cmApartmentpdateId", rowData._id);                                                                     // 64
			Session.get("cmRefreshFlag", new Date());                                                                           // 65
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TClient_viewApartmentUpdatesSection.events({                                                      // 69
	'click .js_viewApartmentUpdateFiles': function () {                                                                   // 70
		function clickJs_viewApartmentUpdateFiles(event) {                                                                   // 70
			$("#Client_apartmentUpdatesFilesModal").modal('show');                                                              // 71
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_viewApartmentUpdateFiles;                                                                             //
	}(),                                                                                                                  //
	'click. js_closeApartmentUpdatesFilesModal': function () {                                                            // 73
		function clickJs_closeApartmentUpdatesFilesModal(event) {                                                            // 73
			$("#Client_apartmentUpdatesFilesModal").modal('hide');                                                              // 74
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_closeApartmentUpdatesFilesModal;                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TClient_apartmentUpdatesFileGallery.helpers({                                                     // 78
	'apartmentUpdateFiles': function () {                                                                                 // 79
		function apartmentUpdateFiles() {                                                                                    // 79
			return ReactiveMethod.call('getApartmentUpdateImageByUpdateId', Session.get('cmApartmentpdateId'), Session.get("apartmentRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return apartmentUpdateFiles;                                                                                         //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 82
		function isImage(url) {                                                                                              // 82
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 83
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.TClient_apartmentUpdatesFileGallery.events({                                                      // 87
	'click .js_apartmentUpdatesThumbnail': function () {                                                                  // 88
		function clickJs_apartmentUpdatesThumbnail() {                                                                       // 88
			$("#Client_apartmentUpdateFilesLoadIFrame").attr('src', this.url);                                                  // 89
			$('#Client_apartmentUpdateFilesModal').modal('show');                                                               // 90
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_apartmentUpdatesThumbnail;                                                                            //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
                                                                                                                       //
//End Client Apartment Updates                                                                                         //
                                                                                                                       //
_templating.Template.TClient_projectUpdateFileGallery.events({                                                         // 99
	'click .js_projectUpdateThumbnail': function () {                                                                     // 100
		function clickJs_projectUpdateThumbnail() {                                                                          // 100
			$("#Client_projectUpdateFilesLoadIFrame").attr('src', this.url);                                                    // 101
			$('#Client_projectUpdateFilesModal').modal('show');                                                                 // 102
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_projectUpdateThumbnail;                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TClient_projectUpdateFileGallery.helpers({                                                        // 105
	'projectUpdateFiles': function () {                                                                                   // 106
		function projectUpdateFiles() {                                                                                      // 106
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId", Session.get("cmProjectUpdateId"), Session.get("cmRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return projectUpdateFiles;                                                                                           //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 109
		function isImage(url) {                                                                                              // 109
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 110
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
//Client Apartment Payments                                                                                            //
_templating.Template.TClient_apartmentPayments.helpers({                                                               // 114
	apartmentIdSelector: function () {                                                                                    // 115
		function apartmentIdSelector() {                                                                                     // 115
			return { apartmentId: ReactiveMethod.call('getApartmentIdByUserId', _meteor.Meteor.userId()) };                     // 116
		}                                                                                                                    //
                                                                                                                       //
		return apartmentIdSelector;                                                                                          //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.TClient_apartmentPayments.events({                                                                // 120
	'click tbody > tr': function () {                                                                                     // 121
		function clickTbodyTr(event) {                                                                                       // 121
			var dataTable = $(event.target).closest('table').DataTable();                                                       // 122
			var rowData = dataTable.row(event.currentTarget).data();                                                            // 123
			if (rowData != null) {                                                                                              // 124
				Session.set("cmApartmentPaymentId", rowData._id);                                                                  // 125
				Session.get("cmRefreshFlag", new Date());                                                                          // 126
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return clickTbodyTr;                                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
// Client Apartment Payment Section                                                                                    //
_templating.Template.TClient_viewApartmentPaymentSection.events({                                                      // 133
	'click .js_viewApartmentPaymentFiles': function () {                                                                  // 134
		function clickJs_viewApartmentPaymentFiles(event) {                                                                  // 134
			$("#Client_apartmentPaymentFilesModal").modal('show');                                                              // 135
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_viewApartmentPaymentFiles;                                                                            //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TClient_apartmentPaymentFileGallery.events({                                                      // 138
	'click .js_imageThumbnail': function () {                                                                             // 139
		function clickJs_imageThumbnail() {                                                                                  // 139
			$("#Client_apartmentPaymentFilesLoadIFrame").attr('src', this.url);                                                 // 140
			$('#Client_apartmentPaymentFilesModal').modal('show');                                                              // 141
		}                                                                                                                    //
                                                                                                                       //
		return clickJs_imageThumbnail;                                                                                       //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.TClient_apartmentPaymentFileGallery.helpers({                                                     // 144
	'apartmentPaymentFiles': function () {                                                                                // 145
		function apartmentPaymentFiles() {                                                                                   // 145
			return ReactiveMethod.call("getApartmentPaymentFileByPaymentId", Session.get("cmApartmentPaymentId"), Session.get("cmRefreshFlag"));
		}                                                                                                                    //
                                                                                                                       //
		return apartmentPaymentFiles;                                                                                        //
	}(),                                                                                                                  //
	'isImage': function () {                                                                                              // 148
		function isImage(url) {                                                                                              // 148
			return url.match(/\.(jpeg|jpg|gif|png)$/) != null;                                                                  // 149
		}                                                                                                                    //
                                                                                                                       //
		return isImage;                                                                                                      //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
// End Client Apartment Payment Section                                                                                //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"main.html":["./template.main.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/main.html                                                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
module.exports = require("./template.main.js");                                                                        // 1
                                                                                                                       // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.main.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/template.main.js                                                                                             //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       // 1
Template.body.addContent((function() {                                                                                 // 2
  var view = this;                                                                                                     // 3
  return "";                                                                                                           // 4
}));                                                                                                                   // 5
Meteor.startup(Template.body.renderToDocument);                                                                        // 6
                                                                                                                       // 7
Template.__checkName("ApplicationLayout");                                                                             // 8
Template["ApplicationLayout"] = new Template("Template.ApplicationLayout", (function() {                               // 9
  var view = this;                                                                                                     // 10
  return HTML.DIV({                                                                                                    // 11
    id: "site-wrapper"                                                                                                 // 12
  }, "\n   ", HTML.DIV({                                                                                               // 13
    id: "site-canvas"                                                                                                  // 14
  }, "	\n	", HTML.DIV({                                                                                                // 15
    id: "site-menu"                                                                                                    // 16
  }, "\n      ", Spacebars.include(view.lookupTemplate("left_navbar")), "\n     "), "\n	", HTML.DIV({                  // 17
    "class": ""                                                                                                        // 18
  }, "\n		", Spacebars.include(view.lookupTemplate("branding_nav")), "\n		", HTML.DIV("\n				", Blaze._TemplateWith(function() {
    return "main";                                                                                                     // 20
  }, function() {                                                                                                      // 21
    return Spacebars.include(view.lookupTemplate("yield"));                                                            // 22
  }), "\n		"), "\n	"), "    \n  "), "\n");                                                                             // 23
}));                                                                                                                   // 24
                                                                                                                       // 25
Template.__checkName("left_navbar");                                                                                   // 26
Template["left_navbar"] = new Template("Template.left_navbar", (function() {                                           // 27
  var view = this;                                                                                                     // 28
  return [ HTML.Raw('<a href="#" class="js-toggle-leftnav" style="color: pink; font-size: 20px;"><i class="fa fa-times"></i></a>\n       '), HTML.NAV({
    "class": "cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left",                                                         // 30
    id: "cbp-spmenu-s1"                                                                                                // 31
  }, "	  \n			", HTML.Raw('<span style="display:inline"><h3>Aparto</h3></span>'), "\n			", HTML.Raw('<a href="/" class="js-toggle-leftnav">Project Details</a>'), "\n\n			", Blaze.If(function() {
    return Spacebars.call(view.lookup("currentUser"));                                                                 // 33
  }, function() {                                                                                                      // 34
    return [ "\n			", HTML.A({                                                                                         // 35
      href: "/projectStatus",                                                                                          // 36
      "class": "js-toggle-leftnav"                                                                                     // 37
    }, "Overall Status"), "\n			", HTML.A({                                                                            // 38
      href: "/myApartment"                                                                                             // 39
    }, "My Project"), "\n			", HTML.A({                                                                                // 40
      href: "/payment"                                                                                                 // 41
    }, "Account Statement"), "\n			", HTML.A({                                                                         // 42
      href: "#"                                                                                                        // 43
    }, "Documents"), "\n			", HTML.A({                                                                                 // 44
      href: "#"                                                                                                        // 45
    }, "Service Requests"), "\n			", HTML.A({                                                                          // 46
      href: "#"                                                                                                        // 47
    }, "Refer a friend"), "			\n			", HTML.A({                                                                         // 48
      href: "#"                                                                                                        // 49
    }, "My Profile"), "		\n			", HTML.A({                                                                              // 50
      href: "/admin"                                                                                                   // 51
    }, "Admin"), "	\n			", HTML.A({                                                                                    // 52
      href: "#",                                                                                                       // 53
      "class": "js-signout"                                                                                            // 54
    }, "Signout"), "\n			" ];                                                                                          // 55
  }), "		\n		") ];                                                                                                     // 56
}));                                                                                                                   // 57
                                                                                                                       // 58
Template.__checkName("branding");                                                                                      // 59
Template["branding"] = new Template("Template.branding", (function() {                                                 // 60
  var view = this;                                                                                                     // 61
  return HTML.Raw('<div>\n		 <button class="toggle-nav btn btn-lg btn-success js-toggle-leftnav"><i class="fa fa-bars"></i></button>\n	</div>');
}));                                                                                                                   // 63
                                                                                                                       // 64
Template.__checkName("branding_nav");                                                                                  // 65
Template["branding_nav"] = new Template("Template.branding_nav", (function() {                                         // 66
  var view = this;                                                                                                     // 67
  return HTML.DIV({                                                                                                    // 68
    "class": "row",                                                                                                    // 69
    style: "background:#47a3da"                                                                                        // 70
  }, "\n			", HTML.DIV({                                                                                               // 71
    "class": "col-md-2"                                                                                                // 72
  }, "\n				", Spacebars.include(view.lookupTemplate("branding")), "\n			"), HTML.Raw('			\n			<div class="col-md-9">\n				<h4 style="align:center"> Pushpam Group</h4>\n			</div>	\n				'), HTML.DIV({
    "class": "col-md-1",                                                                                               // 74
    style: "float:right"                                                                                               // 75
  }, "\n					", Blaze.Unless(function() {                                                                              // 76
    return Spacebars.call(view.lookup("currentUser"));                                                                 // 77
  }, function() {                                                                                                      // 78
    return [ "\n					", HTML.A({                                                                                       // 79
      href: "/login",                                                                                                  // 80
      "class": "btn btn-warning btn-lg",                                                                               // 81
      style: "vertical-align:middle"                                                                                   // 82
    }, "Login"), "\n					" ];                                                                                          // 83
  }), "\n				"), "			\n		");                                                                                           // 84
}));                                                                                                                   // 85
                                                                                                                       // 86
Template.__checkName("home");                                                                                          // 87
Template["home"] = new Template("Template.home", (function() {                                                         // 88
  var view = this;                                                                                                     // 89
  return HTML.Raw('<div>\n	<div style=";color:white">\n		<div class="">\n		  <h1>Pushpam Group</h1> \n		  <p>Pushpam Group is a multinational organization dealing with the diversified areas of Real Estate, Agro business, Floriculture, International Trading, Wellness, Hospitality, Healthcare and Education while creating meaningful living spaces, improving human well-being and empowerment of mankind through power of knowledge, health and happiness</p> \n		</div>\n		<div class="row">\n			<br>\n			<div class=" col-md-4 ">\n			  <h2>Vision</h2> \n			  <p>Play a pioneering role in Realty Sector and create landmarks that are sustainable &amp; add value to the environment, thus transforming the way we build, live and work.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Mission</h2> \n			  <p>Develop safe, eco-friendly and self sustainable living communities.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Quality Policy</h2> \n			  <p>Timely execution with 100 % compliance to agreed set of commitments.</p> \n			</div>\n		</div>\n	</div>\n</div>');
}));                                                                                                                   // 91
                                                                                                                       // 92
Template.__checkName("construction_status");                                                                           // 93
Template["construction_status"] = new Template("Template.construction_status", (function() {                           // 94
  var view = this;                                                                                                     // 95
  return HTML.Raw("<div>\n		\n	</div>");                                                                               // 96
}));                                                                                                                   // 97
Meteor.startup(function() {                                                                                            // 98
  var attrs = {"style":"background-image:url('projectsmain.jpg');"};                                                   // 99
  for (var prop in attrs) {                                                                                            // 100
    document.body.setAttribute(prop, attrs[prop]);                                                                     // 101
  }                                                                                                                    // 102
});                                                                                                                    // 103
                                                                                                                       // 104
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"config.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/config.js                                                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
allowableFormats = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];                          // 1
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["meteor/templating","meteor/reactive-var","./main.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// client/main.js                                                                                                      //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
var _templating = require('meteor/templating');                                                                        // 1
                                                                                                                       //
var _reactiveVar = require('meteor/reactive-var');                                                                     // 2
                                                                                                                       //
require('./main.html');                                                                                                // 4
                                                                                                                       //
Router.configure({                                                                                                     // 7
	layoutTemplate: 'ApplicationLayout'                                                                                   // 8
});                                                                                                                    //
                                                                                                                       //
Router.route('/', function () {                                                                                        // 11
	this.render('home', {                                                                                                 // 12
		to: 'main'                                                                                                           // 13
	});                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
Router.route('/projectStatus', function () {                                                                           // 17
	this.render('myProject', {                                                                                            // 18
		to: 'main'                                                                                                           // 19
	});                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
Router.route('/myApartment', function () {                                                                             // 23
	this.render('myApartment', {                                                                                          // 24
		to: 'main'                                                                                                           // 25
	});                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
Router.route('/login', function () {                                                                                   // 29
	this.render('credential_Section', {                                                                                   // 30
		to: 'main'                                                                                                           // 31
	});                                                                                                                   //
});                                                                                                                    //
Router.route('/admin', function () {                                                                                   // 34
	this.render('admin_section', {                                                                                        // 35
		to: 'main'                                                                                                           // 36
	});                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
Router.route('/payment', function () {                                                                                 // 40
	this.render('Client_payment', {                                                                                       // 41
		to: 'main'                                                                                                           // 42
	});                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
function toggleNav() {                                                                                                 // 46
	if ($('#site-wrapper').hasClass('show-nav')) {                                                                        // 47
		// Do things on Nav Close                                                                                            //
		$('#site-wrapper').removeClass('show-nav');                                                                          // 49
	} else {                                                                                                              //
		// Do things on Nav Open                                                                                             //
		$('#site-wrapper').addClass('show-nav');                                                                             // 52
	}                                                                                                                     //
                                                                                                                       //
	//$('#site-wrapper').toggleClass('show-nav');                                                                         //
}                                                                                                                      // 46
                                                                                                                       //
_templating.Template.branding.events({                                                                                 // 59
	'click .js-toggle-leftnav': function () {                                                                             // 60
		function clickJsToggleLeftnav(event) {                                                                               // 60
			console.log('clicked nav');                                                                                         // 61
			toggleNav();                                                                                                        // 62
		}                                                                                                                    //
                                                                                                                       //
		return clickJsToggleLeftnav;                                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.left_navbar.events({                                                                              // 66
	'click .js-toggle-leftnav': function () {                                                                             // 67
		function clickJsToggleLeftnav(event) {                                                                               // 67
			console.log('clicked nav');                                                                                         // 68
			toggleNav();                                                                                                        // 69
		}                                                                                                                    //
                                                                                                                       //
		return clickJsToggleLeftnav;                                                                                         //
	}(),                                                                                                                  //
	'click .js-signout': function () {                                                                                    // 71
		function clickJsSignout(event) {                                                                                     // 71
			event.preventDefault();                                                                                             // 72
			console.log('Logging out');                                                                                         // 73
			Meteor.logout();                                                                                                    // 74
			Router.go('/login');                                                                                                // 75
		}                                                                                                                    //
                                                                                                                       //
		return clickJsSignout;                                                                                               //
	}()                                                                                                                   //
});                                                                                                                    //
_templating.Template.branding_nav.events({});                                                                          // 78
                                                                                                                       //
_templating.Template.login.events({                                                                                    // 82
	'submit form': function () {                                                                                          // 83
		function submitForm(event) {                                                                                         // 83
			event.preventDefault();                                                                                             // 84
			$("#login_fail").css('display', 'none');                                                                            // 85
			var emailVar = event.target.email.value;                                                                            // 86
			var passwordVar = event.target.password.value;                                                                      // 87
			if (validateEmail(emailVar) && password != null && passwordVar.trim() != "") {                                      // 88
				Meteor.loginWithPassword(emailVar, passwordVar, function (err) {                                                   // 89
					if (err) {                                                                                                        // 90
						console.log("Login Erro");                                                                                       // 91
						console.log(err);                                                                                                // 92
						console.log("Login Err End");                                                                                    // 93
						$("#login_fail").css('display', 'block');                                                                        // 94
					} else {                                                                                                          //
						Router.go('/');                                                                                                  // 97
						console.log("Routing Home");                                                                                     // 98
					}                                                                                                                 //
				});                                                                                                                //
			} else {                                                                                                            //
				$("#login_fail").css('display', 'block');                                                                          // 103
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return submitForm;                                                                                                   //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
_templating.Template.password_recovery.events({                                                                        // 108
	'submit form': function () {                                                                                          // 109
		function submitForm(event) {                                                                                         // 109
			event.preventDefault();                                                                                             // 110
			console.log("recovery requested");                                                                                  // 111
			$("#reset_fail").css('display', 'none');                                                                            // 112
			var emailVar = event.target.email.value;                                                                            // 113
			if (validateEmail(emailVar)) {                                                                                      // 114
				Accounts.forgotPassword({ email: emailVar }, function (err) {                                                      // 115
					if (err) {                                                                                                        // 116
						$("#reset_fail").css('display', 'block');                                                                        // 117
						console.log(err);                                                                                                // 118
					} else {                                                                                                          //
						$("#reset_fail").css('display', 'none');                                                                         // 122
						alert("Password recovery link sent you your email");                                                             // 123
					}                                                                                                                 //
				});                                                                                                                //
			} else {                                                                                                            //
				$("#reset_fail").css('display', 'block');                                                                          // 128
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return submitForm;                                                                                                   //
	}()                                                                                                                   //
});                                                                                                                    //
                                                                                                                       //
_templating.Template.login.rendered = function () {                                                                    // 133
	$("#login_form").validate();                                                                                          // 134
};                                                                                                                     //
                                                                                                                       //
_templating.Template.registration.rendered = function () {                                                             // 137
	$("#registration_form").validate();                                                                                   // 138
};                                                                                                                     //
                                                                                                                       //
_templating.Template.password_recovery.rendered = function () {                                                        // 141
	$("#password_recovery_form").validate();                                                                              // 142
};                                                                                                                     //
                                                                                                                       //
function validateEmail(email) {                                                                                        // 145
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);                                                                                                // 147
}                                                                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"lib":{"dbOperations.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbOperations.js                                                                                                 //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
Projects = new Mongo.Collection('project');                                                                            // 1
ProjectSchemas = {};                                                                                                   // 2
ProjectSchemas.Projects = new SimpleSchema({                                                                           // 3
	projectName: {                                                                                                        // 4
		type: String,                                                                                                        // 5
		label: 'Project Name'                                                                                                // 6
	},                                                                                                                    //
	projectDescription: {                                                                                                 // 8
		type: String,                                                                                                        // 9
		label: 'Project Description'                                                                                         // 10
	},                                                                                                                    //
	projectAddress: {                                                                                                     // 12
		type: String,                                                                                                        // 13
		label: 'Project Address'                                                                                             // 14
	},                                                                                                                    //
	projectAdditionalInfo: {                                                                                              // 16
		type: String,                                                                                                        // 17
		label: 'Project Info'                                                                                                // 18
	}                                                                                                                     //
});                                                                                                                    //
Projects.attachSchema(ProjectSchemas.Projects);                                                                        // 21
TabularTables = {};                                                                                                    // 22
                                                                                                                       //
TabularTables.Projects = new Tabular.Table({                                                                           // 24
	name: "Projects",                                                                                                     // 25
	collection: Projects,                                                                                                 // 26
	columns: [{ data: "projectName", title: "Name" }, { data: "projectDescription", title: "Description" }, { data: "projectAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editProjectSection                                                                 // 32
	}],                                                                                                                   //
	extraFields: ['_id']                                                                                                  // 35
});                                                                                                                    //
Meteor.methods({                                                                                                       // 37
	'createProject': function () {                                                                                        // 38
		function createProject(project) {                                                                                    // 38
			var currentUserId = Meteor.userId();                                                                                // 39
			if (currentUserId) {                                                                                                // 40
				project.createdOn = new Date();                                                                                    // 41
				Projects.insert(project, function (err, data) {                                                                    // 42
					if (data) {                                                                                                       // 43
						console.log("Success project insertion");                                                                        // 44
					}                                                                                                                 //
				});                                                                                                                //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createProject;                                                                                                //
	}(),                                                                                                                  //
	'editProject': function () {                                                                                          // 50
		function editProject(project, val) {                                                                                 // 50
			var currentUserId = Meteor.userId();                                                                                // 51
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 52
			if (currentUserId) {                                                                                                // 53
				Projects.update({ _id: project._id }, project.modifier, function (err, data) {});                                  // 54
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editProject;                                                                                                  //
	}(),                                                                                                                  //
	'getAllProjects': function () {                                                                                       // 58
		function getAllProjects(refreshFlag) {                                                                               // 58
			return Projects.find().fetch();                                                                                     // 59
		}                                                                                                                    //
                                                                                                                       //
		return getAllProjects;                                                                                               //
	}(),                                                                                                                  //
	'getProjectByProjectId': function () {                                                                                // 61
		function getProjectByProjectId(projectId, refreshFlag) {                                                             // 61
			return Projects.findOne({ _id: projectId });                                                                        // 62
		}                                                                                                                    //
                                                                                                                       //
		return getProjectByProjectId;                                                                                        //
	}(),                                                                                                                  //
	'removeProject': function () {                                                                                        // 65
		function removeProject(projectId) {                                                                                  // 65
			console.log("Removing Project" + projectId);                                                                        // 66
			return Projects.remove(projectId);                                                                                  // 67
		}                                                                                                                    //
                                                                                                                       //
		return removeProject;                                                                                                //
	}(),                                                                                                                  //
	'isAuthorized': function () {                                                                                         // 69
		function isAuthorized(userId, document) {                                                                            // 69
			if (userId) return true;                                                                                            // 70
			return false;                                                                                                       // 71
		}                                                                                                                    //
                                                                                                                       //
		return isAuthorized;                                                                                                 //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_apartmentManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbo_apartmentManagement.js                                                                                      //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
ApartmentDocuments = new Mongo.Collection('apartment_documents');                                                      // 1
ApartmentUpdates = new Mongo.Collection('apartment_updates');                                                          // 2
ApartmentDocumentFiles = new Mongo.Collection('apartment_document_file');                                              // 3
ApartmentUpdateFiles = new Mongo.Collection('apartment_update_file');                                                  // 4
ApartmentPayment = new Mongo.Collection('apartment_payment');                                                          // 5
ApartmentPaymentFiles = new Mongo.Collection('apartment_payment_file');                                                // 6
ApartmentServiceRequests = new Mongo.Collection('apartment_service_request');                                          // 7
                                                                                                                       //
Schema = {};                                                                                                           // 9
                                                                                                                       //
Schema.ApartmentUpdates = new SimpleSchema({                                                                           // 11
	apartmentId: {                                                                                                        // 12
		type: String,                                                                                                        // 13
		autoform: {                                                                                                          // 14
			type: "hidden",                                                                                                     // 15
			label: false                                                                                                        // 16
		}                                                                                                                    //
	},                                                                                                                    //
	updateType: {                                                                                                         // 19
		type: String,                                                                                                        // 20
		autoform: {                                                                                                          // 21
			options: {                                                                                                          // 22
				living: "Living Area",                                                                                             // 23
				utility: "Utilities",                                                                                              // 24
				bathroom: "Bathroom"                                                                                               // 25
			}                                                                                                                   //
		}                                                                                                                    //
	},                                                                                                                    //
	updateDescription: {                                                                                                  // 29
		type: String                                                                                                         // 30
	}                                                                                                                     //
});                                                                                                                    //
ApartmentUpdates.attachSchema(Schema.ApartmentUpdates);                                                                // 33
Schema.ApartmentDocuments = new SimpleSchema({                                                                         // 34
	apartmentId: {                                                                                                        // 35
		type: String,                                                                                                        // 36
		autoform: {                                                                                                          // 37
			type: "hidden",                                                                                                     // 38
			label: false                                                                                                        // 39
		}                                                                                                                    //
	},                                                                                                                    //
	documentNumber: {                                                                                                     // 42
		type: String,                                                                                                        // 43
		label: 'Document Number'                                                                                             // 44
	},                                                                                                                    //
	documentDescription: {                                                                                                // 46
		type: String,                                                                                                        // 47
		label: 'Description'                                                                                                 // 48
	},                                                                                                                    //
	documentType: {                                                                                                       // 50
		type: String,                                                                                                        // 51
		autoform: {                                                                                                          // 52
			options: {                                                                                                          // 53
				loan: "Loan",                                                                                                      // 54
				property: "Property"                                                                                               // 55
			},                                                                                                                  //
			label: 'Document Type'                                                                                              // 57
		}                                                                                                                    //
	},                                                                                                                    //
	issueAuthority: {                                                                                                     // 60
		type: String,                                                                                                        // 61
		label: 'Issuing Authority'                                                                                           // 62
	},                                                                                                                    //
	issueDate: {                                                                                                          // 64
		type: String,                                                                                                        // 65
		label: 'Issue Date'                                                                                                  // 66
	},                                                                                                                    //
	expiryDate: {                                                                                                         // 68
		type: String,                                                                                                        // 69
		label: 'Expiry Date'                                                                                                 // 70
	}                                                                                                                     //
});                                                                                                                    //
ApartmentDocuments.attachSchema(Schema.ApartmentDocuments);                                                            // 73
                                                                                                                       //
Schema.ApartmentPayment = new SimpleSchema({                                                                           // 75
	apartmentId: {                                                                                                        // 76
		type: String,                                                                                                        // 77
		autoform: {                                                                                                          // 78
			type: "hidden",                                                                                                     // 79
			label: false                                                                                                        // 80
		}                                                                                                                    //
	},                                                                                                                    //
	paymentNumber: {                                                                                                      // 83
		type: String,                                                                                                        // 84
		label: 'Ref. Number'                                                                                                 // 85
	},                                                                                                                    //
	paymentAmount: {                                                                                                      // 87
		type: Number,                                                                                                        // 88
		decimal: true,                                                                                                       // 89
		label: 'Amount'                                                                                                      // 90
	},                                                                                                                    //
	paymentType: {                                                                                                        // 92
		type: String,                                                                                                        // 93
		autoform: {                                                                                                          // 94
			options: {                                                                                                          // 95
				Loan: "Loan",                                                                                                      // 96
				Cheque: "Cheque",                                                                                                  // 97
				Cash: "Cash",                                                                                                      // 98
				BankersCheque: "Banker's Cheque",                                                                                  // 99
				ElectronicTransfer: "Electronic Transfer"                                                                          // 100
			},                                                                                                                  //
			label: 'Payment Type'                                                                                               // 102
		}                                                                                                                    //
	},                                                                                                                    //
	paymentDescription: {                                                                                                 // 105
		type: String,                                                                                                        // 106
		label: 'Description'                                                                                                 // 107
	},                                                                                                                    //
	paymentStatus: {                                                                                                      // 109
		type: String,                                                                                                        // 110
		autoform: {                                                                                                          // 111
			options: {                                                                                                          // 112
				Pending: "Pending",                                                                                                // 113
				Rejected: "Rejected",                                                                                              // 114
				Completed: "Completed",                                                                                            // 115
				Processing: "Processing"                                                                                           // 116
			},                                                                                                                  //
			label: 'Payment Status'                                                                                             // 118
		}                                                                                                                    //
	},                                                                                                                    //
	paymentDate: {                                                                                                        // 121
		type: String,                                                                                                        // 122
		label: 'Payment Date'                                                                                                // 123
	}                                                                                                                     //
});                                                                                                                    //
ApartmentPayment.attachSchema(Schema.ApartmentPayment);                                                                // 126
Schema.ApartmentServiceRequests = new SimpleSchema({                                                                   // 127
	apartmentId: {                                                                                                        // 128
		type: String,                                                                                                        // 129
		autoform: {                                                                                                          // 130
			type: "hidden",                                                                                                     // 131
			label: false                                                                                                        // 132
		}                                                                                                                    //
	},                                                                                                                    //
	requestType: {                                                                                                        // 135
		type: String,                                                                                                        // 136
		autoform: {                                                                                                          // 137
			options: {                                                                                                          // 138
				utilities: "Utilities",                                                                                            // 139
				lockedOut: "Locked Out",                                                                                           // 140
				others: "Others"                                                                                                   // 141
			},                                                                                                                  //
			label: 'Type'                                                                                                       // 143
		}                                                                                                                    //
	},                                                                                                                    //
	requestDescription: {                                                                                                 // 146
		type: String,                                                                                                        // 147
		label: 'Description',                                                                                                // 148
		autoform: {                                                                                                          // 149
			rows: '3'                                                                                                           // 150
		}                                                                                                                    //
	},                                                                                                                    //
	requestContactNumber: {                                                                                               // 153
		type: String,                                                                                                        // 154
		label: 'Phone No.'                                                                                                   // 155
	},                                                                                                                    //
	requestStatus: {                                                                                                      // 157
		type: String,                                                                                                        // 158
		autoform: {                                                                                                          // 159
			options: {                                                                                                          // 160
				Pending: "Pending",                                                                                                // 161
				Rejected: "Rejected",                                                                                              // 162
				Completed: "Completed",                                                                                            // 163
				Processing: "In Progress"                                                                                          // 164
			},                                                                                                                  //
			label: 'Status'                                                                                                     // 166
		}                                                                                                                    //
	},                                                                                                                    //
	requestResolvedDate: {                                                                                                // 169
		type: String,                                                                                                        // 170
		label: 'Resolved On',                                                                                                // 171
		optional: true                                                                                                       // 172
	}                                                                                                                     //
                                                                                                                       //
});                                                                                                                    //
ApartmentServiceRequests.attachSchema(Schema.ApartmentServiceRequests);                                                // 176
                                                                                                                       //
TabularTables.ApartmentDocuments = new Tabular.Table({                                                                 // 178
	name: "ApartmentDocuments",                                                                                           // 179
	collection: ApartmentDocuments,                                                                                       // 180
	columns: [{ data: "documentNumber", title: "Number" }, { data: "documentDescription", title: "Description" }, { data: "documentType", title: "Type" }, { data: "issueAuthority", title: "Issued By" }, { data: "issueDate", title: "Issued On" }, { data: "expiryDate", title: "Expiry On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentDocumentsSection                                                     // 189
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 192
});                                                                                                                    //
                                                                                                                       //
TabularTables.Client_ApartmentDocuments = new Tabular.Table({                                                          // 195
	name: "ApartmentDocuments",                                                                                           // 196
	collection: ApartmentDocuments,                                                                                       // 197
	columns: [{ data: "documentNumber", title: "Number" }, { data: "documentDescription", title: "Description" }, { data: "documentType", title: "Type" }, { data: "issueAuthority", title: "Issued By" }, { data: "issueDate", title: "Issued On" }, { data: "expiryDate", title: "Expiry On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentDocumentsSection                                              // 206
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 209
});                                                                                                                    //
                                                                                                                       //
TabularTables.ApartmentPayment = new Tabular.Table({                                                                   // 212
	name: "ApartmentPayment",                                                                                             // 213
	collection: ApartmentPayment,                                                                                         // 214
	columns: [{ data: "paymentNumber", title: "Number" }, { data: "paymentAmount", title: "Amount" }, { data: "paymentType", title: "Type" }, { data: "paymentDescription", title: "Description" }, { data: "paymentStatus", title: "Status" }, { data: "paymentDate", title: "Paid On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentPaymentSection                                                       // 223
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 226
});                                                                                                                    //
                                                                                                                       //
TabularTables.Client_ApartmentPayment = new Tabular.Table({                                                            // 229
	name: "Client_ApartmentPayment",                                                                                      // 230
	collection: ApartmentPayment,                                                                                         // 231
	columns: [{ data: "paymentNumber", title: "Number" }, { data: "paymentAmount", title: "Amount" }, { data: "paymentType", title: "Type" }, { data: "paymentDescription", title: "Description" }, { data: "paymentStatus", title: "Status" }, { data: "paymentDate", title: "Paid On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentPaymentSection                                                // 240
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 243
});                                                                                                                    //
                                                                                                                       //
TabularTables.ApartmentUpdates = new Tabular.Table({                                                                   // 246
	name: "ApartmentUpdates",                                                                                             // 247
	collection: ApartmentUpdates,                                                                                         // 248
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentUpdatesSection                                                       // 254
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 257
});                                                                                                                    //
                                                                                                                       //
TabularTables.Client_ApartmentUpdates = new Tabular.Table({                                                            // 260
	name: "Client_ApartmentUpdates",                                                                                      // 261
	collection: ApartmentUpdates,                                                                                         // 262
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewApartmentUpdatesSection                                                // 268
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 271
});                                                                                                                    //
                                                                                                                       //
TabularTables.ApartmentServiceRequests = new Tabular.Table({                                                           // 274
	name: "ApartmentServiceRequests",                                                                                     // 275
	collection: ApartmentServiceRequests,                                                                                 // 276
	columns: [{ data: "requestType", title: "Type" }, { data: "requestDescription", title: "Description" }, { data: "contactNumber", title: "Updated On" }, { data: "requestStatus", title: "Status" }, { data: "requestResolvedDate", title: "Resolved On" }, {
		tmpl: Meteor.isClient && Template.TeditApartmentServiceRequestSection                                                // 284
	}],                                                                                                                   //
	extraFields: ['_id', 'apartmentId']                                                                                   // 287
});                                                                                                                    //
Meteor.methods({                                                                                                       // 289
	'createApartmentDocument': function () {                                                                              // 290
		function createApartmentDocument(apartmentDocument) {                                                                // 290
			var currentUserId = Meteor.userId();                                                                                // 291
			if (currentUserId && apartmentDocument.apartmentId != null) {                                                       // 292
				apartmentDocument.createdOn = new Date();                                                                          // 293
				ApartmentDocuments.insert(apartmentDocument, function (err, data) {});                                             // 294
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentDocument;                                                                                      //
	}(),                                                                                                                  //
	'editApartmentDocument': function () {                                                                                // 298
		function editApartmentDocument(apartmentDocument) {                                                                  // 298
			var currentUserId = Meteor.userId();                                                                                // 299
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 300
			if (currentUserId) {                                                                                                // 301
				ApartmentDocuments.update({ _id: apartmentDocument._id }, apartmentDocument.modifier, function (err, data) {});    // 302
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentDocument;                                                                                        //
	}(),                                                                                                                  //
	'getApartmentDocumentByApartmentDocumentId': function () {                                                            // 306
		function getApartmentDocumentByApartmentDocumentId(apartmentDocumentId, refreshFlag) {                               // 306
			return ApartmentDocuments.findOne({ _id: apartmentDocumentId });                                                    // 307
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentDocumentByApartmentDocumentId;                                                                    //
	}(),                                                                                                                  //
	'createApartmentDocumentFile': function () {                                                                          // 309
		function createApartmentDocumentFile(apartmentDocumentFile) {                                                        // 309
			var currentUserId = Meteor.userId();                                                                                // 310
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 311
			if (currentUserId) {                                                                                                // 312
				if (apartmentDocumentFile.apartmentId != null || apartmentDocumentFile.apartmentDocumentId != null) {              // 313
					apartmentDocumentFile.createdOn = new Date();                                                                     // 314
					ApartmentDocumentFiles.insert(apartmentDocumentFile, function (err, data) {});                                    // 315
				} else {                                                                                                           //
					throw new Meteor.Error(500, 'Apartment/Document Not selected');                                                   // 319
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentDocumentFile;                                                                                  //
	}(),                                                                                                                  //
	'getApartmentDocumentsFilesByDocumentId': function () {                                                               // 324
		function getApartmentDocumentsFilesByDocumentId(apartmentDocumentId, refreshFlag) {                                  // 324
			if (apartmentDocumentId != null) {                                                                                  // 325
				return ApartmentDocumentFiles.find({ apartmentDocumentId: apartmentDocumentId }).fetch();                          // 326
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentDocumentsFilesByDocumentId;                                                                       //
	}(),                                                                                                                  //
	'removeApartmentDocumentFile': function () {                                                                          // 329
		function removeApartmentDocumentFile(id) {                                                                           // 329
			ApartmentDocumentFiles.remove({ _id: id });                                                                         // 330
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentDocumentFile;                                                                                  //
	}(),                                                                                                                  //
                                                                                                                       //
	// Apartment Update Methods                                                                                           //
	'createApartmentUpdate': function () {                                                                                // 334
		function createApartmentUpdate(apartmentUpdate) {                                                                    // 334
			var currentUserId = Meteor.userId();                                                                                // 335
			if (currentUserId) {                                                                                                // 336
				apartmentUpdate.createdOn = new Date();                                                                            // 337
				ApartmentUpdates.insert(apartmentUpdate, function (err, data) {});                                                 // 338
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentUpdate;                                                                                        //
	}(),                                                                                                                  //
	'editApartmentUpdate': function () {                                                                                  // 342
		function editApartmentUpdate(apartmentUpdate) {                                                                      // 342
			var currentUserId = Meteor.userId();                                                                                // 343
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 344
			if (currentUserId) {                                                                                                // 345
				ApartmentUpdates.update({ _id: apartmentUpdate._id }, apartmentUpdate.modifier, function (err, data) {});          // 346
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentUpdate;                                                                                          //
	}(),                                                                                                                  //
	'removeApartmentUpdate': function () {                                                                                // 350
		function removeApartmentUpdate(id) {                                                                                 // 350
			if (id != null) {                                                                                                   // 351
				ApartmentUpdates.remove({ _id: id });                                                                              // 352
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentUpdate;                                                                                        //
	}(),                                                                                                                  //
	'getApartmentUpdateByApartmentUpdateId': function () {                                                                // 356
		function getApartmentUpdateByApartmentUpdateId(apartmentUpdateId, refreshFlag) {                                     // 356
			console.log("Finding" + apartmentUpdateId);                                                                         // 357
			console.log(ApartmentUpdates.findOne({ _id: apartmentUpdateId }));                                                  // 358
			return ApartmentUpdates.findOne({ _id: apartmentUpdateId });                                                        // 359
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentUpdateByApartmentUpdateId;                                                                        //
	}(),                                                                                                                  //
	'createApartmentUpdateImage': function () {                                                                           // 361
		function createApartmentUpdateImage(apartmentUpdateImage) {                                                          // 361
			var currentUserId = Meteor.userId();                                                                                // 362
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 363
			if (currentUserId) {                                                                                                // 364
				if (apartmentUpdateImage.apartmentId != null || apartmentUpdateImage.apartmentUpdateId != null) {                  // 365
					apartmentUpdateImage.createdOn = new Date();                                                                      // 366
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function (err, data) {});                                       // 367
				} else {                                                                                                           //
					throw new Meteor.Error(500, 'Project/Update Not selected');                                                       // 371
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentUpdateImage;                                                                                   //
	}(),                                                                                                                  //
	'getApartmentUpdateImageByUpdateId': function () {                                                                    // 376
		function getApartmentUpdateImageByUpdateId(apartmentUpdateId, refreshFlag) {                                         // 376
			if (apartmentUpdateId != null) {                                                                                    // 377
				console.log("Fetching Update Files");                                                                              // 378
				return ApartmentUpdateFiles.find({ apartmentUpdateId: apartmentUpdateId }).fetch();                                // 379
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentUpdateImageByUpdateId;                                                                            //
	}(),                                                                                                                  //
	'removeApartmentUpdateImage': function () {                                                                           // 382
		function removeApartmentUpdateImage(id) {                                                                            // 382
			ApartmentUpdateFiles.remove({ _id: id });                                                                           // 383
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentUpdateImage;                                                                                   //
	}(),                                                                                                                  //
                                                                                                                       //
	//End Apartment Update                                                                                                //
                                                                                                                       //
	//Apartment Payment Section                                                                                           //
	'createApartmentPayment': function () {                                                                               // 389
		function createApartmentPayment(apartmentPayment) {                                                                  // 389
			var currentUserId = Meteor.userId();                                                                                // 390
			if (currentUserId) {                                                                                                // 391
				apartmentPayment.createdOn = new Date();                                                                           // 392
				ApartmentPayment.insert(apartmentPayment, function (err, data) {});                                                // 393
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentPayment;                                                                                       //
	}(),                                                                                                                  //
	'editApartmentPayment': function () {                                                                                 // 397
		function editApartmentPayment(apartmentPayment) {                                                                    // 397
			var currentUserId = Meteor.userId();                                                                                // 398
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 399
			if (currentUserId) {                                                                                                // 400
				ApartmentPayment.update({ _id: apartmentPayment._id }, apartmentPayment.modifier, function (err, data) {});        // 401
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentPayment;                                                                                         //
	}(),                                                                                                                  //
	'removeApartmentPayment': function () {                                                                               // 405
		function removeApartmentPayment(id) {                                                                                // 405
			if (id != null) {                                                                                                   // 406
				ApartmentPayment.remove({ _id: id });                                                                              // 407
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentPayment;                                                                                       //
	}(),                                                                                                                  //
	'getApartmentPaymentByApartmentPaymentId': function () {                                                              // 411
		function getApartmentPaymentByApartmentPaymentId(apartmentPaymentId, refreshFlag) {                                  // 411
			return ApartmentPayment.findOne({ _id: apartmentPaymentId });                                                       // 412
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentPaymentByApartmentPaymentId;                                                                      //
	}(),                                                                                                                  //
	'createApartmentPaymentFile': function () {                                                                           // 414
		function createApartmentPaymentFile(apartmentPaymentFile) {                                                          // 414
			var currentUserId = Meteor.userId();                                                                                // 415
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 416
			if (currentUserId) {                                                                                                // 417
				if (apartmentPaymentFile.apartmentId != null || apartmentPaymentFile.apartmentPaymentId != null) {                 // 418
					apartmentPaymentFile.createdOn = new Date();                                                                      // 419
					ApartmentPaymentFiles.insert(apartmentPaymentFile, function (err, data) {});                                      // 420
				} else {                                                                                                           //
					throw new Meteor.Error(500, 'Project/Payment Not selected');                                                      // 424
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentPaymentFile;                                                                                   //
	}(),                                                                                                                  //
	'getApartmentPaymentFileByPaymentId': function () {                                                                   // 429
		function getApartmentPaymentFileByPaymentId(apartmentPaymentId, refreshFlag) {                                       // 429
			if (apartmentPaymentId != null) {                                                                                   // 430
				return ApartmentPaymentFiles.find({ apartmentPaymentId: apartmentPaymentId }).fetch();                             // 431
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentPaymentFileByPaymentId;                                                                           //
	}(),                                                                                                                  //
	'removeApartmentPaymentFile': function () {                                                                           // 434
		function removeApartmentPaymentFile(id) {                                                                            // 434
			ApartmentPaymentFiles.remove({ _id: id });                                                                          // 435
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentPaymentFile;                                                                                   //
	}(),                                                                                                                  //
                                                                                                                       //
	// Apartment Service Request                                                                                          //
                                                                                                                       //
	'createApartmentServiceRequest': function () {                                                                        // 440
		function createApartmentServiceRequest(apartmentServiceRequest) {                                                    // 440
			var currentUserId = Meteor.userId();                                                                                // 441
			if (currentUserId) {                                                                                                // 442
				apartmentServiceRequest.createdOn = new Date();                                                                    // 443
				ApartmentServiceRequests.insert(apartmentServiceRequest, function (err, data) {});                                 // 444
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentServiceRequest;                                                                                //
	}(),                                                                                                                  //
	'editApartmentServiceRequest': function () {                                                                          // 448
		function editApartmentServiceRequest(apartmentServiceRequest) {                                                      // 448
			var currentUserId = Meteor.userId();                                                                                // 449
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 450
			if (currentUserId) {                                                                                                // 451
				ApartmentServiceRequests.update({ _id: apartmentServiceRequest._id }, apartmentServiceRequest.modifier, function (err, data) {});
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartmentServiceRequest;                                                                                  //
	}(),                                                                                                                  //
	'removeApartmentServiceRequest': function () {                                                                        // 456
		function removeApartmentServiceRequest(id) {                                                                         // 456
			if (id != null) {                                                                                                   // 457
				ApartmentServiceRequests.remove({ _id: id });                                                                      // 458
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentServiceRequest;                                                                                //
	}(),                                                                                                                  //
	'getApartmentServiceRequestByApartmentServiceRequestId': function () {                                                // 461
		function getApartmentServiceRequestByApartmentServiceRequestId(apartmentServiceRequestId, refreshFlag) {             // 461
			return ApartmentServiceRequests.findOne({ _id: apartmentServiceRequestId });                                        // 462
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentServiceRequestByApartmentServiceRequestId;                                                        //
	}(),                                                                                                                  //
                                                                                                                       //
	//Client Apartmemt Update section                                                                                     //
                                                                                                                       //
	'getApartmentUpdateByApartmentUpdateId': function () {                                                                // 468
		function getApartmentUpdateByApartmentUpdateId(apartmentUpdateId) {                                                  // 468
			return ApartmentUpdates.findOne({ _id: apartmentUpdateId });                                                        // 469
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentUpdateByApartmentUpdateId;                                                                        //
	}(),                                                                                                                  //
	'createApartmentUpdateFile': function () {                                                                            // 471
		function createApartmentUpdateFile(apartmentUpdateImage) {                                                           // 471
			var currentUserId = Meteor.userId();                                                                                // 472
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 473
			if (currentUserId) {                                                                                                // 474
				if (apartmentUpdateImage.apartmentId != null || apartmentUpdateImage.apartmentUpdateId != null) {                  // 475
					console.log("Creating a new Apartment Image");                                                                    // 476
					apartmentUpdateImage.createdOn = new Date();                                                                      // 477
					ApartmentUpdateFiles.insert(apartmentUpdateImage, function (err, data) {});                                       // 478
				} else {                                                                                                           //
					throw new Meteor.Error(500, 'Apartment/Update Not selected');                                                     // 482
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartmentUpdateFile;                                                                                    //
	}(),                                                                                                                  //
	'getApartmentUpdateImageByUpdateId': function () {                                                                    // 487
		function getApartmentUpdateImageByUpdateId(apartmentUpdateId, refreshFlag) {                                         // 487
			if (apartmentUpdateId != null) {                                                                                    // 488
				return ApartmentUpdateFiles.find({ apartmentUpdateId: apartmentUpdateId }).fetch();                                // 489
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentUpdateImageByUpdateId;                                                                            //
	}(),                                                                                                                  //
	'removeApartmentUpdateFile': function () {                                                                            // 492
		function removeApartmentUpdateFile(id) {                                                                             // 492
			ApartmentUpdateFiles.remove({ _id: id });                                                                           // 493
		}                                                                                                                    //
                                                                                                                       //
		return removeApartmentUpdateFile;                                                                                    //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_apartments.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbo_apartments.js                                                                                               //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
Apartments = new Mongo.Collection('apartment');                                                                        // 1
ApartmentSchemas = {};                                                                                                 // 2
ApartmentSchemas.Apartments = new SimpleSchema({                                                                       // 3
	projectId: {                                                                                                          // 4
		type: String,                                                                                                        // 5
		autoform: {                                                                                                          // 6
			options: function () {                                                                                              // 7
				function options() {                                                                                               // 7
					return Projects.find().map(function (c) {                                                                         // 8
						return { label: c.projectName, value: c._id };                                                                   // 9
					});                                                                                                               //
				}                                                                                                                  //
                                                                                                                       //
				return options;                                                                                                    //
			}()                                                                                                                 //
		}                                                                                                                    //
	},                                                                                                                    //
	flatNumber: {                                                                                                         // 14
		type: String,                                                                                                        // 15
		label: 'Flat Number'                                                                                                 // 16
	},                                                                                                                    //
	flatBlock: {                                                                                                          // 18
		type: String,                                                                                                        // 19
		label: 'Block'                                                                                                       // 20
	},                                                                                                                    //
	flatArea: {                                                                                                           // 22
		type: String,                                                                                                        // 23
		label: 'Area (Sqft.)'                                                                                                // 24
	},                                                                                                                    //
	flatFloor: {                                                                                                          // 26
		type: Number,                                                                                                        // 27
		label: 'Floor'                                                                                                       // 28
	},                                                                                                                    //
	flatStatusId: {                                                                                                       // 30
		type: Number,                                                                                                        // 31
		label: 'Status'                                                                                                      // 32
	},                                                                                                                    //
	flatDescription: {                                                                                                    // 34
		type: String,                                                                                                        // 35
		label: 'Info'                                                                                                        // 36
	},                                                                                                                    //
	twoWheelerParking: {                                                                                                  // 38
		type: Number,                                                                                                        // 39
		label: '2 Wheeler Spots'                                                                                             // 40
	},                                                                                                                    //
	fourWheelerSpots: {                                                                                                   // 42
		type: Number,                                                                                                        // 43
		label: '4 Wheeler Spots'                                                                                             // 44
	}                                                                                                                     //
});                                                                                                                    //
Apartments.attachSchema(ApartmentSchemas.Apartments);                                                                  // 47
TabularTables = {};                                                                                                    // 48
                                                                                                                       //
TabularTables.Apartments = new Tabular.Table({                                                                         // 50
	name: "Apartments",                                                                                                   // 51
	collection: Apartments,                                                                                               // 52
	columns: [{ data: "flatNumber", title: "Number" }, { data: "flatBlock", title: "Block" }, { data: "flatFloor", title: "Floor" }, { data: "flatArea", title: "Area" }, { data: "flatDescription", title: "Info" }, {
		tmpl: Meteor.isClient && Template.editApartmentSection                                                               // 60
	}],                                                                                                                   //
	extraFields: ["_id"]                                                                                                  // 63
});                                                                                                                    //
Meteor.methods({                                                                                                       // 65
	'createApartment': function () {                                                                                      // 66
		function createApartment(apartment) {                                                                                // 66
			var currentUserId = Meteor.userId();                                                                                // 67
			if (currentUserId) {                                                                                                // 68
				apartment.createdOn = new Date();                                                                                  // 69
				Apartments.insert(apartment, function (err, data) {                                                                // 70
					if (data) {                                                                                                       // 71
						console.log("Success Apartment insertion");                                                                      // 72
					}                                                                                                                 //
				});                                                                                                                //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createApartment;                                                                                              //
	}(),                                                                                                                  //
	'editApartment': function () {                                                                                        // 78
		function editApartment(apartment, val) {                                                                             // 78
			var currentUserId = Meteor.userId();                                                                                // 79
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 80
			console.log(apartment.modifier);                                                                                    // 81
			if (currentUserId) {                                                                                                // 82
				Apartments.update({ _id: apartment._id }, apartment.modifier, function (err, data) {});                            // 83
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editApartment;                                                                                                //
	}(),                                                                                                                  //
	'getAllApartments': function () {                                                                                     // 87
		function getAllApartments(refreshFlag) {                                                                             // 87
			return Apartments.find().fetch();                                                                                   // 88
		}                                                                                                                    //
                                                                                                                       //
		return getAllApartments;                                                                                             //
	}(),                                                                                                                  //
	'getApartmentByProjectId': function () {                                                                              // 90
		function getApartmentByProjectId(projectId, refreshFlag) {                                                           // 90
			return Apartments.find({ projectId: projectId }).fetch();                                                           // 91
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentByProjectId;                                                                                      //
	}(),                                                                                                                  //
	'getApartmentByApartmentId': function () {                                                                            // 93
		function getApartmentByApartmentId(apartmentId, refreshFlag) {                                                       // 93
			return Apartments.findOne({ _id: apartmentId });                                                                    // 94
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentByApartmentId;                                                                                    //
	}(),                                                                                                                  //
	'removeApartment': function () {                                                                                      // 97
		function removeApartment(apartmentId) {                                                                              // 97
			return Apartments.remove(apartmentId);                                                                              // 98
		}                                                                                                                    //
                                                                                                                       //
		return removeApartment;                                                                                              //
	}(),                                                                                                                  //
	'addAssignment': function () {                                                                                        // 100
		function addAssignment(apartmentId, userId) {                                                                        // 100
			UserApartment.insert({                                                                                              // 101
				userId: userId,                                                                                                    // 102
				apartmentId: apartmentId,                                                                                          // 103
				assignmentStartDate: new Date()                                                                                    // 104
			});                                                                                                                 //
		}                                                                                                                    //
                                                                                                                       //
		return addAssignment;                                                                                                //
	}(),                                                                                                                  //
	'removeAssignment': function () {                                                                                     // 107
		function removeAssignment(apartmentAssignementId) {                                                                  // 107
			UserApartment.remove(apartmentAssignementId);                                                                       // 108
		}                                                                                                                    //
                                                                                                                       //
		return removeAssignment;                                                                                             //
	}()                                                                                                                   //
                                                                                                                       //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_customerManagement.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbo_customerManagement.js                                                                                       //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
                                                                                                                       //
Meteor.methods({                                                                                                       // 2
	'getProjectIdByUserId': function () {                                                                                 // 3
		function getProjectIdByUserId(userId) {                                                                              // 3
			console.log("Apt Id in Project Updates for user" + userId);                                                         // 4
			userApartmentMap = UserApartment.findOne({ userId: userId });                                                       // 5
			console.log(userApartmentMap);                                                                                      // 6
			apartmentId = userApartmentMap['apartmentId'];                                                                      // 7
			console.log(apartmentId);                                                                                           // 8
			apartment = Apartments.findOne({ _id: apartmentId });                                                               // 9
			console.log(apartment);                                                                                             // 10
			projectId = apartment['projectId'];                                                                                 // 11
			console.log("projectId is " + projectId);                                                                           // 12
			return projectId;                                                                                                   // 13
		}                                                                                                                    //
                                                                                                                       //
		return getProjectIdByUserId;                                                                                         //
	}(),                                                                                                                  //
	'getApartmentIdByUserId': function () {                                                                               // 15
		function getApartmentIdByUserId(userId) {                                                                            // 15
			console.log("Findin out " + userId);                                                                                // 16
			console.log("Apt Id for user" + userId);                                                                            // 17
			userApartmentMap = UserApartment.findOne({ userId: userId });                                                       // 18
			console.log(userApartmentMap);                                                                                      // 19
			apartmentId = userApartmentMap['apartmentId'];                                                                      // 20
			return apartmentId;                                                                                                 // 21
		}                                                                                                                    //
                                                                                                                       //
		return getApartmentIdByUserId;                                                                                       //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_projectUpdates.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbo_projectUpdates.js                                                                                           //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
ProjectUpdates = new Mongo.Collection('project_update');                                                               // 1
ProjectUpdateImages = new Mongo.Collection('product_update_image');                                                    // 2
Schema = {};                                                                                                           // 3
                                                                                                                       //
Schema.ProjectUpdates = new SimpleSchema({                                                                             // 5
	projectId: {                                                                                                          // 6
		type: String,                                                                                                        // 7
		autoform: {                                                                                                          // 8
			type: "hidden",                                                                                                     // 9
			label: false                                                                                                        // 10
		}                                                                                                                    //
	},                                                                                                                    //
	updateType: {                                                                                                         // 13
		type: String,                                                                                                        // 14
		autoform: {                                                                                                          // 15
			options: {                                                                                                          // 16
				recreation: "Recreation",                                                                                          // 17
				building: "Building",                                                                                              // 18
				amenities: "Amenities"                                                                                             // 19
			}                                                                                                                   //
		}                                                                                                                    //
	},                                                                                                                    //
	updateDescription: {                                                                                                  // 23
		type: String                                                                                                         // 24
	}                                                                                                                     //
});                                                                                                                    //
ProjectUpdates.attachSchema(Schema.ProjectUpdates);                                                                    // 27
                                                                                                                       //
TabularTables.ProjectUpdates = new Tabular.Table({                                                                     // 29
	name: "ProjectUpdates",                                                                                               // 30
	collection: ProjectUpdates,                                                                                           // 31
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TeditProjectUpdatesSection                                                         // 37
	}],                                                                                                                   //
	extraFields: ['_id', 'projectId']                                                                                     // 40
});                                                                                                                    //
                                                                                                                       //
TabularTables.Client_ProjectUpdates = new Tabular.Table({                                                              // 43
	name: "Client_ProjectUpdates",                                                                                        // 44
	collection: ProjectUpdates,                                                                                           // 45
	columns: [{ data: "updateType", title: "Type" }, { data: "updateDescription", title: "Description" }, { data: "createdOn", title: "Updated On" }, {
		tmpl: Meteor.isClient && Template.TClient_viewProjectUpdatesSection                                                  // 51
	}],                                                                                                                   //
	extraFields: ['_id', 'projectId']                                                                                     // 54
});                                                                                                                    //
                                                                                                                       //
Meteor.methods({                                                                                                       // 57
	'createProjectUpdate': function () {                                                                                  // 58
		function createProjectUpdate(projectUpdate) {                                                                        // 58
			var currentUserId = Meteor.userId();                                                                                // 59
			if (currentUserId) {                                                                                                // 60
				projectUpdate.createdOn = new Date();                                                                              // 61
				ProjectUpdates.insert(projectUpdate, function (err, data) {});                                                     // 62
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createProjectUpdate;                                                                                          //
	}(),                                                                                                                  //
	'editProjectUpdate': function () {                                                                                    // 66
		function editProjectUpdate(projectUpdate) {                                                                          // 66
			var currentUserId = Meteor.userId();                                                                                // 67
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 68
			if (currentUserId) {                                                                                                // 69
				ProjectUpdates.update({ _id: projectUpdate._id }, projectUpdate.modifier, function (err, data) {});                // 70
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editProjectUpdate;                                                                                            //
	}(),                                                                                                                  //
	'getProjectUpdateByProjectUpdateId': function () {                                                                    // 74
		function getProjectUpdateByProjectUpdateId(projectUpdateId) {                                                        // 74
			return ProjectUpdates.findOne({ _id: projectUpdateId });                                                            // 75
		}                                                                                                                    //
                                                                                                                       //
		return getProjectUpdateByProjectUpdateId;                                                                            //
	}(),                                                                                                                  //
	'createProjectUpdateImage': function () {                                                                             // 77
		function createProjectUpdateImage(projectUpdateImage) {                                                              // 77
			var currentUserId = Meteor.userId();                                                                                // 78
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 79
			if (currentUserId) {                                                                                                // 80
				if (projectUpdateImage.projectId != null || projectUpdateImage.projectUpdateId != null) {                          // 81
					projectUpdateImage.createdOn = new Date();                                                                        // 82
					ProjectUpdateImages.insert(projectUpdateImage, function (err, data) {});                                          // 83
				} else {                                                                                                           //
					throw new Meteor.Error(500, 'Project/Update Not selected');                                                       // 87
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return createProjectUpdateImage;                                                                                     //
	}(),                                                                                                                  //
	'getProjectUpdateImageByUpdateId': function () {                                                                      // 92
		function getProjectUpdateImageByUpdateId(projectUpdateId, refreshFlag) {                                             // 92
			//return [{'url':'google.com'},{'url':'faceobooj.com'}];                                                            //
			if (projectUpdateId != null) {                                                                                      // 94
				return ProjectUpdateImages.find({ projectUpdateId: projectUpdateId }).fetch();                                     // 95
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return getProjectUpdateImageByUpdateId;                                                                              //
	}(),                                                                                                                  //
	'removeProjectUpdateImage': function () {                                                                             // 98
		function removeProjectUpdateImage(id) {                                                                              // 98
			ProjectUpdateImages.remove({ _id: id });                                                                            // 99
		}                                                                                                                    //
                                                                                                                       //
		return removeProjectUpdateImage;                                                                                     //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"dbo_users.js":function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                     //
// lib/dbo_users.js                                                                                                    //
//                                                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                       //
UserApartment = new Mongo.Collection('user_apartment');                                                                // 1
Schema = {};                                                                                                           // 2
                                                                                                                       //
Schema.UserProfile = new SimpleSchema({                                                                                // 4
	firstName: {                                                                                                          // 5
		type: String                                                                                                         // 6
	},                                                                                                                    //
	lastName: {                                                                                                           // 8
		type: String                                                                                                         // 9
	},                                                                                                                    //
	dataOfBirth: {                                                                                                        // 11
		type: Date                                                                                                           // 12
	},                                                                                                                    //
	sex: {                                                                                                                // 14
		type: String,                                                                                                        // 15
		autoform: {                                                                                                          // 16
			options: {                                                                                                          // 17
				male: "Male",                                                                                                      // 18
				female: "Female"                                                                                                   // 19
			}                                                                                                                   //
		}                                                                                                                    //
	},                                                                                                                    //
	photoUrl: {                                                                                                           // 23
		type: String                                                                                                         // 24
	},                                                                                                                    //
	userStatus: {                                                                                                         // 26
		type: Number                                                                                                         // 27
	},                                                                                                                    //
	primaryPhone: {                                                                                                       // 29
		type: Number                                                                                                         // 30
	},                                                                                                                    //
	secondaryPhone: {                                                                                                     // 32
		type: Number                                                                                                         // 33
	},                                                                                                                    //
	permanentAddress: {                                                                                                   // 35
		type: String                                                                                                         // 36
	}                                                                                                                     //
});                                                                                                                    //
                                                                                                                       //
Schema.User = new SimpleSchema({                                                                                       // 40
	emails: {                                                                                                             // 41
		type: Array,                                                                                                         // 42
		// For accounts-password, either emails or username is required, but not both. It is OK to make this                 //
		// optional here because the accounts-password package does its own validation.                                      //
		// Third-party login packages may not require either. Adjust this schema as necessary for your usage.                //
		optional: true                                                                                                       // 46
	},                                                                                                                    //
	"emails.$": {                                                                                                         // 48
		type: Object                                                                                                         // 49
	},                                                                                                                    //
	"emails.$.address": {                                                                                                 // 51
		type: String,                                                                                                        // 52
		regEx: SimpleSchema.RegEx.Email                                                                                      // 53
	},                                                                                                                    //
	"emails.$.verified": {                                                                                                // 55
		type: Boolean                                                                                                        // 56
	},                                                                                                                    //
	services: {                                                                                                           // 58
		type: Object,                                                                                                        // 59
		optional: true,                                                                                                      // 60
		blackbox: true,                                                                                                      // 61
		autoform: {                                                                                                          // 62
			type: "hidden",                                                                                                     // 63
			label: false                                                                                                        // 64
		}                                                                                                                    //
	},                                                                                                                    //
                                                                                                                       //
	profile: {                                                                                                            // 68
		type: Schema.UserProfile,                                                                                            // 69
		optional: true                                                                                                       // 70
	}                                                                                                                     //
});                                                                                                                    //
                                                                                                                       //
Meteor.users.attachSchema(Schema.User);                                                                                // 74
                                                                                                                       //
TabularTables.Users = new Tabular.Table({                                                                              // 77
	name: "Users",                                                                                                        // 78
	collection: Meteor.users,                                                                                             // 79
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.editUserSection                                                                    // 86
	}],                                                                                                                   //
	extraFields: ["_id"]                                                                                                  // 89
});                                                                                                                    //
                                                                                                                       //
TabularTables.UserList = new Tabular.Table({                                                                           // 93
	name: "UserList",                                                                                                     // 94
	collection: Meteor.users,                                                                                             // 95
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.assignUser                                                                         // 102
	}],                                                                                                                   //
	extraFields: ["_id"]                                                                                                  // 105
});                                                                                                                    //
                                                                                                                       //
TabularTables.AssignedUserList = new Tabular.Table({                                                                   // 108
	name: "AssignedUserList",                                                                                             // 109
	collection: Meteor.users,                                                                                             // 110
	columns: [{ data: "profile.firstName", title: "Name" }, { data: "emails[1].address", title: "Email" }, { data: "profile.primaryPhone", title: "Phone" }, { data: "profile.permanentAddress", title: "Address" }, {
		tmpl: Meteor.isClient && Template.removeUserAssignSection                                                            // 117
	}],                                                                                                                   //
	extraFields: ["_id"]                                                                                                  // 120
});                                                                                                                    //
                                                                                                                       //
Meteor.methods({                                                                                                       // 125
	'addUser': function () {                                                                                              // 126
		function addUser(user) {                                                                                             // 126
			var currentUserId = Meteor.userId();                                                                                // 127
			user.password = "shiva";                                                                                            // 128
			console.log(user);                                                                                                  // 129
			user.email = user.emails[0].address;                                                                                // 130
			if (currentUserId) {                                                                                                // 131
				Accounts.createUser(user);                                                                                         // 132
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return addUser;                                                                                                      //
	}(),                                                                                                                  //
	'editUser': function () {                                                                                             // 136
		function editUser(user, val) {                                                                                       // 136
			var currentUserId = Meteor.userId();                                                                                // 137
			currentUser = Meteor.users.findOne({ _id: currentUserId });                                                         // 138
			user.email = user.emails[0].address;                                                                                // 139
			if (currentUserId) {                                                                                                // 140
				Meteor.users.update({ _id: user._id }, user.modifier, function (err, data) {});                                    // 141
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return editUser;                                                                                                     //
	}(),                                                                                                                  //
	'getAllUsers': function () {                                                                                          // 145
		function getAllUsers(refreshFlag) {                                                                                  // 145
			return Meteor.users.find().fetch();                                                                                 // 146
		}                                                                                                                    //
                                                                                                                       //
		return getAllUsers;                                                                                                  //
	}(),                                                                                                                  //
	'getUserByUserId': function () {                                                                                      // 148
		function getUserByUserId(userId, refreshFlag) {                                                                      // 148
			return Meteor.users.findOne({ _id: userId });                                                                       // 149
		}                                                                                                                    //
                                                                                                                       //
		return getUserByUserId;                                                                                              //
	}(),                                                                                                                  //
	'removeUser': function () {                                                                                           // 152
		function removeUser(userId) {                                                                                        // 152
			return Meteor.users.remove(userId);                                                                                 // 153
		}                                                                                                                    //
                                                                                                                       //
		return removeUser;                                                                                                   //
	}(),                                                                                                                  //
	'getUsersByApartmentId': function () {                                                                                // 155
		function getUsersByApartmentId(apartmentId, refreshFlag) {                                                           // 155
			u = UserApartment.find({ "apartmentId": apartmentId });                                                             // 156
			idlist = [];                                                                                                        // 157
			u.forEach(function (myDoc) {                                                                                        // 158
				idlist.push(myDoc.userId);                                                                                         // 159
			});                                                                                                                 //
			return idlist;                                                                                                      // 161
			//console.log(Meteor.users.find({"_id": {$in : idlist} }).fetch());                                                 //
			//return Meteor.users.find({"_id": {$in : idlist} });                                                               //
		}                                                                                                                    // 155
                                                                                                                       //
		return getUsersByApartmentId;                                                                                        //
	}(),                                                                                                                  //
	'addUserApartment': function () {                                                                                     // 165
		function addUserApartment(userId, apartmentId) {                                                                     // 165
			if (userId != null && apartmentId != null) {                                                                        // 166
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                            // 167
				if (assignmentRecord == null || assignmentRecord.count() == 0) {                                                   // 168
					UserApartment.insert({                                                                                            // 169
						userId: userId,                                                                                                  // 170
						apartmentId: apartmentId,                                                                                        // 171
						assignmentStartDate: new Date()                                                                                  // 172
					});                                                                                                               //
				}                                                                                                                  //
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return addUserApartment;                                                                                             //
	}(),                                                                                                                  //
	'removeUserAssignment': function () {                                                                                 // 178
		function removeUserAssignment(userId, apartmentId) {                                                                 // 178
			if (userId != null && apartmentId != null) {                                                                        // 179
				assignmentRecord = UserApartment.findOne({ userId: userId, apartmentId: apartmentId });                            // 180
				UserApartment.remove(assignmentRecord);                                                                            // 181
			}                                                                                                                   //
		}                                                                                                                    //
                                                                                                                       //
		return removeUserAssignment;                                                                                         //
	}()                                                                                                                   //
});                                                                                                                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{"extensions":[".js",".json",".html",".css"]});
require("./client/admin/template.adminTemplate.js");
require("./client/admin/template.apartmentManagement.js");
require("./client/admin/template.fileManagement.js");
require("./client/admin/template.projectManagement.js");
require("./client/credentials/template.credentialTemplate.js");
require("./client/customer/template.customerManagement.js");
require("./client/template.main.js");
require("./lib/dbOperations.js");
require("./lib/dbo_apartmentManagement.js");
require("./lib/dbo_apartments.js");
require("./lib/dbo_customerManagement.js");
require("./lib/dbo_projectUpdates.js");
require("./lib/dbo_users.js");
require("./client/admin/adminFunction.js");
require("./client/admin/apartmentManagementFunction.js");
require("./client/admin/projectManagementFunction.js");
require("./client/customer/customerManagement.js");
require("./client/config.js");
require("./client/main.js");
var require = meteorInstall({"client":{"admin":{"adminTemplate.html":["./template.adminTemplate.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/admin/adminTemplate.html                                                                             //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
module.exports = require("./template.adminTemplate.js");                                                       // 1
                                                                                                               // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.adminTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/admin/template.adminTemplate.js                                                                      //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
                                                                                                               // 1
Template.__checkName("admin_section");                                                                         // 2
Template["admin_section"] = new Template("Template.admin_section", (function() {                               // 3
  var view = this;                                                                                             // 4
  return HTML.DIV({                                                                                            // 5
    "class": "container"                                                                                       // 6
  }, "\n		", HTML.DIV({                                                                                        // 7
    "class": "row"                                                                                             // 8
  }, "\n			", HTML.DIV({                                                                                       // 9
    "class": "container jumbotron"                                                                             // 10
  }, "\n			", HTML.DIV({                                                                                       // 11
    "class": "text-center"                                                                                     // 12
  }, "\n				", HTML.DIV({                                                                                      // 13
    role: "tabpanel",                                                                                          // 14
    "class": "login-tab"                                                                                       // 15
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="project-tab" data-target="#project" data-toggle="tab">Project Management</a></li>\n						<li role="presentation"><a id="apartment-tab" data-target="#apartment" data-toggle="tab">Apartment Management</a></li>\n						<li role="presentation"><a id="user-tab" data-target="#user" data-toggle="tab">User Management</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                     // 17
  }, "\n						", HTML.DIV({                                                                                    // 18
    role: "tabpanel",                                                                                          // 19
    "class": "tab-pane active text-center",                                                                    // 20
    id: "project"                                                                                              // 21
  }, "\n							", Spacebars.include(view.lookupTemplate("projectForm")), "\n						"), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="apartment">\n													\n						</div>'), "\n						", HTML.Raw('<div role="tabpanel" class="tab-pane text-center" id="user">\n													\n						</div>'), "\n					"), "\n				"), "\n					\n			"), "\n		"), "\n		"), "\n	");
}));                                                                                                           // 23
                                                                                                               // 24
Template.__checkName("projectForm");                                                                           // 25
Template["projectForm"] = new Template("Template.projectForm", (function() {                                   // 26
  var view = this;                                                                                             // 27
  return [ HTML.Raw('<button class="btn btn-primary js-createProject">Create Project</button>\n	'), HTML.DIV({
    "class": "modal fade",                                                                                     // 29
    tabindex: "-1",                                                                                            // 30
    id: "saveProjectModal",                                                                                    // 31
    role: "dialog"                                                                                             // 32
  }, "\n	  ", HTML.DIV({                                                                                       // 33
    "class": "modal-dialog"                                                                                    // 34
  }, "\n		", HTML.DIV({                                                                                        // 35
    "class": "modal-content"                                                                                   // 36
  }, "\n		  ", HTML.Raw('<div class="modal-header">\n			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n			<h4 class="modal-title">Add/Edit Projects</h4>\n		  </div>'), "\n		  ", HTML.DIV({
    "class": "modal-body"                                                                                      // 38
  }, "\n			 ", HTML.Raw('<span id="project_fail" class="response_error" style="display: none;">Project Save Failed!</span>'), "\n			 ", Blaze._TemplateWith(function() {
    return {                                                                                                   // 40
      schema: Spacebars.call(view.lookup("Projects")),                                                         // 41
      collection: Spacebars.call(view.lookup("Projects")),                                                     // 42
      id: Spacebars.call("addProjectForm"),                                                                    // 43
      type: Spacebars.call("insert")                                                                           // 44
    };                                                                                                         // 45
  }, function() {                                                                                              // 46
    return Spacebars.include(view.lookupTemplate("autoForm"), function() {                                     // 47
      return [ "\n			  ", HTML.FIELDSET("\n				", HTML.LEGEND("Insert Project"), "\n					  ", Blaze._TemplateWith(function() {
        return {                                                                                               // 49
          name: Spacebars.call("projectName")                                                                  // 50
        };                                                                                                     // 51
      }, function() {                                                                                          // 52
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                         // 53
      }), "\n					  ", Blaze._TemplateWith(function() {                                                        // 54
        return {                                                                                               // 55
          name: Spacebars.call("projectDescription")                                                           // 56
        };                                                                                                     // 57
      }, function() {                                                                                          // 58
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                         // 59
      }), "\n					  ", Blaze._TemplateWith(function() {                                                        // 60
        return {                                                                                               // 61
          name: Spacebars.call("projectAddress"),                                                              // 62
          rows: Spacebars.call(3)                                                                              // 63
        };                                                                                                     // 64
      }, function() {                                                                                          // 65
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                         // 66
      }), "\n					  ", Blaze._TemplateWith(function() {                                                        // 67
        return {                                                                                               // 68
          name: Spacebars.call("projectAdditionalInfo")                                                        // 69
        };                                                                                                     // 70
      }, function() {                                                                                          // 71
        return Spacebars.include(view.lookupTemplate("afQuickField"));                                         // 72
      }), "\n				", HTML.DIV("\n				  ", HTML.BUTTON({                                                         // 73
        type: "submit",                                                                                        // 74
        "class": "btn btn-primary"                                                                             // 75
      }, "Submit"), "\n				  ", HTML.BUTTON({                                                                  // 76
        type: "reset",                                                                                         // 77
        "class": "btn btn-default"                                                                             // 78
      }, "Reset"), "\n				"), "\n			  "), "\n			  " ];                                                         // 79
    });                                                                                                        // 80
  }), "\n			\n		  "), "\n		  ", HTML.Raw('<div class="modal-footer">\n			<button type="sumbit" id="saveProject" class="btn btn-block bt-primary">Save</button>\n			<button type="button" id="cancelSaveProject" class="btn btn-block btn-warning">Cancel</button>\n			<div class="clearfix"></div>\n		  </div>'), "\n		"), HTML.Raw("<!-- /.modal-content -->"), "\n	  "), HTML.Raw("<!-- /.modal-dialog -->"), "\n	"), HTML.Raw("<!-- /.modal -->") ];
}));                                                                                                           // 82
                                                                                                               // 83
Template.__checkName("projectList");                                                                           // 84
Template["projectList"] = new Template("Template.projectList", (function() {                                   // 85
  var view = this;                                                                                             // 86
  return Blaze.Each(function() {                                                                               // 87
    return Spacebars.call(view.lookup("projects"));                                                            // 88
  }, function() {                                                                                              // 89
    return "\n		\n	";                                                                                          // 90
  });                                                                                                          // 91
}));                                                                                                           // 92
                                                                                                               // 93
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"adminFunction.js":["meteor/meteor","meteor/templating","meteor/reactive-var","./adminTemplate.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/admin/adminFunction.js                                                                               //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
var _meteor = require('meteor/meteor');                                                                        // 1
                                                                                                               //
var _templating = require('meteor/templating');                                                                // 2
                                                                                                               //
var _reactiveVar = require('meteor/reactive-var');                                                             // 3
                                                                                                               //
require('./adminTemplate.html');                                                                               // 5
                                                                                                               //
_templating.Template.admin_section.events({                                                                    // 7
	'submit form': function () {                                                                                  // 8
		function submitForm(event) {                                                                                 // 8
			event.preventDefault();                                                                                     // 9
			$("#project_fail").css('display', 'none');                                                                  // 10
			var projectName = event.target.projectName.value;                                                           // 11
			var projectDescription = event.target.projectDescription.value;                                             // 12
			var projectAddress = event.target.projectAddress.value;                                                     // 13
			var projectAdditionalInfo = event.target.projectAdditionalInfo.value;                                       // 14
			var projectObject = { 'projectName': projectName, 'projectDescription': projectDescription, 'projectAddress': projectAddress, 'projectAdditionalInfo': projectAdditionalInfo };
                                                                                                               //
			if (projectName.trim() != "" && projectDescription.trim() != "" && projectAddress.trim() != "" && projectAdditionalInfo.trim() != "") {
				_meteor.Meteor.call('createProject', projectObject, function (error, result) {                             // 18
					if (error) {                                                                                              // 19
						$("#project_fail").css('display', 'block');                                                              // 20
					} else {                                                                                                  //
						$("#project_fail").css('display', 'none');                                                               // 23
					}                                                                                                         //
				});                                                                                                        //
			} else {                                                                                                    //
				$("#project_fail").css('display', 'block');                                                                // 28
			}                                                                                                           //
		}                                                                                                            //
                                                                                                               //
		return submitForm;                                                                                           //
	}()                                                                                                           //
                                                                                                               //
});                                                                                                            //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"credentials":{"template.credentialTemplate.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/credentials/template.credentialTemplate.js                                                           //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
                                                                                                               // 1
Template.__checkName("credential_Section");                                                                    // 2
Template["credential_Section"] = new Template("Template.credential_Section", (function() {                     // 3
  var view = this;                                                                                             // 4
  return [ HTML.Raw('<div class="">\n		<h4 class="text-center" id="loginModalLabel">Sign In/Login</h4>\n	</div>\n		'), HTML.DIV({
    "class": "container jumbotron"                                                                             // 6
  }, "\n			", HTML.DIV({                                                                                       // 7
    "class": "text-center"                                                                                     // 8
  }, "\n				", HTML.DIV({                                                                                      // 9
    role: "tabpanel",                                                                                          // 10
    "class": "login-tab"                                                                                       // 11
  }, "\n					", HTML.Raw("<!-- Nav tabs -->"), "\n					", HTML.Raw('<ul class="nav nav-tabs" role="tablist">\n						<li role="presentation" class="active"><a id="signin-taba" data-target="#login" data-toggle="tab">Sign In</a></li>\n						<li role="presentation"><a id="forgetpass-taba" data-target="#forget_password" data-toggle="tab">Forgot Password</a></li>\n					</ul>'), "\n				\n					", HTML.Raw("<!-- Tab panes -->"), "\n					", HTML.DIV({
    "class": "tab-content"                                                                                     // 13
  }, "\n						", HTML.DIV({                                                                                    // 14
    role: "tabpanel",                                                                                          // 15
    "class": "tab-pane active text-center",                                                                    // 16
    id: "login"                                                                                                // 17
  }, "\n							", Spacebars.include(view.lookupTemplate("login")), "\n						"), "\n						", HTML.DIV({         // 18
    role: "tabpanel",                                                                                          // 19
    "class": "tab-pane text-center",                                                                           // 20
    id: "forget_password"                                                                                      // 21
  }, "\n							", Spacebars.include(view.lookupTemplate("password_recovery")), "							\n						"), "\n					"), "\n				"), "\n					\n			"), "\n		") ];
}));                                                                                                           // 23
                                                                                                               // 24
Template.__checkName("login");                                                                                 // 25
Template["login"] = new Template("Template.login", (function() {                                               // 26
  var view = this;                                                                                             // 27
  return HTML.Raw('<div>\n				&nbsp;&nbsp;\n				<span id="login_fail" class="response_error" style="display: none;">Loggin failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="loginForm">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control required email" name="email" id="remail" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n							<input type="password" class="form-control required" name="password" id="password" placeholder="Password">\n						</div>\n						<span class="help-block has-error" id="password-error"></span>\n					</div>\n					<button type="sumbit" id="login_btn" class="btn btn-block bt-login" data-loading-text="Signing In....">Login</button>\n					<div class="clearfix"></div>\n				</form>\n			</div>');
}));                                                                                                           // 29
                                                                                                               // 30
Template.__checkName("registration");                                                                          // 31
Template["registration"] = new Template("Template.registration", (function() {                                 // 32
  var view = this;                                                                                             // 33
  return HTML.Raw('<span id="registration_fail" class="response_error" style="display: none;">Registration failed, please try again.</span>\n				<div class="clearfix"></div>\n				<form id="registration_form">\n					<div class="form-group">\n						<div class="input-group">\n							<div class="input-group-addon"><i class="fa fa-at"></i></div>\n							<input type="text" class="form-control" id="remail" name="email" placeholder="Email">\n						</div>\n						<span class="help-block has-error" data-error="0" id="remail-error"></span>\n					</div>\n					<div class="form-group">\n						<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-lock"></i></div>\n								<input type="password" class="form-control" id="password" placeholder="Password">\n							</div>\n					</div>\n					<button type="submit" id="register_btn" class="btn btn-block bt-login" data-loading-text="Registering....">Register</button>\n					<div class="clearfix"></div>\n					<div class="login-modal-footer">\n					</div>\n				</form>');
}));                                                                                                           // 35
                                                                                                               // 36
Template.__checkName("password_recovery");                                                                     // 37
Template["password_recovery"] = new Template("Template.password_recovery", (function() {                       // 38
  var view = this;                                                                                             // 39
  return HTML.Raw('<span id="reset_fail" class="response_error" style="display: none;">Recovery Failed, check entered email</span>\n					<div class="clearfix"></div>\n					<form id="password_recovery_form">\n						<div class="form-group">\n							<div class="input-group">\n								<div class="input-group-addon"><i class="fa fa-user"></i></div>\n								<input type="text" class="form-control required email" id="femail" name="email" placeholder="Email">\n							</div>\n							<span class="help-block has-error" data-error="0" id="femail-error"></span>\n						</div>\n						\n						<button type="submit" id="reset_btn" class="btn btn-block bt-login" data-loading-text="Please wait....">Recover Password</button>\n						<div class="clearfix"></div>\n					</form>');
}));                                                                                                           // 41
                                                                                                               // 42
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}},"main.html":["./template.main.js",function(require,exports,module){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/main.html                                                                                            //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
module.exports = require("./template.main.js");                                                                // 1
                                                                                                               // 2
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}],"template.main.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/template.main.js                                                                                     //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
                                                                                                               // 1
Template.body.addContent((function() {                                                                         // 2
  var view = this;                                                                                             // 3
  return "";                                                                                                   // 4
}));                                                                                                           // 5
Meteor.startup(Template.body.renderToDocument);                                                                // 6
                                                                                                               // 7
Template.__checkName("ApplicationLayout");                                                                     // 8
Template["ApplicationLayout"] = new Template("Template.ApplicationLayout", (function() {                       // 9
  var view = this;                                                                                             // 10
  return HTML.DIV({                                                                                            // 11
    id: "site-wrapper"                                                                                         // 12
  }, "\n   ", HTML.DIV({                                                                                       // 13
    id: "site-canvas"                                                                                          // 14
  }, "	\n	", HTML.DIV({                                                                                        // 15
    id: "site-menu"                                                                                            // 16
  }, "\n      ", Spacebars.include(view.lookupTemplate("left_navbar")), "\n     "), "\n	", HTML.DIV({          // 17
    "class": ""                                                                                                // 18
  }, "\n		", Spacebars.include(view.lookupTemplate("branding_nav")), "\n		", HTML.DIV("\n			", Blaze._TemplateWith(function() {
    return "main";                                                                                             // 20
  }, function() {                                                                                              // 21
    return Spacebars.include(view.lookupTemplate("yield"));                                                    // 22
  }), "\n		"), "\n	"), "    \n  "), "\n");                                                                     // 23
}));                                                                                                           // 24
                                                                                                               // 25
Template.__checkName("left_navbar");                                                                           // 26
Template["left_navbar"] = new Template("Template.left_navbar", (function() {                                   // 27
  var view = this;                                                                                             // 28
  return [ HTML.Raw('<a href="#" class="js-toggle-leftnav" style="color: pink; font-size: 20px;"><i class="fa fa-times"></i></a>\n       '), HTML.NAV({
    "class": "cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left",                                                 // 30
    id: "cbp-spmenu-s1"                                                                                        // 31
  }, "	  \n			", HTML.Raw('<span style="display:inline"><h3>Aparto</h3></span>'), "\n			", HTML.Raw('<a href="/" class="js-toggle-leftnav">Project Details</a>'), "\n			", HTML.Raw('<a href="#" class="js-toggle-leftnav">Project Status</a>'), "\n			", Blaze.If(function() {
    return Spacebars.call(view.lookup("currentUser"));                                                         // 33
  }, function() {                                                                                              // 34
    return [ "\n			", HTML.A({                                                                                 // 35
      href: "#"                                                                                                // 36
    }, "My Project"), "\n			", HTML.A({                                                                        // 37
      href: "#"                                                                                                // 38
    }, "Account Statement"), "\n			", HTML.A({                                                                 // 39
      href: "#"                                                                                                // 40
    }, "Documents"), "\n			", HTML.A({                                                                         // 41
      href: "#"                                                                                                // 42
    }, "Service Requests"), "\n			", HTML.A({                                                                  // 43
      href: "#"                                                                                                // 44
    }, "Refer a friend"), "			\n			", HTML.A({                                                                 // 45
      href: "#"                                                                                                // 46
    }, "My Profile"), "			\n			", HTML.A({                                                                     // 47
      href: "#",                                                                                               // 48
      "class": "js-signout"                                                                                    // 49
    }, "Signout"), "\n			" ];                                                                                  // 50
  }), "		\n		") ];                                                                                             // 51
}));                                                                                                           // 52
                                                                                                               // 53
Template.__checkName("branding");                                                                              // 54
Template["branding"] = new Template("Template.branding", (function() {                                         // 55
  var view = this;                                                                                             // 56
  return HTML.Raw('<div>\n		 <button class="toggle-nav btn btn-lg btn-success js-toggle-leftnav"><i class="fa fa-bars"></i></button>\n	</div>');
}));                                                                                                           // 58
                                                                                                               // 59
Template.__checkName("branding_nav");                                                                          // 60
Template["branding_nav"] = new Template("Template.branding_nav", (function() {                                 // 61
  var view = this;                                                                                             // 62
  return HTML.DIV({                                                                                            // 63
    "class": "row",                                                                                            // 64
    style: "background:#47a3da"                                                                                // 65
  }, "\n			", HTML.DIV({                                                                                       // 66
    "class": "col-md-2"                                                                                        // 67
  }, "\n				", Spacebars.include(view.lookupTemplate("branding")), "\n			"), HTML.Raw('			\n			<div class="col-md-9">\n				<h4 style="align:center"> Pushpam Group</h4>\n			</div>	\n				'), HTML.DIV({
    "class": "col-md-1",                                                                                       // 69
    style: "float:right"                                                                                       // 70
  }, "\n					", Blaze.Unless(function() {                                                                      // 71
    return Spacebars.call(view.lookup("currentUser"));                                                         // 72
  }, function() {                                                                                              // 73
    return [ "\n					", HTML.A({                                                                               // 74
      href: "/login",                                                                                          // 75
      "class": "btn btn-warning btn-lg",                                                                       // 76
      style: "vertical-align:middle"                                                                           // 77
    }, "Login"), "\n					" ];                                                                                  // 78
  }), "\n				"), "			\n		");                                                                                   // 79
}));                                                                                                           // 80
                                                                                                               // 81
Template.__checkName("home");                                                                                  // 82
Template["home"] = new Template("Template.home", (function() {                                                 // 83
  var view = this;                                                                                             // 84
  return HTML.Raw('<div>\n	<div style=";color:white">\n		<div class="">\n		  <h1>Pushpam Group</h1> \n		  <p>Pushpam Group is a multinational organization dealing with the diversified areas of Real Estate, Agro business, Floriculture, International Trading, Wellness, Hospitality, Healthcare and Education while creating meaningful living spaces, improving human well-being and empowerment of mankind through power of knowledge, health and happiness</p> \n		</div>\n		<div class="row">\n			<br>\n			<div class=" col-md-4 ">\n			  <h2>Vision</h2> \n			  <p>Play a pioneering role in Realty Sector and create landmarks that are sustainable &amp; add value to the environment, thus transforming the way we build, live and work.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Mission</h2> \n			  <p>Develop safe, eco-friendly and self sustainable living communities.</p> \n			</div>\n			<div class="  col-md-4 ">\n			  <h2>Quality Policy</h2> \n			  <p>Timely execution with 100 % compliance to agreed set of commitments.</p> \n			</div>\n		</div>\n	</div>\n</div>');
}));                                                                                                           // 86
                                                                                                               // 87
Template.__checkName("construction_status");                                                                   // 88
Template["construction_status"] = new Template("Template.construction_status", (function() {                   // 89
  var view = this;                                                                                             // 90
  return HTML.Raw("<div>\n		\n	</div>");                                                                       // 91
}));                                                                                                           // 92
Meteor.startup(function() {                                                                                    // 93
  var attrs = {"style":"background-image:url('projectsmain.jpg');"};                                           // 94
  for (var prop in attrs) {                                                                                    // 95
    document.body.setAttribute(prop, attrs[prop]);                                                             // 96
  }                                                                                                            // 97
});                                                                                                            // 98
                                                                                                               // 99
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

},"main.js":["meteor/templating","meteor/reactive-var","./main.html",function(require){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// client/main.js                                                                                              //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
var _templating = require('meteor/templating');                                                                // 1
                                                                                                               //
var _reactiveVar = require('meteor/reactive-var');                                                             // 2
                                                                                                               //
require('./main.html');                                                                                        // 4
                                                                                                               //
Router.configure({                                                                                             // 7
	layoutTemplate: 'ApplicationLayout'                                                                           // 8
});                                                                                                            //
                                                                                                               //
Router.route('/', function () {                                                                                // 11
	this.render('home', {                                                                                         // 12
		to: 'main'                                                                                                   // 13
	});                                                                                                           //
});                                                                                                            //
                                                                                                               //
Router.route('/login', function () {                                                                           // 17
	this.render('credential_Section', {                                                                           // 18
		to: 'main'                                                                                                   // 19
	});                                                                                                           //
});                                                                                                            //
Router.route('/admin', function () {                                                                           // 22
	this.render('admin_section', {                                                                                // 23
		to: 'main'                                                                                                   // 24
	});                                                                                                           //
});                                                                                                            //
                                                                                                               //
function toggleNav() {                                                                                         // 29
	if ($('#site-wrapper').hasClass('show-nav')) {                                                                // 30
		// Do things on Nav Close                                                                                    //
		$('#site-wrapper').removeClass('show-nav');                                                                  // 32
	} else {                                                                                                      //
		// Do things on Nav Open                                                                                     //
		$('#site-wrapper').addClass('show-nav');                                                                     // 35
	}                                                                                                             //
                                                                                                               //
	//$('#site-wrapper').toggleClass('show-nav');                                                                 //
}                                                                                                              // 29
                                                                                                               //
_templating.Template.branding.events({                                                                         // 42
	'click .js-toggle-leftnav': function () {                                                                     // 43
		function clickJsToggleLeftnav(event) {                                                                       // 43
			console.log('clicked nav');                                                                                 // 44
			toggleNav();                                                                                                // 45
		}                                                                                                            //
                                                                                                               //
		return clickJsToggleLeftnav;                                                                                 //
	}()                                                                                                           //
});                                                                                                            //
                                                                                                               //
_templating.Template.left_navbar.events({                                                                      // 49
	'click .js-toggle-leftnav': function () {                                                                     // 50
		function clickJsToggleLeftnav(event) {                                                                       // 50
			console.log('clicked nav');                                                                                 // 51
			toggleNav();                                                                                                // 52
		}                                                                                                            //
                                                                                                               //
		return clickJsToggleLeftnav;                                                                                 //
	}(),                                                                                                          //
	'click .js-signout': function () {                                                                            // 54
		function clickJsSignout(event) {                                                                             // 54
			event.preventDefault();                                                                                     // 55
			console.log('Logging out');                                                                                 // 56
			Meteor.logout();                                                                                            // 57
			Router.go('/login');                                                                                        // 58
		}                                                                                                            //
                                                                                                               //
		return clickJsSignout;                                                                                       //
	}()                                                                                                           //
});                                                                                                            //
_templating.Template.branding_nav.events({});                                                                  // 61
                                                                                                               //
_templating.Template.login.events({                                                                            // 65
	'submit form': function () {                                                                                  // 66
		function submitForm(event) {                                                                                 // 66
			event.preventDefault();                                                                                     // 67
			$("#login_fail").css('display', 'none');                                                                    // 68
			var emailVar = event.target.email.value;                                                                    // 69
			var passwordVar = event.target.password.value;                                                              // 70
			if (validateEmail(emailVar) && password != null && passwordVar.trim() != "") {                              // 71
				Meteor.loginWithPassword(emailVar, passwordVar, function (err) {                                           // 72
					if (err) {                                                                                                // 73
						$("#login_fail").css('display', 'block');                                                                // 74
					} else {                                                                                                  //
						Router.go('/');                                                                                          // 77
						console.log("Routing Home");                                                                             // 78
					}                                                                                                         //
				});                                                                                                        //
			} else {                                                                                                    //
				$("#login_fail").css('display', 'block');                                                                  // 83
			}                                                                                                           //
		}                                                                                                            //
                                                                                                               //
		return submitForm;                                                                                           //
	}()                                                                                                           //
                                                                                                               //
});                                                                                                            //
_templating.Template.password_recovery.events({                                                                // 88
	'submit form': function () {                                                                                  // 89
		function submitForm(event) {                                                                                 // 89
			event.preventDefault();                                                                                     // 90
			console.log("recovery requested");                                                                          // 91
			$("#reset_fail").css('display', 'none');                                                                    // 92
			var emailVar = event.target.email.value;                                                                    // 93
			if (validateEmail(emailVar)) {                                                                              // 94
				Accounts.forgotPassword({ email: emailVar }, function (err) {                                              // 95
					if (err) {                                                                                                // 96
						$("#reset_fail").css('display', 'block');                                                                // 97
					} else {                                                                                                  //
						$("#reset_fail").css('display', 'none');                                                                 // 101
						alert("Password recovery link sent you your email");                                                     // 102
					}                                                                                                         //
				});                                                                                                        //
			} else {                                                                                                    //
				$("#reset_fail").css('display', 'block');                                                                  // 107
			}                                                                                                           //
		}                                                                                                            //
                                                                                                               //
		return submitForm;                                                                                           //
	}()                                                                                                           //
});                                                                                                            //
                                                                                                               //
_templating.Template.login.rendered = function () {                                                            // 112
	$("#login_form").validate();                                                                                  // 113
};                                                                                                             //
                                                                                                               //
_templating.Template.registration.rendered = function () {                                                     // 116
	$("#registration_form").validate();                                                                           // 117
};                                                                                                             //
                                                                                                               //
_templating.Template.password_recovery.rendered = function () {                                                // 120
	$("#password_recovery_form").validate();                                                                      // 121
};                                                                                                             //
                                                                                                               //
function validateEmail(email) {                                                                                // 124
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);                                                                                        // 126
}                                                                                                              //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}]},"lib":{"dbOperations.js":function(){

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                             //
// lib/dbOperations.js                                                                                         //
//                                                                                                             //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                               //
                                                                                                               //
Meteor.methods({                                                                                               // 2
    'createProject': function () {                                                                             // 3
        function createProject(project) {                                                                      // 3
            var currentUserId = Meteor.userId();                                                               // 4
            if (currentUserId) {                                                                               // 5
                Projects.insert({                                                                              // 6
                    projectName: project.projectName,                                                          // 7
                    projectDescription: project.projectDescription,                                            // 8
                    projectAddress: project.projectAddress,                                                    // 9
                    projectAdditionalInfo: project.projectAdditionalInfo,                                      // 10
                    createdBy: currentUserId,                                                                  // 11
                    createdOn: new Date(),                                                                     // 12
                    modifiedBy: currentUserId,                                                                 // 13
                    modifiedOn: new Date()                                                                     // 14
                });                                                                                            //
            }                                                                                                  //
        }                                                                                                      //
                                                                                                               //
        return createProject;                                                                                  //
    }(),                                                                                                       //
    'editProject': function () {                                                                               // 18
        function editProject(projectId, project) {                                                             // 18
            var currentUserId = Meteor.userId();                                                               // 19
            currentUser = Meteor.users.findOne({ _id: currentUserId });                                        // 20
                                                                                                               //
            if (currentUserId && currentUser.role == 1) {                                                      // 22
                Projects.insert({                                                                              // 23
                    projectName: project.projectName,                                                          // 24
                    projectDescription: project.projectDescription,                                            // 25
                    projectAddress: project.projectAddress,                                                    // 26
                    projectAdditionalInfo: project.projectAdditionalInfo,                                      // 27
                    modifiedBy: currentUserId,                                                                 // 28
                    modifiedOn: new Date()                                                                     // 29
                });                                                                                            //
            }                                                                                                  //
        }                                                                                                      //
                                                                                                               //
        return editProject;                                                                                    //
    }()                                                                                                        //
});                                                                                                            //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}}},{"extensions":[".js",".json",".html",".css"]});
require("./client/admin/template.adminTemplate.js");
require("./client/credentials/template.credentialTemplate.js");
require("./client/template.main.js");
require("./lib/dbOperations.js");
require("./client/admin/adminFunction.js");
require("./client/main.js");
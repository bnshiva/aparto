App.info({
  version: '1.0.0',
  buildNumber: '100' // has to be a number
});
App.accessRule('http://*');
App.accessRule('https://*');
App.accessRule("blob:*");
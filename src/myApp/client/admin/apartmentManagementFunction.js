import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './apartmentManagement.html';
import './fileManagement.html';

$.cloudinary.config({
  cloud_name: "aparto"
});

Template.assignedUsers.helpers({
	userIdSelector: function () {
		userIdList=ReactiveMethod.call('getUsersByApartmentId',Session.get("selectedApartmentId"),Session.get("apartmentSelectFlag"));
		return {'_id':{$in:userIdList}} ; 
  }
});

Template.userAssignment.events({
	 'click .js-assignOwners': function (event) {		 
	 $("#assignUserModal").modal('show');
  },
  'click .js_closeAssignUserModal':function(event){
	$("#assignUserModal").modal('hide');  
  }
});
Template.assignUser.events({
	 'click .js_assignUserToApartment': function (event) {		 
	 apartmentId = Session.get("selectedApartmentId");
	 userId = this._id;
	 console.log(apartmentId+" User:"+userId);
	 if(apartmentId !=null && userId !=null){
		 Meteor.call('addUserApartment',userId,apartmentId);
	 }
	 Session.set("apartmentSelectFlag",new Date());
  }
});

Template.removeUserAssignSection.events({
	 'click .js_removeUserAssignment': function (event) {	
	 Session.set("selectedUserId",this._id);
	 $("#confirmRemoveUserAssignmentModal").modal('show');
	 
  }
});
Template.removeAssignmentModal.events({
	 'click .js_cancelRemoveUserAssignment': function (event) {		 
	 $("#confirmRemoveUserAssignmentModal").modal('hide');
  },
   'click .js_confirmRemoveUserAssignment': function (event) {
	 userId = Session.get("selectedUserId");
	 apartmentId = Session.get("selectedApartmentId");
	 if(userId!=null && apartmentId!=null){
		Meteor.call('removeUserAssignment',userId,apartmentId); 
		Session.set("apartmentSelectFlag",new Date());
	 }
	 $("#confirmRemoveUserAssignmentModal").modal('hide');
  },
  
});

Template.TapartmentDocuments.helpers({
	apartmentIdSelector: function () {
		return {apartmentId:Session.get("selectedApartmentId")};
  }
});

Template.TapartmentDocuments.events({
  'click tbody > tr': function (event) {
    var dataTable = $(event.target).closest('table').DataTable();
    var rowData = dataTable.row(event.currentTarget).data();
	Session.set("am_apartmentDocumentId",rowData._id);		
	Session.set("apartmentRefreshFlag", new Date());
  },
  'click .sampleModalShow':function(){
	  $("#editApartmentDocumentModal").modal('show');
  }
});

Template.TCreateApartmentDocument.helpers({
    defaultApartmentDocument: function() {
        return { apartmentId: Session.get('selectedApartmentId') };
    }
});

AutoForm.addHooks(['addNewApartmentDocumentForm'], {
  onSuccess: function(operation, result, template) {
	$("#createApartmentDocumentModal").modal('hide');
  }
});
AutoForm.addHooks(['editApartmentDocumentForm'], {
  onSuccess: function(operation, result, template) {
	$("#editApartmentDocumentModal").modal('hide');
  }
});

Template.TCreateApartmentDocument.events({
    'click .js-createApartmentDocument': function() {
        $("#createApartmentDocumentModal").modal('show');
    }
});

Template.TeditApartmentDocumentForm.helpers({
    'editApartmentUpdateDocument':function(){
				if(Session.get("am_apartmentDocumentId")!=null){
				console.log("Setting apt to"+Session.get("am_apartmentDocumentId"));
				return ReactiveMethod.call('getApartmentDocumentByApartmentDocumentId',Session.get("am_apartmentDocumentId"),Session.get("apartmentRefreshFlag"));
		}
		return null;
	}
});
Template.TeditApartmentDocumentsSection.events({
    'click .js_manageApartmentDocumentFiles':function(){
			$("#s").modal('show');
		},
	'click .js_editApartmentDocument':function(){
		$("#editApartmentDocumentModal").modal('show');
	}
});
Template.TApartmentDocumentsFileGallery.helpers({
    'apartmentDocumentFiles':function(){
				if(Session.get("am_apartmentDocumentId")!=null){	
					return ReactiveMethod.call('getApartmentDocumentsFilesByDocumentId',Session.get("am_apartmentDocumentId"),Session.get("apartmentRefreshFlag"));
		}
	},
	'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}
});
Template.TApartmentDocumentsFileGallery.events({
    'click .js_apartmentDocumentThumbnail':function(){
			$("#apartmentDocumentLoadIFrame").attr('src',this.url);
			$('#apartmentDocumentFileGalleryModal').modal('show');
		},
		'click .js_apartmentDocumentFileDelete':function(){
			var result = confirm("Are you sure you want to delete the document?");
			if(result){
				//console.log(this);
				Cloudinary.delete(this.publicId);
				Meteor.call("removeApartmentDocumentFile",this._id);
				Session.set("apartmentRefreshFlag", new Date());
			}
		}
});


Template.TApartmentDocumentFiles.events({
    'click .js_uploadApartmentDocument':function(){
			var ApartmentDocumentFile={};
			var file = $('#uploadApartmentDocument').get(0).files[0];
			var result={};
			console.log(file);
				Cloudinary._upload_file(file, {"folder":"apartmentDocuments","allowed_formats" : allowableFormats,"resource_type" :"auto"}, function(err, res) {
				  if (err){
					console.log(err);
				  }
				  else{
					 console.log(res);
					  result.url=res.url;
					  result.publicId=res.public_id;
					  console.log(result);
					  apartmentId = Session.get("selectedApartmentId");	
					  apartmentDocumentId = Session.get("am_apartmentDocumentId");
					  if(apartmentId !=null && apartmentDocumentId!=null){						
						ApartmentDocumentFile.apartmentId =apartmentId;
						ApartmentDocumentFile.apartmentDocumentId=apartmentDocumentId;
						ApartmentDocumentFile.publicId = res.public_id;
						ApartmentDocumentFile.url=  res.url;
						ApartmentDocumentFile.fullRes= res;
						Meteor.call("createApartmentDocumentFile",ApartmentDocumentFile);
						Session.set("apartmentRefreshFlag", new Date());
					  }					  
				  }
				  
				});
		},
});

//     Apartment Updates section

Template.TapartmentUpdateFiles.events({
    'click .js_uploadImage':function(){
			var ApartmentUpdateFile={};
			var file = $('#apartmentUpdateFile').get(0).files[0];
			console.log("Apartment File Update Started");
			var result={};
			console.log(file);
				Cloudinary._upload_file(file, {"folder":"apartmentUpdates","allowed_formats" : allowableFormats,"resource_type" :"auto"}, function(err, res) {
				  if (err){
					console.log(err);
				  }
				  else{
					 console.log(res);
					  result.url=res.url;
					  result.publicId=res.public_id;
					  console.log(result);
					  apartmentId = Session.get("selectedApartmentId");	
					  apartmentUpdateId = Session.get("am_apartmentUpdateId");
					  if(apartmentId !=null && apartmentUpdateId!=null){						
						ApartmentUpdateFile.apartmentId =apartmentId;
						ApartmentUpdateFile.apartmentUpdateId=apartmentUpdateId;
						ApartmentUpdateFile.publicId = res.public_id;
						ApartmentUpdateFile.url=  res.url;
						ApartmentUpdateFile.fullRes= res;
						Meteor.call("createApartmentUpdateFile",ApartmentUpdateFile);
						Session.set("apartmentRefreshFlag", new Date());
					  }					  
				  }
				  
				});
		},
});

Template.TApartmentUpdateFileGallery.helpers({
	'apartmentUpdateFiles':function(){
			return ReactiveMethod.call('getApartmentUpdateImageByUpdateId',Session.get('am_apartmentUpdateId'),Session.get("apartmentRefreshFlag"));
			},
			'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}	
});


Template.TApartmentUpdateFileGallery.events({
		'click .js_imageThumbnail':function(){
			console.log("Thumbnail Opening");
			$("#apartment_fileLoadIFrame").attr('src',this.url);
			$('#apartmentFileLoadModal').modal('show');
		},
		'click .js_apartmentUpdateDeleteFile':function(){
			var result = confirm("Are you sure you want to delete the Update File?");
			if(result){
				console.log(this);
				Cloudinary.delete(this.publicId);
				Meteor.call("removeApartmentUpdateImage",this._id);
			}
		}
    });

Template.TapartmentUpdateForm.helpers({
		'defaultApartmentUpdate':function(){
			return {'apartmentId':Session.get('selectedApartmentId')};
		}
});
Template.TapartmentUpdateForm.events({
		'click .js-createApartmentUpdate':function(){
			$("#createApartmentUpdateModal").modal('show');
		}
});


      
Template.TeditApartmentUpdateForm.helpers({
		'editApartmentUpdate':function(){
			if(Session.get('selectedApartmentId')){
				return ReactiveMethod.call('getApartmentUpdateByApartmentUpdateId',Session.get('am_apartmentUpdateId'),Session.get("apartmentRefreshFlag"));
			}
		}
});	

Template.TapartmentUpdates.events({
	 'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		Session.set("am_apartmentUpdateId",rowData._id);		
		Session.set("apartmentRefreshFlag", new Date());
  }
});

Template.TapartmentUpdates.helpers({
	apartmentIdSelector: function () {
		return {apartmentId:Session.get("selectedApartmentId")};
  }
});

Template.TeditApartmentUpdatesSection.events({
		'click .js_manageApartmentUpdateFiles':function(){
			$("#apartmentUpdatesFilesModal").modal('show');
		},
		'click .js_editApartmentUpdate':function(){
			$("#editApartmentUpdateModal").modal('show');
		},
		'click .js_removeApartmentUpdate':function(){
			var result = confirm("Are you sure you want to delete the update, you will lose all the files?");
			if(result){
				Meteor.call("removeApartmentUpdate",this._id);
			}
		},
		
});


AutoForm.addHooks(['editApartmentUpdateForm'], {
  onSuccess: function(operation, result, template) {
	$("#editApartmentUpdateModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewApartmentUpdateForm'], {
  onSuccess: function(operation, result, template) {
	$("#createApartmentUpdateModal").modal('hide');
  }
});

//End Apartment Updates

// Apartment Payment Section

Template.TapartmentPayments.helpers({
	apartmentIdSelector: function () {
		return {apartmentId:Session.get("selectedApartmentId")};
  }
});

Template.TapartmentPaymentForm.helpers({
		'defaultApartmentPayment':function(){
			return {'apartmentId':Session.get('selectedApartmentId')};
		}
});
Template.TapartmentPaymentForm.events({
		'click .js-createApartmentPayment':function(){
			$("#createApartmentPaymentModal").modal('show');
		}
});


      
Template.TeditApartmentPaymentForm.helpers({
		'editApartmentPayment':function(){
			if(Session.get('selectedApartmentId')){
				return ReactiveMethod.call('getApartmentPaymentByApartmentPaymentId',Session.get('am_apartmentPaymentId'),Session.get("apartmentRefreshFlag"));
			}
		}
});	

Template.TapartmentPayments.events({
	 'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		Session.set("am_apartmentPaymentId",rowData._id);		
		Session.set("apartmentRefreshFlag", new Date());
  }
});

Template.TeditApartmentPaymentSection.events({
		'click .js_manageApartmentPaymentFiles':function(){
			//$("#createApartmentPaymentModal").modal('show');
		},
		'click .js_editApartmentPayment':function(){
			$("#editApartmentPaymentModal").modal('show');
		},
		'click .js_removeApartmentPayment':function(){
			var result = confirm("Are you sure you want to delete the payment, you will lose all the files?");
			if(result){
				Meteor.call("removeApartmentPayment",this._id);
			}
		},
		
});


AutoForm.addHooks(['editApartmentPaymentForm'], {
  onSuccess: function(operation, result, template) {
	$("#editApartmentPaymentModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewApartmentPaymentForm'], {
  onSuccess: function(operation, result, template) {
	$("#createApartmentPaymentModal").modal('hide');
  }
});

// Apartment Service Request Section

Template.TapartmentServiceRequests.helpers({
	apartmentIdSelector: function () {
		return {apartmentId:Session.get("selectedApartmentId")};
  }
});

Template.TapartmentServiceRequestForm.helpers({
		'defaultApartmentServiceRequest':function(){
			return {'apartmentId':Session.get('selectedApartmentId')};
		}
});
Template.TapartmentServiceRequestForm.events({
		'click .js-createApartmentServiceRequest':function(){
			$("#createApartmentServiceRequestModal").modal('show');
		}
});


      
Template.TeditApartmentServiceRequestForm.helpers({
		'editApartmentServiceRequest':function(){
			if(Session.get('selectedApartmentId')){
				return ReactiveMethod.call('getApartmentServiceRequestByApartmentServiceRequestId',Session.get('am_apartmentServiceRequestId'),Session.get("apartmentRefreshFlag"));
			}
		}
});	

Template.TapartmentServiceRequests.events({
	 'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		Session.set("am_apartmentServiceRequestId",rowData._id);		
		Session.set("apartmentRefreshFlag", new Date());
  }
});

Template.TeditApartmentServiceRequestSection.events({
		'click .js_editApartmentServiceRequest':function(){
			$("#editApartmentServiceRequestModal").modal('show');
		},
		'click .js_removeApartmentPayment':function(){
			var result = confirm("Are you sure you want to delete the request?");
			if(result){
				Meteor.call("removeApartmentServiceRequest",this._id);
			}
		},
		
});


AutoForm.addHooks(['editApartmentServiceRequestForm'], {
  onSuccess: function(operation, result, template) {
	$("#editApartmentServiceRequestModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewApartmentServiceRequestForm'], {
  onSuccess: function(operation, result, template) {
	$("#createApartmentServiceRequestModal").modal('hide');
  }
});



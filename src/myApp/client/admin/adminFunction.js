import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './adminTemplate.html';

//Template.registerHelper("Collections", Collections);
Template.admin_section.events({

		'click .js-createProject':function(event){
			$("#createProjectModal").modal();
			
		}
		
    });
	
	Template.admin_section.helpers({
		'isProjectSelected':function(){
			return Session.get('pmProjectId');
		}
		
    });

Template.projectList.events({
  'click tbody > tr': function (event) {
    var dataTable = $(event.target).closest('table').DataTable();
    var rowData = dataTable.row(event.currentTarget).data();
	Session.set("pmProjectId",rowData._id);	
	Session.set("pmProjectName",rowData.projectName);		
  },
  
  
});

Template.projectListOptions.helpers({
	'projects':function(){
		return ReactiveMethod.call('getAllProjects',Session.get("getProjectsRefreshFlag"));
	}
});

Template.projectListOptions.events({
    'change .projectNameDropDown': function (event) {
        var projectId = $(event.currentTarget).val();
		Session.set("selectedProjectId",projectId);
       console.log("ProjectId : " + projectId);
    },
});
	
Template.apartmentForm.events({
    'click .js-createApartment': function (event) {
		event.preventDefault();
        $("#createApartmentModal").modal('show');
    }
});
Template.apartmentList.helpers({
  projectIdSelector: function () {
    return {projectId: Session.get("selectedProjectId")}; // this could be pulled from a Session var or something that is reactive
  } 
});
Template.apartmentList.events({
  'click tbody > tr': function (event) {
    var dataTable = $(event.target).closest('table').DataTable();
    var rowData = dataTable.row(event.currentTarget).data();
	Session.set("selectedApartmentId",rowData._id);	
	console.log("select apt"+Session.get("selectedApartmentId"));
	Session.set("apartmentSelectFlag", new Date());
	
  },
  
  
});

Template.userForm.events({
    'click .js-createUser': function (event) {
		event.preventDefault();
        $("#createUserModal").modal('show');
    }
});

Template.editApartmentSection.events({
  'click .js_editApartment': function (event) {
	event.preventDefault();
	Session.set("getProjectsRefreshFlag",new Date());
	Session.set("editApartmentDocument",this);
	$("#editApartmentModal").modal('show');
  },
   'click .js_removeApartment': function (event) {
	event.preventDefault();
	Session.set("getProjectsRefreshFlag",new Date());
	$("#confirmDeleteApartmentDialog").modal('show');
  },
   'click .js_cancelRemoveApartment': function (event) {
	event.preventDefault();
	$("#confirmDeleteApartmentDialog").modal('hide');
  },
  'click .js_confirmRemoveApartment': function (event) {
	event.preventDefault();	
	$("#confirmDeleteApartmentDialog").modal('hide');
	if(Session.get("editApartmentDocument")!=null){
		Meteor.call('removeApartment',Session.get("editApartmentDocument")._id);
	}
	
  },
  
});
Template.editProjectSection.events({
  'click .js_editProject': function (event) {
	event.preventDefault();
	Session.set("getProjectRefreshFlag",new Date());
	Session.set("editProjectDocument",this);
	$("#editProjectModal").modal('show');
  },
   'click .js_removeProject': function (event) {
	event.preventDefault();
	Session.set("getProjectRefreshFlag",new Date());
	Session.set("editProjectDocument",this);
	$("#confirmDeleteProjectDialog").modal('show');
  },
   'click .js_cancelRemoveProject': function (event) {
	event.preventDefault();
	$("#confirmDeleteProjectDialog").modal('hide');
  },
  'click .js_confirmRemoveProject': function (event) {
	event.preventDefault();	
	$("#confirmDeleteProjectDialog").modal('hide');
	if(Session.get("editProjectDocument")!=null){
		Meteor.call('removeProject',Session.get("editProjectDocument")._id);		
	}
	
  },
  
});
Template.editApartmentForm.helpers({	
	'editApartmentDocument':function(){
		if(Session.get("editApartmentDocument")!=null){
				return ReactiveMethod.call('getApartmentByApartmentId',Session.get("editApartmentDocument")._id,Session.get("getProjectsRefreshFlag"));
		}		
	},
});
Template.editProjectForm.helpers({	
	'editProjectDocument':function(){
		if(Session.get("editProjectDocument")!=null){
				return ReactiveMethod.call('getProjectByProjectId',Session.get("editProjectDocument")._id,Session.get("getProjectsRefreshFlag"));
		}		
	},
});
Template.editUserForm.helpers({	
	'editUserDocument':function(){
		if(Session.get("editUserDocument")!=null){
				return ReactiveMethod.call('getUserByUserId',Session.get("editUserDocument")._id,Session.get("userRefreshFlag"));
		}		
	},
});
Template.editUserSection.events({
  'click .js_editUser': function (event) {
	event.preventDefault();
	Session.set("editUserDocument",this);
	$("#editUserModal").modal('show');
  },
   'click .js_removeUser': function (event) {
	event.preventDefault();
	Session.set("editUserDocument",this);
	$("#confirmDeleteUserDialog").modal('show');
  },
   'click .js_cancelRemoveUser': function (event) {
	event.preventDefault();
	$("#confirmDeleteUserDialog").modal('hide');
  },
  'click .js_confirmRemoveUser': function (event) {
	event.preventDefault();	
	$("#confirmDeleteUserDialog").modal('hide');
	if(Session.get("editUserDocument")!=null){
		Meteor.call('removeProject',Session.get("editUserDocument")._id);		
	}
	
  },
  
});

AutoForm.addHooks(['editApartmentForm'], {
  onSuccess: function(operation, result, template) {
	$("#editApartmentModal").modal('hide');
  }
});
AutoForm.addHooks(['editProjectForm'], {
  onSuccess: function(operation, result, template) {
	$("#editProjectModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewProject'], {
  onSuccess: function(operation, result, template) {
	Session.set("getProjectsRefreshFlag",new Date());
	$("#createProjectModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewApartment'], {
  onSuccess: function(operation, result, template) {
	$("#createApartmentModal").modal('hide');
  }
});
AutoForm.addHooks(['addNewUser'], {
  onSuccess: function(operation, result, template) {
	$("#createUserModal").modal('hide');
  }
});
AutoForm.addHooks(['editUserForm'], {
  onSuccess: function(operation, result, template) {
	$("#editUserModal").modal('hide');
	Session.set("userRefreshFlag",new Date());
  }
});


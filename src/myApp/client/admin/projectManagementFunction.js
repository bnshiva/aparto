import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './projectManagement.html';
import './fileManagement.html';

$.cloudinary.config({
  cloud_name: "aparto"
});




	
	Template.TprojectUpdateForm.events({
		'click .js-createProjectUpdate':function(event){
				$("#createProjectUpdateModal").modal('show');
		},
		
    });
	
	Template.TeditProjectUpdatesSection.events({		
		'click .js_editProjectUpdate':function(event){
			event.preventDefault();
			Session.set("editProjectUpdateRecord",this);
			Session.set("getProjectUpdatesRefreshFlag", new Date());
			$("#editProjectUpdateModal").modal('show');
		},
		
		'click .js_manageProjectUpdateFiles':function(event){
			event.preventDefault();
			console.log("Opening File Uploader");
			$("#projectUpdateFilesModal").modal('show');
		}
		
    });
		
	Template.TprojectUpdateFiles.events({
		'click .js_closeProjectUpdateFilesModal':function(event){
			event.preventDefault();
			$("#projectUpdateFilesModal").modal('hide');
		},
		'click .js_uploadImage':function(event){
			var projectUpdateImage={};
			var file = $('#uploadedFile').get(0).files[0];
			var result={};
			console.log(file);
				Cloudinary._upload_file(file, {"folder":"updates","allowed_formats" : allowableFormats,"resource_type" :"auto"}, function(err, res) {
				  if (err){
					console.log(err);
				  }
				  else{
					 console.log(res);
					  result.url=res.url;
					  result.publicId=res.public_id;
					  console.log(result);
					  projectId = Session.get("pmProjectId");	
					  projectUpdateId = Session.get("pmProjectUpdateId");
					  if(projectId !=null && projectUpdateId!=null){						
						projectUpdateImage.projectId =projectId;
						projectUpdateImage.projectUpdateId=projectUpdateId;
						projectUpdateImage.publicId = res.public_id;
						projectUpdateImage.url=  res.url;
						projectUpdateImage.fullRes= res;
						Meteor.call("createProjectUpdateImage",projectUpdateImage);
					  }					  
				  }
				  
				});
		},
		
    });
	
	Template.TprojectUpdateFiles.helpers({
		'uploadedFiles':function(event){
			projectUpdateId = Session.get("pmProjectUpdateId");
			return Meteor.call("getProjectUpdateImageByUpdateId",projectUpdateId);
		}
	});
	
	Template.TeditProjectUpdateForm.helpers({
		'editProjectUpdateDocument':function(){
				if(Session.get("editProjectUpdateRecord")!=null){
				return ReactiveMethod.call('getProjectUpdateByProjectUpdateId',Session.get("editProjectUpdateRecord")._id,Session.get("getProjectUpdatesRefreshFlag"));
		}
		},
		
    });
	Template.TprojectUpdateForm.helpers({
		'projectName':function(){
				return Session.get("pmProjectName");
		},
		
    });
	
	
	Template.TprojectUpdates.helpers({
		projectIdSelector: function () {
		return {projectId: Session.get("pmProjectId")}; // this could be pulled from a Session var or something that is reactive
		} 
	});
	Template.TprojectUpdates.events({
		'click tbody > tr': function (event) {
			var dataTable = $(event.target).closest('table').DataTable();
			var rowData = dataTable.row(event.currentTarget).data();
			Session.set("pmProjectUpdateId",rowData._id);	
			Session.set("getProjectUpdatesRefreshFlag",new Date());
		},
	});
	Template.TProjectUpdateFileGallery.helpers({
		'projectUpdateFiles':function(){
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId",Session.get("pmProjectUpdateId"),Session.get("getProjectUpdatesRefreshFlag"));
		},
		'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}		
    });
	Template.TProjectUpdateFileGallery.events({
		'click .js_imageThumbnail':function(){
			$("#fileLoadIFrame").attr('src',this.url);
			//var title = $(this).parent('a').attr("title");
			//console.log(this);
			//$('#projectFileLoadModal .modal-title').html(title);
			//$($(this).parents('div').html()).appendTo('#projectFileLoadModal .modal-body');
			$('#projectFileLoadModal').modal('show');
		},
		'click .js_projectUpdateDeleteFile':function(){
			var result = confirm("Are you sure you want to delete the document?");
			if(result){
				console.log(this);
				Cloudinary.delete(this.publicId);
				Meteor.call("removeProjectUpdateImage",this._id);
			}
		}
    });
	
	
	
	
	

AutoForm.addHooks(['addNewProjectUpdate'], {
  onSuccess: function(operation, result, template) {
	$("#createProjectUpdateModal").modal('hide');
  }
});
AutoForm.addHooks(['editProjectUpdateForm'], {
  onSuccess: function(operation, result, template) {
	$("#editProjectUpdateModal").modal('hide');
	Session.set("getProjectUpdatesRefreshFlag",new Date());
  }
});
Template.TprojectUpdateForm.helpers({
    defaultValues: function() {
        return { projectId: Session.get('pmProjectId') };
    }
});
	
	
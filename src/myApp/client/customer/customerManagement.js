import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './customerManagement.html';

$.cloudinary.config({
  cloud_name: "aparto"
});
//Client Project Updates
Template.TClient_projectUpdates.helpers({
		projectIdSelector:function () {
		return {
			projectId: ReactiveMethod.call('getProjectIdByUserId',Meteor.userId(),Session.get("cmRefreshFlag"))
		}; 
		} 
	});

Template.TClient_projectUpdates.events({
	'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		Session.set("cmProjectUpdateId",rowData._id);		
		Session.get("cmRefreshFlag", new Date());
  },
		
});
Template.TClient_viewProjectUpdatesSection.events({
	'click .js_viewProjectUpdateFiles': function (event) {
		$("#Client_projectUpdateFilesModal").modal('show');
  },	
  'click. js_closeProjectUpdateFilesModal':function(event){
  		$("#Client_projectUpdateFilesModal").modal('hide');
  }
});
	Template.TClient_projectUpdateFileGallery.events({
		'click .js_projectUpdateThumbnail':function(){
			$("#Client_projectUpdateFilesLoadIFrame").attr('src',this.url);
			$('#Client_projectUpdateFilesModal').modal('show');
		},
});
Template.TClient_projectUpdateFileGallery.helpers({
		'projectUpdateFiles':function(){
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId",Session.get("cmProjectUpdateId"),Session.get("cmRefreshFlag"));
		},
		'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}		
    });
//End Client Project Update
//Client Apartment Updates
Template.TClient_apartmentUpdates.helpers({
		'apartmentIdSelector':function () {
		return {
			apartmentId: ReactiveMethod.call('getApartmentIdByUserId',Meteor.userId(),Session.get("cmRefreshFlag"))
		}; 
		} 
	});

Template.TClient_apartmentUpdates.events({
	'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		Session.set("cmApartmentpdateId",rowData._id);		
		Session.get("cmRefreshFlag", new Date());
  },		
});

Template.TClient_viewApartmentUpdatesSection.events({
	'click .js_viewApartmentUpdateFiles': function (event) {
		$("#Client_apartmentUpdatesFilesModal").modal('show');
  },	
  'click. js_closeApartmentUpdatesFilesModal':function(event){
  		$("#Client_apartmentUpdatesFilesModal").modal('hide');
  }
});

Template.TClient_apartmentUpdatesFileGallery.helpers({
		'apartmentUpdateFiles':function(){
			return ReactiveMethod.call('getApartmentUpdateImageByUpdateId',Session.get('cmApartmentpdateId'),Session.get("apartmentRefreshFlag"));
			},
		'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}	

});
Template.TClient_apartmentUpdatesFileGallery.events({
		'click .js_apartmentUpdatesThumbnail':function(){
			$("#Client_apartmentUpdateFilesLoadIFrame").attr('src',this.url);
			$('#Client_apartmentUpdateFilesModal').modal('show');
		},	

});



//End Client Apartment Updates

	Template.TClient_projectUpdateFileGallery.events({
		'click .js_projectUpdateThumbnail':function(){
			$("#Client_projectUpdateFilesLoadIFrame").attr('src',this.url);
			$('#Client_projectUpdateFilesModal').modal('show');
		},
});
Template.TClient_projectUpdateFileGallery.helpers({
		'projectUpdateFiles':function(){
			return ReactiveMethod.call("getProjectUpdateImageByUpdateId",Session.get("cmProjectUpdateId"),Session.get("cmRefreshFlag"));
		},
		'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}		
    });
	//Client Apartment Payments
Template.TClient_apartmentPayments.helpers({
		apartmentIdSelector: function () {
		return {apartmentId: ReactiveMethod.call('getApartmentIdByUserId',Meteor.userId())}; 
		} 
	});

Template.TClient_apartmentPayments.events({
	'click tbody > tr': function (event) {
		var dataTable = $(event.target).closest('table').DataTable();
		var rowData = dataTable.row(event.currentTarget).data();
		if(rowData!=null){
			Session.set("cmApartmentPaymentId",rowData._id);		
			Session.get("cmRefreshFlag", new Date());
		}
		
  },
		
});
// Client Apartment Payment Section
Template.TClient_viewApartmentPaymentSection.events({
	'click .js_viewApartmentPaymentFiles': function (event) {
		$("#Client_apartmentPaymentFilesModal").modal('show');
  },	
});
	Template.TClient_apartmentPaymentFileGallery.events({
		'click .js_imageThumbnail':function(){
			$("#Client_apartmentPaymentFilesLoadIFrame").attr('src',this.url);
			$('#Client_apartmentPaymentFilesModal').modal('show');
		},
});
Template.TClient_apartmentPaymentFileGallery.helpers({
		'apartmentPaymentFiles':function(){
			return ReactiveMethod.call("getApartmentPaymentFileByPaymentId",Session.get("cmApartmentPaymentId"),Session.get("cmRefreshFlag"));
		},
		'isImage':function(url){
			return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);			
		}		
    });

// End Client Apartment Payment Section
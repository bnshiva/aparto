import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';


Router.configure({
  layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function () {
  this.render('home', {
	  to: 'main'
	  });
});

Router.route('/projectStatus', function () {
  this.render('myProject', {
	  to: 'main'
	  });
});

Router.route('/myApartment', function () {
  this.render('myApartment', {
	  to: 'main'
	  });
});

Router.route('/login', function () {
  this.render('credential_Section', {
	  to: 'main'
	  });
});
Router.route('/admin', function () {
  this.render('admin_section', {
	  to: 'main'
	  });
});

Router.route('/payment', function () {
  this.render('Client_payment', {
	  to: 'main'
	  });
});

function toggleNav() {
    if ($('#site-wrapper').hasClass('show-nav')) {
        // Do things on Nav Close
        $('#site-wrapper').removeClass('show-nav');
    } else {
        // Do things on Nav Open
        $('#site-wrapper').addClass('show-nav');
    }

    //$('#site-wrapper').toggleClass('show-nav');
}


Template.branding.events({
	'click .js-toggle-leftnav':function(event){
		console.log('clicked nav');
		toggleNav();
	},
});

Template.left_navbar.events({
	'click .js-toggle-leftnav':function(event){
		console.log('clicked nav');
		toggleNav();
	},
	'click .js-signout':function (event){
			event.preventDefault();
			console.log('Logging out');
			Meteor.logout();	
			Router.go('/login');			
		}
});
Template.branding_nav.events({
	
});

 Template.login.events({
        'submit form': function(event) {
            event.preventDefault();
			$("#login_fail").css('display','none');
			 var emailVar = event.target.email.value;
			 var passwordVar = event.target.password.value;
			 if(validateEmail(emailVar) && password!=null && passwordVar.trim()!=""){
				 Meteor.loginWithPassword(emailVar, passwordVar, function (err){
					 if(err){
						 console.log("Login Erro");
						 console.log(err);
						 console.log("Login Err End");
						 $("#login_fail").css('display','block');
					 }
					 else{
						 Router.go('/');
						 console.log("Routing Home");
					 }
				 });
			 }
			 else{
				$("#login_fail").css('display','block'); 
			 }			
        },
		
    });
	 Template.password_recovery.events({
        'submit form': function(event) {
            event.preventDefault();
			console.log("recovery requested");
			$("#reset_fail").css('display','none');
			 var emailVar = event.target.email.value;
			 if(validateEmail(emailVar)){
				 Accounts.forgotPassword({email: emailVar}, function(err){
				  if (err){
					 $("#reset_fail").css('display','block'); 
					 console.log(err);
				  }
            
				  else {
					$("#reset_fail").css('display','none'); 
					alert("Password recovery link sent you your email");
				  }
				});
			 }
			 else{
				$("#reset_fail").css('display','block'); 
			 }
        }
    });
	
	Template.login.rendered = function(){
 $("#login_form").validate();
};
	
	Template.registration.rendered = function(){
 $("#registration_form").validate();
};
	
	Template.password_recovery.rendered = function(){
 $("#password_recovery_form").validate();
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}





